package br.com.gleac.context.adapter;

import br.com.gleac.seguranca.AuthenticationUrlFailureHandler;
import br.com.gleac.seguranca.BasedAccessVoter;
import br.com.gleac.seguranca.UsuarioSistemaAuthenticationSuccessHandler;
import br.com.gleac.seguranca.UsuarioSucessLogoutHandle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AuthenticatedVoter;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;


@Configuration
@EnableWebSecurity(debug = false)
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebApplicationSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {


    public static final String J_SPRING_SECURITY_LOGOUT = "/j_spring_security_logout";
    public static final String J_SPRING_SECURITY_CHECK = "/j_spring_security_check";
    public static final String J_USERNAME = "user";
    public static final String J_PASSWORD = "password";
    public static final String LOGIN_ERROR = "/login-error";
    public static final String JSESSIONID = "JSESSIONID";
    public static final String LOGIN_PAGE = "/login";
    public static final String[] URL_PERMITIDO = new String[]{"/public/**", "/resources/**", "/javax.faces.resource/**", "/retorno/pagHiper/**"};
    @Inject
    private UserDetailsService userDetailsService;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Inject
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder());
    }


    @Bean
    @Inject
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new UsuarioSistemaAuthenticationSuccessHandler();
    }

    @Bean
    @Inject
    public SimpleUrlAuthenticationFailureHandler authenticationFailureHandler() {
        return new AuthenticationUrlFailureHandler();
    }

    @Bean
    @Inject
    public UsuarioSucessLogoutHandle usuarioSucessLogoutHandle() {
        return new UsuarioSucessLogoutHandle();
    }

    @Bean
    public AccessDecisionManager accessDecisionManager() {
        List<AccessDecisionVoter<? extends Object>> decisionVoters
            = Arrays.asList(
            new WebExpressionVoter(),
            new RoleVoter(),
            new AuthenticatedVoter(),
            new BasedAccessVoter());
        return new UnanimousBased(decisionVoters);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .authorizeRequests()
            .anyRequest()
            .authenticated()
            .accessDecisionManager(accessDecisionManager())
            .and()
            .formLogin()
            .loginPage(LOGIN_PAGE).permitAll()
            .loginProcessingUrl(J_SPRING_SECURITY_CHECK)
            .usernameParameter(J_USERNAME)
            .passwordParameter(J_PASSWORD)
            .successHandler(authenticationSuccessHandler())
            .failureHandler(authenticationFailureHandler()).failureUrl("/login?invalid=true")
            .and()
            .httpBasic();

        http.authorizeRequests()
            .antMatchers(LOGIN_PAGE).permitAll()
            .antMatchers(URL_PERMITIDO).permitAll();
        http.logout()
            .logoutUrl(J_SPRING_SECURITY_LOGOUT).logoutSuccessHandler(usuarioSucessLogoutHandle())
            .deleteCookies(JSESSIONID)
            .invalidateHttpSession(true)
            .logoutSuccessUrl(LOGIN_PAGE).permitAll();

        http.csrf().disable();

    }


    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(URL_PERMITIDO);

    }
}
