package br.com.gleac.context.event;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * Spring application context loader that checks if database has initial data.
 * If not, it fills database with some mock data.
 *
 * @author Michel Risucci
 */
@Named
public class ApplicationLoaderListener implements
		ApplicationListener<ContextRefreshedEvent> {

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void onApplicationEvent(ContextRefreshedEvent event) {

	}



}
