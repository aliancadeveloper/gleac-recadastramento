package br.com.gleac.context.configuration;


import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class LiquibaseIntegration {

    public static final String DATA_MODEL = "classpath:/db/changelog/master.xml";
    private static final Log log = LogFactory.getLog(LiquibaseIntegration.class);
    @Autowired
    @Qualifier("datasource")
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public SpringLiquibase liquibase() {
        log.info("starting liquibase ");
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog(DATA_MODEL);
        return liquibase;
    }

}
