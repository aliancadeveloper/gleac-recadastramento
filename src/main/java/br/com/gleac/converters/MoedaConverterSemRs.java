package br.com.gleac.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.regex.Pattern;

/**
 * Created by renatoromanini on 06/08/17.
 */
@FacesConverter(value = "moedaConverter")
public class MoedaConverterSemRs implements Converter, Serializable {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String str) throws ConverterException {
        if ( str == null || str.toString().trim().equals("") ) {
            return new BigDecimal("0.00");
        } else {
            str = str.replaceAll(Pattern.quote("R$ "), "");
            str = str.replaceAll(Pattern.quote("."), "");
            try {
                str = str.replaceAll(Pattern.quote(","), ".");
                BigDecimal valor = new BigDecimal(str);
                return valor;
            } catch (Exception e) {
                return new BigDecimal("0.00");
            }
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object obj) {
        if ( obj == null || obj.toString().trim().equals("") ) {
            return "0,00";
        } else {
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(4);
            return nf.format(new BigDecimal(obj.toString()));
        }
    }
}
