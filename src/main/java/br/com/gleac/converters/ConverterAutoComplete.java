package br.com.gleac.converters;

import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Persistencia;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by renatoromanini on 25/08/15.
 */
public class ConverterAutoComplete<T> implements Converter, Serializable {

    protected static final Logger logger = Logger.getLogger("br.com.aliancasolucoes.converters");
    private AbstractRepository facade;
    private Class classe;
    private List<T> lista;

    public ConverterAutoComplete(Class classe, AbstractRepository f) {
        this.facade = f;
        this.classe = classe;
    }

    public ConverterAutoComplete(Class classe, AbstractRepository f, List<T> lista) {
        this.facade = f;
        this.classe = classe;
        this.lista = lista;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {

        Object o = recuperarDaLista(value);
        if (o == null) {
            o = recuperarDoBanco(value);
        }

        return o;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        } else {
            if (value instanceof Long) {
                return String.valueOf(value);
            } else {
                try {
                    return Persistencia.getAttributeValue(value, Persistencia.getFieldId(classe).getName()).toString();
                } catch (Exception e) {
                    return String.valueOf(value);
                }
            }
        }
    }

    private Object recuperarDaLista(String value) {
        try {
            for (Object object : lista) {
                if (object.toString().equals(value)) {
                    return object;
                }
            }
        } catch (NullPointerException npe) {
        }

        return null;
    }

    private Object recuperarDoBanco(String value) {
        try {
            Field fieldId = Persistencia.getFieldId(classe);
            if (fieldId != null) {
                Object chave = fieldId.getType().getConstructor(String.class).newInstance(value);
                return facade.recuperar(classe, chave);
            }
        } catch (Exception ex) {
            //getLogger().debug("Erro em recuperarDoBanco", ex);
            //Evita mensagens durante a digitaçao parcial.
            //logger.log(Level.SEVERE, "Problema ao instanciar a chave", ex);
        }
        return null;
    }
}