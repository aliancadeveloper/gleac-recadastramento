package br.com.gleac.seguranca;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.jpa.UsuarioJPARepository;
import br.com.gleac.service.SistemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by renatoromanini on 26/05/16.
 */
@Transactional
public class UsuarioSistemaAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private final String HOME = "/admin/home";
    private final String LOGIN_ERROR = "/login-error";
    private final String SEM_LOJA = "/loja-error";


    @Autowired
    private UsuarioJPARepository usuarioRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private LojaRepository lojaRepository;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        Optional<Usuario> usuario = usuarioRepository.findOneByLogin(authentication.getName());

        String redirect = httpServletRequest.getContextPath() + HOME;

        if (usuario.isPresent()) {
            definirUsuario(usuario);
            List<Loja> lojas = buscarLojasVinculadasUsuario(usuario);
            definirLojasVinculadaUsuarioByPerfil(lojas);
            redirect = redirecionarUsuarioInativo(httpServletRequest, usuario, redirect);
            redirect = redirecionarUsuarioSemLoja(httpServletRequest, usuario, redirect);
        }
        httpServletResponse.sendRedirect(redirect);
    }

    private String redirecionarUsuarioSemLoja(HttpServletRequest httpServletRequest, Optional<Usuario> usuario, String redirect) {

        if (!sistemaService.isExistsLojaToUsuario(usuario)) {
            try {
                return httpServletRequest.getContextPath() + SEM_LOJA;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return redirect;
    }

    private String redirecionarUsuarioInativo(HttpServletRequest httpServletRequest, Optional<Usuario> usuario, String redirect) {
        if (!usuario.get().getAtivo()) {
            try {
                return httpServletRequest.getContextPath() + LOGIN_ERROR;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return redirect;

    }

    private void definirLojasVinculadaUsuarioByPerfil(List<Loja> lojas) {
        if (!ObjectUtils.isEmpty(lojas)) {
            Collections.sort(lojas);
            sistemaService.setLojaCorrente(lojas.get(0));
            sistemaService.definirLojasUsuarioByPerfil(lojas);
        }
    }

    private List<Loja> buscarLojasVinculadasUsuario(Optional<Usuario> usuario) {
        return lojaRepository.recuperarLojasDoUsuario(usuario.get());
    }

    private void definirUsuario(Optional<Usuario> usuario) {
        sistemaService.definirUsuario(usuario.get());
    }
}
