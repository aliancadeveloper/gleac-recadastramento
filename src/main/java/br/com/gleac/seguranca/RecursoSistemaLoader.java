package br.com.gleac.seguranca;

import br.com.gleac.entidade.seguranca.RecursoSistema;
import br.com.gleac.negocio.RecursoSistemaRepository;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Component
public class RecursoSistemaLoader {

    public static final Logger log = LoggerFactory.getLogger(RecursoSistemaLoader.class);
    public static final String RAIZ = "/recursos";
    public static final String SHA1 = "SHA1";

    @Autowired
    private RecursoSistemaRepository recursoSistemaRepository;

    private List<RecursoSistema> recursos;
    private Set<RecursoSistema> recursosSistemaJson;


    public RecursoSistemaLoader() {
        setRecursos(Lists.newArrayList());
        setRecursosSistemaJson(Sets.newHashSet());
    }

    @EventListener(ContextRefreshedEvent.class)
    @Order
    public void onApplicationLoaderRecursos(ContextRefreshedEvent event) {
        List<File> recursosJson = buscarRecursosJson(getRaizRecursos());


        recursosJson.forEach(arquivo -> {
            try {
                converterJsonToEntity(arquivo);
                getRecursosSistemaJson().forEach(recursoSistema -> {
                    carregarRecursosSistema(recursoSistema);

                });

            } catch (JsonParseException jpe) {
                jpe.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

    }

    private String calcularSumToFile(File arquivo) {
        String checksum = calcularChecksum(arquivo.getAbsolutePath());
        return checksum;
    }

    private void converterJsonToEntity(File arquivo) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<RecursoSistema> recursos = mapper.readValue(arquivo, new TypeReference<List<RecursoSistema>>() {});
        if (!CollectionUtils.isEmpty(recursos)) {
            recursos.forEach(rec -> {
                rec.setChecksum(calcularSumToFile(arquivo));
                getRecursosSistemaJson().add(rec);
            });
        }


    }

    private void carregarRecursosSistema(RecursoSistema recursoSistema) {
        try {
            recursoSistemaRepository.salvarRecurso(recursoSistema);
            log.info("{}", recursoSistema);
        } catch (PersistenceException pe) {
            log.error("erro ao tentar atualizar o recurso {}. ", recursoSistema.getPath());
        }

    }


    public List<File> buscarRecursosJson(String path) {
        try {
            return Files.walk(Paths.get(path))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());
        } catch (IOException e) {
            log.error(" erro ao carregar os recursos do sistema {} ", e.getCause());
        }
        return Lists.newArrayList();
    }

    public String getRaizRecursos() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        URL resource = classLoader.getResource(RAIZ);
        return resource.getPath();
    }

    public String calcularChecksum(String file) {
        try {
            InputStream stream = Files.newInputStream(Paths.get(file));
            MessageDigest digest = MessageDigest.getInstance(SHA1);
            byte[] block = new byte[4096];
            int length;
            while ((length = stream.read(block)) > 0) {
                digest.update(block, 0, length);
            }
            return converterHex(digest.digest());
        } catch (Exception e) {
            log.error("não foi possível calcular o checksum {} ", e.getCause());
        }
        return null;
    }

    public String converterHex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

    public List<RecursoSistema> getRecursos() {
        return recursos;
    }

    public void setRecursos(List<RecursoSistema> recursos) {
        this.recursos = recursos;
    }

    public Set<RecursoSistema> getRecursosSistemaJson() {
        return recursosSistemaJson;
    }

    public void setRecursosSistemaJson(Set<RecursoSistema> recursosSistemaJson) {
        this.recursosSistemaJson = recursosSistemaJson;
    }
}
