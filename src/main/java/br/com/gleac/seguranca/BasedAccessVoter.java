package br.com.gleac.seguranca;

import br.com.gleac.entidade.seguranca.Recurso;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.negocio.RecursoSistemaRepository;
import br.com.gleac.service.SistemaService;
import com.ocpsoft.pretty.PrettyContext;
import com.ocpsoft.pretty.faces.config.PrettyConfigurator;
import com.ocpsoft.pretty.faces.config.mapping.UrlMapping;
import com.ocpsoft.pretty.faces.url.URL;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterInvocation;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;


public class BasedAccessVoter implements AccessDecisionVoter<Object> {

    public static final Logger log = LoggerFactory.getLogger(BasedAccessVoter.class);

    @Autowired
    private RecursoSistemaRepository recursoSistemaRepository;


    @Autowired
    private SistemaService sistemaService;

    PrettyContext ctx;


    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }


    @Override
    public int vote(Authentication authentication, Object object, Collection collection) {
        Usuario usuario = null;
        Boolean isInstanceUser = authentication.getPrincipal() instanceof Usuario;
        if (isInstanceUser) {
            usuario = (Usuario) authentication.getPrincipal();
        }

        if (!isInstanceUser) {
            return ACCESS_GRANTED;
        }

        if (object instanceof FilterInvocation) {
            String uri = toURI((HttpServletRequest) object);
            log.info("URI {} ", uri);

            List<Recurso> recursoSistemas = sistemaService.getRecursos();
            if (ObjectUtils.isEmpty(recursoSistemas)) {
                return ACCESS_DENIED;
            }

            if (!ObjectUtils.isEmpty(recursoSistemas)) {


                UrlMapping urlMap = null;
                for (UrlMapping urlMapping : ctx.getConfig().getMappings()) {
                    if (uri.equals(urlMapping.getViewId())) {
                        urlMap = urlMapping;
                        break;
                    }
                }
                for (Recurso recurso : recursoSistemas) {
                    if (urlMap.getPattern().contains(recurso.getRecursoSistema().getPath())) {
                        return ACCESS_GRANTED;
                    }
                }


            }


        }
        return ACCESS_DENIED;
    }

    @Override
    public boolean supports(Class clazz) {
        return true;
    }

    private String toURI(HttpServletRequest request) {
        String uri = request.getServletPath();
        if (StringUtils.isNotEmpty(request.getPathInfo())) {
            return String.format("%s%s", request.getServletPath(), request.getPathInfo());
        }
        if (StringUtils.isEmpty(request.getPathInfo())) {
            URL url = new URL(uri);
            if (ObjectUtils.isEmpty(ctx)) {
                ctx = PrettyContext.getCurrentInstance(request);
                if (ctx.getConfig().getMappings().isEmpty()) {
                    initPrettyFacesFromServlet(request);
                }
            }
            if (ctx.getConfig().isURLMapped(url)) {
                return getViewID(url);
            }

            return request.getServletPath();
        }
        return uri;
    }

    public void initPrettyFacesFromServlet(HttpServletRequest request) {
        PrettyConfigurator pc = new PrettyConfigurator(request.getServletContext());
        pc.configure();
        ctx.getConfig().setMappings(pc.getConfig().getMappings());
    }

    public String getViewID(URL url) {
        return ctx.getConfig().getMappingForUrl(url).getViewId();
    }


}
