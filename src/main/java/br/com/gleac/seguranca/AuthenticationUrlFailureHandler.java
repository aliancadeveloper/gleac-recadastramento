package br.com.gleac.seguranca;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by zaca on 08/09/16.
 */
@Component
public class AuthenticationUrlFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    public static String DEFAULT_TARGET_PARAMETER = "login-error";

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {

        String redirectUrl = request.getParameter(getTargetUrlParameter());
        if (redirectUrl != null) {
            super.logger.debug("redirect URL: " + redirectUrl);
            getRedirectStrategy().sendRedirect(
                    request,
                    response,
                    redirectUrl);
        } else {
            super.onAuthenticationFailure(request, response, exception);
        }
    }

    public String getTargetUrlParameter() {
        return DEFAULT_TARGET_PARAMETER;
    }

    public void setTargetUrlParameter(String targetUrlParameter) {
        this.DEFAULT_TARGET_PARAMETER = targetUrlParameter;
    }
}
