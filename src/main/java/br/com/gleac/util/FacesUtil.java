package br.com.gleac.util;

import br.com.gleac.controlador.Web;
import br.com.gleac.enums.SummaryMessages;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by renat on 10/05/2016.
 */
public abstract class FacesUtil {

    public static void addWarn(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail));
    }

    public static void addInfo(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
    }

    public static void addError(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    public static void addErrorOperacaoNaoPermitida(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Operação não permitida!", detail));
    }

    public static void addFatal(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, detail));
    }

    public static void addErrorPadrao(Throwable ex) {
        addError("Operação não realizada.", ex.getMessage());
    }

    public static void addErrorGenerico(Throwable ex) {
        addError("Operação não realizada.", "Por favor tente novamente, se o problema persistir entre em contato com o suporte técnico. Detalhes do Erro: " + ex.getMessage());
    }

    public static void navegaEmbora(Object selecionado, String caminhoPadrao) {
        String origem = Web.getCaminhoOrigem();
        if (!origem.isEmpty()) {
            if (origem.endsWith("novo/") || origem.endsWith("edita/")) {
                Web.poeNaSessao(selecionado);
            }
            redirecionamentoInterno(origem);
        } else {
            origem = caminhoPadrao + "listar/";
            redirecionamentoInterno(origem);
        }
    }

    public static void redirecionarHome() {
        redirecionamentoInterno("/admin/home");
    }
    public static void redirecionarLogin() {
        redirecionamentoInterno("/login");
    }


    public static void redirecionamentoInterno(String url) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(getRequestContextPath() + url);
        } catch (Exception ex) {
            Logger.getLogger(FacesUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getRequestContextPath() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
    }

    public static void executaJavaScript(String script) {
        RequestContext.getCurrentInstance().execute(script);
    }

    public static void atualizaComponete(String nomeComponente) {
        RequestContext.getCurrentInstance().update(nomeComponente);
    }

    public static void addCampoObrigatorio(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, getMessageError(SummaryMessages.CAMPO_OBRIGATORIO.getDescricao(), detail));
    }

    public static void addOperacaoNaoRealizada(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, getMessageError(SummaryMessages.OPERACAO_NAO_REALIZADA.getDescricao(), detail));
    }

    public static void addOperacaoNaoPermitida(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, getMessageError(SummaryMessages.OPERACAO_NAO_PERMITIDA.getDescricao(), detail));
    }

    public static void addOperacaoRealizada(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, getMessageInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), detail));
    }

    public static void addAtencao(String detail) {
        FacesContext.getCurrentInstance().addMessage(null, getMessageWarn(SummaryMessages.ATENCAO.getDescricao(), detail));
    }

    private static FacesMessage getMessageInfo(String summary, String detail) {
        return new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
    }

    private static FacesMessage getMessageError(String summary, String detail) {
        return new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
    }

    private static FacesMessage getMessageWarn(String summary, String detail) {
        return new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail);
    }

    public static void printAllMessages(List<MensagemValidacao> lista) {
        FacesMessage.Severity severity;
        for (MensagemValidacao msg : lista) {
            if (msg.getTipo() == TipoMensagemValidacao.ALERTA) {
                severity = FacesMessage.SEVERITY_WARN;
            } else if (msg.getTipo() == TipoMensagemValidacao.ERRO) {
                severity = FacesMessage.SEVERITY_ERROR;
            } else if (msg.getTipo() == TipoMensagemValidacao.ERRO_FATAL) {
                severity = FacesMessage.SEVERITY_FATAL;
            } else {
                severity = FacesMessage.SEVERITY_INFO;
            }
            FacesContext.getCurrentInstance().addMessage(msg.getClientId(), new FacesMessage(severity, msg.getSummary(), msg.getDetail()));
        }
    }

    public static String gerarUrlImagem() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        final StringBuffer url = request.getRequestURL();
        String test = url.toString();
        String[] quebrado = test.split("/");
        StringBuilder b = new StringBuilder();
        b.append(quebrado[0]);
        b.append("/").append(quebrado[1]);
        b.append("/").append(quebrado[2]);
        b.append("/").append(quebrado[3]).append("/");
        return b.toString();
    }

    public static void printAllFacesMessages(List<FacesMessage> lista) {
        for (FacesMessage msg : lista) {
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}

