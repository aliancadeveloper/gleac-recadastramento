package br.com.gleac.util;

/**
 * Created by renatoromanini on 20/08/15.
 */
public enum TipoMensagemValidacao {
    INFORMACAO("SEVERITY_INFO"),
    ALERTA("SEVERITY_WARN"),
    ERRO("SEVERITY_ERROR"),
    ERRO_FATAL("SEVERITY_FATAL");

    String descricao;

    private TipoMensagemValidacao(String descricao) {
        this.descricao = descricao;
    }
}
