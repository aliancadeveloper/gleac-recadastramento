package br.com.gleac.util;

import com.google.common.collect.Lists;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import java.io.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by romanini on 03/06/15.
 */
public class ExcelUtil {

    public static final String XLS_CONTENTTYPE = "application/xls";
    public static final String XLS_EXTENCAO = ".xls";
    public static final String CSV_CONTENTTYPE = "application/csv";
    public static final String CSV_EXTENCAO = ".csv";

    private static final String DELIMITER = ";";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private StreamedContent fileDownload;
    private File file = null;
    private String nomeArquivo;
    private String contentType;
    private String extensao;

    public static String getValorCell(Cell cell) {
        if (cell == null) {
            return "";
        }
        if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
            BigDecimal valor = BigDecimal.valueOf(cell.getNumericCellValue());
            return valor + "";
        } else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
            return cell.getStringCellValue();
        } else if (Cell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
            return cell.getBooleanCellValue() ? "Sim " : "Não";
        } else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
            if (cell.getCachedFormulaResultType() == HSSFCell.CELL_TYPE_STRING) {
                return cell.getStringCellValue();
            } else if (cell.getCachedFormulaResultType() == HSSFCell.CELL_TYPE_NUMERIC) {
                return String.valueOf(cell.getNumericCellValue());
            } else {
                return "";
            }
        } else if (Cell.CELL_TYPE_ERROR == cell.getCellType()) {
            return "error";
        } else if (Cell.CELL_TYPE_BLANK == cell.getCellType()) {
            return "";
        } else {
            return "";

        }
    }

    public static Enum getEnum(String descricao, Object[] valores) {
        for (Object value : Lists.newArrayList(valores)) {
            Class<?> classe = value.getClass();
            if (classe.isEnum()) {
                for (Field field : classe.getDeclaredFields()) {
                    if (Util.getDescricaoDoEnum(classe, field).equals(descricao)) {
                        return Enum.valueOf((Class<? extends Enum>) classe, field.getName());
                    }
                }
            }
        }
        return null;
    }

    public static String getValorCellParaCpf(Cell cell) {
        if (cell == null) {
            return "";
        }
        if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return cell.getStringCellValue().length() < 11 ? Util.zerosAEsquerda(cell.getStringCellValue(), 11) : cell.getStringCellValue();
        } else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
            return cell.getStringCellValue();
        }
        return "";
    }

    public void gerarExcel(String titulo, String nomeDoArquivo, List<String> titulos, List<Object[]> objetos, String filtros, String nomeUsuario) {
        try {
            if (contentType == null) {
                contentType = XLS_CONTENTTYPE;
                extensao = XLS_EXTENCAO;
            }
            nomeArquivo = nomeDoArquivo;
            String nomePlanilha = nomeDoArquivo + "_temp";
            file = File.createTempFile(nomePlanilha, extensao);

            int linhaInicial = 0;

            FileOutputStream fout = new FileOutputStream(file);
            HSSFWorkbook pastaDeTrabalho = new HSSFWorkbook();
            HSSFSheet sheet = criarSheet(pastaDeTrabalho, nomePlanilha);


            HSSFRow municipio = criaRow(sheet, linhaInicial);
            linhaInicial++;

            HSSFRow tituloDaPlanilha = criaRow(sheet, linhaInicial);
            criaCell(tituloDaPlanilha, 0).setCellValue(titulo);
            linhaInicial++;
            if (filtros != null) {
                HSSFRow filtrosUtilizados = criaRow(sheet, linhaInicial);
                criaCell(filtrosUtilizados, 0).setCellValue(filtros.trim());
            }
            linhaInicial++;
            linhaInicial++;


            HSSFRow cabecalho = criaRow(sheet, linhaInicial);
            for (String atributo : titulos) {
                criaCell(cabecalho, titulos.indexOf(atributo)).setCellValue(atributo);
            }
            linhaInicial++;
            for (Object o : objetos) {
                HSSFRow linha = criaRow(sheet, linhaInicial);

                Object[] objeto = (Object[]) o;
                int i = 0;
                for (Object atributo : objeto) {
                    if (atributo != null) {
                        if (atributo instanceof BigDecimal) {
                            HSSFCell hssfCell = criaCell(linha, i);
                            hssfCell.setCellType(Cell.CELL_TYPE_NUMERIC);
                            hssfCell.setCellValue(((BigDecimal) atributo).doubleValue());

                        } else {
                            criaCell(linha, i).setCellValue(atributo.toString());
                        }
                    }
                    i++;
                }
                linhaInicial++;
            }

            linhaInicial = linhaInicial + 5;

            HSSFRow usuario = criaRow(sheet, linhaInicial);
            criaCell(usuario, 1).setCellValue(nomeUsuario);
            linhaInicial++;

            HSSFRow rodape = criaRow(sheet, linhaInicial);

            linhaInicial++;

            HSSFRow geradoEm = criaRow(sheet, linhaInicial);
            criaCell(geradoEm, 1).setCellValue("Gerado em - " + Util.sdf.format(new Date()));

            pastaDeTrabalho.write(fout);
        } catch (IOException ioe) {
            FacesUtil.addError("Erro ao gerar o arquivo", "");
        }
    }

    public void gerarCSV(String titulo, String nomeDoArquivo, List<String> titulos, List<Object[]> objetos, boolean gerarCabecalhoAndRodape, String usuario) {
        BufferedWriter fileWriter = null;
        try {
            if (contentType == null) {
                contentType = CSV_CONTENTTYPE;
                extensao = CSV_EXTENCAO;
            }
            nomeArquivo = nomeDoArquivo;
            String nomePlanilha = nomeDoArquivo + "_temp";
            file = File.createTempFile(nomePlanilha, extensao);
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "ISO-8859-1"));

            if (gerarCabecalhoAndRodape) {
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(titulo);
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(NEW_LINE_SEPARATOR);
            }


            for (String t : titulos) {
                fileWriter.append(t);
                fileWriter.append(DELIMITER);
            }
            fileWriter.append(NEW_LINE_SEPARATOR);


            for (Object o : objetos) {
                Object[] objeto = (Object[]) o;
                for (Object atributo : objeto) {
                    if (atributo != null) {
                        if (atributo instanceof BigDecimal) {
                            fileWriter.append(atributo.toString().replace(".", ","));
                        } else {
                            fileWriter.append(atributo.toString());
                        }
                        fileWriter.append(DELIMITER);
                    }
                }
                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            if (gerarCabecalhoAndRodape) {
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append(usuario);
                fileWriter.append(NEW_LINE_SEPARATOR);
                fileWriter.append("Gerado em - " + Util.sdf.format(new Date()));
            }
        } catch (Exception e) {
            FacesUtil.addError("Erro ao gerar o arquivo", e.getMessage());
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public HSSFSheet criarSheet(HSSFWorkbook pasta, String nomeSheet) {
        return pasta.createSheet(nomeSheet);
    }

    public StreamedContent fileDownload() throws FileNotFoundException, IOException {
        InputStream stream = new FileInputStream(file);
        fileDownload = new DefaultStreamedContent(stream, contentType, nomeArquivo + extensao);
        return fileDownload;
    }

    public HSSFCell criaCell(HSSFRow row, Integer posicao) {
        return row.getCell(posicao) == null ? row.createCell(posicao) : row.getCell(posicao);
    }

    private HSSFCell criaCell(HSSFRow row, Integer posicao, HSSFCellStyle estilo) {
        HSSFCell celula = row.getCell(posicao) == null ? row.createCell(posicao) : row.getCell(posicao);
        celula.setCellStyle(estilo);
        return celula;
    }

    public HSSFRow criaRow(HSSFSheet sheet, Integer linha) {
        return sheet.getRow(linha) == null ? sheet.createRow(linha) : sheet.getRow(linha);
    }

    public StreamedContent getFileDownload() {
        return fileDownload;
    }

    public void setFileDownload(StreamedContent fileDownload) {
        this.fileDownload = fileDownload;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getExtensao() {
        return extensao;
    }

    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }
}
