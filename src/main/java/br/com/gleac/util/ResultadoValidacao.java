package br.com.gleac.util;

import br.com.gleac.supers.AbstractEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by renatoromanini on 20/08/15.
 */
public class ResultadoValidacao {

    private static final Logger logger = LoggerFactory.getLogger(ResultadoValidacao.class);

    private AbstractEntity abstractEntity;
    private boolean validado = true;
    private List<MensagemValidacao> mensagens = new ArrayList();

    public ResultadoValidacao(boolean validado) {

        this.validado = validado;
    }

    public ResultadoValidacao() {
    }

    public ResultadoValidacao(AbstractEntity abstractEntity) {
        this.abstractEntity = abstractEntity;
    }

    public AbstractEntity getAbstractEntity() {
        return abstractEntity;
    }

    public void setAbstractEntity(AbstractEntity abstractEntity) {
        this.abstractEntity = abstractEntity;
    }

    public void setValidado(boolean validado) {
        this.validado = validado;
    }

    public void setMensagens(List<MensagemValidacao> mensagens) {
        this.mensagens = mensagens;
    }

    public void limpaMensagens() {
        mensagens.clear();
    }

    public boolean isValidado() {
        return validado;
    }

    public void addMensagem(MensagemValidacao msg) {
        this.mensagens.add(msg);
    }

    public void invalida() {
        this.validado = false;
    }

    public void valida() {
        this.validado = true;
    }

    public List<MensagemValidacao> getMensagens() {
        return new ArrayList<MensagemValidacao>(mensagens);
    }

    //erro - invalida obrigatoriamente
    public void addErro(String summary, String detail) {
        this.invalida();
        this.addMensagem(MensagemValidacao.error(null, summary, detail));
    }

    //erro fatal - invalida obrigatoriamente
    public void addFatal(String summary, String detail) {
        this.invalida();
        this.addMensagem(MensagemValidacao.fatal(null, summary, detail));
    }

    //alerta - pode invalidar ou não
    public void addWarn(String summary, String detail, boolean invalidaResultado) {
        if (invalidaResultado) {
            this.invalida();
        }
        this.addMensagem(MensagemValidacao.warn(null, summary, detail));
    }

    //informação - não invalida
    public void addInfo(String summary, String detail) {
        this.addMensagem(MensagemValidacao.info(null, summary, detail));
    }

    public String retornarMensagem() {
        if (this.isValidado()) {

            String retorno = Util.getMensagemSucesso(this.getAbstractEntity()) + " ->";
            for (MensagemValidacao mensagemValidacao : this.getMensagens()) {
                retorno += mensagemValidacao.getSummary() + " - " + mensagemValidacao.getDetail() + ";";
            }
//            try {
//                return new String(retorno.getBytes("UTF-8"));
            return retorno;
//            } catch (UnsupportedEncodingException e) {
//                logger.debug("UnsupportedEncodingException em retornarMensagem", e);
//            }
        } else {
            String retorno = this.getAbstractEntity().getArtigoDefinidoDaEntidade().toUpperCase() + " " + this.getAbstractEntity().getNomeDaEntidade() + " possui seguintes Erros: ";
            for (MensagemValidacao mensagemValidacao : this.getMensagens()) {
                retorno += mensagemValidacao.getSummary() + " - " + mensagemValidacao.getDetail() + ";";
            }
            return retorno;
//            try {
//                return new String(retorno.getBytes("UTF-8"));
//            } catch (UnsupportedEncodingException e) {
//                logger.debug("UnsupportedEncodingException em retornarMensagem", e);
//            }
        }
//        return " ";
    }
}
