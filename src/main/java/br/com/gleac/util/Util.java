package br.com.gleac.util;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by renatoromanini on 14/08/15.
 */
public class Util {

    public static final Locale localeBrasil = new Locale("pt_BR_", "pt", "BR");
    public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", localeBrasil);
    public static final SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", localeBrasil);
    public static final SimpleDateFormat sdfh = new SimpleDateFormat("HH:mm", localeBrasil);
    public static final SimpleDateFormat sdft = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", localeBrasil);
    public static final SimpleDateFormat sdft2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", localeBrasil);
    public static final SimpleDateFormat sfta = new SimpleDateFormat("yyyy", localeBrasil);
    public static final String SEPARADOR = System.getProperty("line.separator");

    private static final int DEF_COUNT = 20;

    private static final Logger logger = LoggerFactory.getLogger(Util.class);

    public static void imprimeSQL(String sql, Query q) {
        String comandoSQL = sql;
        Set<Parameter<?>> param = q.getParameters();
        for (Parameter p : param) {
            String chave = ":" + p.getName();
            String valor = q.getParameterValue(p).toString();
            //logger.debug("chave: " + chave + "   valor: " + valor);
            comandoSQL = comandoSQL.replace(chave, valor);
        }
        logger.info("SQL gerado " + comandoSQL);
        System.out.println(comandoSQL);
    }

    public static <T> List<T> adicionarObjetoEmLista(List<T> lista, T objeto) {
        if (lista == null) {
            lista = new ArrayList<T>();
        }

        if (lista.contains(objeto)) {
            lista.set(lista.indexOf(objeto), objeto);
        } else {
            lista.add(objeto);
        }
        return lista;
    }

    public static String getMensagemSucesso(AbstractEntity se) {
        return se.getArtigoDefinidoDaEntidade().toUpperCase() + " " + se.toString() + " foi salva com sucesso!";
    }

    public static String getMensagemErro() {
        return "Não foi possivel salvar.";
    }

    public static void addWarn(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail));
    }

    public static void addInfo(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
    }

    public static void addError(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
    }

    public static void addFatal(String summary, String detail) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, detail));
    }

    public static void addErrorPadrao(Throwable ex) {
        addError("Operação não realizada.", ex.getMessage());
    }

    public static void addErrorGenerico(Throwable ex) {
        addError("Operação não realizada.", "Por favor tente novamente, se o problema persistir entre em contato com o suporte técnico. Detalhes do Erro: " + ex.getMessage());
    }

    public static Boolean validarCamposWebService(ResultadoValidacao resultadoValidacao) {
        AbstractEntity se = resultadoValidacao.getAbstractEntity();
        for (Field field : Persistencia.getAtributos(se.getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(CRUD.class) && field.getAnnotation(CRUD.class).obrigatorio()) {
                if (Persistencia.getAttributeValue(se, field.getName()) == null || Persistencia.getAttributeValue(se, field.getName()).toString().trim().length() <= 0) {
                    String nomeCampo = field.getAnnotation(CRUD.class).label().trim().length() <= 0 ? field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1, field.getName().length()) : field.getAnnotation(CRUD.class).label();
                    resultadoValidacao.addErro("Campo obrigatório.", "O campo " + nomeCampo + " é obrigatório.");
                }
            }
        }
        return true;
    }

    public static List<SelectItem> getListSelectItem(Object[] values, boolean isOrdering) {
        return getListSelectItem(Lists.newArrayList(values), isOrdering);
    }

    public static List<SelectItem> getListSelectItem(Object[] values) {
        return getListSelectItem(Lists.newArrayList(values), false);
    }

    public static List<SelectItem> getListSelectItem(List<?> values) {
        return getListSelectItem(Lists.newArrayList(values), false);
    }

    public static List<SelectItem> getListSelectItem(List<?> values, Boolean isOrdering) {
        List<SelectItem> toReturn = new ArrayList();
        toReturn.add(new SelectItem(null, ""));

        for (Object obj : values) {
            toReturn.add(new SelectItem(obj, obj.toString()));
        }

        return isOrdering ? ordenaSelectItem(toReturn) : toReturn;
    }

    public static List<SelectItem> getListSelectItemSemCampoVazioSemOrdenacao(Object[] values) {
        List<SelectItem> toReturn = new ArrayList();


        for (Object obj : Lists.newArrayList(values)) {
            toReturn.add(new SelectItem(obj, obj.toString()));
        }
        return toReturn;
    }

    public static List<SelectItem> getListSelectItemSemCampoVazioComOrdenacao(Object[] values) {
        List<SelectItem> toReturn = new ArrayList();


        for (Object obj : Lists.newArrayList(values)) {
            toReturn.add(new SelectItem(obj, obj.toString()));
        }
        return ordenaSelectItem(toReturn);
    }


    public static List<SelectItem> getListSelectItemSemOrdenacao(Object[] values) {
        List<SelectItem> toReturn = new ArrayList();
        toReturn.add(new SelectItem(null, ""));

        for (Object obj : values) {
            toReturn.add(new SelectItem(obj, obj.toString()));
        }

        return toReturn;
    }


    public static List<SelectItem> ordenaSelectItem(List<SelectItem> itens) {
        Collections.sort(itens, new OrdenaSelecItem());
        return itens;
    }

    public static String getRequestContextPath() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
    }

    public static String converterBooleanSimOuNao(Boolean o) {
        return o ? "Sim" : "Não";
    }

    public static class OrdenaSelecItem implements Comparator<SelectItem> {

        @Override
        public int compare(SelectItem o1, SelectItem o2) {
            try {
                if (o1.getValue() == null) {
                    return -1;
                }
                if (o2.getValue() == null) {
                    return 1;
                }
                return o1.getLabel().compareTo(o2.getLabel());  //To change body of implemented methods use File | Settings | File Templates.
            } catch (Exception e) {
                return Integer.MAX_VALUE;
            }
        }
    }

    public static String getValorFormatado(BigDecimal valor) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(3);
        return nf.format(new BigDecimal(valor.toString()));
    }

    public static String getValorFormatadoNovo(BigDecimal valor) {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(3);
        return nf.format(valor.toBigInteger());
    }

    public static Object getControladorPeloNome(String nome) {
        ELContext el = FacesContext.getCurrentInstance().getELContext();
        ELResolver er = FacesContext.getCurrentInstance().getApplication().getELResolver();
        return er.getValue(el, null, nome);
    }

    public static Object getSpringBeanPeloNome(String nome) {
        ApplicationContext ap = ContextLoader.getCurrentWebApplicationContext();
        return ap.getBean(nome);
    }

    public static String gerarUrlImagemDir() {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        final StringBuffer url = request.getRequestURL();
        String test = url.toString();
        String[] quebrado = test.split("/");
        StringBuilder b = new StringBuilder();
        b.append(quebrado[0]);
        b.append("/").append(quebrado[1]);
        b.append("/").append(quebrado[2]);
        return b.toString();
    }

    public static void validarCampos(AbstractEntity se) {
        ValidacaoException ex = new ValidacaoException();
        for (Field field : Persistencia.getAtributos(se.getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(CRUD.class) && field.getAnnotation(CRUD.class).obrigatorio()) {
                if (Persistencia.getAttributeValue(se, field.getName()) == null || Persistencia.getAttributeValue(se, field.getName()).toString().trim().length() <= 0) {
                    String nomeCampo = field.getAnnotation(CRUD.class).label() == null
                        || field.getAnnotation(CRUD.class).label().trim().length() <= 0
                        ? field.getName().substring(0, 1).toUpperCase()
                        + field.getName().substring(1, field.getName().length()) : field.getAnnotation(CRUD.class).label();
                    ex.adicionarMensagemDeCampoObrigatorio("O campo " + nomeCampo + " é obrigatório.");

                }
            }
        }

        ex.lancarException();

    }

    public static Boolean containSessao(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().containsKey(key);
    }

    public static void colocarSessao(String key, StreamedContent streamedContent) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, streamedContent);
    }


    public static boolean validarCpfOrCnpj(String s_aux) {
        if (s_aux.isEmpty()) {
            return true;
        } else {
            s_aux = s_aux.replace(".", "");
            s_aux = s_aux.replace("-", "");
//------- Rotina para CPF
            if (s_aux.length() == 11) {
                int d1, d2;
                int digito1, digito2, resto;
                int digitoCPF;
                String nDigResult;
                d1 = d2 = 0;
                digito1 = digito2 = resto = 0;
                for (int n_Count = 1; n_Count < s_aux.length() - 1; n_Count++) {
                    digitoCPF = Integer.valueOf(s_aux.substring(n_Count - 1, n_Count)).intValue();
//--------- Multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
                    d1 = d1 + (11 - n_Count) * digitoCPF;
//--------- Para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
                    d2 = d2 + (12 - n_Count) * digitoCPF;
                }
                ;
//--------- Primeiro resto da divisão por 11.
                resto = (d1 % 11);
//--------- Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
                if (resto < 2) {
                    digito1 = 0;
                } else {
                    digito1 = 11 - resto;
                }
                d2 += 2 * digito1;
//--------- Segundo resto da divisão por 11.
                resto = (d2 % 11);
//--------- Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
                if (resto < 2) {
                    digito2 = 0;
                } else {
                    digito2 = 11 - resto;
                }
//--------- Digito verificador do CPF que está sendo validado.
                String nDigVerific = s_aux.substring(s_aux.length() - 2, s_aux.length());
//--------- Concatenando o primeiro resto com o segundo.
                nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
//--------- Comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
                return nDigVerific.equals(nDigResult);
            } //-------- Rotina para CNPJ
            else {
                return false;
            }
        }
    }

    public static String getTelefone(String phone) {
        if (phone.length() > 10) {
            return "(" + phone.substring(0, 2) + ") " + phone.substring(2, 7) + "-" + phone.substring(7, 11);
        }
        else {
            return "(" + phone.substring(0, 2) + ") " + phone.substring(2, 6) + "-" + phone.substring(6, 10);
        }
    }

    public static void isTamanhoTelefoneValido(String phone) {
        ValidacaoException ve = new ValidacaoException();
        if (phone.length() < 10 ||
            phone.length() > 11) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Informe um telefone válido");
        }
        ve.lancarException();
    }

    public static boolean isValidCpfOrCnpj(String s_aux) {
        if (s_aux == null) {
            return false;
        }
        s_aux = s_aux.replace(".", "");
        s_aux = s_aux.replace("-", "");
        s_aux = s_aux.replace("/", "");
//------- Rotina para CPF
        if (s_aux.length() == 11) {
            if (Sets.newHashSet("00000000000", "11111111111", "999999999").contains(s_aux)) {
                return false;
            }

            int d1, d2;
            int digito1, digito2, resto;
            int digitoCPF;
            String nDigResult;
            d1 = d2 = 0;
            digito1 = digito2 = resto = 0;
            for (int n_Count = 1; n_Count < s_aux.length() - 1; n_Count++) {
                digitoCPF = Integer.valueOf(s_aux.substring(n_Count - 1, n_Count)).intValue();
//--------- Multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
                d1 = d1 + (11 - n_Count) * digitoCPF;
//--------- Para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
                d2 = d2 + (12 - n_Count) * digitoCPF;
            }
            ;
//--------- Primeiro resto da divisão por 11.
            resto = (d1 % 11);
//--------- Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
            if (resto < 2) {
                digito1 = 0;
            } else {
                digito1 = 11 - resto;
            }
            d2 += 2 * digito1;
//--------- Segundo resto da divisão por 11.
            resto = (d2 % 11);
//--------- Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
            if (resto < 2) {
                digito2 = 0;
            } else {
                digito2 = 11 - resto;
            }
//--------- Digito verificador do CPF que está sendo validado.
            String nDigVerific = s_aux.substring(s_aux.length() - 2, s_aux.length());
//--------- Concatenando o primeiro resto com o segundo.
            nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
//--------- Comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
            return nDigVerific.equals(nDigResult);
        } //-------- Rotina para CNPJ
        else if (s_aux.length() == 14) {
            if (Sets.newHashSet("00000000000000", "11111111111111", "999999999999").contains(s_aux)) {
                return false;
            }

            int soma = 0, aux, dig;
            String cnpj_calc = s_aux.substring(0, 12);
            char[] chr_cnpj = s_aux.toCharArray();
//--------- Primeira parte
            for (int i = 0; i < 4; i++) {
                if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                    soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
                }
            }
            for (int i = 0; i < 8; i++) {
                if (chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9) {
                    soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
                }
            }
            dig = 11 - (soma % 11);
            cnpj_calc += (dig == 10 || dig == 11)
                ? "0" : Integer.toString(dig);
//--------- Segunda parte
            soma = 0;
            for (int i = 0; i < 5; i++) {
                if (chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                    soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
                }
            }
            for (int i = 0; i < 8; i++) {
                if (chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9) {
                    soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
                }
            }
            dig = 11 - (soma % 11);
            cnpj_calc += (dig == 10 || dig == 11)
                ? "0" : Integer.toString(dig);
            return s_aux.equals(cnpj_calc);
        } else {
            return false;
        }
    }

    public static String generateTokenData() {
        return RandomStringUtils.randomAlphanumeric(DEF_COUNT);
    }


    public static String localUsuarioAtualmente() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        // URL Completa
        String urlFinal = request.getRequestURL().toString();

        // Geralmente /webpublico/faces
        String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + FacesContext.getCurrentInstance().getExternalContext().getRequestServletPath();
        urlFinal = urlFinal.substring(urlFinal.lastIndexOf(path) + path.length(), urlFinal.length());
        return urlFinal;
    }

    public static String localUsuarioAtualmenteCompleta() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        return request.getRequestURL().toString();
    }

    public static Date adicionaDias(Date data, Integer dias) {
        Calendar c = Calendar.getInstance();
        c.setTime(data);
        c.add(Calendar.DAY_OF_MONTH, dias);
        return c.getTime();
    }

    public static Integer recuperarExercicio() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        return c.get(Calendar.YEAR);
    }

    public InputStream getImagemInputStream(Arquivo arq) {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            buffer.write(arq.getArquivo());
            return new ByteArrayInputStream(buffer.toByteArray());
        } catch (Exception ex) {
            ex.getMessage();
        }
        return null;
    }

    public static String getSaudacao() {
        String texto = "";
        GregorianCalendar calendar = new GregorianCalendar();
        int hora = calendar.get(Calendar.HOUR_OF_DAY);

        if (hora >= 0 && hora <= 12) {
            texto = "Bom dia";

        }
        if (hora > 12 && hora <= 18) {
            texto = "Boa tarde";

        }
        if (hora > 18 && hora <= 24) {
            texto = "Boa noite";
        }
        return texto;
    }

    public static Integer diferencaDeDiasEntreDuasDatas(Date dataAnterior, Date dataPosterior) {
        if (dataAnterior.after(dataPosterior)) {
            return 0;
        }
        int dias = Days.daysBetween(new LocalDate(dataAnterior), new LocalDate((dataPosterior))).getDays();
        return dias;
    }

    public static String getDescricaoDoEnum(Class classe, Field field) {
        try {
            Enum<?> valorDoFieldEnum = Enum.valueOf((Class<? extends Enum>) classe, field.getName());
            String nomeCampo = "descricao";
            String methodName = "get" + nomeCampo.substring(0, 1).toUpperCase() + nomeCampo.substring(1);
            Object valorDescricaoEnum = valorDoFieldEnum.getClass().getMethod(methodName, new Class[]{}).invoke(valorDoFieldEnum, new Object[]{});
            return valorDescricaoEnum.toString();
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return field.getName();
        }
    }

    public static String zerosAEsquerda(String numero, Integer qtdDigitos) {
        String completo = numero;
        while (completo.length() < qtdDigitos) {
            completo = "0" + completo;
        }
        return completo;
    }

    public static Object clonarObjeto(Object obj) {
        try {
            return BeanUtils.cloneBean(obj);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static Date dataSemHorario(Date data) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
