/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.util;


import br.com.gleac.entidade.mail.ConfiguracaoEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author Romanini
 */
public class EmailUtil {



    public static void enviaEmail(ConfiguracaoEmail ce, String Destinatario) {
        final String email = ce.getEmailRemetente();
        final String senha = ce.getSenha();
        String assunto =  "E-mail de Teste da GLEAC";
        String mensagem = "Isso é apenas um e-mail de teste." +
            "<br>" +
            "Parabéns, sua configuração de e-mail está funcionando corretamente." +
            "<br>" +
            "<br>" +
            "<b><i>Equipe de Suporte da GLEAC.</i></b>";
        enviaEmail(email, senha, ce.getHostSmtp(), ce.getSsl(), ce.getTsl(), ce.getPorta(), assunto, mensagem, Destinatario, true);
    }

    public static void enviaEmail(ConfiguracaoEmail ce, String Destinatario, String assunto, String mensagem) {
        final String email = ce.getEmailRemetente();
        final String senha = ce.getSenha();
        enviaEmail(email, senha, ce.getHostSmtp(), ce.getSsl(), ce.getTsl(), ce.getPorta(), assunto, mensagem, Destinatario, true);
    }

    private static void enviaEmail(String emailP,
            String senhaP,
            String hostSMTP,
            Boolean SSL,
            Boolean TSL,
            String porta,
            String assunto,
            String mensagem,
            String destinatario,
            Boolean html) {

        final String email = emailP;
        final String senha = senhaP;

        Properties props = new Properties();

        props.put("mail.smtp.host", hostSMTP);
        if (SSL) {
            props.put("mail.smtp.socketFactory.port", porta);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }
        props.put("mail.smtp.auth", SSL.toString());
        props.put("mail.smtp.starttls.enable", TSL.toString());
        props.put("mail.smtp.port", porta);

//        Session session = Session.getDefaultInstance(props,
//                new javax.mail.Authenticator() {
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(email, senha);
//                    }
//                });
//
//
//        session.setDebug(true);
        Session session = Session.getInstance(props);
        try {

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));

            Address[] toUser = InternetAddress
                    .parse(destinatario);

            message.setRecipients(Message.RecipientType.TO, toUser);
            message.setContent(mensagem, html ? "text/html; charset=UTF-8" : "text/plain; charset=UTF-8");
            message.setSubject(assunto, "UTF-8");
            message.setSentDate(new Date());

//            Transport.send(message);
            Transport t = session.getTransport("smtp");
            try {
                t.connect(hostSMTP, email, senha);
                t.sendMessage(message, message.getAllRecipients());
            } finally {
                t.close();
            }

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
