package br.com.gleac.util;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.view.ColunaView;
import br.com.gleac.util.view.View;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by renatoromanini on 14/08/15.
 */
public class Persistencia {

    private static final Logger logger = Logger.getLogger("br.com.aliancasolucoes");

    /**
     * Obtem o valor da chave primária de uma entidade. O método procura o
     * atributo com a anotação ID na classe e nas superclasses
     *
     * @param entidade entidade que deseja-se a chave
     * @return valor da chave
     */
    public static Object getId(Object entidade) {
        try {
            Field f = getFieldId(entidade.getClass());
            f.setAccessible(true);
            return f.get(entidade);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Não foi possível encontrar a chave primária de " + entidade, ex);
        }
        return null;
    }

    /**
     * Obtém o valor do atributo a partir de seu nome e do objeto que o contém
     *
     * @param entidade
     * @param attrName
     * @return
     */
    public static Object getAttributeValue(Object entidade, String attrName) {
        try {
            //Field f = entidade.getClass().getField(attrName);
            String methodName = "get" + attrName.substring(0, 1).toUpperCase() + attrName.substring(1);
            return entidade.getClass().getMethod(methodName, new Class[]{}).invoke(entidade, new Object[]{});
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Não foi possível encontrar o atributo " + attrName + " na classe " + entidade.getClass().getName(), ex);
        }
        return null;
    }

    /**
     * Altera o valor da chave primária de um objeto, procura pelo
     *
     * @param entidade a entidade a alterar o ID
     * @param valor    novo valor
     * @Id
     */
    public static void setId(Object entidade, Object valor) {
        try {
            Field f = getFieldId(entidade.getClass());
            f.setAccessible(true);
            f.set(entidade, valor);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Não foi possível encontrar a chave primária de " + entidade, ex);
        }
    }

    /**
     * Obtem o atributo que é chave primária de uma entidade. O método procura o
     * atributo com a anotação ID na classe e nas superclasses
     *
     * @return Field da chave
     */
    public static Field getFieldId(Class classe) {
        try {
            for (Field f : getAtributos(classe)) {
                if (f.isAnnotationPresent(Id.class)) {
                    return f;
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Não foi possível encontrar a chave primária de " + classe, ex);
        }
        return null;
    }

    /**
     * Método recursivo para descobrir todos os atributos da entidade, incluindo
     * os das superclasses, se existirem
     *
     * @param classe classe da entidade
     * @return um ArrayList com os atributos (Fields) da entidade
     */
    public static List<Field> getAtributos(Class classe) {
        List<Field> lista = new ArrayList<Field>();
        if (!classe.getSuperclass().equals(Object.class)) {
            lista.addAll(getAtributos(classe.getSuperclass()));
        }
        for (Field f : classe.getDeclaredFields()) {
            if (Modifier.isStatic(f.getModifiers())) {
                continue;
            }
            lista.add(f);
        }
        return lista;
    }

    /**
     * Método que retorna o primeiro atributo da classe de entidade. Não retorna
     * atributos estáticos. Evita retorna atributos com a anotação
     * GeneratedValue, isto é, caso exista algum atributo não estático sem esta
     * anotação, este será retornado. Este método é destinado a construção de
     * consultas genéricas.
     *
     * @param classe Classe da entidade.
     * @return
     */
    public static Field primeiroAtributo(Class classe) {
        Field f = null;
        for (Field atributo : getAtributos(classe)) {
            if (Modifier.isStatic(atributo.getModifiers())) {
                continue;
            }
            if (f == null) {
                f = atributo;
            }
            if (!atributo.isAnnotationPresent(GeneratedValue.class)) {
                return atributo;
            } else if (f == null) {
                f = atributo;
            }
        }
        return f;
    }

    public static void duplicaColecoes(Object destino, Object origem) {
        try {
            for (Field f : getAtributos(origem.getClass())) {
                if (f.isAnnotationPresent(OneToMany.class) || (f.isAnnotationPresent(ManyToMany.class))) {
                    logger.log(Level.INFO, "Colecao " + f);
                    f.setAccessible(true);
                    Collection colecaoDestino = null;
                    Collection colecaoOrigem = (Collection) f.get(origem);
                    if (f.getType().equals(Set.class)) {
                        colecaoDestino = new HashSet();
                    }
                    if (f.getType().equals(List.class)) {
                        colecaoDestino = new ArrayList();
                    }
                    for (Object obj : colecaoOrigem) {
                        colecaoDestino.add(obj);
                    }
                    f.set(destino, colecaoDestino);
                }
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Problema ao duplicar colecoes", ex);
        }
    }

    public static boolean atributoPossuiRelacionamentoOneToOneOneToMany(Field f) {
        if (f.isAnnotationPresent(OneToOne.class)) {
            if (f.getAnnotation(OneToOne.class).mappedBy().trim().length() > 0 && f.getAnnotation(OneToOne.class).orphanRemoval() != true) { // .orphanRemoval() != true, pois pode ser null
                return true;
            }
        }
        if (f.isAnnotationPresent(OneToMany.class)) {
            if (f.getAnnotation(OneToMany.class).mappedBy().trim().length() > 0 && f.getAnnotation(OneToMany.class).orphanRemoval() != true) { // .orphanRemoval() != true, pois pode ser null
                return true;
            }
        }

        return false;
    }

    public static Field getCampo(Class classe, String nomeCampo) {
        Field f = null;
        for (Field atributo : getAtributos(classe)) {
            if (atributo.getName().equals(nomeCampo)) {
                return atributo;
            }
        }
        return f;
    }

    public static List<Field> getCamposComAnotacao(Class clazz, Class anotacao) {
        List<Field> campos = new ArrayList<Field>();
        for (Field f : clazz.getDeclaredFields()) {
            f.setAccessible(true);
            if (f.isAnnotationPresent(anotacao)) {
                campos.add(f);
            }
        }
        return campos;
    }

    public static Object obterConteudoCampo(Object se, Field atributo) throws IllegalArgumentException, IllegalAccessException {
        atributo.setAccessible(true);
        if (se == null) {
            return "";
        }
        if (atributo.get(se) == null) {
            return "";
        }
        Object valor = Persistencia.getAttributeValue(se, atributo.getName());
        if (valor == null) {
            return "";
        }
        if (atributo.isAnnotationPresent(CRUD.class)) {

            if (valor != null) {
                if (atributo.getType().equals(BigDecimal.class)) {
                    NumberFormat nf = NumberFormat.getCurrencyInstance();
                    nf.setMinimumFractionDigits(2);
                    nf.setMaximumFractionDigits(3);
                    return nf.format(new BigDecimal(valor.toString()));
                }

                if (atributo.getType().equals(Date.class)) {
                    Temporal t = atributo.getAnnotation(Temporal.class);
                    if (t.value() == TemporalType.TIME) {
                        return Util.sdfh.format(valor);
                    }
                    if (t.value() == TemporalType.DATE) {
                        return Util.sdf.format(valor);
                    } else {
                        return Util.sdft.format(valor);
                    }
                }

                if (atributo.getType().equals(Boolean.class)) {
                    if (valor.equals(true)) {
                        return "Sim";
                    } else {
                        return "Não";
                    }
                }


                if (atributo.getType().getSuperclass().equals(AbstractEntity.class)) {
                    Object obj = atributo.get(se);
                    if (obj == null) {
                        atributo.set(se, obj);
                    }
                    AbstractEntity e = (AbstractEntity) obj;
                    return e.getToStringHtml();
                }

                if (atributo.getType().isEnum()) {
                    return valor.toString();
                }
                return valor;
            }
        }
        return " ";
    }

    private boolean isAtributoListaDeSuperEntidade(Field atributo) {
        ParameterizedType pt = (ParameterizedType) atributo.getGenericType();
        for (Type t : pt.getActualTypeArguments()) {
            if (isSuperEntidade((Class) t)) {
                return true;
            }
        }
        return false;
    }


    public static boolean isSuperEntidade(Class c) {
        if (c == null) {
            return false;
        }

        if (c.equals(AbstractEntity.class)) {
            return true;
        } else {
            return isSuperEntidade(c.getSuperclass());
        }
    }

    public static List<Field> getAtributosTabelaveis(Class classe) {
        List<Field> filtros = Lists.newArrayList();
        for (Field field : Persistencia.getAtributos(classe)) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(CRUD.class) && field.getAnnotation(CRUD.class).tabelavel()) {
                filtros.add(field);
            }
        }
        return filtros;
    }

    public static String getNomeCampo(Field f) {
        if (f.isAnnotationPresent(CRUD.class)) {
            return f.getAnnotation(CRUD.class).label();
        }

        return f.getName().substring(0, 1) + f.getName().substring(1, f.getName().length());
    }

    public static String getValorCampo(Field field, Object objeto) {
        try {
            return Persistencia.obterConteudoCampo(objeto, field).toString();
        } catch (Exception e) {
            return "";
        }

    }

    private boolean isPossuiAtributosIndentificador(AbstractEntity se) {
        boolean possui = false;
        for (Field field : Persistencia.getAtributos(se.getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Identificador.class)) {
                possui = true;
            }
        }
        return possui;
    }

    public static String adicionarColunas(View view, String alias, Class classe) {

        String sql = "";
        for (Field field : Persistencia.getAtributosTabelaveis(classe)) {
            field.setAccessible(true);
            sql += alias + ".";
            sql += field.getType().isAnnotationPresent(Entity.class) ? field.getName() + "_id" : field.getName();
            sql += ", ";
            view.getColunas().add(new ColunaView(null, Persistencia.getNomeCampo(field), field.getType(), field.getType().isAnnotationPresent(Entity.class), null));
        }
        return sql;
    }

    public static String getNomeEntidade(Class classe) {
        if (classe.isAnnotationPresent(CRUD.class)) {
            CRUD crud = (CRUD) classe.getAnnotation(CRUD.class);
            return crud.label();
        }
        return classe.getSimpleName();
    }

    public static List<Field> getAtributosPesquisaveis(Class classe) {
        List<Field> filtros = Lists.newArrayList();
        for (Field field : Persistencia.getAtributos(classe)) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(CRUD.class) && field.getAnnotation(CRUD.class).pesquisavel()) {
                filtros.add(field);
            }
        }
        return filtros;
    }
}
