package br.com.gleac.util;

import br.com.gleac.anotacao.Paramiter;

import java.lang.reflect.Field;

/**
 * Created by renatoromanini on 15/07/2017.
 */
public class UtilFinanceiro {
    public static final String URL_PAGHIPER_CREATE = "https://api.paghiper.com/transaction/create/";
    public static final String URL_CONFIRM_PAGHIPER = "https://www.paghiper.com/checkout/confirm";
    public static final String URL_PAGHIPER_STATUS = "https://api.paghiper.com/transaction/status/";
    public static final String URL_PAGHIPER_CANCEL = "https://api.paghiper.com/transaction/cancel/";
    public static final String URL_PAGHIPER_CARNE_BOLETO = "https://api.paghiper.com/transaction/multiple_bank_slip/";

    public static String nomeParamiter(Field field) {
        if (field.isAnnotationPresent(Paramiter.class)) {
            Paramiter a = field.getAnnotation(Paramiter.class);
            if (!a.nome().equals("")) {
                return a.nome();
            }
        }
        return field.getName();
    }

    public static class StatusBoleto {
        private String transaction_id;
        private String apiKey;
        private String token;

        public StatusBoleto(String transaction_id, String apiKey, String token) {
            this.transaction_id = transaction_id;
            this.apiKey = apiKey;
            this.token = token;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }

}
