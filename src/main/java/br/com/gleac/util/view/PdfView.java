package br.com.gleac.util.view;

import br.com.gleac.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Locale;

/**
 * Created by renatoromanini on 28/10/16.
 */
public class PdfView {


    private static final Logger logger = LoggerFactory.getLogger(PdfView.class);

    public static void geraPDF(String nome, String conteudo, FacesContext facesContext) {
        try {
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline;filename=" + nome + ".pdf");
            response.setCharacterEncoding("iso-8859-1");
            response.setLocale(new Locale("pt_BR_", "pt", "BR"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Html2Pdf.convert(conteudo, baos);
            byte[] bytes = baos.toByteArray();
            response.setContentLength(bytes.length);
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(bytes, 0, bytes.length);
            outputStream.flush();
            outputStream.close();
            facesContext.responseComplete();
        } catch (Exception e) {
            logger.debug("Erro em geraPDF", e);
        }

    }

    public void imprimirPDF(View view) {
        String conteudo = "";
        String nome = "GLEAC - SERENÍSSIMA GRANDE LOJA MAÇÔNICA DO ESTADO DO ACRE";
        conteudo += " <head>"
                + "<style>"
                + "body{"
                + " font-family:arial, font-size: 12px"
                + "}"
                + "</style>"
                + "<style type=\"text/css\"> " +
                "table.gridtable { " +
                " font-family: verdana,arial,sans-serif; " +
                " font-size:11px; " +
                " color:#333333; " +
                " border-width: 1px; " +
                " border-color: #666666; " +
                " border-collapse: collapse; " +
                "} " +
                "table.gridtable th { " +
                " border-width: 1px; " +
                " padding: 2px; " +
                " border-style: solid; " +
                " border-color: #666666; " +
                " background-color: #dedede; " +
                "} " +
                "table.gridtable td { " +
                " border-width: 1px; " +
                " padding: 2px; " +
                " border-style: solid; " +
                " border-color: #666666; " +
                " background-color: #ffffff; " +
                "} " +
                "</style>"
                + " <title>"
                + " GLEAC - SERENÍSSIMA GRANDE LOJA MAÇÔNICA DO ESTADO DO ACRE"
                + " </title>"
                + " </head>";

        conteudo += " <div > " +
                getImagemLogo() +
                "<center> " +
                "<h2> " + nome + " </h2> " +
                "<h3> " + view.getNomeRelatorio() + "</h3> " +
                "</center> ";
        conteudo += "<table  style=\"width:100%;\" class=\"gridtable\" >";
        conteudo += "<tr>";

        for (ColunaView colunaView : view.getColunasImpressao()) {
            conteudo += "<th> " + colunaView.getNomeColuna() + " </th>";
        }
        conteudo += "</tr>";

        for (ObjetoView objetoView : view.getObjetos()) {
            conteudo += "<tr> ";

            for (ColunaView colunaView : objetoView.getColunasImpressao()) {
                conteudo += "<td> ";
                if (colunaView == null || colunaView.getValor() == null) {
                    conteudo += " ";
                } else {
                    conteudo += colunaView.getValor().toString().replaceAll("\\<.*?>", "");
                }
                conteudo += "</td>";
            }
            conteudo += "</tr>";
        }
        conteudo += "</table>";
        conteudo += "</div>";

        geraPDF("Diartuso", conteudo, FacesContext.getCurrentInstance());
    }

    public String getImagemLogo() {
        return "<img src=\"" + Util.localUsuarioAtualmente() + "resources/images/logo.jpg\" width=\"800\" />";
    }
}
