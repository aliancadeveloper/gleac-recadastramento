package br.com.gleac.util.view;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by renatoromanini on 01/09/15.
 */
public class View {
    private static final Integer MAXIMO_REGISTROS_TABELA = 10;
    private String sqlRecuperadorObjetos;
    private String sqlContadorObjetos;
    private String sqlSomatorioObjetos;
    private String sqlOrdenar;

    private String nomeRelatorio;
    private List<ParametroView> parametros;

    private List<ObjetoView> objetos;
    private List<ColunaView> colunas;


    private Integer inicio;
    private Integer totalDeRegistrosExistentes;
    private Integer maximoRegistrosTabela;

    public View() {
        this.parametros = Lists.newArrayList();
        this.objetos = Lists.newArrayList();
        this.colunas = Lists.newArrayList();
        inicio = 0;
        maximoRegistrosTabela = MAXIMO_REGISTROS_TABELA;
    }

    public List<ColunaView> getColunas() {
        return colunas;
    }

    public List<ColunaView> getColunasImpressao() {
        List<ColunaView> retorno = Lists.newArrayList();
        for (ColunaView colunaView : getColunas()) {
//            if(!colunaView.getClasse().equals(Arquivo.class)){
                retorno.add(colunaView);
//            }
        }
        return retorno;
    }

    public void setColunas(List<ColunaView> colunas) {
        this.colunas = colunas;
    }

    public Integer getMaximoRegistrosTabela() {
        return maximoRegistrosTabela;
    }

    public void setMaximoRegistrosTabela(Integer maximoRegistrosTabela) {
        this.maximoRegistrosTabela = maximoRegistrosTabela;
    }

    public Integer getTotalDeRegistrosExistentes() {
        return totalDeRegistrosExistentes;
    }

    public void setTotalDeRegistrosExistentes(Integer totalDeRegistrosExistentes) {
        this.totalDeRegistrosExistentes = totalDeRegistrosExistentes;
    }

    public Integer getInicio() {
        return inicio;
    }

    public void setInicio(Integer inicio) {
        this.inicio = inicio;
    }


    public String getSqlOrdenar() {
        return sqlOrdenar;
    }

    public void setSqlOrdenar(String sqlOrdenar) {
        this.sqlOrdenar = sqlOrdenar;
    }

    public String getSqlRecuperadorObjetos() {
        return sqlRecuperadorObjetos;
    }

    public void setSqlRecuperadorObjetos(String sqlRecuperadorObjetos) {
        this.sqlRecuperadorObjetos = sqlRecuperadorObjetos;
    }

    public String getSqlContadorObjetos() {
        return sqlContadorObjetos;
    }

    public void setSqlContadorObjetos(String sqlContadorObjetos) {
        this.sqlContadorObjetos = sqlContadorObjetos;
    }

    public List<ParametroView> getParametros() {
        return parametros;
    }

    public void setParametros(List<ParametroView> parametros) {
        this.parametros = parametros;
    }

    public List<ObjetoView> getObjetos() {
        return objetos;
    }

    public void setObjetos(List<ObjetoView> objetos) {
        this.objetos = objetos;
    }

    public String getSqlSomatorioObjetos() {
        return sqlSomatorioObjetos;
    }

    public void setSqlSomatorioObjetos(String sqlSomatorioObjetos) {
        this.sqlSomatorioObjetos = sqlSomatorioObjetos;
    }



    public String getNomeRelatorio() {
        return nomeRelatorio;
    }

    public void setNomeRelatorio(String nomeRelatorio) {
        this.nomeRelatorio = nomeRelatorio;
    }
}
