package br.com.gleac.util.view;

import br.com.gleac.enums.OperacaoCondicaoSql;
import br.com.gleac.enums.TipoComponente;
import br.com.gleac.supers.AbstractController;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by renatoromanini on 01/09/15.
 */
public class ParametroView {
    private String label;
    private String condicao;
    private Object valor;
    private Object valor2;
    private OperacaoCondicaoSql operacao;
    private TipoComponente tipoComponente;
    private Class classe;
    private AbstractController abstractController;

    public ParametroView(String label, String condicao, Object valor) {
        this.label = label;
        this.condicao = condicao;
        this.valor = valor;
        this.operacao = OperacaoCondicaoSql.IGUAL;
    }

    public ParametroView(String label, String condicao, Object valor, OperacaoCondicaoSql operador, TipoComponente tipoComponente, Class classe) {
        this.label = label;
        this.condicao = condicao;
        this.valor = valor;
        this.operacao = operador;
        this.tipoComponente = tipoComponente;
        this.classe = classe;
    }

    public void atribuirControlador(AbstractController abstractController) {
        this.abstractController = abstractController;
    }

    public AbstractController getControlador() {
        return abstractController;
    }

    public String getCondicao() {
        return condicao;
    }

    public void setCondicao(String condicao) {
        this.condicao = condicao;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public OperacaoCondicaoSql getOperacao() {
        return operacao;
    }

    public void setOperacao(OperacaoCondicaoSql operacao) {
        this.operacao = operacao;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TipoComponente getTipoComponente() {
        return tipoComponente;
    }

    public void setTipoComponente(TipoComponente tipoComponente) {
        this.tipoComponente = tipoComponente;
    }

    public Class getClasse() {
        return classe;
    }

    public void setClasse(Class classe) {
        this.classe = classe;
    }

    public Object getValor2() {
        return valor2;
    }

    public void setValor2(Object valor2) {
        this.valor2 = valor2;
    }

    public List<SelectItem> getValoresEnum() {
        String nomeDaClasse = this.classe.toString();
        nomeDaClasse = nomeDaClasse.replace("class ", "");
        Class<?> classe = null;
        try {
            classe = Class.forName(nomeDaClasse);
        } catch (ClassNotFoundException ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro !", "Classe não encontrada : " + nomeDaClasse));
        }
        List<SelectItem> retorno = new ArrayList<SelectItem>();
        retorno.add(new SelectItem(null, ""));
        for (Field field : classe.getDeclaredFields()) {
            if (field.isEnumConstant()) {
                String descricao = "";
                try {
                    Enum<?> valor = Enum.valueOf((Class<? extends Enum>) classe, field.getName());
                    String nomeCampo = "descricao";
                    String methodName = "get" + nomeCampo.substring(0, 1).toUpperCase() + nomeCampo.substring(1);
                    Object valorRecuperado = valor.getClass().getMethod(methodName, new Class[]{}).invoke(valor, new Object[]{});
                    descricao = valorRecuperado.toString();
                } catch (Exception e) {
                    descricao = field.getName();
                }
                retorno.add(new SelectItem(field.getName(), descricao));
            }
        }

        Collections.sort(retorno, new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem o1, SelectItem o2) {
                return o1.getLabel().compareTo(o2.getLabel());
            }
        });
        return retorno;
    }
}
