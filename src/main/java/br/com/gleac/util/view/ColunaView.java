package br.com.gleac.util.view;

/**
 * Created by renatoromanini on 01/09/15.
 */
public class ColunaView {

    private Object valor;
    private String nomeColuna;
    private Class classe;
    private Boolean entidade;
    private Object objeto;
    private Boolean dateTimeStamp;

    public ColunaView(Object valor, String nomeColuna, Class classe, Boolean entidade, Object objeto) {
        this.valor = valor;
        this.nomeColuna = nomeColuna;
        this.classe = classe;
        this.entidade = entidade;
        this.objeto = objeto;
        this.dateTimeStamp = Boolean.FALSE;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public Class getClasse() {
        return classe;
    }

    public void setClasse(Class classe) {
        this.classe = classe;
    }

    public Boolean getEntidade() {
        return entidade;
    }

    public void setEntidade(Boolean entidade) {
        this.entidade = entidade;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public String getNomeColuna() {
        return nomeColuna;
    }

    public void setNomeColuna(String nomeColuna) {
        this.nomeColuna = nomeColuna;
    }

    public Boolean getDateTimeStamp() {
        return dateTimeStamp;
    }

    public void setDateTimeStamp(Boolean dateTimeStamp) {
        this.dateTimeStamp = dateTimeStamp;
    }

    @Override
    public String toString() {
        if (valor != null) {
            return valor.toString();
        } else {
            return nomeColuna;
        }
    }
}
