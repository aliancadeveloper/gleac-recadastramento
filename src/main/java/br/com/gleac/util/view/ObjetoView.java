package br.com.gleac.util.view;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by renatoromanini on 01/09/15.
 */
public class ObjetoView {
    private Object valor;
    private Object id;
    private List<ColunaView> colunas;

    public ObjetoView(Object valor, List<ColunaView> colunas) {
        this.valor = valor;
        this.colunas = colunas;
    }

    public List<ColunaView> getColunas() {
        return colunas;
    }

    public List<ColunaView> getColunasImpressao() {
        List<ColunaView> retorno = Lists.newArrayList();
        for (ColunaView colunaView : getColunas()) {
//            if (!colunaView.getClasse().equals(Arquivo.class)) {
                retorno.add(colunaView);
//            }
        }
        return retorno;
    }

    public void setColunas(List<ColunaView> colunas) {
        this.colunas = colunas;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return valor.toString();
    }
}
