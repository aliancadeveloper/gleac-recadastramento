package br.com.gleac.enums;

/**
 * Created by israeleriston on 11/07/16.
 */
public enum Escolaridade {
    PRIMEIRO_GRAU("1º Grau"),
    SEGUNDO_GRAU("2º Grau"),
    NIVEL_SUPERIOR("Nível Superior"),
    ESPECIALIZACAO("Especialização"),
    MESTRE("Mestre"),
    DOUTOR("Doutor");

    private String descricao;

    Escolaridade(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}


