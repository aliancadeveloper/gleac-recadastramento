package br.com.gleac.enums;

/**
 * Created by renatoromanini on 01/08/17.
 */
public enum SituacaoContaPagar {
    ABERTA("Aberta"),
    PAGA("Paga"),
    BAIXADA("Baixa Manual"),
    ESTORNADA("Estornada"),
    CANCELADO("Cancelado");

    private String descricao;

    SituacaoContaPagar(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

    public boolean isAberta() {
        return SituacaoContaPagar.ABERTA.equals(this);
    }

    public boolean isPaga() {
        return SituacaoContaPagar.PAGA.equals(this);
    }
}
