package br.com.gleac.enums;

import br.com.gleac.entidade.seguranca.Recurso;

import java.util.List;

public enum Modulo {
    CADASTRO("Cadastros", 1, "fa fa-edit"),
    LOJA("Loja", 2, "fa fa-bank"),
    PROCESSO("Processo", 3, "fa fa-tasks"),
    FINANCEIRO("Financeiro", 4, "fa fa-money"),
    ESTOQUE("Estoque", 5, "fa fa-cubes"),
    ENDERECAMENTO("Endereçamento", 6, "fa fa-table"),
    CONFIGURACAO("Configurações", 7, "fa fa-gear"),
    ACESSO("Acesso", 8, "fa fa-users"),
    RELATORIO("Relatórios", 9, "fa fa-print");


    private String descricao;
    private Integer posicao;
    private String icone;
    private List<Recurso> recursos;


    Modulo(String descricao, Integer posicao, String icone) {
        this.descricao = descricao;
        this.posicao = posicao;
        this.icone = icone;
    }

    public String getDescricao() {
        return descricao;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public String getIcone() {
        return icone;
    }

    public List<Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    @Override
    public String toString() {
        return "Modulo{" +
            "descricao='" + descricao + '\'' +
            ", posicao=" + posicao +
            ", icone='" + icone + '\'' +
            '}';
    }
}
