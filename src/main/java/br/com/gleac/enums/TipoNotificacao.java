package br.com.gleac.enums;

/**
 * Created by renato on 11/07/17.
 */
public enum TipoNotificacao {

    INFORMACAO("Informação", "fa-info"),
    ALERTA("Alerta", "fa-star"),
    PROCESSO("Processo", "fa-envelope");

    private String descricao;
    private String icon;

    TipoNotificacao(String descricao, String icon) {
        this.descricao = descricao;
        this.icon = icon;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
