package br.com.gleac.enums;

/**
 * Created by israeleriston on 31/08/16.
 */
public enum TipoHistoricoMaconico {

    INICIACAO("Iniciação"),
    MUDANCA_LOJA("Mudança de Loja");

    private String descricao;

    TipoHistoricoMaconico(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
