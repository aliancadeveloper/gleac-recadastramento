package br.com.gleac.enums;

/**
 * Created by israeleriston on 11/07/16.
 */
public enum TipoSanguineo {
    AP("A+"),
    BP("B+"),
    AN("A-"),
    BN("B-"),
    ABP("AB+"),
    ABN("AB-"),
    OP("O+"),
    ON("O-"),
    NAO_INFORMADO("Não Informado");

    private String descricao;

    TipoSanguineo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
