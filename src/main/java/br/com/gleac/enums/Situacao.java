package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum Situacao {

    REGULAR("Regular"),
    IRREGULAR("Irregular"),
    COBERTURA_DIREITOS("Cobertura de Direitos"),
    QUIT_PLACET("Quit Placet"),
    EMERITO("Emérito"),
    FALECIDO("Falecido"),
    CERTIFICADO_GRAU("Certificado de Grau"),
    EXPULSO("Expulso"),
    PAST_GRA_MESTRE("Past Grão-Mestre"),
    BENEMERITO("Benemérito");

    private String descricao;

    Situacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
