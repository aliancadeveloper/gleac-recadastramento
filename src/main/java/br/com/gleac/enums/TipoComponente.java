package br.com.gleac.enums;

/**
 * Created by renatoromanini on 02/10/16.
 */
public enum TipoComponente {
    STRING("String"),
    DATA("Data"),
    DATA_BETWEEN("Data between"),
    DATA_HORA("Data"),
    HORA("Data"),
    SELECT_MENU("Select Menu"),
    SELECT_MENU_ENUM("Select Menu - ENUM"),
    AUTO_COMPLETE("Auto Complete"),
    PADRAO("Padrão conforme o tipo do campo");

    private String descricao;

    TipoComponente(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
