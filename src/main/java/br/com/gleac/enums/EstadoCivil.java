package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum EstadoCivil {

    SOLTEIRO("Solteiro(a)"),
    CASADO("Casado(a)"),
    SEPARADO("Separado(a)"),
    DIVORCIADO("Divorciado(a)"),
    VIUVO("Viuvo(a)"),
    UNIAO_ESTAVEL("União Estável"),
    OUTROS("Outros");

    private String descricao;


    EstadoCivil(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }


    @Override
    public String toString() {
        return descricao;
    }
}
