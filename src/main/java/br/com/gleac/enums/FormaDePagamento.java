package br.com.gleac.enums;

public enum FormaDePagamento {
    BOLETO("Boleto"),
    CARTAO_CREDITO("Cartão de Crédito"),
    CARTO_DEBITO("Cartão de Débito"),
    CARTAO_PRE_PAGO("Cartão Pré-Pago"),
    CHEQUE("Cheque"),
    PAGAMENTO_ESPECIE("Pagamento em Espécie"),
    TRANSFERENCIA_ELETRONICA("Transferência Eletrônica");


    private String descricao;

    FormaDePagamento(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
