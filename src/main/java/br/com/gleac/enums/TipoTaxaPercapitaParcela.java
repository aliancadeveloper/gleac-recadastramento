package br.com.gleac.enums;

/**
 * Created by renatoromanini on 12/01/2019.
 */
public enum TipoTaxaPercapitaParcela {

    UNICA_COM_DESCONTO("Única com desconto"),
    PRIMEIRA_PARCELA("Primeira Parcela"),
    SEGUNDA_PARCELA("Segunda Parcela"),
    TERCEIRA_PARCELA("Terceira Parcela");

    public static final Integer QUANTIDADE_PARCELA = 3;
    private String descricao;

    TipoTaxaPercapitaParcela(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

}


