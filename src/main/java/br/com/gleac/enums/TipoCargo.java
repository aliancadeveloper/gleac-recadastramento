package br.com.gleac.enums;

/**
 * Created by israeleriston on 12/07/16.
 */
public enum TipoCargo {

    MACONICO("Cargo Maçônico"),
    PROFISSIONAL("Cargo Profissional");

    private String descricao;

    TipoCargo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
