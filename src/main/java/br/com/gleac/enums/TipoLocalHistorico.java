package br.com.gleac.enums;

public enum TipoLocalHistorico {
    INTERNO("Interno (Grande Loja)"),
    EXTERNO("Externo (Fora da Grande Loja) ");

    private String descricao;

    TipoLocalHistorico(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
