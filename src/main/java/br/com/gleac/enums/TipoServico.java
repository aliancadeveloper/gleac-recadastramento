package br.com.gleac.enums;

/**
 * Created by zaca.
 */
public enum TipoServico {
    INICIACAO("Iniciação"),
    ELEVACAO("Elevação"),
    INSTALACAO("Instalação e/ou Posse"),
    EXALTACAO("Exaltação"),
    FILIACAO("Filiação"),
    REGULARIZACAO("Regularização"),
    TAXA("Taxa"),
    COMUM("Comum");

    private String descricao;

    TipoServico(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
