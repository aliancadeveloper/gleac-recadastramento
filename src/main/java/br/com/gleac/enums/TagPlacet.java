package br.com.gleac.enums;

/**
 * Created by zaca on 07/10/17.
 */
public enum TagPlacet {
    SERENISSIMO("Sereníssimo"),
    LOJA("Loja"),
    SERVICO("Serviço"),
    PESSOA("Maçom"),
    TIPO_SERVICO("Tipo Serviço/Cerimônia"),
    RODAPE("Rodapé"),
    CABECALHO("Cabeçalho"),
    NUMERO_PLACET("Número Placet"),
    EXERCICIO("Exercício");

    private String descricao;

    TagPlacet(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return "TagPlacet{" +
                "descricao='" + descricao + '\'' +
                '}';
    }
}
