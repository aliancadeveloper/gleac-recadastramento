package br.com.gleac.enums;

public enum SituacaoHistorico {
    INICIACAO("Iniciação"),
    ELEVACAO("Elevação"),
    EXALTACAO("Exaltação"),
    INSTALACAO("Instalação"),
    COBERTURA_DIREITOS("Cobertura de Direitos"),
    FILIACAO("Filiação"),
    QUIT_PLACET("Quit Placet"),
    ELIMINACAO("Eliminação"),
    CERTIFICADO_GRAU("Certificado de Grau"),
    REJEICAO("Rejeição"),
    REGULARIZACAO("Regularizacao");

    private String descricao;


    SituacaoHistorico(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
