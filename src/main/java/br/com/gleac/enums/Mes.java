package br.com.gleac.enums;

/**
 * Created by renatoromanini on 22/01/17.
 */
public enum Mes {

    JANEIRO("Janeiro", 1),
    FEVEREIRO("Fevereiro", 2),
    MARCO("Março", 3),
    ABRIL("Abril", 4),
    MAIO("Maio", 5),
    JUNHO("Junho", 6),
    JULHO("Julho", 7),
    AGOSTO("Agosto", 8),
    SETEMBRO("Setembro", 9),
    OUTUBRO("Outubro", 10),
    NOVEMBRO("Novembro", 11),
    DEZEMBRO("Dezembro", 12);

    private String descricao;
    private Integer numero;

    Mes(String descricao, Integer mes) {
        this.descricao = descricao;
        this.numero = mes;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getNumero() {
        return numero;
    }

    public String getNumeroComZero() {
        if (numero < 10) {
            return "0" + numero;
        }
        return String.valueOf(numero);
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
