package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum Sexo {

        MASCULINO("1", "Masculino", "M"),
        FEMININO("2", "Feminino", "F");

        private String codigo;
        private String descricao;
        private String sigla;

        Sexo(String codigo, String descricao, String sigla) {
            this.codigo = codigo;
            this.descricao = descricao;
            this.sigla = sigla;
        }

        public String getCodigo() {
            return codigo;
        }

        public String getDescricao() {
            return descricao;
        }

        public String getSigla() {
            return sigla;
        }


        @Override
        public String toString() {
            return descricao;
        }
}
