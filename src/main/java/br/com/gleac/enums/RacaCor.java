package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum RacaCor {

    INDIGENA("1", "Indígena"),
    BRANCA("2", "Branca"),
    NEGRO("4", "Negro"),
    AMARELA("6", "Amarela"),
    PARDA("8", "Parda"),
    NAO_INFORMADA("9", "Não Informado");

    private String codigo;
    private String descricao;

    RacaCor(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
