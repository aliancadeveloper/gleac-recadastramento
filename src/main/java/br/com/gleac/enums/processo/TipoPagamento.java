package br.com.gleac.enums.processo;


public enum TipoPagamento {
    BOLETO("Boleto"),
    TRANSFERENCIA("Transferência");

    private String descricao;

    TipoPagamento(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
