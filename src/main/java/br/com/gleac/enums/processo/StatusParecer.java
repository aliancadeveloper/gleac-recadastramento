package br.com.gleac.enums.processo;

/**
 * Created by zaca.
 */
public enum StatusParecer {
    DEFERIDO("Deferido"),
    INDEFIRIDO("Indeferido"),
    RESSALVA("Ressalva");

    private String descricao;

    StatusParecer(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
