package br.com.gleac.enums.processo;

/**
 * Created by israeleriston on 26/06/17.
 */
public enum StatusProcesso {
    ABERTO("Em Aberto"),
    PARECER("Parecer deferido"),
    INDEFERIDO("Parecer indeferido"),
    AGUARDANDO_BOLETO("Aguardando pagamento do boleto"),
    PAGO("Pago"),
    FINALIZADO("Finalizado"),
    AGUARDANDO("Aguardando Definição"),
    CANCELADO("Cancelado");

    private String descricao;

    StatusProcesso(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
