package br.com.gleac.enums.processo;

/**
 * Created by israeleriston on 25/06/17.
 */
public enum TipoSolicitacao {
    PRODUTO("Produto"),
    SERVICO("Serviço");

    private String descricao;

    TipoSolicitacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
