/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gleac.enums;

/**
 *
 * @author Roma
 */
public enum TipoCriptografica {

    NENHUM("Nenhum"),
    SSL("SSL"),
    TSL("TSL");
    private String descricao;

    private TipoCriptografica(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
