package br.com.gleac.enums;

/**
 * Created by israeleriston on 04/09/16.
 */
public enum TipoInstituicao {
    POTENCIA("Instituição Potência"),
    JURIDICA("Instituição Júridica"),
    LOJA("Loja Maçônica");

    private String descricao;

    TipoInstituicao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
