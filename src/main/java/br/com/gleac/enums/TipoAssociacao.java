package br.com.gleac.enums;

/**
 * Created by israeleriston on 11/07/16.
 */
public enum TipoAssociacao {

    DEMOLEY("Ordem Demolay"),
    ABELHINHA("Abelhinha"),
    LOWTONS("Lowtons"),
    ESCUDEIRO("Escudeiro"),
    FILHASDEJO("Ordem Filhas de Jó");

    private String descricao;

    TipoAssociacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
