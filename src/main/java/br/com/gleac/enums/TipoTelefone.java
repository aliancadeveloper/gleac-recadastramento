package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum TipoTelefone {

    RESIDENCIAL("Residencial"),
    FAX("Fax"),
    FIXO("Fixo"),
    CELULAR("Celular"),
    OUTROS("Outros"),
    COMERCIAL("Comercial");

    private String tipoTelefone;

    TipoTelefone(String tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }

    public String getTipoTelefone() {
        return tipoTelefone;
    }


    @Override
    public String toString() {
        return tipoTelefone;
    }
}
