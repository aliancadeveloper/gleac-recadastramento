package br.com.gleac.enums;

/**
 * Created by israeleriston on 24/06/16.
 */
public enum SummaryMessages {

    CAMPO_OBRIGATORIO("Campo Obrigatório!"),
    OPERACAO_REALIZADA("Operação Realizada!"),
    OPERACAO_NAO_REALIZADA("Operação Não Realizada!"),
    OPERACAO_NAO_PERMITIDA("Operação Não Permitida!"),
    ATENCAO("Atenção!");

    private String descricao;

    private SummaryMessages(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}