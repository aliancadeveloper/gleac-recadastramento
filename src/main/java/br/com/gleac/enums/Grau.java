package br.com.gleac.enums;

/**
 * Created by israeleriston on 23/09/16.
 */
public enum Grau {

    APRENDIZ("Aprendiz"),
    COMPANHEIRO("Companheiro"),
    MESTRE("Mestre"),
    MESTRE_INSTALADO("Mestre Instalado"),
    GRAO_MESTRE("Grão-Mestre");


    private String descricao;

    Grau(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
