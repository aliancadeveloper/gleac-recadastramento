package br.com.gleac.enums;

/**
 * Created by renatoromanini on 02/10/16.
 */
public enum OperacaoCondicaoSql {
    IGUAL("Igual", "="),
    LIKE("Like", "like"),
    BETWEEN("Between","between");

    private String descricao;
    private String operacao;

    OperacaoCondicaoSql(String descricao, String operacao) {
        this.descricao = descricao;
        this.operacao = operacao;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
