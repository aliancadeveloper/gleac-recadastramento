package br.com.gleac.enums;

public enum TipoOperacao {
    DEBITO("Débito"),
    CREDITO("Crédito");
    private String descricao;

    private TipoOperacao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
