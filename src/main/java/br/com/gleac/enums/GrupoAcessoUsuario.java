/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.enums;

/**
 *
 * @author renato
 */
public enum GrupoAcessoUsuario {

    ADMINISTRADOR("Administrador"),
    USUARIO("Usuário"),
    FINANCEIRO("Financeiro"),
    MACON("Maçom");
    private String descricao;

    private GrupoAcessoUsuario(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
