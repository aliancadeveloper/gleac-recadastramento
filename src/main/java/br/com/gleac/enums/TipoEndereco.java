package br.com.gleac.enums;

/**
 * Created by israeleriston on 28/05/16.
 */
public enum TipoEndereco {

    RESIDENCIAL("Residencial"),
    COMERCIAL("Comercial"),
    COBRANCA("Cobrança"),
    CORRESPONDENCIA("Correspondência"),
    RURAL("Rural"),
    DOMICILIO_FISCAL("Domicílio Fiscal"),
    OUTROS("Outros");
    private String descricao;

    TipoEndereco(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
