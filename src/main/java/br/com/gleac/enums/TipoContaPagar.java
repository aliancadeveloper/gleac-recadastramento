package br.com.gleac.enums;

/**
 * Created by renatoromanini on 01/08/17.
 */
public enum TipoContaPagar {

    CLIENTE("Cliente"),
    PROCESSO("Processo"),
    TAXA("Taxa"),
    DESPESA_DIVERSA("Despesa diversas"),
    FORNECEDOR("Fornecedor"),
    FUNCIONARIO("Funcionário"),
    OUTROS("Outros");

    private String descricao;

    TipoContaPagar(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isDespesaDiversas() {
        return TipoContaPagar.DESPESA_DIVERSA.equals(this);
    }

    public boolean isTaxa() {
        return TipoContaPagar.TAXA.equals(this);
    }

    @Override
    public String toString() {
        return descricao;
    }
}
