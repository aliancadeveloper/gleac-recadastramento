package br.com.gleac.enums;

/**
 * Created by renatoromanini on 01/04/2019.
 */
public enum FormaPagamentoTaxa {
    AVISTA_COM_DESCONTO("à vista e com desconto"),
    AVISTA_SEM_DESCONTO("à vista e sem desconto"),
    PARCELADO("Parcelado"),
    TODOS("Todas");

    private String descricao;

    FormaPagamentoTaxa(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}


