package br.com.gleac.enums;

/**
 * Created by israeleriston on 12/07/16.
 */
public enum TipoCargoExercido {

        GRANDE_LOJA("Exercido na Grande Loja"),
        LOJA("Exercido em Loja");

    private String descricao;

    TipoCargoExercido(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
