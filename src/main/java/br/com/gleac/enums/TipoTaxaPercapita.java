package br.com.gleac.enums;

/**
 * Created by renatoromanini on 12/01/2019.
 */
public enum TipoTaxaPercapita {
    POR_MACOM("Por Maçom"),
    POR_LOJA("Por Loja");

    private String descricao;

    TipoTaxaPercapita(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}


