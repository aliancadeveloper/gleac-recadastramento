package br.com.gleac.enums;

public enum TipoMovimentoConciliacao {

    RECEITA("Receita"),
    DESPESA("Despesa");
    private String descricao;

    TipoMovimentoConciliacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }

    public boolean isReceita() {
        return TipoMovimentoConciliacao.RECEITA.equals(this);
    }

    public boolean isDespesa() {
        return TipoMovimentoConciliacao.DESPESA.equals(this);
    }
}
