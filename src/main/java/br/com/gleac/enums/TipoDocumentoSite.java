package br.com.gleac.enums;

/**
 * Created by carlos on 28/09/16.
 */
public enum TipoDocumentoSite {
    ATOS("Atos"),
    DECRETOS("Decretos"),
    BOLETINS("Boletins"),
    BALANCETES("Balancetes"),
    EMOLUMENTOS("Emolumentos"),
    TABELAS("Tabelas"),
    EDITAIS("Editais");

    private String descricao;

    TipoDocumentoSite(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
