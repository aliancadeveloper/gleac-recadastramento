package br.com.gleac.enums;

/**
 * Created by renatoromanini on 26/05/16.
 */
public enum TagDocumentoManual {

    LOGO("Logo");

    private String descricao;
    private TipoModeloDocumento tipoModeloDocumento;

    TagDocumentoManual(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


    @Override
    public String toString() {
        return descricao;
    }
}
