package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class ContaCorrenteBancariaRepository extends AbstractRepository<ContaCorrenteBancaria> {

    @PersistenceContext
    private EntityManager em;


    public ContaCorrenteBancariaRepository() {
        super(ContaCorrenteBancaria.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    public String complementarCriacaoObjeto(String q) {
        q += " new ContaCorrenteBancaria(";
        q += "obj.id,  obj.numero, obj.agencia, obj.nome ";
        q += ") ";
        return q;
    }

    @Override
    @Transactional
    public ContaCorrenteBancaria recuperar(Class entidade, Object id) {
        ContaCorrenteBancaria conta = (ContaCorrenteBancaria) em.find(entidade, id);
        conta.getContas().size();
        return conta;
    }
}
