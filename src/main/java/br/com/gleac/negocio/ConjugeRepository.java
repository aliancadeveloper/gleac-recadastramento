package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Conjuge;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 19/07/16.
 */
@Repository
public class ConjugeRepository extends AbstractRepository<Conjuge> {

    @PersistenceContext
    private EntityManager em;

    public ConjugeRepository() {
        super(Conjuge.class);
    }


    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public Conjuge recuperar(Class entidade, Object id) {
        Conjuge con = (Conjuge) em.find(entidade, id);
        return con;
    }

    @Transactional
    public List<Conjuge> recuperarConjugesByNome(String filter) {
        String sql = " select conj.* from conjuge conj " +
                " inner join pessoafisica pf on conj.pessoafisica_id = pf.id " +
                " where lower(pf.nome) like :filter ";

        Query q = em.createNativeQuery(sql, Conjuge.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }


}
