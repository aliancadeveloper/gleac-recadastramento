package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Formulario;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.exception.FormularioNotFoundException;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca.
 */
@Repository
public class FormularioRepository extends AbstractRepository<Formulario>{

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public FormularioRepository() {
        super(Formulario.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<Formulario> completarFormularioByDescricao(String filtro) {
        String sql = "select form.* from formulario form " +
                "where form.descricao like :filtro";

        Query q = em.createNativeQuery(sql, Formulario.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Formulario> buscarTodosFormulariosByServico(Servico servico, Date data) {
        String sql = " select form.* from configuracaoservico configServ " +
                " inner join configuracaoservicoformulario configForm on configServ.id = configForm.configuracaoservico_id" +
                " inner join servico serv on configServ.servico_id = serv.id" +
                " inner join formulario form on configForm.formulario_id = form.id " +
                " where serv.id = :idServico " +
            " and to_date(:dataVigente,'dd/MM/yyyy') between configServ.dataInicial and coalesce(configServ.dataFinal,to_date(:dataVigente,'dd/MM/yyyy'))";

        Query q = em.createNativeQuery(sql, Formulario.class);
        q.setParameter("idServico", servico.getId());
        q.setParameter("dataVigente", Util.sdf.format(data));
        if (q.getResultList().isEmpty()) {
            throw new FormularioNotFoundException("Não foi possível encontrar formulários cadastrado para esse serviço.");
        }
        return q.getResultList();
    }
}
