package br.com.gleac.negocio;

import br.com.gleac.entidade.seguranca.GrupoAcesso;
import br.com.gleac.entidade.seguranca.GrupoUsuario;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.GrupoAcessoUsuario;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by israeleriston on 11/05/16.
 */
@Repository
@Transactional
public class GrupoAcessoRepository extends AbstractRepository<GrupoAcesso>{

    @PersistenceContext
    private EntityManager em;

    public GrupoAcessoRepository() {
        super(GrupoAcesso.class);
    }



    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Override
    public GrupoAcesso recuperar(Class entidade, Object id) {
        GrupoAcesso retorno = (GrupoAcesso) em.find(entidade, id);
        retorno.getRecursos().size();
        return retorno;
    }

    @Transactional
    public GrupoAcesso recuperarPorTipo(GrupoAcessoUsuario grupoAcessoUsuario) {
        String sql ="select g.* from grupoacesso g where g.grupoAcessoUsuario = :tipo";
        Query q = em.createNativeQuery(sql, GrupoAcesso.class);
        q.setParameter("tipo", grupoAcessoUsuario.name());
        q.setMaxResults(1);
        return (GrupoAcesso) q.getSingleResult();
    }

    public List<GrupoAcesso> buscarGrupoAcessoPorUsuario(Usuario usu) {
        String sql = "select ga.* from usuario usu " +
            "inner join grupousuariosistema gus on gus.usuario_id = usu.id " +
            "inner join grupousuario gu on gu.id = gus.grupousuario_id" +
            " inner join grupoacesso ga on ga.grupousuario_id = gu.id " +
            "where usu.id = :usuarioID ";

        Query q = em.createNativeQuery(sql, GrupoUsuario.class);
        q.setParameter("usuarioID", usu.getId());

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }

    }
}
