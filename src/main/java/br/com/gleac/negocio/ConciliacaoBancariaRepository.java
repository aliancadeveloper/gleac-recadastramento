package br.com.gleac.negocio;

import br.com.gleac.entidade.auxiliares.MovimentoConciliacaoBancaria;
import br.com.gleac.entidade.financeiro.*;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.enums.TipoMovimentoConciliacao;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Repository
public class ConciliacaoBancariaRepository extends AbstractRepository<ConciliacaoBancaria> {

    @PersistenceContext
    private EntityManager em;

    public ConciliacaoBancariaRepository() {
        super(ConciliacaoBancaria.class);
    }

    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private ContaCorrenteRepository contaCorrenteRepository;
    @Autowired
    private SaldoBancarioRepository saldoBancarioRepository;
    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private IdentificadorRepository identificadorRepository;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    public SistemaService getSistemaService() {
        return sistemaService;
    }

    public ContaCorrenteRepository getContaCorrenteRepository() {
        return contaCorrenteRepository;
    }

    public SaldoBancarioRepository getSaldoBancarioRepository() {
        return saldoBancarioRepository;
    }

    public List<MovimentoConciliacaoBancaria> buscarMovimentosConciliacao(ContaCorrenteBancaria contaCorrente, Date dataLancamento) {
        String sql = " select idMovimento, datalancamento, dataConciliacao, credito, debito, tipoMovimento, tipoLancamento, identificador " +
            "from (select rec.id                 as idMovimento, " +
            "             rec.datalancamento     as datalancamento, " +
            "             rec.dataconciliacao    as dataConciliacao, " +
            "             coalesce(rec.valor, 0) as credito, " +
            "             0                      as debito, " +
            "             'RECEITA'              as tipoMovimento, " +
            "             rec.formadepagamento   as tipoLancamento, " +
            "             rec.identificador_id   as identificador " +
            "      from receita rec " +
            "             inner join contacorrente conta on rec.contacorrente_id = conta.id " +
            "             inner join contacorrentebancaria cc on conta.contacorrentebancaria_id = cc.id" +
            "             inner join lojaconta l on conta.id = l.contacorrente_id " +
            "             inner join loja on l.loja_id = loja.id " +
            "      where rec.datalancamento <= to_date(:dataLancamento, 'dd/MM/yyyy') " +
            "        and cc.id = :idContaCorrente " +
            "      union all " +
            "      select pag.id                 as idMovimento, " +
            "             pag.datalancamento     as datalancamento, " +
            "             pag.dataconciliacao    as dataConciliacao, " +
            "             0                      as creito, " +
            "             coalesce(pag.valor, 0) as debito, " +
            "             'DESPESA'              as tipoMovimento, " +
            "             pag.formadepagamento   as tipoLancamento, " +
            "             pag.identificador_id   as identificador " +
            "      from pagamento pag " +
            "             inner join contacorrente conta on pag.contacorrente_id = conta.id " +
            "             inner join contacorrentebancaria cc on conta.contacorrentebancaria_id = cc.id" +
            "             inner join lojaconta l on conta.id = l.contacorrente_id " +
            "             inner join loja on l.loja_id = loja.id " +
            "      where pag.datalancamento <= to_date(:dataLancamento, 'dd/MM/yyyy') " +
            "        and cc.id = :idContaCorrente ) dados " +
            "order by dataLancamento";
        Query consulta = em.createNativeQuery(sql);
        consulta.setParameter("idContaCorrente", contaCorrente.getId());
        consulta.setParameter("dataLancamento", Util.sdf.format(dataLancamento));
        List<MovimentoConciliacaoBancaria> retorno = Lists.newArrayList();

        List<Object[]> resultado = consulta.getResultList();
        if (!resultado.isEmpty()) {
            for (Object[] obj : resultado) {
                MovimentoConciliacaoBancaria movimento = preencherMovimentoConciliacaoBancaria(obj);
                Util.adicionarObjetoEmLista(retorno, movimento);
            }
        }
        return retorno;
    }

    private MovimentoConciliacaoBancaria preencherMovimentoConciliacaoBancaria(Object[] obj) {
        MovimentoConciliacaoBancaria movimento = new MovimentoConciliacaoBancaria();
        movimento.setIdMovimento(((BigDecimal) obj[0]).longValue());
        movimento.setDataLancamento((Date) obj[1]);
        movimento.setDataConciliacao((Date) obj[2]);
        movimento.setCredito((BigDecimal) obj[3]);
        movimento.setDebito((BigDecimal) obj[4]);
        movimento.setTipoMovimentoConciliacao(obj[5] != null ? TipoMovimentoConciliacao.valueOf((String) obj[5]) : null);
        if (movimento.getTipoMovimentoConciliacao().isReceita()) {
            movimento.setTipoLancamento(obj[6] != null ? FormaDePagamento.valueOf((String) obj[6]).getDescricao() : null);
        } else {
            movimento.setTipoLancamento(obj[6] != null ? FormaDePagamento.valueOf((String) obj[6]).getDescricao() : null);
        }
        if (obj[7] != null) {
            Identificador identificador = identificadorRepository.recuperar(Identificador.class, ((Number) obj[7]).longValue());
            movimento.setIdentificador(identificador);
        }
        return movimento;
    }

    @Transactional
    public void concluirConciliacao(ConciliacaoBancaria entity, List<MovimentoConciliacaoBancaria> movimentosConciliacao) {
        try {
            conciliar(entity, movimentosConciliacao);
            salvar(entity);
        } catch (Exception ex) {
            throw new ExceptionGenerica("Erro ao conciliar " + ex.getMessage());
        }
    }

    @Transactional
    public void conciliar(ConciliacaoBancaria entity, List<MovimentoConciliacaoBancaria> movimentosConciliacao) {
        for (MovimentoConciliacaoBancaria movimento : movimentosConciliacao) {
            if (movimento.getTipoMovimentoConciliacao() == null) {
                throw new CampoObrigatorioException("Tipo de Movimento da Conciliação não recuperado para conciliar.");
            }
            switch (movimento.getTipoMovimentoConciliacao()) {
                case RECEITA:
                    Receita receita = receitaRepository.recuperarSemDependencia(movimento.getIdMovimento());
                    receita.setIdentificador(movimento.getIdentificador());
                    receitaRepository.conciliar(receita, entity.getDataConciliacao());
                    break;
                case DESPESA:
                    Pagamento despesa = pagamentoRepository.recuperarSemDependencia(movimento.getIdMovimento());
                    despesa.setIdentificador(movimento.getIdentificador());
                    pagamentoRepository.conciliar(despesa, entity.getDataConciliacao());
                    break;
            }
        }
    }

    public PagamentoRepository getPagamentoRepository() {
        return pagamentoRepository;
    }

    public ReceitaRepository getReceitaRepository() {
        return receitaRepository;
    }

    public IdentificadorRepository getIdentificadorRepository() {
        return identificadorRepository;
    }
}
