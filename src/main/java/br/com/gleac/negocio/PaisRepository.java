package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Pais;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@Repository
public class PaisRepository extends AbstractRepository<Pais> {

    @PersistenceContext
    private EntityManager em;

    public PaisRepository() {
        super(Pais.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Pais> buscarPaisPorNome(String filter){
        String hql = " select obj from Pais obj where lower(obj.nome) like :filter ";

        Query q = em.createQuery(hql);

        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);

        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }


}
