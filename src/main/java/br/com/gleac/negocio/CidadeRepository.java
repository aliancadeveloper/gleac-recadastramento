package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Cidade;
import br.com.gleac.entidade.comum.Estado;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by israeleriston on 24/06/16.
 */
@Repository
public class CidadeRepository extends AbstractRepository<Cidade> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

//    @Inject
//    public CidadeJPARepository jpaRepository;
//


    public CidadeRepository() {
        super(Cidade.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    public List<Cidade> completarCidadeByNome(String filter, Estado estado) {
        String sql = " SELECT distinct c.* from cidade c where lower(c.nome) like :nome";
        if (estado != null) {
            sql += " and c.estado_id = :estado";
        }

        Query q = em.createNativeQuery(sql, Cidade.class);
        q.setParameter("nome", "%" + filter.trim().toLowerCase() + "%");
        if (estado != null) {
            q.setParameter("estado", estado.getId());
        }
        q.setMaxResults(10);
        return q.getResultList().isEmpty() ? Lists.newArrayList() : q.getResultList();
    }
}
