package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.comum.PessoaJuridica;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by renatoromanini on 02/08/17.
 */
@Repository
public class PessoaRepository extends AbstractRepository<Pessoa> {

    @PersistenceContext
    private EntityManager em;

    public PessoaRepository() {
        super(Pessoa.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public Pessoa recuperar(Class entidade, Object id) {
        Pessoa retorno = (Pessoa) em.find(entidade, id);
        retorno.getTelefones().size();
        retorno.getEnderecos().size();
        retorno.getContas().size();
        return retorno;
    }

    @Transactional
    public List<Pessoa> buscarPessoa(String param, Boolean isComum) {
        List<Pessoa> result = Lists.newArrayList();
        String sqlPf = " " +
            "SELECT DISTINCT pf.*, p.* " +
            " FROM pessoa p " +
            "  INNER JOIN pessoafisica pf ON pf.id = p.id ";
        if (!isComum) {
            sqlPf += " inner join macon mac on pf.id = mac.pessoafisica_id ";
        }
        sqlPf += "      WHERE (UPPER (pf.nome) LIKE :param " +
            "           OR replace(replace(pf.cpf,'.',''),'-','') LIKE :param " +
            "           OR pf.cpf LIKE :param)";
        Query queryPf = getEntityManager().createNativeQuery(sqlPf, PessoaFisica.class);
        queryPf.setParameter("param", "%" + param.toUpperCase() + "%");
        result.addAll(queryPf.getResultList());

        String sqlPj = " " +
            "SELECT DISTINCT pj.*, p.* " +
            " FROM pessoa p " +
            "  INNER JOIN pessoajuridica pj ON pj.id = p.id " +
            "      WHERE (UPPER(pj.nomeFantasia) LIKE :param " +
            "           OR UPPER(pj.razaoSocial) LIKE :param " +
            "           OR replace(replace(replace(pj.cnpj,'.',''),'/',''),'-','') LIKE :param " +
            "           OR pj.cnpj LIKE :param) ";
        Query queryPj = getEntityManager().createNativeQuery(sqlPj, PessoaJuridica.class);
        queryPj.setParameter("param", "%" + param.toUpperCase() + "%");
        result.addAll(queryPj.getResultList());
        return result;
    }

}
