package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.Identificador;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class IdentificadorRepository extends AbstractRepository<Identificador> {

    @PersistenceContext
    private EntityManager em;


    public IdentificadorRepository() {
        super(Identificador.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
