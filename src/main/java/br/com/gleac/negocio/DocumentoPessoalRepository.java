package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.DocumentoPessoal;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by israeleriston on 07/09/16.
 */
@Repository
public class DocumentoPessoalRepository extends AbstractRepository<DocumentoPessoal> {

    @PersistenceContext
    private EntityManager em;

    public DocumentoPessoalRepository() {
        super(DocumentoPessoal.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
