package br.com.gleac.negocio;


import br.com.gleac.entidade.estoque.SaidaEstoque;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

@Repository
public class SaidaEstoqueRepository extends AbstractRepository<SaidaEstoque> {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private SaldoEstoqueRepository saldoEstoqueRepository;
    @Autowired
    private ProdutoRepository produtoRepository;


    public SaidaEstoqueRepository() {
        super(SaidaEstoque.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    @Transactional
    public SaidaEstoque recuperar(Class entidade, Object id) {
        SaidaEstoque saidaEstoque = em.find(SaidaEstoque.class, id);
        saidaEstoque.getItens().size();
        return saidaEstoque;
    }


    @Transactional
    public void salvarNovo(SaidaEstoque entity) {
        try {
            preSaveOrUpdate((SaidaEstoque) entity);
            getEntityManager().persist(entity);
            saldoEstoqueRepository.gerarSaldoEstoqueSaida(entity);
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional
    public void salvar(SaidaEstoque entity) {
        try {
            preSaveOrUpdate((SaidaEstoque) entity);
            entity = em.merge(entity);
        } catch (PersistenceException e) {
            getLogger().error("Erro ao atualizar o registro " + e.getMessage());
        }
    }

    public ProdutoRepository getProdutoRepository() {
        return produtoRepository;
    }
}
