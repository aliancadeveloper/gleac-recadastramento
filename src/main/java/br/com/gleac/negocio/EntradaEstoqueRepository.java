package br.com.gleac.negocio;


import br.com.gleac.entidade.estoque.EntradaEstoque;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class EntradaEstoqueRepository extends AbstractRepository<EntradaEstoque> {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private SaldoEstoqueRepository saldoEstoqueRepository;
    @Autowired
    private ProdutoRepository produtoRepository;


    public EntradaEstoqueRepository() {
        super(EntradaEstoque.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    @Transactional
    public EntradaEstoque recuperar(Class entidade, Object id) {
        EntradaEstoque entradaEstoque = em.find(EntradaEstoque.class, id);
        entradaEstoque.getItens().size();
        return entradaEstoque;
    }


    @Transactional
    public void salvarNovo(EntradaEstoque entity) {
        try {
            preSaveOrUpdate((EntradaEstoque) entity);
            getEntityManager().persist(entity);
            saldoEstoqueRepository.gerarSaldoEstoqueEntrada(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public ProdutoRepository getProdutoRepository() {
        return produtoRepository;
    }
}
