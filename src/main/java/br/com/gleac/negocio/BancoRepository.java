package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.Banco;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class BancoRepository extends AbstractRepository<Banco> {

    @PersistenceContext
    private EntityManager em;


    public BancoRepository() {
        super(Banco.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


}
