package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.financeiro.Receita;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;


@Repository
public class ReceitaRepository extends AbstractRepository<Receita> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private LojaRepository lojaRepository;

    public ReceitaRepository() {
        super(Receita.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public Receita recuperarSemDependencia(Object id) {
        return getEntityManager().find(Receita.class, id);
    }

    @Transactional
    public void salvarNovo(Receita entity) {
        try {
            preSaveOrUpdate((Receita) entity);
            getEntityManager().persist(entity);
            if (entity.getContasReceber() != null) {
                entity.getContasReceber().setSituacaoContaPagar(SituacaoContaPagar.BAIXADA);
                contasReceberRepository.salvar(entity.getContasReceber());
            }
            lancamentoFinanceiroRepository.gerarSaldo(entity);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @Transactional
    public BigDecimal recuperarSoma(Integer ano, String mes, Date data, Loja loja) {
        try {
            String sql = "select coalesce(sum(valor),0) from Receita where 1=1";
            if (ano != null) {
                sql += " and to_char(dataLancamento,'YYYY') = :ano";
            }

            if (data != null) {
                sql += " and dataLancamento = to_date(:data,'dd/MM/yyyy')";
            }

            if (loja != null) {
                sql += " and loja_id = :loja";
            }
            if (mes != null) {
                sql += " and to_char(dataLancamento,'MM') = :mes ";
            }

            Query q = em.createNativeQuery(sql);
            if (mes != null) {
                q.setParameter("mes", mes);
            }
            if (data != null) {
                q.setParameter("data", Util.sdf.format(data));
            }
            if (ano != null) {
                q.setParameter("ano", String.valueOf(ano));
            }

            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }


            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    @Transactional
    public BigDecimal recuperarSomaPorData(Date dataReferencia, Loja loja) {
        try {
            String sql = "select coalesce(sum(valor),0) from Receita " +
                " where datalancamento <= to_date(:dataReferencia,'dd/MM/yyyy') ";
            if (loja != null) {
                sql += " and loja_id = :loja";
            }

            Query q = em.createNativeQuery(sql);
            q.setParameter("dataReferencia", Util.sdf.format(dataReferencia));
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }
            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    @Transactional
    public void gerarReceitaPorProcesso(ProcessoFinanceiro selecionado) {
        Loja loja = lojaRepository.recuperar(Loja.class, selecionado.getSolicitacaoProcesso().getDepartamento().getLoja().getId());
        Receita receita = new Receita();
        receita.setLoja(loja);
        if (loja.getContas() != null && !loja.getContas().isEmpty()) {
            receita.setContaCorrente(loja.getContas().get(0).getContaCorrente());
        }
        receita.setValor(selecionado.getBoleto().getValorTotal());
        receita.setDataLancamento(selecionado.getGeradoEm());
        receita.setFormaDePagamento(FormaDePagamento.BOLETO);
        receita.setOrigem("Boleto pago do processo " + selecionado.getSolicitacaoProcesso().getNumero() + ".");
        receita.setContasReceber(selecionado.getBoleto().getContasReceber());
        receita.getContasReceber().setSituacaoContaPagar(SituacaoContaPagar.PAGA);
        em.merge(receita.getContasReceber());
        em.persist(receita);
    }

    @Transactional
    public void gerarReceitaPorProcesso(ContasReceber selecionado) {
        Loja loja = lojaRepository.recuperar(Loja.class, selecionado.getLoja().getId());
        Receita receita = new Receita();
        receita.setLoja(loja);
        if (loja.getContas() != null && !loja.getContas().isEmpty()) {
            receita.setContaCorrente(loja.getContas().get(0).getContaCorrente());
        }
        receita.setValor(selecionado.getValor());
        receita.setDataLancamento(selecionado.getBoleto().getDataTransacao());
        receita.setFormaDePagamento(FormaDePagamento.BOLETO);
        receita.setOrigem("Boleto pago da conta a recerber " + selecionado.toString() + ".");
        receita.setContasReceber(selecionado);
        receita.getContasReceber().setSituacaoContaPagar(SituacaoContaPagar.PAGA);
        em.merge(receita.getContasReceber());
        em.persist(receita);
    }

    @Transactional
    public Receita conciliar(Receita entity, Date dataConciliacao) {
        entity.setDataConciliacao(dataConciliacao);
        entity = em.merge(entity);
        return entity;
    }

    @Transactional
    public Receita estornarConciliacao(Receita entity) {
        entity.setDataConciliacao(null);
        entity = em.merge(entity);
        return entity;
    }
}
