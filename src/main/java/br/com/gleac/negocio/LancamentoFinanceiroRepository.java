package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.entidade.financeiro.Pagamento;
import br.com.gleac.entidade.financeiro.Receita;
import br.com.gleac.entidade.financeiro.SaldoFinanceiro;
import br.com.gleac.enums.TipoOperacao;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.interfaces.ISaldoFinanceiro;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.*;


@Repository
public class LancamentoFinanceiroRepository {

    @PersistenceContext
    private EntityManager em;


    public LancamentoFinanceiroRepository() {

    }

    @Transactional
    public void gerarSaldo(ISaldoFinanceiro iSaldoFinanceiro) {
        try {

            SaldoFinanceiro movimento = new SaldoFinanceiro(iSaldoFinanceiro);
            Util.validarCampos(movimento);
            SaldoFinanceiro ultimoSaldo = getUltimoSaldoPorDataLojaConta(movimento);

            if (validarSaldoDisponivel(iSaldoFinanceiro, ultimoSaldo)) {
                if (ultimoSaldo == null || ultimoSaldo.getId() == null) {
                    ultimoSaldo = definirValorSaldoSubConta(movimento);
                    ultimoSaldo = alterarValorSaldo(ultimoSaldo, iSaldoFinanceiro.getTipoOperacao(), iSaldoFinanceiro.getValor());
                    em.persist(ultimoSaldo);
                } else {
                    if (Util.dataSemHorario(ultimoSaldo.getData()).compareTo(Util.dataSemHorario(movimento.getData())) == 0) {
                        ultimoSaldo = alterarValorSaldo(ultimoSaldo, iSaldoFinanceiro.getTipoOperacao(), iSaldoFinanceiro.getValor());
                        em.merge(ultimoSaldo);
                    } else {
                        SaldoFinanceiro novoSaldo = definirValorSaldoSubConta(movimento);
                        novoSaldo = copiarValoresDoSaldoRecuperado(ultimoSaldo, novoSaldo);
                        novoSaldo = alterarValorSaldo(novoSaldo, iSaldoFinanceiro.getTipoOperacao(), iSaldoFinanceiro.getValor());
                        em.persist(novoSaldo);
                    }
                }
                gerarSaldoPosteriores(iSaldoFinanceiro);
            }
        } catch (OptimisticLockException optex) {
            throw new ExceptionGenerica("Saldo está sendo movimentado por outro usuário. Por favor, tente salvar o movimento em alguns instantes.");
        } catch (Exception ex) {
            throw ex;
        }
    }

    private void gerarSaldoPosteriores(ISaldoFinanceiro saldo) {
        List<SaldoFinanceiro> saldosPosteriores = getSaldoFuturoPorDataLojaConta(new SaldoFinanceiro(saldo));
        if (!saldosPosteriores.isEmpty()) {
            for (SaldoFinanceiro saldoParaAtualizar : saldosPosteriores) {
                saldoParaAtualizar = alterarValorSaldo(saldoParaAtualizar, saldo.getTipoOperacao(), saldo.getValor());
                BigDecimal saldoDoDia = saldoParaAtualizar.getSaldo().subtract(saldoParaAtualizar.getSaldo());

                if (saldoDoDia.compareTo(BigDecimal.ZERO) < 0) {
                    throw new ExceptionGenerica("O Saldo ficará negativo em <b> " + Util.getValorFormatado(saldoDoDia) + "</b>, na data " + Util.sdf.format(saldoParaAtualizar.getData()) + " para a Conta  " + saldoParaAtualizar.getContaCorrente() + ".");
                }

                em.merge(saldoParaAtualizar);
            }
        }
    }

    private SaldoFinanceiro copiarValoresDoSaldoRecuperado(SaldoFinanceiro saldo, SaldoFinanceiro novoSaldo) {
        try {
            novoSaldo.setSaldo(saldo.getSaldo());
            return novoSaldo;
        } catch (Exception ex) {
            throw new ExceptionGenerica(ex.getMessage());
        }
    }

    private SaldoFinanceiro definirValorSaldoSubConta(SaldoFinanceiro mov) {
        try {
            SaldoFinanceiro saldo = new SaldoFinanceiro();
            saldo.setData(mov.getData());
            saldo.setContaCorrente(mov.getContaCorrente());
            saldo.setLoja(mov.getLoja());
            saldo.setSaldo(BigDecimal.ZERO);
            return saldo;
        } catch (Exception ex) {
            throw new ExceptionGenerica(ex.getMessage());
        }
    }

    private SaldoFinanceiro alterarValorSaldo(SaldoFinanceiro saldo, TipoOperacao operacao, BigDecimal valor) {
        try {
            if (operacao.equals(TipoOperacao.CREDITO)) {
                saldo.setSaldo(saldo.getSaldo().add(valor));
            } else {
                saldo.setSaldo(saldo.getSaldo().subtract(valor));
            }
            return saldo;
        } catch (Exception ex) {
            throw new ExceptionGenerica(ex.getMessage());
        }
    }


    private Boolean validarSaldoDisponivel(ISaldoFinanceiro mov, SaldoFinanceiro ultimoSaldo) {

        if (TipoOperacao.DEBITO.equals(mov.getTipoOperacao())) {
            if (ultimoSaldo.getSaldo().compareTo(mov.getValor()) < 0) {
                throw new ExceptionGenerica(" O Saldo disponível de " + "<b>" + Util.getValorFormatado(ultimoSaldo.getSaldo()) + "</b>" +
                    " na Conta : " + mov.getContaCorrente() +
                    " é insuficiente para realizar a operação no valor de " + "<b>" + Util.getValorFormatado(mov.getValor()) + "</b>");
            }
        }
        return true;
    }

    @Transactional
    public List<SaldoFinanceiro> getSaldoFuturoPorDataLojaConta(SaldoFinanceiro movimento) {

        String sql = "select saldo.* from SaldoFinanceiro saldo "
            + " where saldo.data > to_date(:data, 'dd/MM/yyyy') "
            + " and saldo.contacorrente_id = :conta "
            + " and saldo.loja_id = :loja"
            + " order by saldo.data desc";

        Query q = em.createNativeQuery(sql, SaldoFinanceiro.class);
        q.setParameter("data", Util.sdf.format(movimento.getData()));
        q.setParameter("conta", movimento.getContaCorrente().getId());
        q.setParameter("loja", movimento.getLoja().getId());
        if (q.getResultList().isEmpty()) {
            return new ArrayList<>();
        } else {
            return q.getResultList();
        }
    }

    @Transactional
    public SaldoFinanceiro getUltimoSaldoPorDataLojaConta(SaldoFinanceiro movimento) {

        String sql = "select saldo.* from SaldoFinanceiro saldo "
            + " where saldo.data <= to_date(:data, 'dd/MM/yyyy')";
        if (movimento.getContaCorrente() != null) {
            sql += " and saldo.contacorrente_id = :conta ";
        }
        if (movimento.getLoja() != null) {
            sql += " and saldo.loja_id = :loja";
        }
        sql += " order by saldo.data desc";

        Query q = em.createNativeQuery(sql, SaldoFinanceiro.class);
        q.setParameter("data", Util.sdf.format(movimento.getData()));
        if (movimento.getContaCorrente() != null) {
            q.setParameter("conta", movimento.getContaCorrente().getId());
        }
        if (movimento.getLoja() != null) {
            q.setParameter("loja", movimento.getLoja().getId());
        }
        try {
            q.setMaxResults(1);
            return (SaldoFinanceiro) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public SaldoFinanceiro getSaldoNaData(SaldoFinanceiro movimento) {

        String sql = "select saldo.* from SaldoFinanceiro saldo "
            + " where saldo.data = to_date(:data, 'dd/MM/yyyy')";
        if (movimento.getContaCorrente() != null) {
            sql += " and saldo.contacorrente_id = :conta ";
        }
        if (movimento.getLoja() != null) {
            sql += " and saldo.loja_id = :loja";
        }
        sql += " order by saldo.data desc";

        Query q = em.createNativeQuery(sql, SaldoFinanceiro.class);
        q.setParameter("data", Util.sdf.format(movimento.getData()));
        if (movimento.getContaCorrente() != null) {
            q.setParameter("conta", movimento.getContaCorrente().getId());
        }
        if (movimento.getLoja() != null) {
            q.setParameter("loja", movimento.getLoja().getId());
        }
        try {
            q.setMaxResults(1);
            return (SaldoFinanceiro) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public List<ISaldoFinanceiro> recuperarLancamentos(Loja loja, ContaCorrente contaCorrente, Date dataInicial, Date dataFinal) {
        List<ISaldoFinanceiro> retorno = Lists.newArrayList();
        for (Pagamento pagamento : recuperarLancamentosDespesa(loja, contaCorrente, dataInicial, dataFinal)) {
            retorno.add(pagamento);
        }

        for (Receita receita : recuperarLancamentosReceita(loja, contaCorrente, dataInicial, dataFinal)) {
            retorno.add(receita);
        }
        Collections.sort(retorno, new Comparator<ISaldoFinanceiro>() {
            @Override
            public int compare(ISaldoFinanceiro o1, ISaldoFinanceiro o2) {
                return o1.getDataLancamento().compareTo(o2.getDataLancamento());
            }
        });
        return retorno;
    }

    @Transactional
    private List<Receita> recuperarLancamentosReceita(Loja loja, ContaCorrente contaCorrente, Date dataInicial, Date dataFinal) {
        String sql = " select rec.* from receita rec " +
            " inner join contacorrente conta on rec.contacorrente_id = conta.id " +
            " inner join lojaconta l on conta.id = l.contacorrente_id" +
            " inner join loja on l.loja_id = loja.id" +
            " where 1=1";
        if (dataInicial != null && dataInicial != null) {
            sql += " and (rec.datalancamento >= to_date(:dataInicial, 'dd/MM/yyyy')" +
                " and rec.datalancamento <= to_date(:dataFinal, 'dd/MM/yyyy')) ";
        }
        if (contaCorrente != null) {
            sql += " and conta.id = :conta ";
        }
        if (loja != null) {
            sql += " and loja.id = :loja ";

        }
        sql += " order by rec.datalancamento desc, rec.id desc";
        Query q = em.createNativeQuery(sql, Receita.class);
        if (dataInicial != null && dataInicial != null) {
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
        }
        if (contaCorrente != null) {
            q.setParameter("conta", contaCorrente.getId());
        }
        if (loja != null) {
            q.setParameter("loja", loja.getId());
        }
        try {
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    private List<Pagamento> recuperarLancamentosDespesa(Loja loja, ContaCorrente contaCorrente, Date dataInicial, Date dataFinal) {
        String sql = " select pag.* from pagamento pag " +
            " inner join contacorrente conta on pag.contacorrente_id = conta.id " +
            " inner join lojaconta l on conta.id = l.contacorrente_id" +
            " inner join loja on l.loja_id = loja.id" +
            " where 1=1";
        if (dataInicial != null && dataInicial != null) {
            sql += " and (pag.datalancamento >= to_date(:dataInicial, 'dd/MM/yyyy')" +
                " and pag.datalancamento <= to_date(:dataFinal, 'dd/MM/yyyy')) ";
        }
        if (contaCorrente != null) {
            sql += " and conta.id = :conta ";
        }
        if (loja != null) {
            sql += " and loja.id = :loja ";

        }
        sql += " order by pag.datalancamento desc, pag.id desc";
        Query q = em.createNativeQuery(sql, Pagamento.class);
        if (dataInicial != null && dataInicial != null) {
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
        }
        if (contaCorrente != null) {
            q.setParameter("conta", contaCorrente.getId());
        }
        if (loja != null) {
            q.setParameter("loja", loja.getId());
        }
        try {
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    public List<ISaldoFinanceiro> recuperarUltimosLancamentos(Loja loja, Integer maxResultados) {
        List<ISaldoFinanceiro> lancamentoFinanceiros = recuperarLancamentos(loja, null, null, null);
        return lancamentoFinanceiros.subList(0, maxResultados);
    }

    @Transactional
    public void removerTodos() {
        String sql = " delete from saldofinanceiro";
        Query q = em.createNativeQuery(sql);
        q.executeUpdate();
    }
}

