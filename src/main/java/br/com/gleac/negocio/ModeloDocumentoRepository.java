package br.com.gleac.negocio;

import br.com.gleac.entidade.certificado.ModeloDocumento;
import br.com.gleac.entidade.processo.Placet;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.enums.TagPlacet;
import br.com.gleac.enums.TipoModeloDocumento;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Map;

/**
 * Created by israeleriston on 13/05/16.
 */
@Repository
public class ModeloDocumentoRepository extends AbstractRepository<ModeloDocumento> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    @Autowired
    private PlacetRepository placetRepository;

    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;


    public ModeloDocumentoRepository() {
        super(ModeloDocumento.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public String trocarTags(ModeloDocumento modeloDocumento, Object entidade) {
        String template = modeloDocumento.getConteudo();
        if (TipoModeloDocumento.PLACET.equals(modeloDocumento.getTipoModeloDocumento())) {
            SolicitacaoProcesso solicitacaoProcesso = buscarSolicitacaoProcessoPorPlacet((Placet) entidade);
            Placet placet = (Placet) entidade;
            Map<String, Object> values = Maps.newHashMap();
            values.put(TagPlacet.LOJA.name(), placet.getLoja().getNome());
            values.put(TagPlacet.PESSOA.name(), placet.getPessoaFisica());
            values.put(TagPlacet.NUMERO_PLACET.name(), placet.getNumero());
            values.put(TagPlacet.SERENISSIMO.name(), placet.getGestao().getMacon());
            values.put(TagPlacet.EXERCICIO.name(), Util.recuperarExercicio());
            if(solicitacaoProcesso.isSolicitacaoServico()) {
                values.put(TagPlacet.TIPO_SERVICO.name(), solicitacaoProcesso.getServico().getTipoServico().getDescricao());
                values.put(TagPlacet.SERVICO.name(), solicitacaoProcesso.getServico().getDescricao().toUpperCase());
            }
            values.put(TagPlacet.CABECALHO.name(), buscarCabecalhoDocumento());
            values.put(TagPlacet.RODAPE.name(), buscarRodapeDocumento());

            for (String key : values.keySet()) {
                String tag = "$" + key;
                if (template.contains(tag)) {
                    template = template.replace(tag, values.get(key).toString());
                }
            }

        }

        return template;
    }

    private String buscarCabecalhoDocumento() {
        String conteudo =
                "<img src=\"" + Util.gerarUrlImagemDir() + "/resources/images/cabecalho_doc.jpeg\" alt=\"CABECALHO\" style='width: 100%'/>";
        return conteudo;
    }

    private String buscarRodapeDocumento() {
        String conteudo =
                "<img src=\"" + Util.gerarUrlImagemDir() + "/resources/images/rodape_doc.jpeg\" alt=\"LOGO\" style='width: 100%' />";
        return conteudo;
    }


    private SolicitacaoProcesso buscarSolicitacaoProcessoPorPlacet(Placet placet) {
        return solicitacaoProcessoRepository.recuperar(SolicitacaoProcesso.class, placet.getProcessoFinanceiro().getSolicitacaoProcesso().getId());
    }


    /**
     *
     * @param tipo of document
     * @return
     */
    @Transactional
    public ModeloDocumento buscarModeloDocumentoPorTipo(TipoModeloDocumento tipo) {
        String sql = " select mo.* from modelodocumento mo where mo.tipomodelodocumento = :tipo";

        Query q = em.createNativeQuery(sql, ModeloDocumento.class);
        q.setParameter("tipo", tipo.name());
        q.setMaxResults(1);
        try {
            return (ModeloDocumento) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

}
