package br.com.gleac.negocio;

import org.springframework.stereotype.Repository;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

/**
 * Created by israeleriston on 14/05/16.
 */
@Repository
public class SistemaRepository {


    public static String obtemLogin() {
        String login = "SEM_LOGIN";
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Principal principal = facesContext.getExternalContext().getUserPrincipal();
            if (principal != null) {
                login = principal.getName();
            }
        } catch (Exception ex) {
        }
        return login;
    }

    public static String obtemIp() {
        String ip = "Local";
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            ip = httpServletRequest.getRemoteAddr();
        } catch (Exception ex) {
        }
        return ip;
    }
}
