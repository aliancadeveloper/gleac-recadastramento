package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class ContaCorrenteRepository extends AbstractRepository<ContaCorrente> {

    @PersistenceContext
    private EntityManager em;


    public ContaCorrenteRepository() {
        super(ContaCorrente.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    public String complementarCriacaoObjeto(String q) {
        q += " new ContaCorrente(";
        q += "obj.id,  obj.numero, obj.agencia, obj.nome ";
        q += ") ";
        return q;
    }
}
