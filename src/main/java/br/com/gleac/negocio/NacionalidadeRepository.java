package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Nacionalidade;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by israeleriston on 20/07/16.
 */
@Repository
public class NacionalidadeRepository extends AbstractRepository<Nacionalidade> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public NacionalidadeRepository() {
        super(Nacionalidade.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    public List<Nacionalidade> findAllNacionalidadeByDescricaoOrCodigo(String filter) {
        return (List<Nacionalidade>) em.createQuery("select obj from Nacionalidade obj " +
                " where lower(obj.descricao) like :filter ")
                .setMaxResults(10)
                .setParameter("filter", "%" + filter.trim().toLowerCase() + "%").getResultList();
    }
}
