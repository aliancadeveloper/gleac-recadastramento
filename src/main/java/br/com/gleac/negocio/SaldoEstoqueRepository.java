package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.estoque.*;
import br.com.gleac.enums.SomaSubtrai;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 19/07/17.
 */
@Repository
public class SaldoEstoqueRepository extends AbstractRepository<SaldoEstoque> {

    @PersistenceContext
    private EntityManager em;

    public SaldoEstoqueRepository() {
        super(SaldoEstoque.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public void gerarSaldoEstoqueEntrada(EntradaEstoque entradaEstoque) {
        for (ItemEntradaEstoque itemEntradaEstoque : entradaEstoque.getItens()) {
            movimentarSaldo(entradaEstoque.getData(), entradaEstoque.getDepartamento(), itemEntradaEstoque.getProduto(), itemEntradaEstoque.getQuantidade(), SomaSubtrai.SOMA);
        }
    }

    @Transactional
    private void movimentarSaldo(Date data, Departamento departamento, Produto produto, Integer quantidade, SomaSubtrai somaSubtrai) {
        SaldoEstoque ultimoSaldo = recuperarUltimoSaldo(data, departamento, produto);
        if (ultimoSaldo == null) {
            SaldoEstoque saldo = new SaldoEstoque();
            saldo.setData(data);
            saldo.setDepartamento(departamento);
            saldo.setProduto(produto);
            alterarQuantidadeSaldo(saldo, quantidade, somaSubtrai);
            em.persist(saldo);
        } else {
            if (ultimoSaldo.getData().compareTo(data) == 0) {
                alterarQuantidadeSaldo(ultimoSaldo, quantidade, somaSubtrai);
                em.merge(ultimoSaldo);
            } else {
                SaldoEstoque saldo = new SaldoEstoque();
                saldo.setData(data);
                saldo.setDepartamento(departamento);
                saldo.setProduto(produto);
                saldo.setQuantidade(ultimoSaldo.getQuantidade());
                alterarQuantidadeSaldo(saldo, quantidade, somaSubtrai);
                em.persist(saldo);
            }
        }
        movimentarSaldosFuturos(data, departamento, produto, quantidade, somaSubtrai);
    }

    private void movimentarSaldosFuturos(Date data, Departamento departamento, Produto produto, Integer quantidade, SomaSubtrai somaSubtrai) {
        List<SaldoEstoque> saldoEstoques = recuperarSaldosFuturos(data, departamento, produto);
        if (saldoEstoques != null) {
            for (SaldoEstoque saldoEstoque : saldoEstoques) {
                alterarQuantidadeSaldo(saldoEstoque, quantidade, somaSubtrai);
                em.merge(saldoEstoque);
            }
        }
    }

    private void alterarQuantidadeSaldo(SaldoEstoque ultimoSaldo, Integer quantidade, SomaSubtrai somaSubtrai) {



        if (SomaSubtrai.SOMA.equals(somaSubtrai)) {
            ultimoSaldo.setQuantidade(ultimoSaldo.getQuantidade() + quantidade);
        } else {
            validarSaldo(quantidade, ultimoSaldo.getProduto(), ultimoSaldo.getDepartamento(), ultimoSaldo);
            ultimoSaldo.setQuantidade(ultimoSaldo.getQuantidade() - quantidade);
        }
    }

    @Transactional
    public void gerarSaldoEstoqueSaida(SaidaEstoque saidaEstoque) {
        for (ItemSaidaEstoque itemSaidaEstoque : saidaEstoque.getItens()) {
            movimentarSaldo(saidaEstoque.getData(), saidaEstoque.getDepartamento(), itemSaidaEstoque.getProduto(), itemSaidaEstoque.getQuantidade(), SomaSubtrai.SUBTRAI);
        }

    }

    private void validarSaldo(Integer quantidadeSolicitada, Produto produto, Departamento departamento, SaldoEstoque saldoEstoque) {
        if (saldoEstoque == null) {
            throw new ExceptionGenerica("Não foi encontrado saldo para o produto " + produto + " no departamento " + departamento + ".");
        } else {
            Integer saldo = saldoEstoque.getQuantidade();
            if (saldo < quantidadeSolicitada) {
                throw new ExceptionGenerica("Não foi encontrado saldo para o produto " + produto + " no departamento " + departamento
                    + "; <b> Saldo dispónivel é de " + saldo + "</b>");
            }
        }
    }

    @Transactional
    public SaldoEstoque recuperarUltimoSaldo(Date data, Departamento departamento, Produto produto) {
        String sql = "select saldo.* from SaldoEstoque saldo " +
            " where saldo.data <= to_date(:data, 'dd/MM/yyyy')" +
            " and saldo.departamento_id = :departamento" +
            " and saldo.produto_id = :produto" +
            " order by saldo.data desc";
        Query q = em.createNativeQuery(sql, SaldoEstoque.class);
        q.setParameter("data", Util.sdf.format(data));
        q.setParameter("departamento", departamento.getId());
        q.setParameter("produto", produto.getId());
        q.setMaxResults(1);
        try {
            return (SaldoEstoque) q.getSingleResult();
        } catch (NoResultException no) {
            return null;
        }
    }


    @Transactional
    public List<SaldoEstoque> recuperarSaldosFuturos(Date data, Departamento departamento, Produto produto) {
        String sql = "select saldo.* from SaldoEstoque saldo " +
            " where saldo.data > to_date(:data, 'dd/MM/yyyy')" +
            " and saldo.departamento_id = :departamento" +
            " and saldo.produto_id = :produto" +
            " order by saldo.data desc";
        Query q = em.createNativeQuery(sql, SaldoEstoque.class);
        q.setParameter("data", Util.sdf.format(data));
        q.setParameter("departamento", departamento.getId());
        q.setParameter("produto", produto.getId());
        try {
            return (List<SaldoEstoque>) q.getResultList();
        } catch (NoResultException no) {
            return null;
        }
    }

    @Transactional
    public List<SaldoEstoque> recuperarSaldos(Date datainicial, Date dataFinal, Departamento departamento, Produto produto) {

        String sql = "select saldo.* from SaldoEstoque saldo " +
            " where saldo.data = (select max(s.data) from saldoestoque s where s.produto_id = saldo.produto_id and s.departamento_id = saldo.departamento_id )";
        if (departamento != null) {
            sql += " and saldo.departamento_id = :departamento ";
        }
        if (datainicial != null) {
            sql += " and saldo.data >= to_date(:dataInicial, 'dd/MM/yyyy')";
        }
        if (datainicial != null) {
            sql += " and saldo.data <= to_date(:dataFinal, 'dd/MM/yyyy')";
        }
        if (produto != null) {
            sql += " and saldo.produto_id = :produto";
        }

        sql += " order by saldo.data desc";
        Query q = em.createNativeQuery(sql, SaldoEstoque.class);
        if (departamento != null) {
            q.setParameter("departamento", departamento.getId());
        }
        if (datainicial != null) {
            q.setParameter("dataInicial", Util.sdf.format(datainicial));
        }
        if (dataFinal != null) {
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
        }
        if (produto != null) {
            q.setParameter("produto", produto.getId());
        }
        try {
            return (List<SaldoEstoque>) q.getResultList();
        } catch (NoResultException no) {
            return null;
        } catch (Exception no) {
            no.printStackTrace();
            return null;
        }
    }
}
