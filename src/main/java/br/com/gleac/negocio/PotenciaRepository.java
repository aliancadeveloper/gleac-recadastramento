package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Potencia;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by zaca on 06/10/16.
 */
@Repository
public class PotenciaRepository extends AbstractRepository<Potencia> {

    @PersistenceContext
    private EntityManager em;

    public PotenciaRepository() {
        super(Potencia.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Potencia> buscarPotenciaPorNome(String filter){
        String sql = " select * from potencia where lower(nome) like :filter ";

        Query q = em.createNativeQuery(sql, Potencia.class);
        q.setParameter("filter", "%"+ filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);

        if (!q.getResultList().isEmpty())
            return q.getResultList();

        return Lists.newArrayList();
    }
}
