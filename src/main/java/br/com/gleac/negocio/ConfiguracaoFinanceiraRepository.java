package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.ConfiguracaoFinanceira;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by renato on 30/09/17.
 */
@Repository
public class ConfiguracaoFinanceiraRepository extends AbstractRepository<ConfiguracaoFinanceira> {

    @PersistenceContext
    private EntityManager em;


    public ConfiguracaoFinanceiraRepository() {
        super(ConfiguracaoFinanceira.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public ConfiguracaoFinanceira recuperarConfiguracao(){
        List<ConfiguracaoFinanceira> listar = listar();
        if(listar!= null && !listar.isEmpty()){
            return listar.get(0);
        }
        return null;
    }
}
