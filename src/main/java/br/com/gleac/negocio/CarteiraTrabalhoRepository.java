package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.CarteiraTrabalho;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by israeleriston on 02/09/16.
 */
@Repository
public class CarteiraTrabalhoRepository extends AbstractRepository<CarteiraTrabalho> {


    @PersistenceContext
    private EntityManager em;

    public CarteiraTrabalhoRepository() {
        super(CarteiraTrabalho.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
