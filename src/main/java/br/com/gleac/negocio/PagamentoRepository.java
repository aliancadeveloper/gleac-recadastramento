package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.Pagamento;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;


@Repository
public class PagamentoRepository extends AbstractRepository<Pagamento> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
    @Autowired
    private ContasPagarRepository contasPagarRepository;

    public PagamentoRepository() {
        super(Pagamento.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public Pagamento recuperarSemDependencia(Object id) {
        return getEntityManager().find(Pagamento.class, id);
    }


    @Transactional
    public void salvarNovo(Pagamento entity) {
        try {
            preSaveOrUpdate((Pagamento) entity);
            getEntityManager().persist(entity);
            if (entity.getContasPagar() != null) {
                entity.getContasPagar().setSituacaoContaPagar(SituacaoContaPagar.PAGA);
                contasPagarRepository.salvar(entity.getContasPagar());
            }
            lancamentoFinanceiroRepository.gerarSaldo(entity);
        } catch (Exception e) {
            entity.setId(null);
            throw e;
        }
    }


    @Transactional
    public BigDecimal recuperarSoma(Integer ano, String mes, Date data, Loja loja) {
        try {
            String sql = "select coalesce(sum(valor),0) from Pagamento where 1=1 ";
            if (ano != null) {
                sql += "  and to_char(dataLancamento,'YYYY') = :ano";
            }

            if (data != null) {
                sql += " and dataLancamento = to_date(:data,'dd/MM/yyyy')";
            }

            if (loja != null) {
                sql += " and loja_id = :loja";
            }
            if (mes != null) {
                sql += " and to_char(dataLancamento,'MM') = :mes ";
            }
            Query q = em.createNativeQuery(sql);
            if (mes != null) {
                q.setParameter("mes", mes);
            }
            if (data != null) {
                q.setParameter("data", Util.sdf.format(data));
            }
            if (ano != null) {
                q.setParameter("ano", String.valueOf(ano));
            }
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }


            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    @Transactional
    public BigDecimal recuperarSomaPorData(Date dataReferencia, Loja loja) {
        try {
            String sql = "select coalesce(sum(valor),0) from Pagamento " +
                " where datalancamento <= to_date(:dataReferencia,'dd/MM/yyyy') ";
            if (loja != null) {
                sql += " and loja_id = :loja";
            }

            Query q = em.createNativeQuery(sql);
            q.setParameter("dataReferencia", Util.sdf.format(dataReferencia));
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }
            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    @Transactional
    public Pagamento conciliar(Pagamento entity, Date dataConciliacao) {
        entity.setDataConciliacao(dataConciliacao);
        entity = em.merge(entity);
        return entity;
    }

    @Transactional
    public Pagamento estornarConciliacao(Pagamento entity) {
        entity.setDataConciliacao(null);
        entity = em.merge(entity);
        return entity;
    }
}
