package br.com.gleac.negocio;

import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.exception.GestaoNotCurrentException;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by zaca on 30/09/17.
 */
@Repository
public class GestaoRepository extends AbstractRepository<Gestao> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public GestaoRepository() {
        super(Gestao.class);
    }

    @Transactional
    @Override
    public Gestao recuperar(Class entidade, Object id) {
        Gestao gestao = super.recuperar(entidade, id);
        gestao.getGestaoLojas().size();
        return gestao;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public Gestao buscarGestaoVigente() throws GestaoNotCurrentException {
        String sql = " select ges.* from gestao ges where CURRENT_DATE  between ges.inicioem and coalesce(ges.fimem, CURRENT_DATE )";

        Query q = em.createNativeQuery(sql, Gestao.class);
        q.setMaxResults(1);

        try {
            return (Gestao) q.getResultList().get(0);
        } catch (NoResultException nre) {
            throw new GestaoNotCurrentException("Não foi encontrada nenhuma Gestão com configuração Vigente ");
        }
    }
}
