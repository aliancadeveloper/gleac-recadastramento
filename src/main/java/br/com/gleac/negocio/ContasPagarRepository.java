package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContasPagar;
import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoNotificacao;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Repository
public class ContasPagarRepository extends AbstractRepository<ContasPagar> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private SistemaService sistemaService;

    public ContasPagarRepository() {
        super(ContasPagar.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Override
    @Transactional
    public void salvarNovo(ContasPagar entity) {
        super.salvarNovo(entity);
        notificacaoRepository.salvarNovo(new Notificacao(entity.getDataVencimento(),
            sistemaService.getUsuarioCorrente(),
            TipoNotificacao.ALERTA,
            "Contas à pagar no valor de " + Util.getValorFormatado(entity.getValor()) + " !",
            "/admin/contas-pagar/editar/" + entity.getId() + "/"));
    }

    @Transactional
    public List<ContasPagar> completarEstaEntidade(String param) {
        String hql = "select ";
        hql = complementarCriacaoObjeto(hql);

        hql += " from ContasPagar obj";
        hql += " where obj.situacaoContaPagar <> '" + SituacaoContaPagar.ESTORNADA.name() + "' and ";

        hql = complementarWhereQuery(hql, param);

        Query q = em.createQuery(hql, ContasPagar.class);
        q.setMaxResults(10);
        q.setParameter("param", "%" + param + "%");
        return q.getResultList();
    }

    public List<ContasPagar> getContasAPagarVencendoHoje(Date date) {
        try {
            String sql = "select contas.* from ContasPagar contas " +
                " where contas.dataVencimento <= to_date(:data,'dd/MM/yyyy') and contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'" +
                " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasPagar.class);
            q.setParameter("data", Util.sdf.format(date));
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<ContasPagar> recuperarLancamentos(Loja loja, Date dataInicial, Date dataFinal) {
        try {
            String sql = "select contas.* from ContasPagar contas " +
                " where (contas.dataVencimento >= to_date(:dataInicial,'dd/MM/yyyy') and contas.dataVencimento <= to_date(:dataFinal,'dd/MM/yyyy'))" +
                " and contas.loja_id = :loja" +
                " and contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'" +
                " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasPagar.class);
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("loja", loja.getId());
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }

    public List<ContasPagar> recuperarLancamentos(Pessoa pessoa, Date dataInicial, Date dataFinal) {
        try {
            String sql = "select contas.* from ContasPagar contas " +
                " where (contas.dataVencimento >= to_date(:dataInicial,'dd/MM/yyyy') and contas.dataVencimento <= to_date(:dataFinal,'dd/MM/yyyy'))" +
                " and contas.pessoa_id = :pessoa" +
                " and contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'" +
                " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasPagar.class);
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("pessoa", pessoa.getId());
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }

    @Transactional
    public BigDecimal recuperarSomaPorDataAndSituacao(Date date, Loja loja, SituacaoContaPagar situacao) {
        try {
            String sql = "" +
                "  select coalesce(sum(contas.valor),0) " +
                "   from ContasPagar contas " +
                "  where contas.dataVencimento <= to_date(:data,'dd/MM/yyyy') " +
                "  and contas.situacaoContaPagar = :situacao ";
            if (loja != null) {
                sql += " and loja_id = :loja";
            }
            Query q = em.createNativeQuery(sql);
            q.setParameter("data", Util.sdf.format(date));
            q.setParameter("situacao", situacao.name());
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }
            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }
}
