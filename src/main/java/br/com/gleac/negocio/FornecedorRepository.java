package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Fornecedor;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class FornecedorRepository extends AbstractRepository<Fornecedor> {
    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public FornecedorRepository() {
        super(Fornecedor.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Fornecedor> completarFornecedorPorNumeroContrato(String filter) {
        Query q = em.createNativeQuery(" select f.* from fornecedor f where lower(f.numeroContrato) like :filtro ", Fornecedor.class);
        q.setParameter("filtro", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Fornecedor> completarFornecedorByNome(String filter) {
        String sql = " SELECT " +
            "  f.*, " +
            "  coalesce(pf.nome, pj.razaosocial) " +
            "FROM fornecedor f " +
            "  INNER JOIN pessoa p ON p.id = f.pessoa_id " +
            "  LEFT JOIN pessoafisica pf ON pf.id = p.id " +
            "  LEFT JOIN pessoajuridica pj ON pj.id = p.id " +
            "WHERE coalesce(pf.nome, pj.razaosocial) LIKE :filter " +
            "ORDER BY coalesce(pf.nome, pj.razaosocial) ";
        Query q = em.createNativeQuery(sql, Fornecedor.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Override
    @Transactional
    public Fornecedor recuperar(Class entidade, Object id) {
        Fornecedor fornecedor = em.find(Fornecedor.class, id);
        fornecedor.getDocumentosFornecedor().size();
        return fornecedor;
    }
}
