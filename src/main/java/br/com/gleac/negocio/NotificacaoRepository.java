package br.com.gleac.negocio;


import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.negocio.jpa.NotificacaoJPARepository;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;


@Repository
@Transactional
public class NotificacaoRepository extends AbstractRepository<Notificacao> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private NotificacaoJPARepository notificacaoJPARepository;

    public NotificacaoRepository() {
        super(Notificacao.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NotificacaoJPARepository getNotificacaoJPARepository() {
        return notificacaoJPARepository;
    }

    public void marcarNotificaoVisualizada(Long id) {
        try {
            String sql = "select n.* from notificacao n where n.link like :id or n.mensagem like :id and n.visualizado = false";
            Query q = em.createNativeQuery(sql, Notificacao.class);
            q.setParameter("id", "%" + id + "%");
            q.setMaxResults(1);

            Notificacao recuperar = (Notificacao) q.getSingleResult();

            if (recuperar != null) {
                recuperar.setVisualizado(Boolean.TRUE);
                salvar(recuperar);
            }
        } catch (Exception e) {
            System.out.println(" notificação não encontrada .. ");
        }
    }


    public List<Notificacao> buscarNotificacaoPorUsuarioAndVisualizacao(Usuario usuario, Boolean isVisualizado) {
        String sql = " select n.* from notificacao n " +
            " where n.usuario_id = :usuarioID and n.visualizado = :visualizado ";

        Query q = em.createNativeQuery(sql, Notificacao.class);

        q.setParameter("usuarioID", usuario.getId());
        q.setParameter("visualizado", BooleanUtils.toBoolean(isVisualizado));

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public Integer buscarAndContarNotificacaoUsuarioAndVisualizado(Usuario usuario, Boolean isVisualizado) {
        String sql = " select coalesce(cast(count(n.id) as numeric), 0) from notificacao n " +
            " where n.usuario_id = :usuarioID and n.visualizado = :visualizado ";

        Query q = em.createNativeQuery(sql);
        q.setParameter("usuarioID", usuario.getId());
        q.setParameter("visualizado", BooleanUtils.toBoolean(isVisualizado));
        q.setMaxResults(1);
        try {
            BigDecimal total = (BigDecimal) q.getSingleResult();
            return total.intValue();
        } catch (NoResultException nre) {
            return Integer.valueOf("0");
        }
    }
}
