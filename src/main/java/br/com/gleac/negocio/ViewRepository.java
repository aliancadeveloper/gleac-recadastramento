package br.com.gleac.negocio;

import br.com.gleac.enums.OperacaoCondicaoSql;
import br.com.gleac.enums.TipoComponente;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import br.com.gleac.util.view.ColunaView;
import br.com.gleac.util.view.ObjetoView;
import br.com.gleac.util.view.ParametroView;
import br.com.gleac.util.view.View;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

/**
 * Created by renat on 14/05/2016.
 */
@Repository
public class ViewRepository {
    private static final Logger logger = LoggerFactory.getLogger(ViewRepository.class);

    @PersistenceContext
    private EntityManager em;

    public ViewRepository() {
    }

    @Transactional
    public View recuperarObjetos(View view) {
        String sql = view.getSqlRecuperadorObjetos();
        sql = getStringSql(view, sql, true);
        Query consulta = em.createNativeQuery(sql);
        for (ParametroView parametroView : view.getParametros()) {
            atribuirParametroNaSql(consulta, parametroView);
        }
        if (view.getMaximoRegistrosTabela() != 0) {
            consulta.setMaxResults(view.getMaximoRegistrosTabela());
            consulta.setFirstResult(view.getInicio());
        }

        view.getObjetos().clear();

        List<Object> objetos = consulta.getResultList();


        for (Object objeto : objetos) {
            Object[] array = (Object[]) objeto;

            List<ColunaView> colunaViews = Lists.newArrayList();
            int posicao = 0;
            for (Object o : array) {
                if (posicao != 0) {
                    ColunaView coluna = preparaInstanciaColunaView(view, o, posicao - 1);
                    colunaViews.add(coluna);
                }
                posicao++;
            }
            ObjetoView objetoView = new ObjetoView(objeto, colunaViews);
            objetoView.setId(array[0]);
            view.getObjetos().add(objetoView);
        }

        return view;
    }

    private void atribuirParametroNaSql(Query consulta, ParametroView parametroView) {
        if (parametroView.getValor() != null) {
            if (!parametroView.getValor().toString().trim().isEmpty()) {
                if (parametroView.getOperacao().equals(OperacaoCondicaoSql.LIKE)) {
                    consulta.setParameter(parametroView.getCondicao(), "%" + parametroView.getValor() + "%");
                } else {
                    if (parametroView.getTipoComponente() == null) {
                        consulta.setParameter(parametroView.getCondicao(), parametroView.getValor());
                    } else {
                        if (parametroView.getTipoComponente().equals(TipoComponente.AUTO_COMPLETE)
                            || parametroView.getTipoComponente().equals(TipoComponente.SELECT_MENU)) {
                            consulta.setParameter(parametroView.getCondicao(), Persistencia.getId(parametroView.getValor()));
                        } else {
                            if(parametroView.getTipoComponente().equals(TipoComponente.DATA_BETWEEN)){
                                consulta.setParameter(parametroView.getCondicao()+"_1", parametroView.getValor());
                                consulta.setParameter(parametroView.getCondicao()+"_2", parametroView.getValor2());

                            }else {
                                consulta.setParameter(parametroView.getCondicao(), parametroView.getValor());
                            }
                        }
                    }
                }
            }
        }
    }

    private ColunaView preparaInstanciaColunaView(View view, Object o, int posicao) {

        ColunaView colunaView = view.getColunas().get(posicao);

        if (o == null) {
            return new ColunaView(null, colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), null);
        }

        if (colunaView.getEntidade()) {
            Object objetoRecuperado = null;
            if (o instanceof AbstractEntity) {
                objetoRecuperado = em.find(colunaView.getClasse(), Persistencia.getId(o));
            } else {
                objetoRecuperado = em.find(colunaView.getClasse(), ((BigDecimal) o).longValue());
            }

           /* if (colunaView.getClasse().equals(Arquivo.class)) {
                Object idObjetoRecuperado = Persistencia.getId(objetoRecuperado);
                String retorno = Util.getRequestContextPath() + "/arquivo?id=" + (idObjetoRecuperado != null ? idObjetoRecuperado.toString() : "");
                String icone = "<img src='" + Util.getRequestContextPath() + "/resources/images/icons/download.png'/>";
                retorno = "<a class='icone-25 ui-button ui-widget ui-state-default ui-corner-all' href='"
                        + retorno + "' style='background-position:center!important'><div class='icone-25' >"
                        + icone + " </div></a>";
                return new ColunaView(retorno, colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o);
            }*/

            return new ColunaView(((AbstractEntity) objetoRecuperado).getToStringHtml(), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), objetoRecuperado);
        }
        if (colunaView.getClasse().equals(BigDecimal.class)) {
            if (o instanceof Integer) {
                return new ColunaView("<b> 0 </b>", colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), " 0 ");
            }
            return new ColunaView("<b>" + Util.getValorFormatado(new BigDecimal(((Number) o).doubleValue())) + "</b>", colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), Util.getValorFormatado(new BigDecimal(((Number) o).doubleValue())));
        }
        if (colunaView.getClasse().equals(Date.class)) {
            if (colunaView.getDateTimeStamp()) {
                return new ColunaView(Util.sdft.format(o), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o);
            } else {
                return new ColunaView(Util.sdf.format(o), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o);
            }
        }
        if (colunaView.getClasse().equals(Boolean.class)) {
            return new ColunaView(Util.converterBooleanSimOuNao((Boolean) o), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o.toString());
        }
        if (colunaView.getClasse().isEnum()) {

            try {
                for (Field field : colunaView.getClasse().getDeclaredFields()) {
                    if (field.getName().equals(o.toString())) {
                        return new ColunaView(Enum.valueOf(colunaView.getClasse(), field.getName()), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), Enum.valueOf(colunaView.getClasse(), field.getName()));
                    }
                }
            } catch (Exception e) {
                logger.debug("Erro em preparaInstanciaColunaView", e);
                return new ColunaView(o.toString(), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o.toString());
            }
        }
        return new ColunaView(o.toString(), colunaView.getNomeColuna(), colunaView.getClasse(), colunaView.getEntidade(), o.toString());
    }

    @Transactional
    public Long recuperarQuantidadeRegistroExistente(View view) {
        try {
            String sql = view.getSqlContadorObjetos();
            sql = getStringSql(view, sql, false);
            Query consulta = em.createNativeQuery(sql);
            for (ParametroView parametroView : view.getParametros()) {
                atribuirParametroNaSql(consulta, parametroView);
            }
            return ((BigInteger) consulta.getSingleResult()).longValue();
        } catch (Exception e) {
            return 0L;
        }
    }

    private String getStringSql(View view, String sql, Boolean porOrdenador) {
        String condicao = "";
        String juncao = "";
        for (ParametroView parametroView : view.getParametros()) {
            if (parametroView.getValor() != null) {
                if (!parametroView.getValor().toString().trim().isEmpty()) {
                    if (parametroView.getOperacao().equals(OperacaoCondicaoSql.LIKE)) {
                        condicao += juncao + "upper(cast( " + parametroView.getCondicao() + " as text)) " + parametroView.getOperacao().getOperacao() + " :" + parametroView.getCondicao() + " ";
                    } else {
                        if (parametroView.getOperacao().equals(OperacaoCondicaoSql.BETWEEN)) {
                            condicao += juncao + parametroView.getCondicao() + " " + parametroView.getOperacao().getOperacao() + " :" + parametroView.getCondicao() + "_1 and :" + parametroView.getCondicao() + "_2 ";
                        }else {
                            condicao += juncao + parametroView.getCondicao() + " " + parametroView.getOperacao().getOperacao() + " :" + parametroView.getCondicao() + " ";
                        }
                    }
                    juncao = " and ";
                }
            }
        }

        if (!condicao.trim().isEmpty()) {
            sql = sql.replace("1=1", condicao);
        }
        if (porOrdenador) {
            sql += view.getSqlOrdenar();
        }
        return sql;
    }

    public View getViewObjetosJaRecuperados(List objetos, AbstractEntity superEntidade, Boolean colocarColunaDetalhes) {
        Class classe = superEntidade.getClass();
//        String urlVer = superEntidade.getUrlVer();
        View view = new View();
        view.setNomeRelatorio("Relatorio de " + classe.getSimpleName());
        view.getColunas().clear();
        view.getParametros().clear();

        if (colocarColunaDetalhes) {
//            view.setUrlVer(urlVer);
//            view.getColunas().add(new ColunaView(null, "Detalhes", Id.class, false, null));
        }
        for (Field field : Persistencia.getAtributosTabelaveis(classe)) {
            field.setAccessible(true);
            ColunaView e = new ColunaView(null, Persistencia.getNomeCampo(field), field.getType(), field.getType().isAnnotationPresent(Entity.class), null);
            if (field.getType().equals(Date.class)) {
                Temporal t = field.getAnnotation(Temporal.class);
                if (t.value() == TemporalType.TIMESTAMP) {
                    e.setDateTimeStamp(Boolean.TRUE);
                }
            }
            view.getColunas().add(e);
        }
        if (objetos != null) {
            for (Object obj : objetos) {
                List<ColunaView> colunas = Lists.newArrayList();
//                int posicao = colocarColunaDetalhes ? 1 : 0;
                int posicao = 0;
                try {
                    if (colocarColunaDetalhes) {
//                        colunas.add(preparaInstanciaColunaView(view, Persistencia.getId(obj), 0));
                    }

                    for (Field field : Persistencia.getAtributosTabelaveis(classe)) {
                        field.setAccessible(true);
                        ColunaView coluna = preparaInstanciaColunaView(view, field.get(obj), posicao);
                        colunas.add(coluna);
                        posicao++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                view.getObjetos().add(new ObjetoView(obj, colunas));
            }
        }
        return view;
    }
}

