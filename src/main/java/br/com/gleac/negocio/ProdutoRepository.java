package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by zaca.
 */
@Repository
public class ProdutoRepository extends AbstractRepository<Produto> {
    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public ProdutoRepository() {
        super(Produto.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Produto> completarProdutoPorNome(String filter) {
        Query q = em.createNativeQuery(" select p.* from produto p where lower(p.descricao) like :filtro ", Produto.class);
        q.setParameter("filtro", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }
}
