package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Profissao;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zaca on 03/09/16.
 */
@Repository
public class ProfissaoRepository extends AbstractRepository<Profissao> {

    @PersistenceContext
    private EntityManager em;

    public ProfissaoRepository() {
        super(Profissao.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public Profissao recuperar(Class entidade, Object id) {
        Profissao profissao = (Profissao) em.find(entidade, id);
        return profissao;
    }


    @Transactional
    public List<Profissao> buscarProfissaoPorNome(String filter) {
        String sql = " SELECT pro.* FROM profissao pro WHERE lower(pro.nome) LIKE :filter ";
        Query q = em.createNativeQuery(sql, Profissao.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);

        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }
}
