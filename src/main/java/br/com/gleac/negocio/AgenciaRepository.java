package br.com.gleac.negocio;


import br.com.gleac.entidade.financeiro.Agencia;
import br.com.gleac.entidade.financeiro.Banco;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Repository
public class AgenciaRepository extends AbstractRepository<Agencia> {

    @PersistenceContext
    private EntityManager em;


    public AgenciaRepository() {
        super(Agencia.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Agencia> buscarAgenciaPorBanco(String parte, Banco banco) {
        String sql = " select a.* from agencia a  " +
                "       where a.banco_id = :idBanco " +
                "       and lower(a.descricao) like :parte ";
        Query q = em.createNativeQuery(sql, Agencia.class);
        q.setParameter("parte", "%" + parte.toLowerCase().trim() + "%");
        q.setParameter("idBanco", banco.getId());
        if (!q.getResultList().isEmpty()) {
            return q.getResultList();
        }
        return new ArrayList<>();
    }
}
