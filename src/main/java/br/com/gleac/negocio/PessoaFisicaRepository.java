package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zacarias on 21/05/16.
 */
@Repository
public class PessoaFisicaRepository extends AbstractRepository<PessoaFisica> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;
    @Autowired
    private ServicoRepository servicoRepository;

    public PessoaFisicaRepository() {
        super(PessoaFisica.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public PessoaFisica recuperar(Class entidade, Object id) {
        PessoaFisica pf = (PessoaFisica) em.find(entidade, id);
        pf.getTelefones().size();
        pf.getEnderecos().size();
        pf.getDocumentosPessoais().size();
        pf.getFilhos().size();
        pf.getFormacoes().size();
        pf.getConjuges().size();
        pf.getContas().size();
        return pf;
    }

    @Transactional
    public List<Pessoa> findAllPessoasByNome(String filter) {
        List<Pessoa> retorno = Lists.newArrayList();
        retorno.addAll(pessoaJuridicaRepository.buscarPessoaJuridica(filter));
        retorno.addAll(buscarPessoaFisicaPorNome(filter));
        return retorno;
    }

    @Transactional
    public List<PessoaFisica> buscarPessoaFisicaPorNome(String filter) {
        String sql = " select obj.* from pessoafisica obj " +
            " where lower(obj.nome) like :filter ";

        Query q = em.createNativeQuery(sql, PessoaFisica.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }


    @Transactional
    public PessoaFisica buscarPessoaFisicaWithUsuario (Pessoa pf) {
        String sql = " select pf.* from usuario us " +
            " inner join pessoa p on us.pessoa_id = p.id " +
            " inner join pessoafisica pf on p.id = pf.id " +
            " where pf.id = :pessoaFisica ";

        Query q = em.createNativeQuery(sql, PessoaFisica.class);

        q.setParameter("pessoaFisica", pf.getId());
        q.setMaxResults(1);

        try {
            return (PessoaFisica) q.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }

    }

    /**
     *
     * @param status status of processing
     * @param filter filter by name
     * @return all person with processing at pending
     */
    @Transactional
    public List<PessoaFisica> completarPessoaFisicaComSolicitacaoPorStatus(StatusProcesso status, String filter) {
        String sql = " select pf.* from pessoafisica pf " +
                " inner join solicitacaoprocesso sol on sol.pessoafisica_id = pf.id " +
                " inner join processofinanceiro pro on sol.id = pro.solicitacaoprocesso_id " +
                " where sol.statusprocesso = :status and lower(pf.nome) like :filter ";

        Query q = em.createNativeQuery(sql, PessoaFisica.class);
        q.setParameter("status", status.name());
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<PessoaFisica> getPessoasComMesmoCPF(String parametro) {
        String sql = " SELECT PF.* FROM PESSOAFISICA PF " +
            "WHERE PF.CPF = :parametro";
        Query q = getEntityManager().createNativeQuery(sql, PessoaFisica.class);
        q.setParameter("parametro", parametro);
        return q.getResultList();
    }
}
