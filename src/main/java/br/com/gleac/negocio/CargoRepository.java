package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Cargo;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 31/08/16.
 */
@Repository
public class CargoRepository extends AbstractRepository<Cargo> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CargoRepository() {
        super(Cargo.class);
    }

    @Transactional
    @Override
    public Cargo recuperar(Class entidade, Object id) {
        Cargo cargo = (Cargo) em.find(entidade, id);
        return cargo;
    }

    @Transactional
    public List<Cargo> buscarCargoPorDescricao(String filter) {
        String sql = " select car.* from Cargo car where lower(car.descricao) like :filter ";
        Query q = em.createNativeQuery(sql, Cargo.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);

        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();

    }

    @Transactional
    public Boolean isCargoExistente(Cargo selecionado) {
        StringBuilder sql = new StringBuilder(" select 1 from cargo car where lower(TRIM(car.descricao)) = :descricao  ");

        if (selecionado.getId() != null) {
            sql.append(" and car.id <> :selecionado ");
        }

        Query q = em.createNativeQuery(sql.toString());
        q.setParameter("descricao", selecionado.getDescricao().trim().toLowerCase());

        if (selecionado.getId() != null) {
            q.setParameter("selecionado", selecionado.getId());
        }

        q.setMaxResults(1);
        return !q.getResultList().isEmpty();

    }
}
