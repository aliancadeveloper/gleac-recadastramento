package br.com.gleac.negocio;

import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.entidade.financeiro.SaldoBancario;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class SaldoBancarioRepository extends AbstractRepository<SaldoBancario> {

    @PersistenceContext
    private EntityManager em;

    public SaldoBancarioRepository() {
        super(SaldoBancario.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<SaldoBancario> buscarUltimosSaldoBancarioPorContaCorrente(Date dataReferencia, ContaCorrenteBancaria contaCorrente) {
        String sql = " " +
            " select saldo.* " +
            "   from saldobancario saldo " +
            " where saldo.contacorrentebancaria_id = :idConta " +
            "   and saldo.dataSaldo <= to_date(:dataReferencia, 'dd/MM/yyyy') " +
            " order by saldo.datasaldo desc ";
        Query consulta = em.createNativeQuery(sql, SaldoBancario.class);
        consulta.setParameter("idConta", contaCorrente.getId());
        consulta.setParameter("dataReferencia", Util.sdf.format(dataReferencia));
        try {
            return consulta.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }


    public SaldoBancario recuperarUltimoSaldo(Date dataReferencia, ContaCorrenteBancaria contaCorrente) {
        String sql = " " +
            " select saldo.* " +
            "   from saldobancario saldo " +
            " where saldo.contacorrentebancaria_id = :idConta " +
            "   and saldo.dataSaldo <= to_date(:dataReferencia, 'dd/MM/yyyy') " +
            " order by saldo.datasaldo desc ";
        Query consulta = em.createNativeQuery(sql, SaldoBancario.class);
        consulta.setParameter("idConta", contaCorrente.getId());
        consulta.setParameter("dataReferencia", Util.sdf.format(dataReferencia));
        consulta.setMaxResults(1);
        try {
            return (SaldoBancario) consulta.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
