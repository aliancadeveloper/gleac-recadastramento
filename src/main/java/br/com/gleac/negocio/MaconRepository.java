package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.jpa.MaconJPARepository;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zaca on 05/09/16.
 */
@Repository
public class MaconRepository extends AbstractRepository<Macon> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;
    @Autowired
    private MaconJPARepository maconJPARepository;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MaconJPARepository getMaconJPARepository() {
        return maconJPARepository;
    }

    public MaconRepository() {
        super(Macon.class);
    }

    @Transactional
    @Override
    public Macon recuperar(Class entidade, Object id) {
        Macon macon = (Macon) em.find(entidade, id);
        macon.getHistoricoCargos().size();
        macon.getHistoricosObreiro().size();
        macon.getHistoricoMaconicos().size();
        macon.getDocumentosMacon().size();
        if (macon.getPessoaFisica() != null) {
            macon.getPessoaFisica().getEnderecos().size();
            macon.getPessoaFisica().getTelefones().size();
        }

        return macon;
    }

    @Transactional
    public Boolean isNumeroMaconExistente(Macon entity) {

        String sql = " select 1 from macon m where m.identificacao = :identificacao";

        if (entity.getId() != null) {
            sql += " and m.id <> :id ";
        }
        Query q = em.createNativeQuery(sql);
        q.setParameter("identificacao", entity.getIdentificacao());
        if (entity.getId() != null) {
            q.setParameter("id", entity.getId());
        }
        return !q.getResultList().isEmpty();
    }

    @Transactional
    public List<Macon> completarMaconAtivoPorNome(String filter) {
        String sql = " SELECT ma.* " +
            "          FROM macon ma " +
            "          INNER JOIN pessoafisica pf ON ma.pessoafisica_id = pf.id " +
            "          WHERE ma.situacao  in (:situacao) AND (lower(pf.nome) LIKE :filter or cast(ma.identificacao as text) like :filter)";

        Query q = em.createNativeQuery(sql, Macon.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setParameter("situacao", Arrays.asList(Situacao.REGULAR.name(), Situacao.EMERITO.name()));

        if (!q.getResultList().isEmpty()) {
            List<Macon> macons = (List<Macon>) q.getResultList();

            for (Macon macon : macons) {
                macon.getHistoricoCargos().size();
                macon.getHistoricosObreiro().size();
                macon.getHistoricoMaconicos().size();
                if (macon.getPessoaFisica() != null) {
                    macon.getPessoaFisica().getEnderecos().size();
                    macon.getPessoaFisica().getTelefones().size();
                }
            }

            return macons;

        }

        return new ArrayList<>();
    }

    @Transactional
    public List<Macon> completarMaconAtivoPorNomeSimples(String filter) {
        String sql = " SELECT ma.* " +
            "          FROM macon ma " +
            "          INNER JOIN pessoafisica pf ON ma.pessoafisica_id = pf.id " +
            "          WHERE ma.situacao  in (:situacao) AND (lower(pf.nome) LIKE :filter or cast(ma.identificacao as text) like :filter)" +
            " order by ma.identificacao";

        Query q = em.createNativeQuery(sql, Macon.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setParameter("situacao", Arrays.asList(Situacao.REGULAR.name(), Situacao.EMERITO.name()));
        q.setMaxResults(10);
        try {
            List<Macon> macons = (List<Macon>) q.getResultList();
            return macons;
        } catch (Exception e) {
            return new ArrayList<>();
        }

    }

    @Transactional
    public Macon recuperarMaconPorUsuario(Usuario usuario) {
        String sql = "select mac.* from macon mac" +
            " inner join pessoafisica pf on mac.pessoafisica_id = pf.id" +
            " inner join usuario usuario on pf.id = usuario.pessoa_id " +
            " where usuario.id = :usuario ";
        Query q = em.createNativeQuery(sql, Macon.class);
        q.setParameter("usuario", usuario.getId());
        q.setMaxResults(1);
        try {
            return (Macon) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public List<Macon> completarMacomPorLojaAndSituacao(String parte, Loja loja, String... situacao) {
        String sql = getStringSQL(false);
        Query q = em.createNativeQuery(sql, Macon.class);
        q.setParameter("loja", loja.getId());
        q.setParameter("filter", "%" + parte.trim().toLowerCase() + "%");
        q.setParameter("situacao", Arrays.asList(situacao));
        try {
            return (List<Macon>) q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public Integer contarMacomPorLojaAndSituacao(String parte, Loja loja, String... situacao) {
        String sql = getStringSQL(true);
        Query q = em.createNativeQuery(sql);
        q.setParameter("loja", loja.getId());
        q.setParameter("filter", "%" + parte.trim().toLowerCase() + "%");
        q.setParameter("situacao", Arrays.asList(situacao));
        try {
            return ((Number) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return null;
        }
    }

    private String getStringSQL(Boolean count) {
        String sql = count ? "select count(m.id) from macon m " : "select m.* from macon m ";

        sql += " INNER JOIN pessoafisica pf ON m.pessoafisica_id = pf.id" +
            " inner join historicomaconico hist on m.id = hist.macon_id " +
            " and hist.datahistorico = (SELECT max(x.datahistorico) from historicomaconico x " +
            "                                               where x.macon_id = hist.macon_id " +
            "                                               and x.loja_id is not null) " +
            " where hist.loja_id = :loja" +
            " and (lower(pf.nome) LIKE :filter or cast(m.identificacao as text) like :filter)" +
            " and m.situacao IN (:situacao) " +
            " and (m.licenciamento = false or m.licenciamento is null)";
        return sql;
    }

    @Transactional
    public Long contarMacomPorLoja(Loja loja) {
        String sql = "select count(m.id) from macon m " +
            " INNER JOIN pessoafisica pf ON m.pessoafisica_id = pf.id" +
            " inner join historicomaconico hist on m.id = hist.macon_id " +
            " and hist.datahistorico = (SELECT max(x.datahistorico) from historicomaconico x " +
            "                                               where x.macon_id = hist.macon_id " +
            "                                               and x.loja_id is not null) " +
            " where hist.loja_id = :loja";
        Query q = em.createNativeQuery(sql);
        q.setParameter("loja", loja.getId());
        q.setMaxResults(1);
        try {
            return ((Number) q.getSingleResult()).longValue();
        } catch (Exception e) {
            return 0l;
        }
    }
}
