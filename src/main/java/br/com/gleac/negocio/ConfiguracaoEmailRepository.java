package br.com.gleac.negocio;

import br.com.gleac.entidade.mail.ConfiguracaoEmail;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by renato on 14/02/17.
 */
@Repository
public class ConfiguracaoEmailRepository extends AbstractRepository<ConfiguracaoEmail> {

    @PersistenceContext
    private EntityManager em;

    public ConfiguracaoEmailRepository() {
        super(ConfiguracaoEmail.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public ConfiguracaoEmail recuperarConfiguracaoAtual() {
        List<ConfiguracaoEmail> listar = listar();
        if (!listar.isEmpty()) {
            return listar.get(0);
        }
        return null;
    }

}
