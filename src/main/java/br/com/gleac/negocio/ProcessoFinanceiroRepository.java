package br.com.gleac.negocio;


import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.estoque.ItemSaidaEstoque;
import br.com.gleac.entidade.estoque.SaidaEstoque;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.processo.SolicitacaoProcessoProduto;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.service.BoletoService;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Repository
public class ProcessoFinanceiroRepository extends AbstractRepository<ProcessoFinanceiro> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private BoletoRepository boletoRepository;
    @Autowired
    private BoletoService boletoService;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private SaidaEstoqueRepository saidaEstoqueRepository;
    @Autowired
    private TaxaPercapitaRepository taxaPercapitaRepository;
    @Autowired
    private GestaoRepository gestaoRepository;


    public ProcessoFinanceiroRepository() {
        super(ProcessoFinanceiro.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Override
    @Transactional
    public void salvarNovo(ProcessoFinanceiro entity) {
        try {
            preSaveOrUpdate((AbstractEntity) entity);
            if (entity.getSolicitacaoProcesso().isTipoPagamentoBoleto()) {
                boletoService.gerarBoleto(entity);
                verificarBoletoGerado(entity);
            }
            gerarContasReceber(entity);
            limparNotificacao(entity);
            alterarStatusSolicitacao(entity);
            gerarSaidaEstoque(entity);
            getEntityManager().persist(entity);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void gerarSaidaEstoque(ProcessoFinanceiro entity) {
        if (entity.getSolicitacaoProcesso().isSolicitacaoProduto()) {
            Gestao gestao = gestaoRepository.buscarGestaoVigente();
            if (gestao == null) {
                throw new ExceptionGenerica("Nenhuma gestão foi encontrada.");
            } else {
                if (gestao.getDepartamentoEstoque() == null) {
                    throw new ExceptionGenerica("Nenhum departamento de estoque foi encontrado para a gestão vigente.");
                }
            }
            SaidaEstoque saidaEstoque = new SaidaEstoque();
            saidaEstoque.setData(entity.getGeradoEm());
            saidaEstoque.setDepartamento(gestao.getDepartamentoEstoque());
            saidaEstoque.setObservacao("Gerado atraves do processo " + entity.getSolicitacaoProcesso().getNumero());
            saidaEstoque.setItens(Lists.newArrayList());

            for (SolicitacaoProcessoProduto solicitacaoProduto : entity.getSolicitacaoProcesso().getSolicitacaoProdutos()) {
                ItemSaidaEstoque item = new ItemSaidaEstoque();
                item.setSaidaEstoque(saidaEstoque);
                item.setProduto(solicitacaoProduto.getProduto());
                item.setQuantidade(solicitacaoProduto.getQuantidade());

                saidaEstoque.getItens().add(item);
            }
            entity.setSaidaEstoque(saidaEstoque);
            saidaEstoqueRepository.salvarNovo(saidaEstoque);
        }
    }

    private void limparNotificacao(ProcessoFinanceiro entity) {
        notificacaoRepository.marcarNotificaoVisualizada(entity.getSolicitacaoProcesso().getId());
    }

    private void verificarBoletoGerado(ProcessoFinanceiro entity) {
        if (entity.getBoleto() == null) {
            throw new ExceptionGenerica("Problema ao gerar o Boleto.");
        }
    }

    private void gerarContasReceber(ProcessoFinanceiro entity) {
        if (entity.getBoleto() != null) {
            String descricao = "Contas a receber referente ao boleto do proceso "
                + entity.getSolicitacaoProcesso().getNumero()
                + " no valor de " + Util.getValorFormatado(entity.getBoleto().getValorTotal());

            contasReceberRepository.gerarPorBoleto(entity.getBoleto(),
                entity.getSolicitacaoProcesso().getDepartamento().getLoja(),
                entity.getSolicitacaoProcesso().getPessoa(),
                descricao,
                TipoContaPagar.PROCESSO);
        }
    }

    private void alterarStatusSolicitacao(ProcessoFinanceiro entity) {
        entity.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.AGUARDANDO_BOLETO);
        solicitacaoProcessoRepository.salvar(entity.getSolicitacaoProcesso());
    }

    @Transactional
    public void verificarBoleto(ProcessoFinanceiro selecionado) {
        boletoService.verificarBoleto(selecionado);
    }

    @Transactional
    public void verificarBoleto(ContasReceber selecionado) {
        boletoService.verificarBoleto(selecionado, taxaPercapitaRepository);
    }

    @Transactional
    public void cancelarBoleto(ProcessoFinanceiro selecionado) {
        boletoService.cancelarBoleto(selecionado);
    }

    @Transactional
    public void cancelarBoleto(ContasReceber selecionado) {
        boletoService.cancelarBoleto(selecionado);
    }

    @Transactional
    public String gerarBoletosAgrupados(List<Boleto> boletos) {
        return boletoService.gerarBoletosAgrupados(boletos);
    }

    @Transactional
    public ProcessoFinanceiro recuperarPorBoleto(Boleto boleto) {
        return boletoRepository.recuperarPorBoleto(boleto);
    }


    @Transactional
    public Boleto recuperarBoleto(String idPlataforma) {
        return boletoRepository.recuperarBoleto(idPlataforma);
    }

    @Transactional
    public void gerarReceitaBoletoPago(ProcessoFinanceiro selecionado, String status) {
        boletoService.gerarReceitaBoletoPago(selecionado, status);
    }

    @Transactional
    public List<ProcessoFinanceiro> completarProcessoFinanceiroPorSituacao(StatusProcesso status, String filter) {
        String sql = " select pro.* from processofinanceiro pro " +
            " inner join solicitacaoprocesso sol on pro.solicitacaoprocesso_id = sol.id" +
            " where ( " +
            "  lower(sol.descricao) like :filter or " +
            "  cast(sol.numero as text) like :filter" +
            " ) " +
            " and sol.statusprocesso = :situacao ";

        Query q = em.createNativeQuery(sql, ProcessoFinanceiro.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setParameter("situacao", status.name());
        try {

            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<ProcessoFinanceiro> completarProcessoFinanceiroPorSituacao(String filter) {
        String sql = " select pro.* from processofinanceiro pro " +
            " inner join solicitacaoprocesso sol on pro.solicitacaoprocesso_id = sol.id" +
            " where ( " +
            "  lower(sol.descricao) like :filter or " +
            "  cast(sol.numero as text) like :filter" +
            " ) ";

        Query q = em.createNativeQuery(sql, ProcessoFinanceiro.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {

            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }
}
