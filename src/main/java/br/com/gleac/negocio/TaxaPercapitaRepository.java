package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.TaxaPercapita;
import br.com.gleac.entidade.financeiro.TaxaPercapitaParcela;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.enums.TipoTaxaPercapitaParcela;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.service.BoletoService;
import br.com.gleac.supers.AbstractRepository;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 14/10/17.
 */
@Repository
public class TaxaPercapitaRepository extends AbstractRepository<TaxaPercapita> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private BoletoRepository boletoRepository;
    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private BoletoService boletoService;
    @Autowired
    private ConfiguracaoServicoRepository configuracaoServicoRepository;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TaxaPercapitaRepository() {
        super(TaxaPercapita.class);
    }

    @Transactional
    public TaxaPercapita gerarTaxa(TaxaPercapita taxaPercapita) {
        Boleto boleto = boletoService.gerarBoleto(taxaPercapita);
        Pessoa pessoa = getPessoa(taxaPercapita);
        Loja loja = getLoja(taxaPercapita);

        String descricao = "";
        if (taxaPercapita.isServico()) {
            descricao = "Contas a Receber da taxa " + taxaPercapita.getServico().getDescricao() + ".";
        } else {
            if (taxaPercapita.getProdutos().size() == 1) {
                descricao = "Contas a Receber da taxa " + taxaPercapita.getProdutos().get(0).getProduto().getDescricao() + ".";
            } else {
                descricao = "Contas a Receber da taxa de produtos.";
            }
        }
        ContasReceber contasReceber = contasReceberRepository.gerarPorBoleto(boleto, loja, pessoa, descricao, TipoContaPagar.TAXA);
        taxaPercapita.setContasReceber(contasReceber);

        if (taxaPercapita.isTodos()) {
            Boolean geraComDesconto = Boolean.TRUE;
            if (TipoSolicitacao.SERVICO.equals(taxaPercapita.getTipoSolicitacao())) {
                ConfiguracaoServico configuracaoServico = configuracaoServicoRepository.buscarConfiguracao(taxaPercapita.getData(), taxaPercapita.getServico());
                if (configuracaoServico.getValorDesconto().compareTo(BigDecimal.ZERO) == 0) {
                    geraComDesconto = Boolean.FALSE;
                }
            }
            if (geraComDesconto) {
                criarParcelasTaxa(taxaPercapita, descricao, TipoTaxaPercapitaParcela.UNICA_COM_DESCONTO, new Date(), taxaPercapita.getData());
            }
            criarParcelasTaxa(taxaPercapita, descricao, TipoTaxaPercapitaParcela.PRIMEIRA_PARCELA, new Date(), taxaPercapita.getData());
            criarParcelasTaxa(taxaPercapita, descricao, TipoTaxaPercapitaParcela.SEGUNDA_PARCELA, new Date(), new LocalDate(taxaPercapita.getData()).plusDays(30).toDate());
            criarParcelasTaxa(taxaPercapita, descricao, TipoTaxaPercapitaParcela.TERCEIRA_PARCELA, new Date(), new LocalDate(taxaPercapita.getData()).plusDays(60).toDate());
        }

        em.persist(taxaPercapita);
        return taxaPercapita;
    }


    private void criarParcelasTaxa(TaxaPercapita taxaPercapita, String descricao, TipoTaxaPercapitaParcela tipo, Date dataInicio, Date dataFim) {
        TaxaPercapitaParcela parcela = new TaxaPercapitaParcela();
        parcela.setTaxaPercapita(taxaPercapita);
        parcela.setTipo(tipo);

        Boleto boleto = boletoService.gerarBoleto(parcela, dataInicio, dataFim);
        descricao = "Parcela " + parcela.getTipo().getDescricao() + " taxa " + descricao;

        Pessoa pessoa = getPessoa(taxaPercapita);
        Loja loja = getLoja(taxaPercapita);
        ContasReceber contasReceber = contasReceberRepository.gerarPorBoleto(boleto, loja, pessoa, descricao, TipoContaPagar.TAXA);
        parcela.setContasReceber(contasReceber);

        taxaPercapita.getParcelas().add(parcela);
    }

    private Loja getLoja(TaxaPercapita taxaPercapita) {
        Loja loja = null;
        if (taxaPercapita.isPorLoja()) {
            loja = lojaRepository.buscarLojaGestora();
        } else {
            loja = taxaPercapita.getLoja();
        }
        return loja;
    }

    private Pessoa getPessoa(TaxaPercapita taxaPercapita) {
        Pessoa pessoa = null;
        if (taxaPercapita.isPorLoja()) {
            pessoa = taxaPercapita.getLoja().getPessoaJuridica();
        } else {
            pessoa = taxaPercapita.getMacon().getPessoaFisica();
        }
        return pessoa;
    }

    @Transactional
    public TaxaPercapita buscarTaxaPorContaReceber(ContasReceber selecionado) {
        String sql = "select taxa.* from taxapercapita taxa " +
            "where taxa.contasreceber_id = :conta ";
        Query q = em.createNativeQuery(sql, TaxaPercapita.class);
        q.setMaxResults(1);
        q.setParameter("conta", selecionado.getId());
        return (TaxaPercapita) q.getSingleResult();
    }

    @Transactional
    public TaxaPercapitaParcela buscarTaxaParcelaPorContaReceber(ContasReceber selecionado) {
        String sql = "select taxa.* from TaxaPercapitaParcela taxa " +
            "where taxa.contasreceber_id = :conta ";
        Query q = em.createNativeQuery(sql, TaxaPercapitaParcela.class);
        q.setMaxResults(1);
        q.setParameter("conta", selecionado.getId());
        return (TaxaPercapitaParcela) q.getSingleResult();
    }

    @Override
    @Transactional
    public TaxaPercapita recuperar(Class entidade, Object id) {
        TaxaPercapita taxa = em.find(TaxaPercapita.class, id);
        taxa.getParcelas().size();
        return taxa;
    }

    @Transactional
    public String gerarBoletosAgrupados(List<Boleto> boletos) {
        return boletoService.gerarBoletosAgrupados(boletos);
    }
}
