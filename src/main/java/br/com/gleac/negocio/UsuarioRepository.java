package br.com.gleac.negocio;

import br.com.gleac.entidade.auxiliares.PrimeiroAcesso;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.mail.ConfiguracaoEmail;
import br.com.gleac.entidade.seguranca.GrupoAcesso;
import br.com.gleac.entidade.seguranca.GrupoUsuario;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.GrupoAcessoUsuario;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.seguranca.Seguranca;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.EmailUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by israeleriston on 11/05/16.
 */
@Repository
@Transactional
public class UsuarioRepository extends AbstractRepository<Usuario> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;
    @Autowired
    private ConfiguracaoEmailRepository configuracaoEmailRepository;
    @Autowired
    private GrupoAcessoRepository grupoAcessoRepository;

    public UsuarioRepository() {
        super(Usuario.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    @Override
    public Usuario recuperar(Class entidade, Object id) {
        Usuario retorno = (Usuario) em.find(entidade, id);
        retorno.getGruposUsuario().size();
        return retorno;
    }


    @Transactional
    public Usuario recuperarUsuarioPorLogin(String login) {
        Query consulta = em.createQuery("select u from Usuario u where upper(u.login) = :login");
        consulta.setParameter("login", login.toUpperCase());
        consulta.setMaxResults(1);
        try {
            Usuario usuario = (Usuario) consulta.getSingleResult();
            usuario = recuperar(Usuario.class, usuario.getId());
            return usuario;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public Usuario recuperarUsuarioPorPessoa(Pessoa p) {
        Query consulta = em.createQuery("select u from Usuario u where u.pessoa.id = :idPessoa");
        consulta.setParameter("idPessoa", p.getId());
        consulta.setMaxResults(1);
        try {
            Usuario usuario = (Usuario) consulta.getSingleResult();
            usuario = recuperar(Usuario.class, usuario.getId());
            return usuario;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public Usuario recuperarUsuarioPorToken(String token) {
        Query consulta = em.createQuery("select u from Usuario u where u.token = :token order by u.expiraPrimeiroAcesso desc");
        consulta.setParameter("token", token);
        consulta.setMaxResults(1);
        try {
            Usuario usuario = (Usuario) consulta.getSingleResult();
            usuario = recuperar(Usuario.class, usuario.getId());
            return usuario;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public void verificarPrimeiroAcesso(PrimeiroAcesso primeiroAcesso) {
        try {
            recuperarPessoaFisicaPrimeiroAcesso(primeiroAcesso);
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        }
    }

    @Transactional
    public void verificarNomeDeUsuario(String paramentro) {
        ValidacaoException ve = new ValidacaoException();
        String hql = "from Usuario where login = :parametro ";
        Query consulta = em.createQuery(hql);
        consulta.setParameter("parametro", paramentro.trim().toLowerCase());
        if (!consulta.getResultList().isEmpty()) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("O nome de usuário informado já está sendo utilizado, por favor escolha um outro nome de usuário.");
        }
        ve.lancarException();
    }

    @Transactional
    public PessoaFisica recuperarPessoaFisicaPrimeiroAcesso(PrimeiroAcesso primeiroAcesso) {
        String sql = "select pf.* from pessoa p " +
            " inner join pessoafisica pf on p.id = pf.id" +
            " where pf.cpf = :cpf" +
            " and p.email = :email ";
        Query consulta = em.createNativeQuery(sql, PessoaFisica.class);
        consulta.setParameter("cpf", primeiroAcesso.getCpf());
        consulta.setParameter("email", primeiroAcesso.getEmail());
        consulta.setMaxResults(1);
        try {
            return (PessoaFisica) consulta.getSingleResult();
        } catch (NoResultException e) {
            throw new ExceptionGenerica("Não foi encontrado nenhum cadastro com as informações. Verifique seu cadastro junto ao administrador do sistema.");
        }
    }

    @Transactional
    public void criarNovoUsuarioEnviandoEmailPrimeiroAcesso(PrimeiroAcesso primeiroAcesso) {
        String titulo = "Acesso ao sistema GLEAC";
        PessoaFisica pessoa = recuperarPessoaFisicaPrimeiroAcesso(primeiroAcesso);
        Usuario usuario = criarUsuario(primeiroAcesso, pessoa);
        salvarNovo(usuario);
        String conteudo = getConteudoEmail(usuario);
        enviarEmail(primeiroAcesso, conteudo, titulo);
    }

    private String getConteudoEmail(Usuario usuario) {
        StringBuilder stringBuilder = new StringBuilder();
        String url = Util.localUsuarioAtualmenteCompleta().replace("primeiro-acesso.xhtml", "");
        url += "public/token?id=" + usuario.getToken() + "";
        stringBuilder.append("<center>")
            .append("<h1>Sistema GLEAC</h1>")
            .append("<h2>Seja Bem vindo(a) ").append(usuario.getPessoa().getNomePessoa()).append("</h2>")
            .append("<p>O seu login é: <b>").append(usuario.getLogin()).append("</b></p>")
            .append("<p>Para acessar o sistema, utilize o link abaixo e informe sua nova senha.</p>")
            .append("<p>Obs.: Caso apareça a mensagem 'Usuário sem loja cadastrada.', " +
                "favor informar a secretaria da Grande Loja para providenciar o(s) acesso(s).</p>")
            .append("<br /> <br /> <a href=\"").append(url).append("\"> Acesse aqui </a>")
            .append("</center>");
        return stringBuilder.toString();
    }

    @Transactional
    private Usuario criarUsuario(PrimeiroAcesso primeiroAcesso, PessoaFisica pessoa) {
        Usuario usuario = new Usuario();
        usuario.setAtivo(true);
        PessoaFisica pessoaFisica = pessoa;
        usuario.setLogin(primeiroAcesso.getLogin().toLowerCase());
        usuario.setPessoa(pessoaFisica);
        usuario.setSenha(Seguranca.encodedSenha("123mudar"));
        usuario.setToken(Util.generateTokenData());
        usuario.setExpiraPrimeiroAcesso(Util.adicionaDias(new Date(), 2));
        return usuario;
    }

    @Transactional
    private GrupoUsuario criarGrupoAcesso(Usuario usuario) {
        GrupoUsuario grupoUsuario = new GrupoUsuario();
        GrupoAcesso grupoAcesso = grupoAcessoRepository.recuperarPorTipo(GrupoAcessoUsuario.MACON);
        grupoUsuario.getGruposAcessos().add(grupoAcesso);
        grupoUsuario.getUsuarios().add(usuario);
        return grupoUsuario;
    }

    private void enviarEmail(PrimeiroAcesso primeiroAcesso, String conteudo, String titulo) {
        ConfiguracaoEmail configuracaoEmail = configuracaoEmailRepository.recuperarConfiguracaoAtual();
        String destinatario = primeiroAcesso.getEmail();
        EmailUtil.enviaEmail(configuracaoEmail, destinatario, titulo, conteudo);
    }

    public Boolean checkPermissaoUsuarioByGrupoAcessoUsuario(Usuario usuario, GrupoAcessoUsuario grupoAcessoUsuario, Loja loja) {
        String sql = "select 1 from usuario us\n" +
            "inner join grupousuariosistema gus on gus.usuario_id = us.id\n" +
            "inner join grupousuario gu on gu.id = gus.grupousuario_id\n" +
            "inner join grupousuarioacesso gua on gua.grupousuario_id = gu.id\n" +
            "inner join grupoacesso ga on ga.id = gua.grupoacesso_id\n" +
            "inner join loja l on l.id = ga.loja_id\n" +
            "where us.id = :usuario and l.id = :loja and ga.grupoacessousuario = :grupoAcesso ";

        Query q = em.createNativeQuery(sql);

        q.setParameter("usuario", usuario.getId());
        q.setParameter("grupoAcesso", grupoAcessoUsuario.name());
        q.setParameter("loja", loja.getId());

        return !q.getResultList().isEmpty();
    }


    @Transactional
    @Override
    public void salvar(Usuario entity) {
        if (entity.getId() == null) {
            getEntityManager().persist(entity);
        } else {
            getEntityManager().merge(entity);
        }
    }

    public List<Usuario> completarUsuario(String filter) {
        String sql = " select us.* from usuario us " +
            " where lower(us.login) like :login ";

        Query q = em.createNativeQuery(sql, Usuario.class);
        q.setParameter("login", "%" + StringUtils.defaultIfEmpty(filter.trim().toLowerCase(), "") +"%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

}
