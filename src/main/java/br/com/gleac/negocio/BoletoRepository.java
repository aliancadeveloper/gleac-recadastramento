package br.com.gleac.negocio;


import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.configuracao.ConfiguracaoProduto;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.entidade.financeiro.*;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.processo.SolicitacaoProcessoProduto;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.Situacao;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import br.com.gleac.util.UtilFinanceiro;
import com.google.common.base.Strings;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;


@Repository
public class BoletoRepository extends AbstractRepository<Boleto> {

    @PersistenceContext
    private EntityManager em;


    public BoletoRepository() {
        super(Boleto.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    public ProcessoFinanceiro recuperarPorBoleto(Boleto boleto) {
        Query q = em.createNativeQuery("SELECT  obj.* from processofinanceiro obj where obj.boleto_id = :boleto", ProcessoFinanceiro.class);
        q.setParameter("boleto", boleto.getId());
        return (ProcessoFinanceiro) q.getSingleResult();
    }


    @Transactional
    public Boleto recuperarBoleto(String idPlataforma) {
        Query q = em.createNativeQuery("SELECT  obj.* from boleto obj where obj.idPlataforma = :idPlataforma", Boleto.class);
        q.setParameter("idPlataforma", idPlataforma);
        return (Boleto) q.getSingleResult();
    }
}
