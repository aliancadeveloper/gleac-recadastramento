package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.ConfiguracaoCabecalho;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by Edi on 30/07/2017.
 */
@Repository
public class ConfiguracaoCabecalhoRepository extends AbstractRepository<ConfiguracaoCabecalho> {

    @PersistenceContext
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Autowired
    private ArquivoRepository arquivoRepository;

    public ConfiguracaoCabecalhoRepository() {
        super(ConfiguracaoCabecalho.class);
    }

    public ArquivoRepository getArquivoRepository() {
        return arquivoRepository;
    }

    @Transactional
    public ConfiguracaoCabecalho buscarConfiguracaoCabecalho() {
        String sql = " " +
            " SELECT config.* " +
            "   FROM ConfiguracaoCabecalho config ";
        Query q = em.createNativeQuery(sql, ConfiguracaoCabecalho.class);

        q.setMaxResults(1);
        try {
            return (ConfiguracaoCabecalho) q.getResultList().get(0);
        } catch (Exception ex) {
            throw new ExceptionGenerica("Configuração de cabeçalho não encontrada!");
        }
    }


    @Transactional
    public Boolean verificarConfiguracaoExistente(ConfiguracaoCabecalho configuracao) {
        String sql = " " +
                " SELECT config.* " +
                "   FROM ConfiguracaoCabecalho config ";
        if (configuracao != null && configuracao.getId() != null) {
            sql += "AND config.id <> :idConfig ";
        }
        Query q = em.createNativeQuery(sql);

        q.setMaxResults(1);
        if (configuracao != null && configuracao.getId() != null) {
            q.setParameter("idConfig", configuracao.getId());
        }
        try {
            return !q.getResultList().isEmpty();
        } catch (Exception ex) {
            throw new ExceptionGenerica("Configuração existente não encontrada. ");
        }
    }
}
