package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.PessoaJuridica;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 31/08/16.
 */
@Repository
public class PessoaJuridicaRepository extends AbstractRepository<PessoaJuridica> {

    @PersistenceContext
    private EntityManager em;

    public PessoaJuridicaRepository() {
        super(PessoaJuridica.class);
    }

    @Transactional
    @Override
    public PessoaJuridica recuperar(Class entidade, Object id) {
        PessoaJuridica pj = (PessoaJuridica) em.find(entidade, id);
        pj.getEnderecos().size();
        pj.getTelefones().size();
        pj.getContas().size();
        return pj;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<PessoaJuridica> buscarPessoaJuridica(String filter) {
        String sql = " select pj.* from pessoajuridica pj where lower(pj.nomefantasia) like  :filter ";

        Query q = em.createNativeQuery(sql, PessoaJuridica.class);

        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }

    @Transactional
    public List<PessoaJuridica> getPessoasComMesmoCNPJ(String parametro) {
        String sql = " SELECT PF.* FROM PESSOAJURIDICA PF " +
            "WHERE PF.CNPJ = :parametro";
        Query q = getEntityManager().createNativeQuery(sql, PessoaJuridica.class);
        q.setParameter("parametro", parametro);
        return q.getResultList();
    }
}
