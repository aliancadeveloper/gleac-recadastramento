package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by zaca.
 */
@Repository
public class ServicoRepository extends AbstractRepository<Servico> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    @Autowired
    private SingletonGeradorCodigo singletonGeradorCodigo;

    public ServicoRepository() {
        super(Servico.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Servico> completarServicoPorDescricao(String filtro) {
        Query q = em.createNativeQuery(" select s.* from servico s where lower(s.descricao) like :filtro ", Servico.class);
        q.setMaxResults(10);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    @Override
    public void salvarNovo(Servico entity) {
        entity.isCamposObrigatoriosPreenchidos();
        entity.setNumero(getSingletonGeradorCodigo().getProximoCodigo(entity.getClass(), "numero").intValue());
        super.salvarNovo(entity);
    }

    public SingletonGeradorCodigo getSingletonGeradorCodigo() {
        return singletonGeradorCodigo;
    }
}
