package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.LojaConta;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.negocio.jpa.LojaJPARepository;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by zaca on 06/10/16.
 */
@Repository
public class LojaRepository extends AbstractRepository<Loja> {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private LojaJPARepository lojaJPARepository;

    public LojaRepository() {
        super(Loja.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LojaJPARepository getLojaJPARepository() {
        return lojaJPARepository;
    }

    @Override
    @Transactional
    public Loja recuperar(Class entidade, Object id) {
        Loja loja = em.find(Loja.class, id);
        loja.getDepartamentos().size();
        loja.getPessoaJuridica().getContas().size();
        loja.getContas().size();
        return loja;
    }

    @Transactional
    public List<Loja> recuperarLojasDoUsuario(Usuario usuario) {
        String sql = "select distinct l.* from usuario us " +
            "  inner join grupousuariosistema gus on gus.usuario_id = us.id " +
            "  inner join grupousuario gu on gu.id = gus.grupousuario_id " +
            "  inner join grupousuarioacesso gua on gua.grupousuario_id = gu.id " +
            "  inner join grupoacesso ga on ga.id = gua.grupoacesso_id " +
            "  inner join loja l on l.id = ga.loja_id " +
            "where us.id = :usuario order BY  l.nome desc ";
        Query q = em.createNativeQuery(sql, Loja.class);
        q.setParameter("usuario", usuario.getId());
        try {
            if (!ObjectUtils.isEmpty(q.getResultList())) {
                List<Loja> lojas = Lists.newArrayList();
                for (Object o : q.getResultList()) {
                    Loja l = (Loja) o;
                    lojas.add(recuperar(Loja.class, l.getId()));
                }
                return lojas;

            }
            return Lists.newArrayList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Loja> completarLojasByNome(String filter) {
        String sql = " select loja.* from loja loja where (lower(loja.nome) like :filter or " +
            " lower(loja.numero) like :filter) order by loja.numero ";

        Query q = em.createNativeQuery(sql, Loja.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Loja> buscarTodasLojas() {
        String sql = " select l.* from loja l order by l.numero desc ";

        Query q = em.createNativeQuery(sql, Loja.class);
        List<Loja> retorno = Lists.newArrayList();
        List<Loja> lojas = q.getResultList();
        if (lojas != null && !lojas.isEmpty()) {
            for (Loja loja : lojas) {
                Loja recuperada = recuperar(Loja.class, loja.getId());
                retorno.add(recuperada);
            }

        }
        try {
            return retorno;
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public List<SelectItem> getContasDaLojaForSelectItem(Loja loja) {
        if (loja == null) {
            return Lists.newArrayList();
        } else {
            List<LojaConta> contas = loja.getContas();
            List<SelectItem> retorno = Lists.newArrayList();
            for (LojaConta conta : contas) {
                retorno.add(new SelectItem(conta.getContaCorrente(), conta.getContaCorrente().toString()));
            }

            return retorno;
        }
    }

    @Transactional
    public Loja buscarLojaGestora() {
        String sql = " select l.* from loja l join gestao g on g.loja_id = l.id order by g.id desc";

        Query q = em.createNativeQuery(sql, Loja.class);
        q.setMaxResults(1);
        try {
            return (Loja) q.getSingleResult();
        } catch (NoResultException nre) {
            return new Loja();
        }
    }

}
