package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.TipoNotificacao;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.service.SistemaService;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by israeleriston on 26/06/17.
 */
@Repository
public class SolicitacaoProcessoRepository extends AbstractRepository<SolicitacaoProcesso> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    @Autowired
    private SingletonGeradorCodigo singletonGeradorCodigo;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private GestaoRepository gestaoRepository;

    public SolicitacaoProcessoRepository() {
        super(SolicitacaoProcesso.class);
    }

    @Transactional
    @Override
    public SolicitacaoProcesso recuperar(Class entidade, Object id) {
        SolicitacaoProcesso objeto = getObjeto((Long) id);
        objeto.getSolicitacaoProcessoFormularios().size();
        objeto.getSolicitacaoProdutos().size();
        return objeto;
    }

    public SolicitacaoProcesso getObjeto(Long id) {
        return (SolicitacaoProcesso) em.createNativeQuery(" SELECT sol.* from solicitacaoprocesso sol" +
            " left join solicitacaoprocessoproduto prod on sol.id = prod.solicitacaoprocesso_id " +
            " left join solicitacaoprocessoformulario form on sol.id = form.solicitacaoprocesso_id" +
            " where sol.id = :id ", SolicitacaoProcesso.class)
            .setParameter("id", id)
            .getSingleResult();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    @Transactional
    public void salvarNovo(SolicitacaoProcesso entity) {
        entity.setNumero(singletonGeradorCodigo.getProximoCodigo(entity.getClass(), "numero").intValue());
        super.salvarNovo(entity);

        Macon secretario = gestaoRepository.buscarGestaoVigente().getSecretario();
        if(secretario == null){
            throw new ExceptionGenerica("Nenhum secretário foi configurado para a gestão vigente.");
        }
        PessoaFisica pessoaFisica = secretario.getPessoaFisica();
        Usuario usuario = usuarioRepository.recuperarUsuarioPorPessoa(pessoaFisica);
        notificacaoRepository.salvarNovo(new Notificacao(entity.getSolicitadoEm(),
            usuario,
            TipoNotificacao.PROCESSO,
            "Solicitação de processo " + entity.getNumero() + " criada!",
            "/admin/parecer-solicitacao-processo/nova-por-solicitacao/" + entity.getId() + "/"));
    }

    @Transactional
    public List<SolicitacaoProcesso> completarSolicitacaoProcessoPorDescricaoOrNumero(String filtro, StatusProcesso statusProcesso) {
        String sql = "select sol.* from solicitacaoprocesso sol " +
            "  inner join pessoa p on p.id = sol.pessoa_id " +
            "  left join pessoafisica pf on p.id = pf.id " +
            "  left join pessoajuridica pj on p.id = pj.id " +
            "where ( " +
            "  lower(sol.descricao) like :filtro or " +
            "  cast(sol.numero as text) like :filtro or " +
            "  lower(pf.nome) like :filtro or " +
            "  cast(pf.cpf as text) like :filtro or" +
            "  lower(pj.razaosocial) like :filtro or " +
            "  cast(pj.cnpj as text) like :filtro" +
            " )" +
            " and sol.statusprocesso = :status";

        Query q = em.createNativeQuery(sql, SolicitacaoProcesso.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setParameter("status", statusProcesso.name());
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<SolicitacaoProcesso> completarSolicitacaoProcessoPorDescricaoOrNumero(String filtro) {
        String sql = "select sol.* from solicitacaoprocesso sol " +
            "  inner join pessoa p on p.id = sol.pessoa_id " +
            "  left join pessoafisica pf on p.id = pf.id " +
            "  left join pessoajuridica pj on p.id = pj.id " +
            "where ( " +
            "  lower(sol.descricao) like :filtro or " +
            "  cast(sol.numero as text) like :filtro or " +
            "  lower(pf.nome) like :filtro or " +
            "  cast(pf.cpf as text) like :filtro or" +
            "  lower(pj.razaosocial) like :filtro or " +
            "  cast(pj.cnpj as text) like :filtro" +
            " )";

        Query q = em.createNativeQuery(sql, SolicitacaoProcesso.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<SolicitacaoProcesso> recuperarSolicitacaoPorPessoa(PessoaFisica pessoaFisica) {
        String sql = "select sol.* from solicitacaoprocesso sol" +
            "  where sol.pessoa_id = :pessoa";

        Query q = em.createNativeQuery(sql, SolicitacaoProcesso.class);
        q.setParameter("pessoa", pessoaFisica.getId());
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }
}
