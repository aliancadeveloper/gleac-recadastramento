package br.com.gleac.negocio;

import br.com.gleac.entidade.financeiro.DadosRetornoPgHiper;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by renatoromanini on 04/10/2017.
 */
@Repository
public class DadosRetornoPgHiperRepository extends AbstractRepository<DadosRetornoPgHiper> {

    @PersistenceContext
    private EntityManager em;

    public DadosRetornoPgHiperRepository() {
        super(DadosRetornoPgHiper.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
