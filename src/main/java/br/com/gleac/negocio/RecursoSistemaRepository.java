package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Recurso;
import br.com.gleac.entidade.seguranca.RecursoSistema;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;


@Repository
@Transactional
public class RecursoSistemaRepository extends AbstractRepository<RecursoSistema> {

    public static final Logger log = LoggerFactory.getLogger(RecursoSistemaRepository.class);

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public RecursoSistemaRepository() {
        super(RecursoSistema.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public List<RecursoSistema> buscarTodosRecursosSistema() {
        String sql = " select rs.* from recursosistema rs ";

        Query q = em.createNativeQuery(sql, RecursoSistema.class);

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public RecursoSistema buscarRecursosSistemaPorRef(String ref) {
        String sql = " select rs.* from recursosistema rs " +
            "where rs.ref = :ref ";
        Query q = em.createNativeQuery(sql, RecursoSistema.class);

        q.setParameter("ref", ref);
        q.setMaxResults(1);

        try {
            return (RecursoSistema) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException nue) {
            throw new NonUniqueResultException("Erro ao carregar os recursos, mais de um recurso com a mesma referencia");
        }
    }

    public List<RecursoSistema> buscarRecursoSistemaPorUsuarioAndLoja(Usuario usuario, Loja loja) {
        String sql = "select distinct rs.* from usuario usu\n" +
            "  inner join grupousuariosistema gus on gus.usuario_id = usu.id\n" +
            "  inner join grupousuario gp on gp.id = gus.grupousuario_id\n" +
            "  inner join grupousuarioacesso gua on gua.grupousuario_id = gp.id\n" +
            "  inner join grupoacesso gpa  on gpa.id = gua.grupoacesso_id\n" +
            "  inner join recurso rc on rc.grupoacesso_id = gpa.id\n" +
            "  inner join recursosistema rs on rs.id = rc.recursosistema_id\n" +
            "where usu.id = :usuarioID and gpa.loja_id = :lojaID ";

        Query q = em.createNativeQuery(sql, RecursoSistema.class);
        q.setParameter("usuarioID", usuario.getId());
        q.setParameter("lojaID", loja.getId());

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public List<Recurso> buscarRecursoPorUsuarioAndLoja(Usuario usuario, Loja loja) {
        String sql = "select distinct rc.* from usuario usu\n" +
            "  inner join grupousuariosistema gus on gus.usuario_id = usu.id\n" +
            "  inner join grupousuario gp on gp.id = gus.grupousuario_id\n" +
            "  inner join grupousuarioacesso gua on gua.grupousuario_id = gp.id\n" +
            "  inner join grupoacesso gpa  on gpa.id = gua.grupoacesso_id\n" +
            "  inner join recurso rc on rc.grupoacesso_id = gpa.id\n" +
            "  inner join recursosistema rs on rs.id = rc.recursosistema_id\n" +
            "where usu.id = :usuarioID and gpa.loja_id = :lojaID ";

        Query q = em.createNativeQuery(sql, Recurso.class);
        q.setParameter("usuarioID", usuario.getId());
        q.setParameter("lojaID", loja.getId());

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public List<RecursoSistema> buscarRecursoSistemaPorUsuario(Usuario usuario) {
        String sql = " select distinct rs.* from usuario usu\n" +
            "  inner join grupousuariosistema gus on gus.usuario_id = usu.id\n" +
            "  inner join grupousuario gp on gp.id = gus.grupousuario_id\n" +
            "  inner join grupousuarioacesso gua on gua.grupousuario_id = gp.id\n" +
            "  inner join grupoacesso gpa  on gpa.id = gua.grupoacesso_id\n" +
            "  inner join recurso rc on rc.grupoacesso_id = gpa.id\n" +
            "  inner join recursosistema rs on rs.id = rc.recursosistema_id\n" +
            "where usu.id = :usuarioID and gpa.loja_id = :lojaID ";

        Query q = em.createNativeQuery(sql, RecursoSistema.class);
        q.setParameter("usuarioID", usuario.getId());

        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public List<RecursoSistema> completarRecursoSistemaPorDescricao(String filter) {
        String sql = " select rs.* from recursosistema rs " +
            " where lower(rs.descricao) like :filter or lower(rs.path) like :filter ";

        Query q = em.createNativeQuery(sql, RecursoSistema.class);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    public void salvarRecurso(RecursoSistema rs) {
        try {
            if (rs.getRef() != null && rs.getChecksum() != null) {
                RecursoSistema recursoSistema = buscarRecursosSistemaPorRef(rs.getRef());
                if (recursoSistema == null) {
                    em.merge(rs);
                }
                if ((recursoSistema != null
                    && recursoSistema.getChecksum() != null
                    && rs.getChecksum() != null)
                    && isChecksumDiff(rs, recursoSistema)
                    && isRefEquals(rs, recursoSistema)) {
                    em.merge(rs);
                }

            }
        } catch (PersistenceException pe) {
            log.error("Não foi possível salvar o recurso do sistema [{}] ", rs);
        }
    }

    private boolean isRefEquals(RecursoSistema rs, RecursoSistema recursoSistema) {
        return rs.getRef().equals(recursoSistema.getRef());
    }

    private boolean isChecksumDiff(RecursoSistema rs, RecursoSistema recursoSistema) {
        return !recursoSistema.getChecksum().equals(rs.getChecksum());
    }
}
