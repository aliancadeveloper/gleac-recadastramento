package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.enums.TipoServico;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca.
 */
@Repository
public class ConfiguracaoServicoRepository extends AbstractRepository<ConfiguracaoServico> {

    @Autowired
    private EntityManager em;

    public ConfiguracaoServicoRepository() {
        super(ConfiguracaoServico.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public ConfiguracaoServico recuperar(Class entidade, Object id) {
        ConfiguracaoServico config = getObjeto((Long) id);
        config.getConfiguracaoServicoFormularios().size();
        return config;
    }

    public ConfiguracaoServico getObjeto(Long id) {
        return (ConfiguracaoServico) em.createNativeQuery(
            " select configServ.* from configuracaoservico configServ  " +
                " inner join configuracaoservicoformulario configForm on configServ.id = configForm.configuracaoservico_id " +
                " where configServ.id = :id ", ConfiguracaoServico.class)
            .setParameter("id", id)
            .getSingleResult();
    }

    @Transactional
    public List<Servico> completarServicosByDescricao(String filtro) {
        String sql = " select serv.* from servico serv " +
            " where lower(serv.descricao) like :filtro ";

        Query q = em.createNativeQuery(sql, Servico.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Servico> completarServicosLessConfiguracaoByDescricao(String filtro) {
        String sql = " select serv.* from servico serv " +
            " where not exists (select 1 from servico ser " +
            " inner join configuracaoservico config on config.servico_id = ser.id" +
            " where  serv.id = ser.id )" +
            " and lower(serv.descricao) like :filtro ";

        Query q = em.createNativeQuery(sql, Servico.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Servico> completarServicosWithConfiguracaoByDescricaoAndTipo(String filtro, TipoServico tipoServico) {
        String sql = " select serv.* from servico serv " +
            " inner join configuracaoservico config on serv.id = config.servico_id " +
            " where lower(serv.descricao) like :filtro " +
            " and to_date(:data ,'dd/MM/yyyy') between config.dataInicial and coalesce(config.dataFinal, to_date(:data ,'dd/MM/yyyy')) ";
        if (tipoServico != null) {
            sql += " and serv.tipoServico = :tipo";
        }

        Query q = em.createNativeQuery(sql, Servico.class);
        q.setParameter("data", Util.sdf.format(new Date()));
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (tipoServico != null) {
            q.setParameter("tipo", tipoServico.name());
        }
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public ConfiguracaoServico buscarConfiguracao(Date data, Servico servico) {
        String sql = " select config.* from ConfiguracaoServico config " +
            " where to_date(:data ,'dd/MM/yyyy') between config.dataInicial and coalesce(config.dataFinal, to_date(:data ,'dd/MM/yyyy') )" +
            " and config.servico_id = :servico";

        Query q = em.createNativeQuery(sql, ConfiguracaoServico.class);
        q.setParameter("data", Util.sdf.format(data));
        q.setParameter("servico", servico.getId());
        q.setMaxResults(1);

        try {
            return (ConfiguracaoServico) q.getSingleResult();
        } catch (NoResultException nre) {
            throw new ExceptionGenerica("Não foi encontrada nenhuma Configuração de Serviço Vigente! ");
        }
    }
}
