package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by renato on 24/07/17.
 */
public interface NotificacaoJPARepository extends CrudRepository<Notificacao, Long> {

    List<Notificacao> findAllByUsuarioAndVisualizado(Usuario usuario, Boolean visualizado);

    Integer countAllByUsuarioAndVisualizado(Usuario usuario, Boolean visualizado);
}
