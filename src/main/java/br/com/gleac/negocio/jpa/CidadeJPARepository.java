package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.comum.Cidade;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by israeleriston on 24/06/16.
 */
public interface CidadeJPARepository extends CrudRepository<Cidade,Long> {

    Cidade findAllCidadeByNome(String nome);
}
