package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Potencia;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by renatoromanini on 10/06/17.
 */
public interface LojaJPARepository extends JpaRepository<Loja, Long> {

    List<Loja> findAllByPotencia(Potencia potencia);

    List<Loja> findTop100ByOrderByNumero();
}
