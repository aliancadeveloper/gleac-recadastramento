package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.seguranca.Pessoa;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by israeleriston on 24/06/16.
 */
public interface PessoaJPARepository extends CrudRepository<Pessoa,Long> {
}
