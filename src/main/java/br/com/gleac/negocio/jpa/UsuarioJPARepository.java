package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.seguranca.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by israeleriston on 04/06/16.
 */
public interface UsuarioJPARepository  extends JpaRepository<Usuario, Long> {

    Optional<Usuario> findOneByLogin(String login);


    @Override
    void delete(Usuario t);
}
