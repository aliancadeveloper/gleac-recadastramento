package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.comum.Macon;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by renatoromanini on 21/10/17.
 */
public interface MaconJPARepository extends JpaRepository<Macon, Long> {

}
