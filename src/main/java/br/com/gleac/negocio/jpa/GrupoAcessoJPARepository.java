package br.com.gleac.negocio.jpa;

import br.com.gleac.entidade.seguranca.GrupoAcesso;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by israeleriston on 01/06/16.
 */
public interface GrupoAcessoJPARepository extends CrudRepository<GrupoAcesso, Long> {

    @Override
    GrupoAcesso findOne(Long aLong);

    @Override
    GrupoAcesso save(GrupoAcesso selecionado);
}
