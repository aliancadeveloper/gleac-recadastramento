package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.supers.AbstractRepository;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by renatoromanini on 24/05/16.
 */
@Repository
public class ArquivoRepository extends AbstractRepository<Arquivo>  {

    @PersistenceContext
    private EntityManager em;

    public ArquivoRepository() {
        super(Arquivo.class);
    }

    public EntityManager getEntityManager() {
        return em;
    }


    @Transactional
    public OutputStream prepararOutputStream(Long id, OutputStream out) throws Exception {
        if (id != null) {
            Arquivo arquivo = em.find(Arquivo.class, id);
            if (arquivo != null) {
                out.write(arquivo.getArquivo());
            }
        }
        return out;
    }

    public Arquivo criarArquivo(FileUploadEvent event) throws IOException {
        Arquivo arquivo = new Arquivo();
        arquivo.setTipo(event.getFile().getContentType());
        arquivo.setNome(event.getFile().getFileName());
        byte[] content = IOUtils.toByteArray(event.getFile().getInputstream());
        arquivo.setArquivo(content);
        return arquivo;
    }
}

