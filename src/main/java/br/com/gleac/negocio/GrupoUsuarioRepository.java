package br.com.gleac.negocio;

import br.com.gleac.entidade.seguranca.GrupoUsuario;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional
public class GrupoUsuarioRepository extends AbstractRepository<GrupoUsuario> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public GrupoUsuarioRepository() {
        super(GrupoUsuario.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public GrupoUsuario recuperar(Class entidade, Object id) {
        GrupoUsuario gu = em.find(GrupoUsuario.class, id);
        gu.getUsuarios().size();
        gu.getGruposAcessos().size();
        return gu;
    }

    @Override
    public void salvar(GrupoUsuario entity) {
        em.merge(entity);
    }
}
