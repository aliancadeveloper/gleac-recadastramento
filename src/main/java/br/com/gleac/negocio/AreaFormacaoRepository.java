package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.AreaFormacao;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AreaFormacaoRepository extends AbstractRepository<AreaFormacao> {

    @PersistenceContext
    private EntityManager em;

    public AreaFormacaoRepository() {
        super(AreaFormacao.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<AreaFormacao> buscarAreaFormacaoPorNome(String filter){
        String hql = " select a from AreaFormacao a where lower(a.nome) like :filter ";
        Query q = em.createQuery(hql);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }
}
