package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.configuracao.ConfiguracaoProduto;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

@Repository
public class ConfiguracaoProdutoRepository extends AbstractRepository<ConfiguracaoProduto> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public ConfiguracaoProdutoRepository() {
        super(ConfiguracaoProduto.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public ConfiguracaoProduto recuperar(Class entidade, Object id) {
        ConfiguracaoProduto config = getObjeto((Long) id);
        return config;
    }

    public ConfiguracaoProduto getObjeto(Long id) {
        return (ConfiguracaoProduto)  em.createNativeQuery(
            " select configProd.* from configuracaoproduto configProd  " +
                " where configProd.id = :id ", ConfiguracaoProduto.class)
            .setParameter("id", id)
            .getSingleResult();
    }

    @Transactional
    public List<Produto> completarProdutoLessConfiguracaoByDescricao(String filtro) {
        String sql = " select prod.* from produto prod " +
            " where not exists (select 1 from produto ser " +
            " inner join configuracaoproduto config on config.produto_id = prod.id" +
            " where  prod.id = ser.id )" +
            " and lower(prod.descricao) like :filtro ";

        Query q = em.createNativeQuery(sql, Produto.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Produto> completarProdutoWithConfiguracaoByDescricao(String filtro) {
        String sql = " select prod.* from produto prod " +
            " inner join configuracaoproduto config on prod.id = config.produto_id " +
            " where prod.descricao like :filtro ";

        Query q = em.createNativeQuery(sql, Produto.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public ConfiguracaoProduto buscarConfiguracao(Date data, Produto produto) {
        String sql = " select config.* from ConfiguracaoProduto config " +
            " where to_date(:data ,'dd/MM/yyyy') between config.dataInicial and coalesce(config.dataFinal, to_date(:data ,'dd/MM/yyyy') )" +
            " and config.produto_id = :produto";

        Query q = em.createNativeQuery(sql, ConfiguracaoProduto.class);
        q.setParameter("data", Util.sdf.format(data));
        q.setParameter("produto", produto.getId());
        q.setMaxResults(1);

        try {

            return (ConfiguracaoProduto) q.getSingleResult();
        } catch (NoResultException nre) {
            throw new ExceptionGenerica("Não foi encontrada nenhuma Configuração de Produto Vigente! ");
        }
    }
}
