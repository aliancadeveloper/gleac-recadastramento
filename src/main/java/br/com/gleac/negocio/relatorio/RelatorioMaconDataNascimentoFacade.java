package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconDataNascimentoAuxiliar;
import br.com.gleac.util.Util;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconDataNascimentoFacade {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconDataNascimentoFacade() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconDataNascimentoAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconDataNascimentoAuxiliar> relatorioMaconDataNascimentoAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioMaconDataNascimentoAuxiliar>> resultado = new AsyncResult<>(relatorioMaconDataNascimentoAuxiliarList);
        String sql = " SELECT " +
            "  initcap(pf.nome)                           AS nome, " +
            "  to_char(pf.datanascimento, 'dd/mm/yyyy')   AS data_nascimento, " +
            "  cast(extract(YEARS FROM age(pf.datanascimento)) as integer) AS idade, " +
            "  (SELECT CASE WHEN length(regexp_replace(tel.telefone, '[^\\d]', '', 'g')) <= 10 " +
            "    THEN regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{4})([\\d{4}])', '(\\1) \\2 - \\3') " +
            "          ELSE regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{5})([\\d{4}])', " +
            "                              '(\\1) \\2 - \\3') END AS telefone " +
            "   FROM telefone tel " +
            "   WHERE tel.pessoa_id = p.id " +
            "   LIMIT 1), " +
            "  lower(p.email)                             AS email " +
            " FROM macon m " +
            "  INNER JOIN pessoafisica pf ON pf.id = m.pessoafisica_id " +
            "  INNER JOIN pessoa p ON p.id = pf.id " +
            "  LEFT JOIN HISTORICOMACONICO HM ON HM.MACON_ID = M.ID AND HM.DATAHISTORICO = (SELECT MAX(H.DATAHISTORICO) " +
            "                                                                               FROM HISTORICOMACONICO H " +
            "                                                                               WHERE H.MACON_ID = M.ID) " +
            "  LEFT JOIN LOJA L ON L.ID = HM.LOJA_ID " +
            "  LEFT JOIN PESSOAJURIDICA PJ ON PJ.ID = L.PESSOAJURIDICA_ID ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);

        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconDataNascimentoAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconDataNascimentoAuxiliar relatorioMaconDataNascimentoAuxiliar = new RelatorioMaconDataNascimentoAuxiliar();
            relatorioMaconDataNascimentoAuxiliar.setObreiro((String) objeto[0]);
            relatorioMaconDataNascimentoAuxiliar.setDataNascimento((String) objeto[1]);
            relatorioMaconDataNascimentoAuxiliar.setIdade((Integer) objeto[2]);
            relatorioMaconDataNascimentoAuxiliar.setTelefone((String) objeto[3]);
            relatorioMaconDataNascimentoAuxiliar.setEmail((String) objeto[4]);
            resultado.get().add(relatorioMaconDataNascimentoAuxiliar);
        }
    }
}
