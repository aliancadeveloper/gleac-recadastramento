package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconEndTelEmailAuxiliar;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconEndTelEmailRepository {
    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconEndTelEmailRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconEndTelEmailAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconEndTelEmailAuxiliar> relatorioMaconEndTelEmailAuxiliars, String filtros) {
        AsyncResult<List<RelatorioMaconEndTelEmailAuxiliar>> resultado = new AsyncResult<>(relatorioMaconEndTelEmailAuxiliars);
        String sql = " SELECT " +
            "  initcap(pf.nome) AS nome, " +
            "  (SELECT initcap(endereco.logradouro) || CASE WHEN " +
            "    trim(endereco.numero) <> '' " +
            "    THEN ', ' || initcap(endereco.numero) " +
            "                                 ELSE '' END " +
            "   FROM endereco endereco " +
            "   WHERE endereco.pessoa_id = p.id " +
            "   LIMIT 1) as endereco, " +
            "  (SELECT CASE WHEN length(regexp_replace(tel.telefone, '[^\\d]', '', 'g'))  <= 10 " +
            "    THEN regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{4})([\\d{4}])', '(\\1) \\2 - \\3') " +
            "          ELSE regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{5})([\\d{4}])', " +
            "                              '(\\1) \\2 - \\3') END AS telefone " +
            "   FROM telefone tel " +
            "   WHERE tel.pessoa_id = p.id " +
            "   LIMIT 1), " +
            "  lower(p.email) as email " +
            "FROM macon M " +
            "  INNER JOIN pessoafisica pf ON pf.id = M.pessoafisica_id " +
            "  INNER JOIN pessoa p ON p.id = pf.id " +
            "  LEFT JOIN HISTORICOMACONICO HM ON HM.MACON_ID = M.ID AND HM.DATAHISTORICO = (SELECT MAX(H.DATAHISTORICO) " +
            "                                                                               FROM HISTORICOMACONICO H " +
            "                                                                               WHERE H.MACON_ID = M.ID) " +
            "  LEFT JOIN LOJA L ON L.ID = HM.LOJA_ID " +
            "  LEFT JOIN PESSOAJURIDICA PJ ON PJ.ID = L.PESSOAJURIDICA_ID ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);
        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconEndTelEmailAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconEndTelEmailAuxiliar relatorioMaconEndTelEmailAuxiliar = new RelatorioMaconEndTelEmailAuxiliar();
            relatorioMaconEndTelEmailAuxiliar.setObreiro((String) objeto[0]);
            relatorioMaconEndTelEmailAuxiliar.setEndereco((String) objeto[1]);
            relatorioMaconEndTelEmailAuxiliar.setTelefone((String) objeto[2]);
            relatorioMaconEndTelEmailAuxiliar.setEmail((String) objeto[3]);
            resultado.get().add(relatorioMaconEndTelEmailAuxiliar);
        }
    }
}
