package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioFornecedorAuxiliar;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioFornecedorRepository {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioFornecedorRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioFornecedorAuxiliar>> buscarDadosRelatorio(List<RelatorioFornecedorAuxiliar> relatorioFornecedorAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioFornecedorAuxiliar>> resultado = new AsyncResult<>(relatorioFornecedorAuxiliarList);
        String sql = " SELECT " +
            "  F.NUMEROCONTRATO                     AS CONTRATO, " +
            "  COALESCE(PF.NOME, PJ.NOMEFANTASIA)    AS FORNECEDOR, " +
            "  COALESCE(PF.CPF, PJ.CNPJ)            AS IDENT_FORNECEDOR, " +
            "  F.PRODUTO                            AS PRODUTO, " +
            "  P.EMAIL                              AS EMAIL, " +
            "  (SELECT T.TELEFONE " +
            "   FROM TELEFONE T " +
            "     INNER JOIN PESSOA PESSOA ON PESSOA.ID = T.PESSOA_ID " +
            "   WHERE PESSOA.ID = P.ID " +
            "   LIMIT 1)                            AS TELEFONE, " +
            "  (SELECT T.PESSOACONTATO " +
            "   FROM TELEFONE T " +
            "     INNER JOIN PESSOA PESSOA ON PESSOA.ID = T.PESSOA_ID " +
            "   WHERE PESSOA.ID = P.ID " +
            "   LIMIT 1)                            AS CONTATO_TELEFONE " +
            "FROM FORNECEDOR F " +
            "  INNER JOIN PESSOA P ON P.ID = F.PESSOA_ID " +
            "  LEFT JOIN PESSOAFISICA PF ON PF.ID = F.PESSOA_ID " +
            "  LEFT JOIN PESSOAJURIDICA PJ ON PJ.ID = F.PESSOA_ID " +
            "  INNER JOIN TELEFONE T ON T.PESSOA_ID = P.ID AND T.PRINCIPAL IS NOT NULL ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        sql += " ORDER BY 2 ";
        Query q = em.createNativeQuery(sql);
        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioFornecedorAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioFornecedorAuxiliar relatorioFornecedorAuxiliar = new RelatorioFornecedorAuxiliar();
            relatorioFornecedorAuxiliar.setContrato((String) objeto[0]);
            relatorioFornecedorAuxiliar.setFornecedor((String) objeto[1]);
            relatorioFornecedorAuxiliar.setIdentFornecedor((String) objeto[2]);
            relatorioFornecedorAuxiliar.setProduto((String) objeto[3]);
            relatorioFornecedorAuxiliar.setEmail((String) objeto[4]);
            relatorioFornecedorAuxiliar.setTelefone((String) objeto[5]);
            relatorioFornecedorAuxiliar.setContato((String) objeto[6]);
            resultado.get().add(relatorioFornecedorAuxiliar);
        }
    }
}
