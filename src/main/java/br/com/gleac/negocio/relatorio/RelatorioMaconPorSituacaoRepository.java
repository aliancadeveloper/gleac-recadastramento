package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconPorSituacaoAuxiliar;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconPorSituacaoRepository {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconPorSituacaoRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconPorSituacaoAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconPorSituacaoAuxiliar> relatorioMaconPorSituacaoAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioMaconPorSituacaoAuxiliar>> resultado = new AsyncResult<>(relatorioMaconPorSituacaoAuxiliarList);
        String sql = "  SELECT  " +
            "  'Loja - ' || numero AS LOJA,  " +
            "  cast(count((SELECT mac.id  " +
            "   FROM macon mac  " +
            "   WHERE mac.id = historico.macon_id AND mac.situacao = 'REGULAR')) as Integer) as REGULAR,  " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'IRREGULAR')) as Integer) as IRREGULAR,  " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'COBERTURA_DIREITOS')) as Integer) as COBERTURA_DIREITOS,  " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'QUIT_PLACET')) as Integer) as QUIT_PLACET,  " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'EMERITO')) as Integer) as EMERITO, " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'FALECIDO')) as Integer) as FALECIDO, " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'CERTIFICADO_GRAU')) as Integer) as CERTIFICADO_GRAU, " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'EXPULSO')) as Integer) as EXPULSO, " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'PAST_GRA_MESTRE')) as Integer) as PAST_GRA_MESTRE, " +
            "  cast(count((SELECT mac.id  " +
            "         FROM macon mac  " +
            "         WHERE mac.id = historico.macon_id AND mac.situacao = 'BENEMERITO')) as Integer) as BENEMERITO, " +
            "  cast(count(m.id) as Integer) AS TOTAL  " +
            "FROM loja l  " +
            "  INNER JOIN historicomaconico historico ON historico.loja_id = l.id  " +
            "  INNER JOIN macon m ON m.id = historico.macon_id  " +
            "WHERE historico.datahistorico = (SELECT max(h.datahistorico)  " +
            "                                 FROM historicomaconico h  " +
            "                                 WHERE h.macon_id = m.id)  ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);
        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconPorSituacaoAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconPorSituacaoAuxiliar relatorioMaconPorSituacaoAuxiliar = new RelatorioMaconPorSituacaoAuxiliar();
            relatorioMaconPorSituacaoAuxiliar.setLoja((String) objeto[0]);
            relatorioMaconPorSituacaoAuxiliar.setRegular((Integer) objeto[1]);
            relatorioMaconPorSituacaoAuxiliar.setIrregular((Integer) objeto[2]);
            relatorioMaconPorSituacaoAuxiliar.setCoberturaDireito((Integer) objeto[3]);
            relatorioMaconPorSituacaoAuxiliar.setQuitPlacet((Integer) objeto[4]);
            relatorioMaconPorSituacaoAuxiliar.setEmerito((Integer) objeto[5]);
            relatorioMaconPorSituacaoAuxiliar.setFalecido((Integer) objeto[6]);
            relatorioMaconPorSituacaoAuxiliar.setCertificadoGrau((Integer) objeto[7]);
            relatorioMaconPorSituacaoAuxiliar.setExpulso((Integer) objeto[8]);
            relatorioMaconPorSituacaoAuxiliar.setPastGraoMestre((Integer) objeto[9]);
            relatorioMaconPorSituacaoAuxiliar.setBenemerito((Integer) objeto[10]);
            relatorioMaconPorSituacaoAuxiliar.setTotal((Integer) objeto[11]);
            resultado.get().add(relatorioMaconPorSituacaoAuxiliar);
        }
    }
}
