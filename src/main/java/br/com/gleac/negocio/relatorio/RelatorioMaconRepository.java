package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconAuxiliar;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconRepository {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconAuxiliar> relatorioMaconAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioMaconAuxiliar>> resultado = new AsyncResult<>(relatorioMaconAuxiliarList);
        String sql = " SELECT DISTINCT " +
            "  CAST(M.IDENTIFICACAO AS INTEGER)                           AS CADASTRO, " +
            "  PF.NOME || ' - ' || PF.CPF                AS OBREIRO, " +
            "  COALESCE(PJ.NOMEFANTASIA, HM.LOJAEXTERNA) AS LOJA, " +
            "  M.GRAU                                    AS GRAU, " +
            "  M.SITUACAO                                AS SITUACAOMACON, " +
            "  HM.SITUACAOHISTORICO                      AS SITUACAOHISTORICO " +
            "FROM MACON M " +
            "  INNER JOIN PESSOAFISICA PF ON PF.ID = M.PESSOAFISICA_ID " +
            "  LEFT JOIN HISTORICOMACONICO HM ON HM.MACON_ID = M.ID AND HM.DATAHISTORICO = (SELECT MAX(H.DATAHISTORICO) " +
            "                                                                               FROM HISTORICOMACONICO H " +
            "                                                                               WHERE H.MACON_ID = M.ID) " +
            "  LEFT JOIN LOJA L ON L.ID = HM.LOJA_ID " +
            "  LEFT JOIN PESSOAJURIDICA PJ ON PJ.ID = L.PESSOAJURIDICA_ID ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);
        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconAuxiliar relatorioMaconAuxiliar = new RelatorioMaconAuxiliar();
            relatorioMaconAuxiliar.setCadastro((Integer) objeto[0]);
            relatorioMaconAuxiliar.setObreiro((String) objeto[1]);
            relatorioMaconAuxiliar.setLoja((String) objeto[2]);
            relatorioMaconAuxiliar.setGrau((String) objeto[3]);
            relatorioMaconAuxiliar.setSituacaoMacon((String) objeto[4]);
            relatorioMaconAuxiliar.setSituacaoHistorico((String) objeto[5]);
            resultado.get().add(relatorioMaconAuxiliar);
        }
    }
}
