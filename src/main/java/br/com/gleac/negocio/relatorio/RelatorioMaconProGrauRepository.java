package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconPorGrauAuxiliar;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconProGrauRepository {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconProGrauRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconPorGrauAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconPorGrauAuxiliar> relatorioMaconPorGrauAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioMaconPorGrauAuxiliar>> resultado = new AsyncResult<>(relatorioMaconPorGrauAuxiliarList);
        String sql = " SELECT " +
            "  'Loja - ' || numero AS LOJA, " +
            "  cast(count((SELECT mac.id " +
            "   FROM macon mac " +
            "   WHERE mac.id = historico.macon_id AND mac.grau = 'APRENDIZ')) as Integer) as APRENDIZ, " +
            "  cast(count((SELECT mac.id " +
            "         FROM macon mac " +
            "         WHERE mac.id = historico.macon_id AND mac.grau = 'COMPANHEIRO')) as Integer) as COMPANHEIRO, " +
            "  cast(count((SELECT mac.id " +
            "         FROM macon mac " +
            "         WHERE mac.id = historico.macon_id AND mac.grau = 'MESTRE')) as Integer) as MESTRE, " +
            "  cast(count((SELECT mac.id " +
            "         FROM macon mac " +
            "         WHERE mac.id = historico.macon_id AND mac.grau = 'MESTRE_INSTALADO')) as Integer) as MESTRE_INSTALADO, " +
            "  cast(count((SELECT mac.id " +
            "         FROM macon mac " +
            "         WHERE mac.id = historico.macon_id AND mac.grau = 'GRAO_MESTRE')) as Integer) as GRAO_MESTRE, " +
            "  cast(count(m.id) as Integer) AS TOTAL " +
            "FROM loja l " +
            "  INNER JOIN historicomaconico historico ON historico.loja_id = l.id " +
            "  INNER JOIN macon m ON m.id = historico.macon_id " +
            "WHERE historico.datahistorico = (SELECT max(h.datahistorico) " +
            "                                 FROM historicomaconico h " +
            "                                 WHERE h.macon_id = m.id) ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);
        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconPorGrauAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconPorGrauAuxiliar relatorioMaconPorGrauAuxiliar = new RelatorioMaconPorGrauAuxiliar();
            relatorioMaconPorGrauAuxiliar.setLoja((String) objeto[0]);
            relatorioMaconPorGrauAuxiliar.setAprendiz((Integer) objeto[1]);
            relatorioMaconPorGrauAuxiliar.setCompanheiro((Integer) objeto[2]);
            relatorioMaconPorGrauAuxiliar.setMestre((Integer) objeto[3]);
            relatorioMaconPorGrauAuxiliar.setMestraInstalado((Integer) objeto[4]);
            relatorioMaconPorGrauAuxiliar.setGraoMestre((Integer) objeto[5]);
            relatorioMaconPorGrauAuxiliar.setTotal((Integer) objeto[6]);
            resultado.get().add(relatorioMaconPorGrauAuxiliar);
        }
    }
}
