package br.com.gleac.negocio.relatorio;

import br.com.gleac.entidade.auxiliares.RelatorioMaconProfissaoAuxiliar;
import br.com.gleac.util.Util;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.concurrent.Future;

@Repository
public class RelatorioMaconProfissaoRepository {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public EntityManager getEntityManager() {
        return em;
    }

    public RelatorioMaconProfissaoRepository() {
    }

    @Async
    @Transactional
    public Future<List<RelatorioMaconProfissaoAuxiliar>> buscarDadosRelatorio(List<RelatorioMaconProfissaoAuxiliar> relatorioMaconProfissaoAuxiliarList, String filtros) {
        AsyncResult<List<RelatorioMaconProfissaoAuxiliar>> resultado = new AsyncResult<>(relatorioMaconProfissaoAuxiliarList);
        String sql = " SELECT " +
            "  initcap(pf.nome)        AS nome, " +
            "  initcap(profissao.nome) AS profissao, " +
            "  (SELECT CASE WHEN length(regexp_replace(tel.telefone, '[^\\d]', '', 'g'))  <= 10 " +
            "    THEN regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{4})([\\d{4}])', '(\\1) \\2 - \\3') " +
            "          ELSE regexp_replace(regexp_replace(tel.telefone, '[^\\d]', '', 'g'), '([\\d]{2})([\\d]{5})([\\d{4}])', " +
            "                              '(\\1) \\2 - \\3') END AS telefone " +
            "   FROM telefone tel " +
            "   WHERE tel.pessoa_id = p.id " +
            "   LIMIT 1), " +
            "  lower(p.email)          AS email " +
            "FROM macon m " +
            "  INNER JOIN pessoafisica pf ON pf.id = m.pessoafisica_id " +
            "  INNER JOIN pessoa p ON p.id = pf.id " +
            " " +
            "  LEFT JOIN HISTORICOMACONICO HM ON HM.MACON_ID = M.ID AND HM.DATAHISTORICO = (SELECT MAX(H.DATAHISTORICO) " +
            "                                                                               FROM HISTORICOMACONICO H " +
            "                                                                               WHERE H.MACON_ID = M.ID) " +
            "  LEFT JOIN LOJA L ON L.ID = HM.LOJA_ID " +
            "  LEFT JOIN PESSOAJURIDICA PJ ON PJ.ID = L.PESSOAJURIDICA_ID " +
            "  INNER JOIN documentopessoal dp ON DP.pessoafisica_id = PF.ID " +
            "  INNER JOIN carteiratrabalho ctps ON ctps.id = dp.id " +
            "  INNER JOIN profissao profissao ON profissao.id = ctps.profissao_id ";
        if (StringUtils.isNotEmpty(filtros)) {
            sql += filtros;
        }
        Query q = em.createNativeQuery(sql);

        List resultList = q.getResultList();

        try {
            if (resultList != null && !resultList.isEmpty()) {
                criarObjetosRetorno(resultado, resultList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    private void criarObjetosRetorno(AsyncResult<List<RelatorioMaconProfissaoAuxiliar>> resultado, List resultList) throws InterruptedException, java.util.concurrent.ExecutionException {
        for (Object o : resultList) {
            Object[] objeto = (Object[]) o;
            RelatorioMaconProfissaoAuxiliar relatorioMaconProfissaoAuxiliar = new RelatorioMaconProfissaoAuxiliar();
            relatorioMaconProfissaoAuxiliar.setNome((String) objeto[0]);
            relatorioMaconProfissaoAuxiliar.setProfissao((String) objeto[1]);
            relatorioMaconProfissaoAuxiliar.setTelefone((String) objeto[2]);
            relatorioMaconProfissaoAuxiliar.setEmail((String) objeto[3]);
            resultado.get().add(relatorioMaconProfissaoAuxiliar);
        }
    }
}
