package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.ParecerSolicitacaoProcesso;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.TipoNotificacao;
import br.com.gleac.enums.processo.StatusParecer;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.service.SistemaService;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;

/**
 * Created by zaca.
 */
@Repository
public class ParecerSolicitacaoProcessoRepository extends AbstractRepository<ParecerSolicitacaoProcesso> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;
    @Autowired
    private SingletonGeradorCodigo singletonGeradorCodigo;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private GestaoRepository gestaoRepository;
    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;

    public ParecerSolicitacaoProcessoRepository() {
        super(ParecerSolicitacaoProcesso.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    @Transactional
    public void salvarNovo(ParecerSolicitacaoProcesso entity) {
        entity.setNumero(singletonGeradorCodigo.getProximoCodigo(entity.getClass(), "numero").longValue());
        super.salvarNovo(entity);
        notificacaoRepository.marcarNotificaoVisualizada(entity.getSolicitacaoProcesso().getId());

        Macon tesoureiro = gestaoRepository.buscarGestaoVigente().getTesoureiro();
        if (tesoureiro == null) {
            throw new ExceptionGenerica("Nenhum tesoureiro foi configurado para a gestão vigente.");
        }
        PessoaFisica pessoaFisica = tesoureiro.getPessoaFisica();
        Usuario usuario = usuarioRepository.recuperarUsuarioPorPessoa(pessoaFisica);
        if (StatusParecer.DEFERIDO.equals(entity.getStatusParecer())) {
            entity.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.PARECER);
            solicitacaoProcessoRepository.salvar(entity.getSolicitacaoProcesso());
            notificacaoRepository.salvarNovo(new Notificacao(entity.getEmitidoEm(),
                usuario,
                TipoNotificacao.PROCESSO,
                "Parecer da solicitação processo " + entity.getSolicitacaoProcesso().getNumero() + " realizado!",
                "/admin/processo-financeiro/nova-por-solicitacao/" + entity.getSolicitacaoProcesso().getId() + "/"));
        }
        if (StatusParecer.INDEFIRIDO.equals(entity.getStatusParecer())) {
            entity.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.INDEFERIDO);
            solicitacaoProcessoRepository.salvar(entity.getSolicitacaoProcesso());
        }

    }

    @Transactional
    public ProcessoFinanceiro salvarNovoGerandoProcesso(ParecerSolicitacaoProcesso entity) {
        entity.setNumero(singletonGeradorCodigo.getProximoCodigo(entity.getClass(), "numero").longValue());
        Date vencimentoBoleto = entity.getVencimentoBoleto();

        super.salvarNovo(entity);
        if (StatusParecer.DEFERIDO.equals(entity.getStatusParecer())) {
            ProcessoFinanceiro processo = new ProcessoFinanceiro();
            processo.setVencimentoBoleto(vencimentoBoleto);
            processo.setSolicitacaoProcesso(entity.getSolicitacaoProcesso());

            processoFinanceiroRepository.salvarNovo(processo);
            return processo;
        }
        return null;
    }
}
