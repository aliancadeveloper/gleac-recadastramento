package br.com.gleac.negocio;

import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.processo.Placet;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.GestaoNotCurrentException;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by zaca.
 */
@Repository
public class PlacetRepository extends AbstractRepository<Placet> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    @Autowired
    private SingletonGeradorCodigo singletonGeradorCodigo;

    @Autowired
    private GestaoRepository gestaoRepository;

    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;

    public PlacetRepository() {
        super(Placet.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    @Override
    public Placet recuperar(Class entidade, Object id) {
        return super.recuperar(Placet.class, id);
    }

    @Transactional
    @Override
    public void salvarNovo(Placet entity) {
        gerarPlacet(entity);
    }

    @Transactional
    public Placet gerarPlacet(Placet entity) {
        validarPlacet(entity);
        buscarNumeroPlacetPorGestao(entity);
        buscarGestaoVigente(entity);
        finalizarSolicitacaoProcesso(entity);
        getEntityManager().persist(entity);
        return entity;
    }

    @Transactional
    @Override
    public void excluir(Placet entity) {
        reverterStatusSolicitacaoProcesso(entity);
        super.excluir(entity);
    }

    /**
     * reverting state the processing
     * @param entity selected of exclusion
     */
    @Transactional
    private void reverterStatusSolicitacaoProcesso(Placet entity) {
        SolicitacaoProcesso solicitacaoProcesso = entity.getProcessoFinanceiro().getSolicitacaoProcesso();
        solicitacaoProcesso.setStatusProcesso(StatusProcesso.PAGO);
        entity.getProcessoFinanceiro().setSolicitacaoProcesso(getEntityManager().merge(solicitacaoProcesso));
    }

    private void finalizarSolicitacaoProcesso(Placet entity) {
        SolicitacaoProcesso solicitacaoProcesso = entity.getProcessoFinanceiro().getSolicitacaoProcesso();
        solicitacaoProcesso.setStatusProcesso(StatusProcesso.FINALIZADO);
        entity.getProcessoFinanceiro().setSolicitacaoProcesso(getEntityManager().merge(solicitacaoProcesso));
    }

    private void buscarNumeroPlacetPorGestao(Placet entity) {
        Long proximoPlacet = getSingletonGeradorCodigo().getProximoPlacet();
        entity.setNumero(proximoPlacet);
    }

    private void buscarGestaoVigente(Placet entity) {
        try {
            if (entity.getGestao() == null) {
                Gestao gestao = getGestaoRepository().buscarGestaoVigente();
                entity.setGestao(gestao);
            }
        } catch (GestaoNotCurrentException gnce) {
            throw gnce;
        }
    }

    private void validarPlacet(Placet entity) {
        try {
            entity.validarCamposObrigatorios();
        } catch (CampoObrigatorioException ce ){
            throw ce;
        }
    }

    public SingletonGeradorCodigo getSingletonGeradorCodigo() {
        return singletonGeradorCodigo;
    }

    public GestaoRepository getGestaoRepository() {
        return gestaoRepository;
    }


}
