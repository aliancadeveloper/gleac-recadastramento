package br.com.gleac.negocio;


import br.com.gleac.entidade.auxiliares.FiltroBoleto;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Repository
public class ContasReceberRepository extends AbstractRepository<ContasReceber> {

    @PersistenceContext
    private EntityManager em;

    public ContasReceberRepository() {
        super(ContasReceber.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;
    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;


    @Transactional
    public List<ContasReceber> completarEstaEntidade(String param) {
        String hql = "select ";
        hql = complementarCriacaoObjeto(hql);

        hql += " from ContasReceber obj";
        hql += " where obj.situacaoContaPagar <> '" + SituacaoContaPagar.ESTORNADA.name() + "' and ";

        hql = complementarWhereQuery(hql, param);

        Query q = em.createQuery(hql, ContasReceber.class);
        q.setMaxResults(10);
        q.setParameter("param", "%" + param + "%");
        return q.getResultList();
    }


    @Transactional
    public List<ContasReceber> recuperarLancamentos(Loja loja, Date dataInicial, Date dataFinal) {
        try {
            String sql = "select contas.* from ContasReceber contas " +
                " where (contas.dataVencimento >= to_date(:dataInicial,'dd/MM/yyyy') and contas.dataVencimento <= to_date(:dataFinal,'dd/MM/yyyy'))" +
                " and contas.loja_id = :loja" +
                " and contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'" +
                " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasReceber.class);
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("loja", loja.getId());
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }

    public List<ContasReceber> recuperarLancamentos(Pessoa pessoa, Date dataInicial, Date dataFinal) {
        try {
            String sql = "select contas.* from ContasReceber contas " +
                " where (contas.dataVencimento >= to_date(:dataInicial,'dd/MM/yyyy') and contas.dataVencimento <= to_date(:dataFinal,'dd/MM/yyyy'))" +
                " and contas.pessoa_id = :pessoa" +
                " and contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'" +
                " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasReceber.class);
            q.setParameter("dataInicial", Util.sdf.format(dataInicial));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("dataFinal", Util.sdf.format(dataFinal));
            q.setParameter("pessoa", pessoa.getId());
            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<ContasReceber> recuperarContasReceberAbertaComBoleto(Loja loja, Pessoa pessoa) {
        try {
            String sql = "select contas.* from ContasReceber contas " +
                " inner join boleto b on contas.boleto_id = b.id " +
                " where contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'";
            if (pessoa != null) {
                sql += " and contas.pessoa_id = :pessoa";
            }
            if (loja != null) {
                sql += " and contas.loja_id = :loja";
            }
            sql += " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasReceber.class);
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }
            if (pessoa != null) {
                q.setParameter("pessoa", pessoa.getId());
            }
            return q.getResultList();
        } catch (Exception e) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<ContasReceber> recuperarContasReceberAbertaComBoleto(FiltroBoleto filtroBoleto) {
        try {
            String sql = "select contas.* from ContasReceber contas " +
                " inner join boleto b on contas.boleto_id = b.id ";
            if (filtroBoleto.getSituacaoContaPagar() == null) {
                sql += " where contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'";
            } else {
                sql += " where contas.situacaoContaPagar = '" + filtroBoleto.getSituacaoContaPagar().name() + "'";
            }

            if (filtroBoleto.getPessoa() != null) {
                sql += " and contas.pessoa_id = :pessoa";
            }
            if (filtroBoleto.getLoja() != null) {
                sql += " and contas.loja_id = :loja";
            }

            if (filtroBoleto.getDataInicial() != null) {
                sql += " and contas.datavencimento >= to_date(:data_inicial, 'dd/MM/yyyy')";
            }
            if (filtroBoleto.getDataFinal() != null) {
                sql += " and contas.datavencimento <= to_date(:data_final, 'dd/MM/yyyy')";
            }
            sql += " order by contas.dataVencimento desc";
            Query q = em.createNativeQuery(sql, ContasReceber.class);
            if (filtroBoleto.getLoja() != null) {
                q.setParameter("loja", filtroBoleto.getLoja().getId());
            }
            if (filtroBoleto.getPessoa() != null) {
                q.setParameter("pessoa", filtroBoleto.getPessoa().getId());
            }
            if (filtroBoleto.getDataInicial() != null) {
                q.setParameter("data_inicial", Util.sdf.format(filtroBoleto.getDataInicial()));
            }
            if (filtroBoleto.getDataFinal() != null) {
                q.setParameter("data_final", Util.sdf.format(filtroBoleto.getDataFinal()));
            }
            return q.getResultList();
        } catch (Exception e) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public BigDecimal recuperarSomaPorDataAndSituacao(Date date, Loja loja, SituacaoContaPagar situacao) {
        try {
            String sql = "" +
                " select coalesce(sum(contas.valor), 0) " +
                " from ContasReceber contas " +
                "   where contas.dataVencimento <= to_date(:data,'dd/MM/yyyy') " +
                "    and contas.situacaoContaPagar = :situacao ";
            if (loja != null) {
                sql += " and contas.loja_id = :loja";
            }
            Query q = em.createNativeQuery(sql);
            q.setParameter("data", Util.sdf.format(date));
            q.setParameter("situacao", situacao.name());
            if (loja != null) {
                q.setParameter("loja", loja.getId());
            }
            q.setMaxResults(1);
            return (BigDecimal) q.getSingleResult();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public PessoaJuridicaRepository getPessoaJuridicaRepository() {
        return pessoaJuridicaRepository;
    }

    public PessoaFisicaRepository getPessoaFisicaRepository() {
        return pessoaFisicaRepository;
    }

    @Transactional
    public ContasReceber gerarPorBoleto(Boleto boleto, Loja loja, Pessoa pessoa, String descricao, TipoContaPagar tipoContaPagar) {
        ContasReceber contasReceber = new ContasReceber();
        contasReceber.setBoleto(boleto);
        contasReceber.setDescricao(descricao);
        contasReceber.setPessoa(pessoa);
        contasReceber.setTipoContaPagar(tipoContaPagar);
        contasReceber.setSituacaoContaPagar(SituacaoContaPagar.ABERTA);
        contasReceber.setDataVencimento(boleto.getDataVencimento());
        contasReceber.setLoja(loja);
        contasReceber.setValor(boleto.getValorTotal());
        em.persist(contasReceber);
        return contasReceber;
    }

    @Transactional
    public List<ContasReceber> recuperarTaxasAbertaComBoleto() {
        try {
            String sql = "select contas.* from taxapercapita taxa" +
                " inner join ContasReceber contas on taxa.contasreceber_id = contas.id" +
                " inner join boleto b on contas.boleto_id = b.id " +
                " where contas.situacaoContaPagar = '" + SituacaoContaPagar.ABERTA.name() + "'";
            Query q = em.createNativeQuery(sql, ContasReceber.class);

            return q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Lists.newArrayList();
        }
    }
}
