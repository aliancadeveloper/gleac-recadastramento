package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.DocumentoSite;
import br.com.gleac.supers.AbstractRepository;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by carlos on 28/09/16.
 */
@Repository
public class DocumentoSiteRepository extends AbstractRepository<DocumentoSite> {

    public static final String URL_SERVER = URL_SITE + "/ws/api/documento";
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ArquivoRepository arquivoRepository;


    public DocumentoSiteRepository() {
        super(DocumentoSite.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ArquivoRepository getArquivoRepository() {
        return arquivoRepository;
    }

    @Override
    @Transactional
    public void salvar(DocumentoSite entity) {
        super.salvar(entity);
    }

    @Override
    @Transactional
    public void salvarNovo(DocumentoSite entity) {
        super.salvarNovo(entity);
    }

    @Override
    @Transactional
    public void excluir(DocumentoSite entity) {
        super.excluir(entity);
    }

    private JSONObject montarJsonArquivo(Arquivo arq) {
        try {
            if (arq != null) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("nome", arq.getNome());
                jsonObject.put("tipo", arq.getTipo());
                jsonObject.put("bytes", montarBytesArquivoToString(arq));
                return jsonObject;
            }
        } catch (Exception e) {

        }
        return null;
    }

    private String montarBytesArquivoToString(Arquivo arquivo) throws IOException {
        if (arquivo.getId() != null) {
            arquivo = arquivoRepository.recuperar(Arquivo.class, arquivo.getId());
        }
        arquivo.carregarImagem();

        File file = File.createTempFile("temp", "webpublico");
        FileInputStream imageInFile = new FileInputStream(file);
        byte[] bytes = IOUtils.toByteArray(arquivo.getStreamedContent().getStream());
        imageInFile.read(bytes);

        imageInFile.close();
        return encodeImage(bytes);
    }

    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }


    private JSONObject getJsonObject(DocumentoSite entity) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("descricao", entity.getDescricao());
        jsonObject.put("tipoDocumentoSite", entity.getTipoDocumentoSite().name());
        jsonObject.put("dataPublicacao", entity.getDataPublicacao().getTime());
        jsonObject.put("codigo", entity.getCodigo());
        if (entity.getArquivo() != null) {
            jsonObject.put("arquivo", montarJsonArquivo(entity.getArquivo()));
        }
        return jsonObject;
    }
}
