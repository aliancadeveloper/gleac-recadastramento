package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Bairro;
import br.com.gleac.entidade.comum.Cidade;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 30/08/16.
 */
@Repository
public class BairroRepository extends AbstractRepository<Bairro> {

    @PersistenceContext
    private EntityManager em;

    public BairroRepository() {
        super(Bairro.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Bairro> buscarBairroPorNome(String filter){
        String hql = " select ba from Bairro ba where lower(ba.nome) like :filter ";
        Query q = em.createQuery(hql);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }

    @Transactional
    public List<Bairro> buscarBairroPorNomeCidade(String filter, Cidade cidade){
        String hql = " select ba from Bairro ba where ba.cidade = :cidade  and lower(ba.nome) like :filter";
        Query q = em.createQuery(hql);
        q.setParameter("cidade", cidade);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }

}
