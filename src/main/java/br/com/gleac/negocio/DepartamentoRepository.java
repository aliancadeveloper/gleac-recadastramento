package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by zaca on 15/07/17.
 */
@Repository
public class DepartamentoRepository extends AbstractRepository<Departamento> {

    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;

    public DepartamentoRepository() {
        super(Departamento.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Departamento> buscarTodosDepartamentos(String filtro, Loja loja) {
        String sql = " select de.* from departamento de " +
            " where (lower(de.nome) like :filtro or de.numero like :filtro ) ";
        if (loja != null) {
            sql += " and de.loja_id = :loja ";
        }
        Query q = em.createNativeQuery(sql, Departamento.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        if (loja != null) {
            q.setParameter("loja", loja.getId());
        }
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }

    @Transactional
    public List<Departamento> buscarTodosDepartamentos(String filtro) {
        String sql = " select de.* from departamento de " +
            " where (lower(de.nome) like :filtro or de.numero like :filtro ) order by 2 ";
        Query q = em.createNativeQuery(sql, Departamento.class);
        q.setParameter("filtro", "%" + filtro.trim().toLowerCase() + "%");
        q.setMaxResults(10);
        try {
            return q.getResultList();
        } catch (NoResultException nre) {
            return Lists.newArrayList();
        }
    }
}
