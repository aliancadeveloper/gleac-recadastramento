package br.com.gleac.negocio;

import br.com.gleac.entidade.comum.Estado;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 02/09/16.
 */
@Repository
public class EstadoRepository extends AbstractRepository<Estado> {

    @PersistenceContext
    private EntityManager em;

    public EstadoRepository() {
        super(Estado.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Transactional
    public List<Estado> buscarEstadoPorNome(String filter){
        String hql  = "select obj from Estado obj where lower(obj.nome) like :filter";


        Query q = em.createQuery(hql);
        q.setParameter("filter", "%" + filter.trim().toLowerCase() + "%");
        q.setMaxResults(10);

        if (!q.getResultList().isEmpty())
            return q.getResultList();
        return new ArrayList<>();
    }
}
