package br.com.gleac.negocio;

import br.com.gleac.entidade.gerenciamentosite.PalavraSemestral;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by zaca on 06/10/16.
 */
@Repository
public class PalavraSemestralRepository extends AbstractRepository<PalavraSemestral> {

    public static final String URL_SERVER = URL_SITE + "/ws/api/palavra";

    @PersistenceContext
    private EntityManager em;

    public PalavraSemestralRepository() {
        super(PalavraSemestral.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    @Transactional
    public void salvar(PalavraSemestral entity) {
        super.salvar(entity);
    }

    @Override
    @Transactional
    public void salvarNovo(PalavraSemestral entity) {
        super.salvarNovo(entity);
    }

    @Override
    @Transactional
    public void excluir(PalavraSemestral entity) {
        super.excluir(entity);
    }

    public void validarPalavraSemestralVigencia(PalavraSemestral palavraSemestral) {
        String sql = "select pa.* from palavraSemestral pa " +
            " where ((pa.datainicial >= to_date(:inicio,'dd/MM/yyyy') " +
            "   and pa.datafinal <= to_date(:fim,'dd/MM/yyyy')) " +
            "     or " +
            " (pa.datainicial BETWEEN to_date(:inicio,'dd/MM/yyyy') and to_date(:fim,'dd/MM/yyyy')) " +
            "     or " +
            " (pa.datafinal BETWEEN to_date(:inicio,'dd/MM/yyyy') and to_date(:fim,'dd/MM/yyyy')) " +
            ") ";

        if (palavraSemestral.getId() != null) {
            sql += " and id <> :id";
        }

        Query q = em.createNativeQuery(sql, PalavraSemestral.class);
        q.setParameter("inicio", Util.sdf.format(palavraSemestral.getDataInicial()));
        q.setParameter("fim", Util.sdf.format(palavraSemestral.getDataFinal()));
        if (palavraSemestral.getId() != null) {
            q.setParameter("id", palavraSemestral.getId());
        }
        q.setMaxResults(1);
        try {

            PalavraSemestral retorno = (PalavraSemestral) q.getSingleResult();
            if (retorno != null) {
                throw new ExceptionGenerica(
                    "Já existe uma Palavra Semestral para a vigência "
                        + Util.sdf.format(retorno.getDataInicial())
                        + " e "
                        + Util.sdf.format(retorno.getDataFinal()) + ".");
            }
        } catch (NoResultException nre) {

        }
    }

    private JSONObject getJsonObject(PalavraSemestral entity) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("palavra", entity.getPalavra());
        jsonObject.put("dataInicial", entity.getDataInicial().getTime());
        jsonObject.put("dataFinal", entity.getDataFinal().getTime());
        jsonObject.put("codigo", entity.getCodigo());
        return jsonObject;
    }
}
