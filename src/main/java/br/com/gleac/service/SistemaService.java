package br.com.gleac.service;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.entidade.seguranca.Recurso;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.GrupoAcessoUsuario;
import br.com.gleac.negocio.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Created by renatoromanini on 24/05/16.
 */
@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SistemaService implements Serializable {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private RecursoSistemaRepository recursoSistemaRepository;
    @Autowired
    private ContaCorrenteBancariaRepository contaCorrenteBancariaRepository;
    @Autowired
    private GestaoRepository gestaoRepository;
    private Usuario usuario;
    private Loja lojaCorrente;
    private Loja lojaGestora;
    private List<Loja> lojas;
    private Macon macon;
    private List<Recurso> recursos;
    private ContaCorrenteBancaria contaGestao;
    private Boolean perfilFinanceiro;
    private Boolean perfilMacon;


    /**
     * Creates a new instance of SistemaControlador
     */
    @SuppressWarnings("unchecked")
    public SistemaService() {
       /* ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
*/
    }

    public Usuario getUsuarioCorrente() {
        if (usuario == null) {
            String login = SistemaRepository.obtemLogin();
            this.usuario = usuarioRepository.recuperarUsuarioPorLogin(login);
        }
        return usuario;
    }

    public static String getLoginUsuarioCorrente() {
        return SistemaRepository.obtemLogin();
    }

    public Loja getLojaCorrente() {
        return lojaCorrente;
    }

    public Loja getLojaGestora() {
        if (lojaGestora == null) {
            lojaGestora = lojaRepository.buscarLojaGestora();
        }
        return lojaGestora;
    }

    public ContaCorrenteBancaria getContaGestao() {
        if (contaGestao == null) {
            ContaCorrenteBancaria contaCorrenteBancaria = gestaoRepository.buscarGestaoVigente().getContaCorrenteBancaria();
            if (contaCorrenteBancaria == null) {
                return null;
            }
            Long id = contaCorrenteBancaria.getId();
            contaGestao = contaCorrenteBancariaRepository.recuperar(ContaCorrenteBancaria.class, id);
        }
        return contaGestao;
    }

    public Boolean isLojaGestora() {
        return getLojaGestora().getId().equals(getLojaCorrente().getId());

    }

    public void setLojaCorrente(Loja lojaCorrente) {
        this.lojaCorrente = lojaCorrente;
        lojaGestora = null;
        this.perfilMacon = null;
        this.perfilFinanceiro = null;
        getLojaGestora();
        this.recursos = recursoSistemaRepository.buscarRecursoPorUsuarioAndLoja(usuario, lojaCorrente);
    }

    public void definirUsuario(Usuario usuario) {
        this.usuario = usuarioRepository.recuperar(Usuario.class, usuario.getId());
    }

    public void definirLojasUsuarioByPerfil(List<Loja> lojas) {
        setLojas(lojas);
        setLojaCorrente(lojas.get(0));
        carregarRecursos(getUsuarioCorrente(), getLojaCorrente());
        if (isPerfilMacon()) {
            macon = maconRepository.recuperarMaconPorUsuario(getUsuarioCorrente());
            Loja lojaInternaCorrente = macon.getUltimaLojaInterna();
            setLojaCorrente(lojaInternaCorrente);
            this.recursos = recursoSistemaRepository.buscarRecursoPorUsuarioAndLoja(usuario, lojaInternaCorrente);
        }
    }

    private void carregarRecursos(Usuario usuarioCorrente, Loja lojaCorrente) {
        this.setRecursos(recursoSistemaRepository.buscarRecursoPorUsuarioAndLoja(usuarioCorrente, lojaCorrente));
    }

    public Boolean isExistsLojaToUsuario(Optional<Usuario> usuario) {
        try {
            return !lojaRepository.recuperarLojasDoUsuario(usuario.get()).isEmpty();
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    public Macon getMacon() {
        return macon;
    }

    public Boolean isMacon() {
        return macon != null;
    }

    public Boolean isPerfilFinanceiro() {
        if (perfilFinanceiro == null) {
            perfilFinanceiro = isPerfil(GrupoAcessoUsuario.FINANCEIRO);
        }
        return perfilFinanceiro;
    }

    public Boolean isPerfilMacon() {
        if (perfilMacon == null) {
            perfilMacon = isPerfil(GrupoAcessoUsuario.MACON);
        }
        return perfilMacon;
    }

    private Boolean isPerfil(GrupoAcessoUsuario grupoAcessoUsuario) {
        try {
            if (getUsuarioCorrente() != null) {
                return usuarioRepository.checkPermissaoUsuarioByGrupoAcessoUsuario(getUsuarioCorrente(), grupoAcessoUsuario, getLojaCorrente());
            }
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.FALSE;
    }

    public static String getIP() {
        String ip = "Local";
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            ip = httpServletRequest.getRemoteAddr();
        } catch (Exception ex) {
        }
        return ip;
    }

    public List<Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    public List<Loja> getLojas() {
        return lojas;
    }

    public void setLojas(List<Loja> lojas) {
        this.lojas = lojas;
    }
}
