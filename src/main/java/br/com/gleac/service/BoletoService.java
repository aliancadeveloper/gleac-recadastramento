package br.com.gleac.service;

import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.configuracao.ConfiguracaoProduto;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.entidade.financeiro.*;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.processo.SolicitacaoProcessoProduto;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.Situacao;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoTaxaPercapitaParcela;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.negocio.*;
import br.com.gleac.singleton.SingletonGeradorCodigo;
import br.com.gleac.util.Util;
import br.com.gleac.util.UtilFinanceiro;
import com.google.common.base.Strings;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class BoletoService implements Serializable {

    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private ConfiguracaoFinanceiraRepository configuracaoFinanceiraRepository;
    @Autowired
    private SingletonGeradorCodigo singletonGeradorCodigo;
    @Autowired
    private ConfiguracaoServicoRepository configuracaoServicoRepository;
    @Autowired
    private ConfiguracaoProdutoRepository configuracaoProdutoRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private BoletoRepository boletoRepository;


    @Transactional
    public void gerarBoleto(ProcessoFinanceiro selecionado) {

        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            SolicitacaoProcesso sol = solicitacaoProcessoRepository.recuperar(SolicitacaoProcesso.class, selecionado.getSolicitacaoProcesso().getId());
            Long numero = getProximoNumeroBoleto();
            Pessoa pf = selecionado.getSolicitacaoProcesso().getPessoa();
            Integer quantidadeDiasVencimento = Util.diferencaDeDiasEntreDuasDatas(selecionado.getGeradoEm(), selecionado.getVencimentoBoleto());

            JSONObject jsonObject = criarJsonComum(pf, numero, quantidadeDiasVencimento);
            if (TipoSolicitacao.PRODUTO.equals(selecionado.getSolicitacaoProcesso().getTipoSolicitacao())) {
                JSONArray array = new JSONArray();
                for (SolicitacaoProcessoProduto solicitacaoProcessoProduto : sol.getSolicitacaoProdutos()) {
                    JSONObject item = criarJsonProduto(selecionado.getGeradoEm(), solicitacaoProcessoProduto.getProduto(), 1, false);
                    array.put(item);
                }
                jsonObject.put("items", array);
            } else if (TipoSolicitacao.SERVICO.equals(selecionado.getSolicitacaoProcesso().getTipoSolicitacao())) {
                Servico servico = selecionado.getSolicitacaoProcesso().getServico();
                JSONObject item = criarJSONServico(servico, selecionado.getGeradoEm(), 1, false);
                JSONArray array = new JSONArray();
                array.put(item);
                jsonObject.put("items", array);
            }

            ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_CREATE, headers, jsonObject);
            Boleto boleto = criarBoletoDoRetorno(numero, response.getBody());

            selecionado.setRetorno(response.getBody());
            selecionado.setBoleto(boleto);

        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + selecionado.getRetorno());
        }

    }

    @Transactional
    public Boleto gerarBoleto(TaxaPercapita taxaPercapita) {

        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            Pessoa pessoa = getPessoaTaxaPercapita(taxaPercapita);

            Long numero = getProximoNumeroBoleto();
            Integer quantidadeDiasVencimento = Util.diferencaDeDiasEntreDuasDatas(new Date(), taxaPercapita.getData());

            JSONObject jsonObject = null;
            if (TipoSolicitacao.SERVICO.equals(taxaPercapita.getTipoSolicitacao())) {
                jsonObject = getJsonObjectTaxaPercapita(taxaPercapita, taxaPercapita.isComDesconto(), pessoa, numero, quantidadeDiasVencimento, false);
            } else {
                jsonObject = getJsonObjectTaxaPercapitaProduto(taxaPercapita, taxaPercapita.isComDesconto(), pessoa, numero, quantidadeDiasVencimento, false);
            }
            ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_CREATE, headers, jsonObject);

            Boleto boleto = criarBoletoDoRetorno(numero, response.getBody());
            taxaPercapita.setRetorno(response.getBody());

            return boleto;
        } catch (ExceptionGenerica e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + taxaPercapita.getRetorno());
        }
    }

    @Transactional
    public Boleto gerarBoleto(TaxaPercapitaParcela parcela, Date ultimoData, Date novaData) {

        try {
            TaxaPercapita taxaPercapita = parcela.getTaxaPercapita();
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            Pessoa pessoa = getPessoaTaxaPercapita(taxaPercapita);
            Long numero = getProximoNumeroBoleto();
            Integer quantidadeDiasVencimento = Util.diferencaDeDiasEntreDuasDatas(ultimoData, novaData);

            JSONObject jsonObject = null;
            if (TipoSolicitacao.SERVICO.equals(taxaPercapita.getTipoSolicitacao())) {
                jsonObject = getJsonObjectTaxaPercapita(taxaPercapita, parcela.isComDesconto(), pessoa, numero, quantidadeDiasVencimento, !parcela.isComDesconto());
            } else {
                jsonObject = getJsonObjectTaxaPercapitaProduto(taxaPercapita, parcela.isComDesconto(), pessoa, numero, quantidadeDiasVencimento, !parcela.isComDesconto());
            }
            ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_CREATE, headers, jsonObject);

            Boleto boleto = criarBoletoDoRetorno(numero, response.getBody());
            taxaPercapita.setRetorno(response.getBody());
            return boleto;
        } catch (ExceptionGenerica e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + parcela.getTaxaPercapita().getRetorno());
        }
    }

    private ResponseEntity<String> tratarRetorno(RestTemplate restTemplate, String url, HttpHeaders headers, JSONObject jsonObject) {
        HttpEntity<String> request = new HttpEntity(jsonObject.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

        URI location = response.getHeaders().getLocation();

        return response;
    }

    private JSONObject criarJsonProduto(Date data, Produto produto, Integer quantidade, Boolean dividir) {
        ConfiguracaoProduto configuracaoProduto = configuracaoProdutoRepository.buscarConfiguracao(data, produto);
        BigDecimal valor = configuracaoProduto.getValor();
        valor = valor.multiply(new BigDecimal(quantidade)).setScale(2, RoundingMode.HALF_UP);
        if (dividir) {
            BigDecimal quantidadeParcela = new BigDecimal(TipoTaxaPercapitaParcela.QUANTIDADE_PARCELA);
            valor = valor.divide(quantidadeParcela, 2, RoundingMode.HALF_UP);
        }
        valor = valor.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
        ;
        JSONObject item = new JSONObject();
        item.put("item_id", produto.getId().toString());
        item.put("price_cents", valor.toString());
        item.put("description", produto.getDescricao());
        item.put("quantity", "1");
        return item;
    }

    private JSONObject criarJsonConsulta(String idTransaction) {
        ConfiguracaoFinanceira configuracaoFinanceira = getConfiguracaoFinanceira();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("apiKey", configuracaoFinanceira.getApiKey());
        jsonObject.put("token", configuracaoFinanceira.getToken());
        jsonObject.put("transaction_id", idTransaction);
        return jsonObject;
    }

    private JSONObject criarJsonCancelamento(String idTransaction) {
        ConfiguracaoFinanceira configuracaoFinanceira = getConfiguracaoFinanceira();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("apiKey", configuracaoFinanceira.getApiKey());
        jsonObject.put("token", configuracaoFinanceira.getToken());
        jsonObject.put("transaction_id", idTransaction);
        jsonObject.put("status", "canceled");
        return jsonObject;
    }

    private JSONObject criarJsonComum(Pessoa pf, Long numero, Integer quantidadeDiasVencimento) {
        JSONObject jsonObject = new JSONObject();
        ConfiguracaoFinanceira configuracaoFinanceira = getConfiguracaoFinanceira();
        jsonObject.put("apiKey", configuracaoFinanceira.getApiKey());
        jsonObject.put("order_id", numero.toString());
        if (!Strings.isNullOrEmpty(configuracaoFinanceira.getUrlNotificacao())) {
            jsonObject.put("notification_url", configuracaoFinanceira.getUrlNotificacao());
        }
        if (pf.getEmail() == null || pf.getEmail().trim().isEmpty()) {
            throw new ExceptionGenerica("O campo E-mail é obrigatório.");
        }
        jsonObject.put("payer_name", pf.getNomePessoa());
        jsonObject.put("payer_cpf_cnpj", pf.getCpf_Cnpj());
        jsonObject.put("payer_email", pf.getEmail());
        jsonObject.put("type_bank_slip", "boletoA4");
        jsonObject.put("days_due_date", quantidadeDiasVencimento.toString());
        jsonObject.put("open_after_day_due", 30);
        return jsonObject;
    }

    private HttpHeaders criarHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));
        headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));
        return headers;
    }


    private JSONObject criarJSONServico(Servico servico, Date data, Integer quantidade, Boolean dividir) {
        ConfiguracaoServico configuracaoServico = configuracaoServicoRepository.buscarConfiguracao(data, servico);
        BigDecimal valor = configuracaoServico.getValor();
        valor = valor.multiply(new BigDecimal(quantidade)).setScale(2, RoundingMode.HALF_UP);
        if (dividir) {
            BigDecimal quantidadeParcela = new BigDecimal(TipoTaxaPercapitaParcela.QUANTIDADE_PARCELA);
            valor = valor.divide(quantidadeParcela, 2, RoundingMode.HALF_UP);
        }
        valor = valor.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
        JSONObject item = new JSONObject();
        item.put("item_id", servico.getId().toString());
        item.put("price_cents", valor.toString());
        item.put("description", servico.getDescricao());
        item.put("quantity", "1");
        return item;
    }


    private JSONObject getJsonObjectTaxaPercapita(TaxaPercapita taxaPercapita, Boolean comDesconto, Pessoa pessoa, Long numero, Integer quantidadeDiasVencimento, Boolean multiplicarValorBoleto) {
        Integer qtdMacon = maconRepository.contarMacomPorLojaAndSituacao("", taxaPercapita.getLoja(), Situacao.REGULAR.name());
        BigDecimal valor;
        JSONObject jsonObject = criarJsonComum(pessoa, numero, quantidadeDiasVencimento);
        if (comDesconto) {
            ConfiguracaoServico configuracaoServico = configuracaoServicoRepository.buscarConfiguracao(taxaPercapita.getData(), taxaPercapita.getServico());
            if (configuracaoServico.getAnuidade()) {
                valor = configuracaoServico.getValorDesconto().multiply(new BigDecimal(qtdMacon)).setScale(2, RoundingMode.HALF_EVEN);
            } else {
                valor = configuracaoServico.getValorDesconto();
            }
            if (valor.compareTo(BigDecimal.ZERO) != 0 && configuracaoServico.getQuantidadeDiasDesconto() != 0) {
                valor = valor.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP);
                jsonObject.put("early_payment_discounts_days", configuracaoServico.getQuantidadeDiasDesconto());
                jsonObject.put("early_payment_discounts_cents", valor.toString());
            }
        }
        Integer quantidade = getQuantidadeTaxaPercapta(taxaPercapita);
        Servico servico = taxaPercapita.getServico();
        JSONObject item = criarJSONServico(servico, taxaPercapita.getData(), quantidade, multiplicarValorBoleto);
        JSONArray array = new JSONArray();
        array.put(item);
        jsonObject.put("items", array);
        return jsonObject;
    }

    private JSONObject getJsonObjectTaxaPercapitaProduto(TaxaPercapita taxaPercapita, Boolean comDesconto, Pessoa pessoa, Long numero, Integer quantidadeDiasVencimento, Boolean multiplicarValorBoleto) {
        JSONObject jsonObject = criarJsonComum(pessoa, numero, quantidadeDiasVencimento);
        if (comDesconto) {
            /*se tiver desconto de produto colocar aqui*/
        }
        Integer quantidade = getQuantidadeTaxaPercapta(taxaPercapita);
        JSONArray array = new JSONArray();
        for (TaxaPercapitaProduto taxaPercapitaProduto : taxaPercapita.getProdutos()) {
            JSONObject item = criarJsonProduto(taxaPercapita.getData(), taxaPercapitaProduto.getProduto(), quantidade, multiplicarValorBoleto);
            array.put(item);
        }
        jsonObject.put("items", array);
        return jsonObject;
    }

    private Long getProximoNumeroBoleto() {
        return singletonGeradorCodigo.getProximoCodigo(Boleto.class, "numero");
    }

    private Integer getQuantidadeTaxaPercapta(TaxaPercapita taxaPercapita) {
        Integer quantidade = 0;
        if (taxaPercapita.isPorLoja()) {
            ConfiguracaoServico configuracaoServico = configuracaoServicoRepository.buscarConfiguracao(taxaPercapita.getData(), taxaPercapita.getServico());
            if (configuracaoServico.getAnuidade()) {
                quantidade = maconRepository.contarMacomPorLojaAndSituacao("", taxaPercapita.getLoja(), Situacao.REGULAR.name());
            } else {
                quantidade = 1;
            }
        } else {
            quantidade = 1;
        }
        return quantidade;
    }

    private Pessoa getPessoaTaxaPercapita(TaxaPercapita taxaPercapita) {
        Pessoa pessoa = null;
        if (taxaPercapita.isPorLoja()) {
            pessoa = taxaPercapita.getLoja().getPessoaJuridica();
        } else {
            pessoa = taxaPercapita.getMacon().getPessoaFisica();
        }
        return pessoa;
    }


    private Boleto criarBoletoDoRetorno(Long numero, String body) {
        JSONObject jsonObject = new JSONObject(body);
        JSONObject request = (JSONObject) jsonObject.get("create_request");

        return criarBoleto(request, numero);
    }

    private ConfiguracaoFinanceira getConfiguracaoFinanceira() {
        ConfiguracaoFinanceira configuracaoFinanceira = configuracaoFinanceiraRepository.recuperarConfiguracao();
        if (configuracaoFinanceira == null) {
            throw new ExceptionGenerica("Nenhuma configuração financeira foi encontrada!");
        }
        return configuracaoFinanceira;
    }

    @Transactional
    public void verificarBoleto(ProcessoFinanceiro selecionado) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            String status = enviarTratarConsultaBoleto(selecionado.getBoleto().getIdTransacao(), restTemplate, headers);
            selecionado.setRetorno("status do boleto: " + status);
            if (isBoletoPago(status)) {
                gerarReceitaBoletoPago(selecionado, status);
            } else if (isBoletoCancelado(status)) {
                selecionado.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.CANCELADO);
                selecionado.getBoleto().setStatus(status);
                selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));
                solicitacaoProcessoRepository.salvar(selecionado.getSolicitacaoProcesso());
            }
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + selecionado.getRetorno());
        }
    }

    @Transactional
    public void cancelarBoleto(ProcessoFinanceiro selecionado) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            String mensagem = enviarTratarCancelamentoBoleto(selecionado.getBoleto().getIdTransacao(), restTemplate, headers);
            selecionado.setRetorno(mensagem);
            if (mensagem.contains("Cancelado com Sucesso")) {
                selecionado.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.CANCELADO);
                solicitacaoProcessoRepository.salvar(selecionado.getSolicitacaoProcesso());
                selecionado.getBoleto().setStatus("canceled");
                selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));
            }
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + selecionado.getRetorno());
        }
    }

    private String enviarTratarConsultaBoleto(String idTransaction, RestTemplate restTemplate, HttpHeaders headers) {
        JSONObject jsonConsulta = criarJsonConsulta(idTransaction);

        ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_STATUS, headers, jsonConsulta);

        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject request = (JSONObject) jsonObject.get("status_request");

        String status = request.getString("status");

        return status;
    }

    private String enviarTratarCancelamentoBoleto(String idTransaction, RestTemplate restTemplate, HttpHeaders headers) {
        JSONObject jsonConsulta = criarJsonCancelamento(idTransaction);

        ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_CANCEL, headers, jsonConsulta);

        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject request = (JSONObject) jsonObject.get("cancellation_request");

        String mensagem = request.getString("response_message");

        return mensagem;
    }

    private boolean isBoletoCancelado(String status) {
        return status.equals("canceled") || status.equals("cancelado");
    }

    private boolean isBoletoPago(String status) {
        /*
            *    pending	aguardando
                reserved	reservado
                canceled	cancelado
                completed	completo
                paid	    aprovado
                processing	analise
                refunded	estornado
            * */
        return status.equals("paid") || status.equals("completed");
    }

    @Transactional
    public void verificarBoleto(ContasReceber selecionado, TaxaPercapitaRepository taxaPercapitaRepository) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            String status = enviarTratarConsultaBoleto(selecionado.getBoleto().getIdTransacao(), restTemplate, headers);
            if (isBoletoPago(status)) {
                gerarReceitaBoletoPago(selecionado, status);
                cancelarBoletoPrincipalOuParcelas(selecionado, taxaPercapitaRepository);
            } else if (isBoletoCancelado(status)) {
                cancelarContaReceber(selecionado, status);
            }
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + selecionado);
        }
    }

    private void cancelarBoletoPrincipalOuParcelas(ContasReceber selecionado, TaxaPercapitaRepository taxaPercapitaRepository) {
        if (selecionado.getTipoContaPagar().isTaxa()) {
            TaxaPercapita taxa = taxaPercapitaRepository.buscarTaxaPorContaReceber(selecionado);
            if (taxa != null) {
                taxa = taxaPercapitaRepository.recuperar(TaxaPercapita.class, taxa.getId());
                for (TaxaPercapitaParcela parcela : taxa.getParcelas()) {
                    cancelarBoleto(parcela.getContasReceber());
                }
            }
            TaxaPercapitaParcela parcela = taxaPercapitaRepository.buscarTaxaParcelaPorContaReceber(selecionado);
            if (parcela != null) {
                cancelarBoleto(parcela.getTaxaPercapita().getContasReceber());
            }
        }
    }

    private void cancelarContaReceber(ContasReceber selecionado, String status) {
        selecionado.setSituacaoContaPagar(SituacaoContaPagar.CANCELADO);
        selecionado.getBoleto().setStatus(status);
        selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));
        contasReceberRepository.salvar(selecionado);
    }

    @Transactional
    public void cancelarBoleto(ContasReceber selecionado) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            String mensagem = enviarTratarCancelamentoBoleto(selecionado.getBoleto().getIdTransacao(), restTemplate, headers);

            if (mensagem.contains("Cancelado com Sucesso")) {
                selecionado.setSituacaoContaPagar(SituacaoContaPagar.CANCELADO);
                contasReceberRepository.salvar(selecionado);
                selecionado.getBoleto().setStatus("canceled");
                selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));
            }
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage() + ": Erro " + selecionado);
        }
    }

    @Transactional
    public void gerarReceitaBoletoPago(ProcessoFinanceiro selecionado, String status) {
        selecionado.getSolicitacaoProcesso().setStatusProcesso(StatusProcesso.PAGO);
        solicitacaoProcessoRepository.salvar(selecionado.getSolicitacaoProcesso());

        selecionado.getBoleto().setStatus(status);
        selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));

        receitaRepository.gerarReceitaPorProcesso(selecionado);
    }

    @Transactional
    public void gerarReceitaBoletoPago(ContasReceber selecionado, String status) {
        selecionado.setSituacaoContaPagar(SituacaoContaPagar.PAGA);
        contasReceberRepository.salvar(selecionado);

        selecionado.getBoleto().setStatus(status);
        selecionado.setBoleto(boletoRepository.salvarRetornando(selecionado.getBoleto()));

        receitaRepository.gerarReceitaPorProcesso(selecionado);
    }


    private Boleto criarBoleto(JSONObject obj, Long numero) {
        Boleto boleto = new Boleto();
        try {

            boleto.setNumero(numero);
            boleto.setDataTransacao(Util.sdft2.parse(obj.getString("created_date")));
            boleto.setDataVencimento(Util.sdf2.parse(obj.getString("due_date")));
            boleto.setIdPlataforma(obj.getString("order_id"));
            boleto.setIdTransacao(obj.getString("transaction_id"));
            boleto.setStatus(obj.getString("status"));

            BigDecimal valor = new BigDecimal(obj.getDouble("value_cents"));
            valor = valor.divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
            boleto.setValorTotal(valor);

            JSONObject dadosPagamento = (JSONObject) obj.get("bank_slip");

            boleto.setLinhaDigitavel(dadosPagamento.getString("digitable_line"));
            boleto.setUrlPagamento(dadosPagamento.getString("url_slip"));
            boleto.setUrlPdfPagamento(dadosPagamento.getString("url_slip_pdf"));

            //todo- campos pra adicioanr
            //response_message
            //result
            //http_code
        } catch (Exception e) {
            e.printStackTrace();
        }
        return boleto;
    }

    @Transactional
    public String gerarBoletosAgrupados(List<Boleto> boletos) {
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = criarHeader();

            String[] transactions = new String[boletos.size()];
            for (Boleto boleto : boletos) {
                transactions[boletos.indexOf(boleto)] = boleto.getIdTransacao();
            }
            JSONObject jsonConsulta = criarJsonBoletosAgrupados(transactions);

            ResponseEntity<String> response = tratarRetorno(restTemplate, UtilFinanceiro.URL_PAGHIPER_CARNE_BOLETO, headers, jsonConsulta);

            JSONObject jsonObject = new JSONObject(response.getBody());
            JSONObject request = (JSONObject) jsonObject.get("status_request");
            return request.getString("bank_slip_group");
        } catch (ExceptionGenerica e) {
            throw new ExceptionGenerica(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ExceptionGenerica(e.getMessage());
        }
    }

    private JSONObject criarJsonBoletosAgrupados(String[] transactions) {
        ConfiguracaoFinanceira configuracaoFinanceira = getConfiguracaoFinanceira();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("apiKey", configuracaoFinanceira.getApiKey());
        jsonObject.put("token", configuracaoFinanceira.getToken());
        jsonObject.put("transactions", transactions);
        jsonObject.put("type_bank_slip", "boletoCarne");
        return jsonObject;
    }
}
