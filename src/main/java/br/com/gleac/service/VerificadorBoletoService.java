package br.com.gleac.service;

import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.negocio.ContasReceberRepository;
import br.com.gleac.negocio.ProcessoFinanceiroRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
public class VerificadorBoletoService implements Serializable {


    private final Logger log = LoggerFactory.getLogger(VerificadorBoletoService.class);
    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;

    /*
    * A B C D E F
        A: Segundos (0 – 59).
        B: Minutos (0 – 59).
        C: Horas (0 – 23).
        D: Dia (1 – 31).
        E: Mês (1 – 12).
        F: Dia da semana (0 – 6).
    * */
    @Scheduled(cron = "0 0 6 * * *")
    @Async
    public void verificarBoletosDasSeis() {
        log.info("iniciando verificação dos boleto das 06:00 ");
        verificarBoletosDeTaxa();
        verificarBoletosDeProcessos();
    }

    @Scheduled(cron = "0 0 13 * * *")
    @Async
    public void verificarBoletosDas13() {
        log.info("iniciando verificação dos boleto das 13:00 ");
        verificarBoletosDeTaxa();
        verificarBoletosDeProcessos();
    }

    private void verificarBoletosDeTaxa() {
        SituacaoContaPagar situacaoContaPagar = SituacaoContaPagar.ABERTA;
        log.info("iniciando verificação de taxas com situação {}", situacaoContaPagar.getDescricao());
        List<ContasReceber> contas = contasReceberRepository.recuperarTaxasAbertaComBoleto();
        if (contas != null) {
            log.info("recuperou {}", contas.size());
            for (ContasReceber conta : contas) {
                verificarContaReceber(conta);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void verificarContaReceber(ContasReceber conta) {
        try {
            log.info("verificando taxa {}", conta.getId());
            processoFinanceiroRepository.verificarBoleto(conta);
        } catch (Exception e) {
            log.error("erro na taxa {}", conta.getId() + " -> " + e.getMessage());
        }
    }

    
    public void verificarBoletosDeProcessos() {
        StatusProcesso status = StatusProcesso.AGUARDANDO_BOLETO;
        log.info("iniciando verificação de processo com situação {}", status.getDescricao());
        List<ProcessoFinanceiro> processoFinanceiros = processoFinanceiroRepository.completarProcessoFinanceiroPorSituacao(status, "");
        if (processoFinanceiros != null) {
            log.info("recuperou {}", processoFinanceiros.size());
            for (ProcessoFinanceiro processoFinanceiro : processoFinanceiros) {
                verificarProcesso(processoFinanceiro);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    private void verificarProcesso(ProcessoFinanceiro processoFinanceiro) {
        try {
            log.info("verificando processo {}", processoFinanceiro.getId());
            processoFinanceiroRepository.verificarBoleto(processoFinanceiro);
        } catch (Exception e) {
            log.error("erro no processo {}", processoFinanceiro.getId() + " -> " + e.getMessage());
        }
    }
}
