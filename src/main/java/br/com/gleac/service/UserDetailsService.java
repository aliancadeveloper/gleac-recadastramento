package br.com.gleac.service;

import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.exception.UserNotActivatedException;
import br.com.gleac.negocio.jpa.UsuarioJPARepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by israeleriston on 04/06/16.
 */
@Component("userDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Inject
    private UsuarioJPARepository usuarioRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {

        try {
            Optional<Usuario> userFromDatabase = usuarioRepository.findOneByLogin(login);
            return userFromDatabase.map(user -> {
                if (!user.getAtivo()) {
                    try {
                        throw new UserNotActivatedException("Usuario " + login + " não esta ativo! ");
                    } catch (UserNotActivatedException e) {

                    }
                }
                List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getAuthority()))
                    .collect(Collectors.toList());

                return new org.springframework.security.core.userdetails.User(login,
                    user.getSenha(),
                    grantedAuthorities);

            }).orElseThrow(() -> new UsernameNotFoundException("Usuario " + login + " não foi encotrado na base! "));
        } catch (Exception e) {
            throw new UsernameNotFoundException("User " + login + " was not found in the database");
        }
    }
}
