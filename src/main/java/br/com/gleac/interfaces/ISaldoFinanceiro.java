package br.com.gleac.interfaces;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.enums.TipoOperacao;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 01/08/17.
 */
public interface ISaldoFinanceiro {

    public Date getDataLancamento();

    public Loja getLoja();

    public ContaCorrente getContaCorrente();

    public BigDecimal getValor();

    public String getReferencia();

    public TipoOperacao getTipoOperacao();

    public String getHistorico();
}
