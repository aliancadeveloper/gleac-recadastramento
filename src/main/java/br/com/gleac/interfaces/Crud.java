package br.com.gleac.interfaces;

/**
 * Created by israeleriston on 07/05/16.
 */
public interface Crud {

    public void novo();
    public void editar();
    public void salvar();
    public void visualizar();


}
