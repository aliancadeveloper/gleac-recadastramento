package br.com.gleac.singleton;

import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.processo.Placet;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.negocio.GestaoRepository;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.HashMap;


/**
 * Created by zaca.
 */
@Component
@Scope("singleton")
public class SingletonGeradorCodigo {

    public static final Class<Placet> PLACET = Placet.class;
    @PersistenceContext(unitName = "gleacPU")
    private EntityManager em;
    @Autowired
    private GestaoRepository gestaoRepository;

    private HashMap<Class, Long> codigos;
    private HashMap<Class, Long> placets;

    @Lock(LockModeType.WRITE)
    @Transactional
    public Long getProximoCodigo(Class classe, String campo) {
        if (getCodigos() == null) {
            setCodigos(Maps.newHashMap());
        }
        Long codigo;
        if (getCodigos().containsKey(classe)) {
            codigo = getCodigos().get(classe);
        } else {
            codigo = getUltimoCodigo(classe, campo);
        }
        codigo++;
        getCodigos().put(classe, codigo);
        return codigo;
    }

    @Transactional
    private Long getUltimoCodigo(Class classe, String campo) {
        String sql = "select coalesce(max(" + campo + "),0) from " + classe.getSimpleName();
        Object obj = em.createNativeQuery(sql).getSingleResult();
        if (obj instanceof BigDecimal) {
            return ((BigDecimal) obj).longValue();
        }
        if (obj instanceof String) {
            return Long.valueOf((String) obj);
        }
        try {
            return ((Number) obj).longValue();
        } catch (ClassCastException e) {
            throw new ExceptionGenerica("Não foi possível converter " + obj + " para Long");
        }
    }

    @Lock(LockModeType.WRITE)
    @Transactional
    public Long getProximoPlacet() {
        if (getPlacets() == null) {
            setPlacets(Maps.newHashMap());
        }
        Long codigo;
        codigo = getUltimoNumeroPlacetPorGestao();
        codigo++;
        getPlacets().put(PLACET, codigo);
        return codigo;
    }


    @Transactional
    private Long getUltimoNumeroPlacetPorGestao() {
        Gestao gestao = gestaoRepository.buscarGestaoVigente();

        String sql = "select coalesce(max(numero),0) from placet where gestao_id = :gestao";
        Query q = em.createNativeQuery(sql);
        q.setParameter("gestao", gestao.getId());
        q.setMaxResults(1);

        Object obj = q.getSingleResult();
        try {
            Long value = ((Number) obj).longValue();
            if (value.compareTo(0L) <= 0L && gestao.getNumeroPlacet() != null && gestao.getNumeroPlacet().compareTo(0L) <= 0) {
                return value;
            }
            if (value.compareTo(0L) <= 0L && gestao.getNumeroPlacet() != null && gestao.getNumeroPlacet().compareTo(0L) >= 0) {
                return gestao.getNumeroPlacet();
            }
            return value;
        } catch (ClassCastException e) {
            throw new ExceptionGenerica("Não foi possível converter " + obj + " para Long");
        }
    }

    @Scheduled(cron = "0 0 0 1 1 *")
    @Async
    public void zerarCodigoPlacet() {
        Gestao gestao = gestaoRepository.buscarGestaoVigente();
        gestao.setNumeroPlacet(0L);
        gestaoRepository.salvar(gestao);
    }


    public HashMap<Class, Long> getCodigos() {
        return codigos;
    }

    public void setCodigos(HashMap<Class, Long> codigos) {
        this.codigos = codigos;
    }

    public HashMap<Class, Long> getPlacets() {
        return placets;
    }

    public void setPlacets(HashMap<Class, Long> placets) {
        this.placets = placets;
    }
}
