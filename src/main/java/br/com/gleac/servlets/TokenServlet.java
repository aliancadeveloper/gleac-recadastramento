package br.com.gleac.servlets;


import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.negocio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by renatoromanini on 12/11/15.
 */
@WebServlet(name = "forneceToken", urlPatterns = {"/public/token"})
public class TokenServlet extends HttpServlet {

    @Autowired
    private UsuarioRepository usuarioRepository;
    private final String HOME = "/public/primeiro-acesso/senha/";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        OutputStream out = response.getOutputStream();
        try {
            if (!request.getParameter("id").equals("")) {
                String token = request.getParameter("id");
                Usuario usuario = usuarioRepository.recuperarUsuarioPorToken(token);

                if (usuario != null && usuario.getExpiraPrimeiroAcesso().after(new Date())) {
                    out.write("Usuario valido".getBytes());
                    String redirect = request.getContextPath() + HOME + usuario.getId();
                    response.sendRedirect(redirect);
                } else {
                    out.write("Token invalido".getBytes());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TokenServlet.class.getName()).log(Level.SEVERE, null, ex);
            out.write("SEM ARQUIVO".getBytes());
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }
}
