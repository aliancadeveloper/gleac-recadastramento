package br.com.gleac.servlets;

import br.com.gleac.anotacao.Paramiter;
import br.com.gleac.anotacao.ParamiterIgnore;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.ConfiguracaoFinanceira;
import br.com.gleac.entidade.financeiro.DadosRetornoPgHiper;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.TipoNotificacao;
import br.com.gleac.negocio.*;
import br.com.gleac.util.UtilFinanceiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by renato on 30/09/17.
 */
@WebServlet(name = "retornoAutomatico", urlPatterns = {"/retorno/pagHiper/"})
@CrossOrigin(origins = "*")
public class RetornoAutomaticoResource extends HttpServlet {

    @Autowired
    private ConfiguracaoFinanceiraRepository configuracaoFinanceiraRepository;
    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;
    @Autowired
    private DadosRetornoPgHiperRepository dadosRetornoPgHiperRepository;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private GestaoRepository gestaoRepository;


    private DadosRetornoPgHiper dadosRetorno;
    private Boleto boleto;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    protected synchronized void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

    }

    @Override
    @CrossOrigin(origins = "*")
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        super.doGet(httpServletRequest, httpServletResponse);
    }

    @Override
    @CrossOrigin(origins = "*")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        try {
            receberDadosRetorno(request);
            validaDadosRetorno();


            verificarPostJaRecebido();
            confimarComDadosTransacao();

            confirmarPostPgHiper();
            if (dadosRetorno.getStatus().toLowerCase().equals("aprovado")) {
                gerarMovimentacoes();
                salvarRetorno(Boolean.TRUE);
            }
/*            FacesContext.getCurrentInstance().responseComplete();*/

        } catch (Exception ex) {
            try {
                salvarRetorno(Boolean.FALSE);
            } catch (Exception ex1) {
                ex.printStackTrace();
            }
            response.setStatus(404);
            ex.printStackTrace();
        }

    }

    /**
     * Seta os parametros de acordo com as anotações os parametros vindo da
     * requisicao no objeto dados de dados do retorno
     * <p>
     * verifica se se os parametros que devem ser setados de acordo com a
     * anotação paramiter presente no classe DadosRetorno
     *
     * @param request
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException //     * @throws InvocationTargetException
     */
    private void receberDadosRetorno(HttpServletRequest request) throws Exception {

        if (request.getParameterMap() != null && request.getParameterMap().size() > 0) {
            dadosRetorno = new DadosRetornoPgHiper();
            dadosRetorno.setIpHost(request.getRemoteHost() + " , " + request.getRemoteAddr());

            dadosRetorno.setUri(request.getRequestURI());
            Class c = dadosRetorno.getClass();
            Map<String, String[]> params = request.getParameterMap();

            String key = "";
            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                if (c.isAnnotationPresent(Paramiter.class)) {
                    if (!field.isAnnotationPresent(ParamiterIgnore.class)) {
                        setParamiter(field, request);
                    }
                } else if (field.isAnnotationPresent(Paramiter.class)) {
                    setParamiter(field, request);
                }
            }

        } else {
            throw new Exception("Falha recuperar os parametros");
        }

    }

    /**
     * Responsavel por setar efetivamente o parametro que esta presente na
     * entidade com a informação presente no mapa de paramentros da requisiçao
     *
     * @param field
     * @param request
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException //     * @throws InvocationTargetException
     */
    private void setParamiter(Field field, HttpServletRequest request) throws Exception {
        Map<String, String[]> params = request.getParameterMap();

        Method method;
        String nomeMetodo = field.getName();

        nomeMetodo = nomeMetodo.substring(0, 1).toUpperCase()
            + nomeMetodo.substring(1);
        method = dadosRetorno.getClass().getMethod("set" + nomeMetodo, field.getType());
        if (params.get(UtilFinanceiro.nomeParamiter(field)) != null) {
            method.invoke(dadosRetorno, params.get(UtilFinanceiro.nomeParamiter(field))[0]);
        }
    }

    private void validaDadosRetorno() throws Exception {

        if (dadosRetorno.getIdTransacao() == null || dadosRetorno.getIdTransacao().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando id da Transação");
        }
        if (dadosRetorno.getIdPlataforma() == null || dadosRetorno.getIdPlataforma().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando id da plataforma");
        }
        if (dadosRetorno.getStatus() == null || dadosRetorno.getStatus().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando  Status");
        }
        if (dadosRetorno.getValorOriginal() == null || dadosRetorno.getValorOriginal().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando  Valor Original");
        }
        if (dadosRetorno.getValorLoja() == null || dadosRetorno.getValorLoja().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando  Valor Da Loja");
        }
        if (dadosRetorno.getCodRetorno() == null || dadosRetorno.getCodRetorno().equals("")) {
            throw new Exception("Retorno Inconsistente - fantando  Codigo Retorno");
        }

    }

    private void verificarPostJaRecebido() throws Exception {
//        boolean possuiRetonoFechado = dadosRetornoFacade.verificaRetonoFechado(dadosRetorno.getCodRetorno().replace("'", ""));
//        if (possuiRetonoFechado) {
//            throw new Exception("Ja exite um retono para este post com movimentações Realizadas");
//        }
    }

    private void salvarRetorno(Boolean verificado) throws Exception {
        dadosRetorno.setVerificado(verificado);
        dadosRetornoPgHiperRepository.salvarNovo(dadosRetorno);
    }

    private void confimarComDadosTransacao() throws Exception {
        boleto = processoFinanceiroRepository.recuperarBoleto(dadosRetorno.getIdPlataforma().replace("'", ""));
        if (!boleto.getIdTransacao().equals(dadosRetorno.getIdTransacao())) {
            throw new Exception("Id da Transação não confere");
        }

        if (boleto.getValorTotal().compareTo(new BigDecimal(dadosRetorno.getValorOriginal())) != 0) {
            throw new Exception("Valor total não confere");
        }

    }

    private void confirmarPostPgHiper() throws Exception {

        String param = paranRequestConf();
        String retorno;
        retorno = new Comunicacao().post("https://www.paghiper.com/checkout/confirm", param);
        Boolean verificou = "VERIFICADO".equals(retorno);

        if (!verificou) {
            throw new Exception("Falha ao confimar com servidor pagHiper");
        }

    }

    /**
     * Recupera os parametros nessessarios para fazer uma consulta de
     * confirmação de atualiz
     *
     * @return
     */
    private String paranRequestConf() throws UnsupportedEncodingException {
        ConfiguracaoFinanceira configuracaoFinanceira = configuracaoFinanceiraRepository.recuperarConfiguracao();
        HashMap<String, String> params = new HashMap<>();

        params.put("idTransacao", boleto.getIdTransacao());
        params.put("status", dadosRetorno.getStatus());
        params.put("codRetorno", dadosRetorno.getCodRetorno());
        params.put("valorOriginal", dadosRetorno.getValorOriginal());
        params.put("valorLoja", dadosRetorno.getValorLoja());
        params.put("token", configuracaoFinanceira.getToken());
        String retorno = params.toString().replace("{", "").replace("}", "").replace(", ", "&");
        return retorno;
    }

    private void gerarMovimentacoes() throws Exception {
        ProcessoFinanceiro processoFinanceiro = processoFinanceiroRepository.recuperarPorBoleto(boleto);
        processoFinanceiroRepository.gerarReceitaBoletoPago(processoFinanceiro, dadosRetorno.getStatus());

        PessoaFisica pessoaFisica = gestaoRepository.buscarGestaoVigente().getSecretario().getPessoaFisica();
        Usuario usuario = usuarioRepository.recuperarUsuarioPorPessoa(pessoaFisica);
        notificacaoRepository.salvarNovo(new Notificacao(new Date(),
            usuario,
            TipoNotificacao.PROCESSO,
            "Confirmação de pagamento do processo " + processoFinanceiro.getSolicitacaoProcesso().getNumero() + "!",
            "/admin/processo-financeiro/editar/" + processoFinanceiro.getId() + "/"));
    }


    public class Comunicacao {

        public String post(String enderecoServico, String parametros) throws IOException {
            URL url = new URL(enderecoServico);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Request-Method", "POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.connect();
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
            writer.write(parametros);
            writer.flush();
            writer.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer newData = new StringBuffer(100000);
            String s = "";
            while (null != ((s = br.readLine()))) {
                newData.append(s);
            }
            String resposta = newData.toString();
            br.close();
            return resposta;
        }

        public StringBuilder Get(String enderecoServico) throws IOException {

            BufferedReader in = null;
            StringBuilder ret = new StringBuilder("");

            URL url = new URL(enderecoServico);
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                ret.append(str);
            }

            return ret;
        }

    }
}

