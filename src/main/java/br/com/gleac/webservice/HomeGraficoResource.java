package br.com.gleac.webservice;


import br.com.gleac.entidade.financeiro.SaldoFinanceiro;
import br.com.gleac.negocio.LancamentoFinanceiroRepository;
import br.com.gleac.negocio.PagamentoRepository;
import br.com.gleac.negocio.ReceitaRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.util.Util;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by renatoromanini on 21/10/17.
 */

@RestController
@RequestMapping("/grafico")
public class HomeGraficoResource implements Serializable {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;

    @RequestMapping(value = "/despesa-receita", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> despesaReceita() throws UnsupportedEncodingException {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("labels", getJsonMeses());
            jsonObject.put("datasets", getJsonValores());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }


    private JSONArray getJsonValores() throws JSONException {
        JSONArray jsonObject = new JSONArray();
        jsonObject.put(getJsonDadosDespesa());
        jsonObject.put(getJsonDadosReceita());
        jsonObject.put(getJsonDadosSaldo());
        return jsonObject;
    }

    private JSONObject getJsonDadosSaldo() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("label", "Dados secundários");
        jsonObject.put("fillColor", "rgba(51, 102, 153,0.2)");
        /*jsonObject.put("strokeColor", "rgba(51, 102, 153,1)");*/
        jsonObject.put("pointColor", "rgba(51, 102, 153,1)");
        jsonObject.put("pointStrokeColor", "#fff");
        jsonObject.put("pointHighlightFill", "#fff");
        jsonObject.put("pointHighlightStroke", "rgba(51, 102, 153,1)");
        jsonObject.put("legend", "2");
        jsonObject.put("data", getJsonDadosSaldoValores());
        return jsonObject;
    }

    private JSONObject getJsonDadosDespesa() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("label", "Dados secundários");
        jsonObject.put("fillColor", "rgba(255, 0, 0,0.2)");
        /*jsonObject.put("strokeColor", "rgba(255, 0, 0,1)");*/
        jsonObject.put("pointColor", "rgba(255, 0, 0,1)");
        jsonObject.put("pointStrokeColor", "#fff");
        jsonObject.put("pointHighlightFill", "#fff");
        jsonObject.put("pointHighlightStroke", "rgba(255, 0, 0,1)");
        jsonObject.put("legend", "2");
        jsonObject.put("data", getJsonDadosDespesaValores());
        return jsonObject;
    }

    private JSONObject getJsonDadosReceita() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("label", "Dados primários");
        jsonObject.put("fillColor", "rgba(26,179,148,0.5)");
        /*jsonObject.put("strokeColor", "rgba(26,179,148,0.7)");*/
        jsonObject.put("pointColor", "rgba(26,179,148,1)");
        jsonObject.put("pointStrokeColor", "#fff");
        jsonObject.put("pointHighlightFill", "#fff");
        jsonObject.put("pointHighlightStroke", "rgba(26,179,148,1)");
        jsonObject.put("legend", "1");
        jsonObject.put("data", getJsonDadosReceitaValores());
        return jsonObject;
    }

    private JSONArray getJsonDadosDespesaValores() {
        JSONArray jsonObject = new JSONArray();
        /*for (Mes mes : Mes.values()) {
            BigDecimal valorDespesa = pagamentoRepository.recuperarSoma(Util.recuperarExercicio(), mes.getNumeroComZero(), sistemaService.getLojaCorrente());
            jsonObject.put(valorDespesa);
        }*/

        Date dataInicial = new Date();
        Date mesAtraz = ultimosQuinzeDias(dataInicial);
        int i = diasEntreDate(mesAtraz, dataInicial);

        Date ultimaData = adicionaDias(mesAtraz, 1);
        for (int j = 0; j < i; j++) {
            ultimaData = adicionaDias(ultimaData, +1);
            BigDecimal valorDespesa = pagamentoRepository.recuperarSoma(Util.recuperarExercicio(), null, ultimaData, sistemaService.getLojaCorrente());
            jsonObject.put(valorDespesa);
        }
        return jsonObject;
    }

    private JSONArray getJsonDadosReceitaValores() {
        JSONArray jsonObject = new JSONArray();
       /* for (Mes mes : Mes.values()) {
            BigDecimal valorDespesa = receitaRepository.recuperarSoma(Util.recuperarExercicio(), mes.getNumeroComZero(), sistemaService.getLojaCorrente());
            jsonObject.put(valorDespesa);
        }*/

        Date dataInicial = new Date();
        Date mesAtraz = ultimosQuinzeDias(dataInicial);
        int i = diasEntreDate(mesAtraz, dataInicial);

        Date ultimaData = adicionaDias(mesAtraz, 1);
        for (int j = 0; j < i; j++) {
            ultimaData = adicionaDias(ultimaData, +1);
            BigDecimal valorDespesa = receitaRepository.recuperarSoma(Util.recuperarExercicio(), null, ultimaData, sistemaService.getLojaCorrente());
            jsonObject.put(valorDespesa);
        }
        return jsonObject;
    }

    private JSONArray getJsonDadosSaldoValores() {
        JSONArray jsonObject = new JSONArray();
       /* for (Mes mes : Mes.values()) {
            BigDecimal valorDespesa = receitaRepository.recuperarSoma(Util.recuperarExercicio(), mes.getNumeroComZero(), sistemaService.getLojaCorrente());
            jsonObject.put(valorDespesa);
        }*/
        Date dataInicial = new Date();
        Date mesAtraz = ultimosQuinzeDias(dataInicial);
        int i = diasEntreDate(mesAtraz, dataInicial);

        Date ultimaData = adicionaDias(mesAtraz, 1);
        for (int j = 0; j < i; j++) {
            ultimaData = adicionaDias(ultimaData, +1);
            SaldoFinanceiro saldoFinanceiro = new SaldoFinanceiro(sistemaService.getLojaCorrente(), null, null, ultimaData);
            SaldoFinanceiro ultimoSaldoPorDataLojaConta = lancamentoFinanceiroRepository.getSaldoNaData(saldoFinanceiro);
            if (ultimoSaldoPorDataLojaConta == null) {
                jsonObject.put(BigDecimal.ZERO);
            } else {
                jsonObject.put(ultimoSaldoPorDataLojaConta.getSaldo());
            }
        }
        return jsonObject;
    }


    private JSONArray getJsonMeses() {
        JSONArray jsonObject = new JSONArray();
        Date dataInicial = new Date();
        Date mesAtraz = ultimosQuinzeDias(dataInicial);
        int i = diasEntreDate(mesAtraz, dataInicial);

        Date ultimaData = adicionaDias(mesAtraz, 1);
        for (int j = 0; j < i; j++) {
            ultimaData = adicionaDias(ultimaData, +1);
            jsonObject.put(Util.sdf.format(ultimaData));
        }

        /*for (Mes mes : Mes.values()) {
            jsonObject.put(mes.getDescricao());
        }*/


        return jsonObject;
    }

    public static int diasEntreDate(Date begin, Date end) {
        return diasEntre(new DateTime(begin), new DateTime(end));
    }

    public static int diasEntre(DateTime begin, DateTime end) {
        return Days.daysBetween(begin, end).getDays() + 1;
    }

    public static Date adicionaDias(Date dataAtual, Integer dias) {
        Calendar c = Calendar.getInstance();
        c.setTime(dataAtual);
        c.add(Calendar.DAY_OF_MONTH, dias);
        return c.getTime();
    }

    public static Date removerDias(Date dataAtual, Integer dias) {
        Calendar c = Calendar.getInstance();
        c.setTime(dataAtual);
        c.add(Calendar.DAY_OF_MONTH, -dias);
        return c.getTime();
    }

    public static Date ultimosQuinzeDias(Date dataAtual) {
        Calendar c = Calendar.getInstance();
        c.setTime(dataAtual);
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH) - 15);
        return c.getTime();
    }
}
