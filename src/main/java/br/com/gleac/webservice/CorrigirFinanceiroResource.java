package br.com.gleac.webservice;


import br.com.gleac.entidade.financeiro.Pagamento;
import br.com.gleac.entidade.financeiro.Receita;
import br.com.gleac.negocio.LancamentoFinanceiroRepository;
import br.com.gleac.negocio.PagamentoRepository;
import br.com.gleac.negocio.ReceitaRepository;
import br.com.gleac.service.SistemaService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by renatoromanini on 21/10/17.
 */

@RestController
@RequestMapping("/financeiro")
public class CorrigirFinanceiroResource implements Serializable {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;

    @RequestMapping(value = "/corrigir", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional(timeout = 10000)
    public ResponseEntity<String> corrigir() throws UnsupportedEncodingException {
        try {
            lancamentoFinanceiroRepository.removerTodos();
            List<Pagamento> despesas = pagamentoRepository.listar();
            for (Pagamento despesa : despesas) {
                lancamentoFinanceiroRepository.gerarSaldo(despesa);
            }
            List<Receita> receitas = receitaRepository.listar();
            for (Receita receita : receitas) {
                lancamentoFinanceiroRepository.gerarSaldo(receita);
            }
            return new ResponseEntity<>("Corrigido com sucesso.... Despesa: " + despesas.size() + "/ Receita:" + receitas.size() + ".", HttpStatus.OK);
        } catch (JSONException e) {
            return new ResponseEntity<>(e.getMessage().toString(), HttpStatus.OK);
        }
    }
}
