package br.com.gleac.supers;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.util.Persistencia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by renatoromanini on 14/08/15.
 */

public abstract class AbstractRepository<T> {

    public static final String ERRO_PADRAO = "Ocorreu um erro ao tentar executar esta ação, por favor refaça o processo, se o erro persistir contate o suporte técnico.";
    public static final String URL_SITE = "http://127.0.0.1:8080/site";
    private Class<T> classe;

    public AbstractRepository(Class<T> classe) {
        this.classe = classe;
    }

    public Class<T> getClasse() {
        return classe;
    }

    protected Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }

    protected abstract EntityManager getEntityManager();

    @Transactional
    public List<T> listar() {
        Field idField = Persistencia.getFieldId(classe);
        String hql = "from " + classe.getSimpleName() + " obj order by obj." + (idField != null ? idField.getName() : "id");
        Query q = getEntityManager().createQuery(hql);

        return q.getResultList();
    }

    @Transactional
    public void salvar(T entity) {
        try {
            preSaveOrUpdate((AbstractEntity) entity);
            getEntityManager().merge(entity);
        } catch (PersistenceException e) {
            getLogger().error("Erro ao atualizar o registro " + e.getMessage());
        }
    }

    @Transactional
    public T salvarRetornando(T entity) {
        try {
            preSaveOrUpdate((AbstractEntity) entity);
            return getEntityManager().merge(entity);
        } catch (PersistenceException e) {
            getLogger().error("Erro ao atualizar o registro " + e.getMessage());
            return entity;
        }

    }


    @Transactional
    public void salvarNovo(T entity) {
        try {

            preSaveOrUpdate((AbstractEntity) entity);
            getEntityManager().persist(entity);
            getLogger().info("veio no abstract negocio e salvou novo");
        } catch (RuntimeException e) {
            throw e;
        }


    }

    public void preSaveOrUpdate(AbstractEntity se) {
        se.validarRegrasPrincipais();
    }

    @Transactional
    public T recuperar(Class entidade, Object id) {
        try {
            return (T) getEntityManager().find(entidade, id);
        } catch (Exception exception) {
            throw new ExceptionGenerica(ERRO_PADRAO);
        }

    }

    @Transactional
    public Long count() {

        try {
            Query query = getEntityManager().createQuery("select count(obj.id) from " + classe.getSimpleName() + " obj");
            return (Long) query.getSingleResult();
        } catch (NoResultException nre) {
            return 0L;
        }
    }

    @Transactional
    public void excluir(T entity) {
        try {
            Object chave = Persistencia.getId(entity);
            Object obj = recuperar(this.classe, chave);
            if (obj != null) {
                getEntityManager().remove(obj);
            }
        } catch (ClassCastException cce) {
            getLogger().error("Erro ao excluir o registro (ClassCastException): " + cce.getMessage());
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            getLogger().error("Erro ao excluir o registro (Exception)" + ex.getMessage());
        }


    }

    @Transactional
    public List<T> completarEstaEntidade(String param) {
        String hql = "select ";
        hql = complementarCriacaoObjeto(hql);

        hql += " from " + classe.getSimpleName() + " obj";
        hql += " where ";

        hql = complementarWhereQuery(hql, param);
        Query q = getEntityManager().createQuery(hql, classe);
        q.setMaxResults(10);
        q.setParameter("param", "%" + param + "%");
        return q.getResultList();
    }

    @Transactional
    public String complementarCriacaoObjeto(String q) {
        q += " new " + classe.getSimpleName() + "(";
        q += "obj." + Persistencia.getFieldId(classe).getName() + ", ";

        for (Field f : Persistencia.getAtributos(classe)) {
            if (f.isAnnotationPresent(CRUD.class)) {
                CRUD annotation = f.getAnnotation(CRUD.class);
                if (annotation.pesquisavelAutoComplete()) {
                    q += " obj." + f.getName() + ", ";
                }
            }
        }

        q = q.endsWith(", ") ? q.substring(0, q.length() - 2) : q;
        q += ") ";
        return q;
    }

    @Transactional
    public String complementarWhereQuery(String q, String p) {
        q += "(";
        for (Field f : Persistencia.getAtributos(classe)) {
            if (f.isAnnotationPresent(CRUD.class)) {
                CRUD annotation = f.getAnnotation(CRUD.class);
                if (annotation.pesquisavelAutoComplete()) {
                    q += " lower(cast(obj." + f.getName() + " as text)) like lower(:param) or";
                }
            }
        }
        q = q.endsWith(" or") ? q.substring(0, q.length() - 3) : q;
        q += ")";
        return q;
    }


}
