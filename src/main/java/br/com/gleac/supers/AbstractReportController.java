package br.com.gleac.supers;

import br.com.gleac.entidade.comum.ConfiguracaoCabecalho;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.ConfiguracaoCabecalhoRepository;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.util.FacesUtil;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.util.HashMap;

/**
 * Created by renatoromanini on 25/08/15.
 */
public abstract class AbstractReportController {


    @Autowired
    private AbstractReport abstractReport;
    @Autowired
    private ConfiguracaoCabecalhoRepository configuracaoCabecalhoRepository;
    private Loja lojaCorrente;
    @Autowired
    protected SistemaService sistemaService;
    @Autowired
    private LojaRepository lojaRepository;

    public AbstractReportController() {
        injetarDependenciasSpring();
    }

    protected Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    public void gerarRelatorio() {
        try {
            abstractReport.gerarRelatorio(getNomeJasper(), getParametros());
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addError("Problema ao gerar relatório!", e.getMessage());
        }
    }

    public void gerarRelatorio(HashMap parametros) {
        try {
            abstractReport.gerarRelatorio(getNomeJasper(), parametros);
        } catch (ValidacaoException ex) {
            ex.printStackTrace();
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addError("Problema ao gerar relatório!", e.getMessage());
        }
    }

    public void gerarRelatorioComDadosEmCollection(JRBeanCollectionDataSource jrbc) {
        try {
            abstractReport.gerarRelatorioComDadosEmCollectionAlterandoNomeArquivo(getNomePDF(), getNomeJasper(), getParametros(), jrbc);
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception e) {
            FacesUtil.addError("Problema ao gerar relatório!", e.getMessage());
        }
    }

    public ConfiguracaoCabecalho getConfiguracaoCabecalho() {
        ConfiguracaoCabecalho configuracao = configuracaoCabecalhoRepository.buscarConfiguracaoCabecalho();
        try {
            return configuracao;
        } catch (Exception ex) {
            FacesUtil.addOperacaoNaoRealizada("Configuração não encontrada!");
            return null;
        }
    }

    public Loja getLojaCorrente() {
        if (lojaCorrente == null) {
            lojaCorrente = sistemaService.getLojaCorrente();
        }
        return lojaCorrente;
    }

    public boolean isLojaGestora() {
        return lojaRepository.buscarLojaGestora().equals(getLojaCorrente());
    }

    public String getCaminho() {
        return abstractReport.getCaminho();
    }

    public abstract String getNomeJasper();

    public abstract String getNomePDF();

    public abstract HashMap getParametros();

    public AbstractReport getAbstractReport() {
        return abstractReport;
    }
}
