package br.com.gleac.supers;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Recurso;
import br.com.gleac.enums.OperacaoCondicaoSql;
import br.com.gleac.enums.TipoComponente;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.ViewRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import br.com.gleac.util.view.*;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.PrettyContext;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by renatoromanini on 25/08/15.
 */
public abstract class AbstractController<T> {

    public static final Logger log = LoggerFactory.getLogger(AbstractController.class);

    private static final Integer MAXIMO_REGISTROS_TABELA = 10;
    private List lista;
    private ConverterAutoComplete converterAutoComplete;
    protected Class<T> classe;
    public T selecionado;
    private Long id;
    private View view;
    @Autowired
    private ViewRepository viewRepository;
    @Autowired
    private SistemaService sistemaService;

    public abstract AbstractRepository getRepository();


    public AbstractController() {
        injetarDependenciasSpring();
    }

    protected Logger getLogger() {
        return LoggerFactory.getLogger(this.getClass());
    }

    public void realizarValidacao() {
        Util.validarCampos((AbstractEntity) selecionado);
    }

    public AbstractController(Class<T> classe) {
        this.classe = classe;
        this.view = new View();
        injetarDependenciasSpring();
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    public ConverterAutoComplete getConverterAutoComplete() {
        if (converterAutoComplete == null) {
            converterAutoComplete = new ConverterAutoComplete(classe, getRepository());
        }
        return converterAutoComplete;
    }

    public void novo() {

        try {

            if (selecionado == null) {
                this.selecionado = classe.newInstance();
            }
            recuperarLojaSessao();
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void recuperarLojaSessao() {
        if (esteSelecionadoPossuiLoja()) {
            try {
                getFieldLoja().set(selecionado, sistemaService.getLojaCorrente());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public Field getFieldLoja() {
        for (Field field : Persistencia.getAtributos(classe)) {
            field.setAccessible(true);
            if (field.getType().equals(Loja.class)) {
                return field;
            }
        }
        return null;
    }

    public Boolean esteSelecionadoPossuiLoja() {
        for (Field field : Persistencia.getAtributos(classe)) {
            field.setAccessible(true);
            if (field.getType().equals(Loja.class)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public void listar() {
        filtrar();
    }

    public void editar() {
        this.selecionado = (T) getRepository().recuperar(classe, id);


    }

    public void visualizar() {
        this.selecionado = (T) getRepository().recuperar(classe, id);
    }

    public void salvar() {
        try {
            if (Persistencia.getId(this.selecionado) == null) {
                getRepository().salvarNovo(this.selecionado);
            } else {
                getRepository().salvar(this.selecionado);
            }
            FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.selecionado.toString() + " foi <b> salvo </b> com sucesso!");
        } catch (RuntimeException ex) {
            if (ex instanceof ValidacaoException) {
                FacesUtil.printAllFacesMessages(((ValidacaoException) ex).getMensagens());
            } else {
                FacesUtil.addErrorPadrao(ex);
            }
            return;
        } catch (Exception ex) {
            FacesUtil.addErrorPadrao(ex);
            return;
        }

        navegarEmbora();

    }

    public void navegarEmbora() {
        FacesUtil.navegaEmbora(selecionado, getCaminhoPadrao());
    }

    public String getCaminhoPadrao() {
        return ((AbstractEntity) selecionado).getCaminhoPadrao();
    }

    public void excluir() {
        try {
            getRepository().excluir(selecionado);
            lista = null;
            FacesUtil.addInfo("Operação realizada com sucesso.", "Registro excluido com sucesso");
            navegarEmbora();
        } catch (RuntimeException ex) {
            FacesUtil.addError("Operação não permitida.", ex.getMessage());
        } catch (Exception ex) {
            FacesUtil.addErrorPadrao(ex);
        }
    }

    public String getCaminhoNovo() {
        return getClasseAsSuperEntidade().getCaminhoPadrao() + "novo/";
    }

    public List<SelectItem> getObjetosSelectItem() {
        List<SelectItem> toReturn = new ArrayList();
        try {

            if (selecionado == null) {
                this.selecionado = classe.newInstance();
            }
            AbstractEntity se = (AbstractEntity) this.selecionado;
            String todos = "TOD" + se.getArtigoDefinidoDaEntidade() + "S " + se.getArtigoDefinidoDaEntidade() + "S " + se.getNomePluralDaEntidade() + ".";
            toReturn.add(new SelectItem(null, todos.toUpperCase()));
            for (Object obj : getRepository().listar()) {
                toReturn.add(new SelectItem(obj, obj.toString()));
            }


        } catch (Exception e) {

        }
        return Util.ordenaSelectItem(toReturn);

    }

    public List<T> recuperarTodos() {
        return getRepository().listar();
    }


    public List<Field> getAtributosTabelaveis() {
        return Persistencia.getAtributosTabelaveis(classe);
    }

    public String getNomeCampo(Field field) {
        return Persistencia.getNomeCampo(field);
    }

    public String getValorCampo(Field field, Object objeto) {
        return Persistencia.getValorCampo(field, objeto);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public T getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(T selecionado) {
        this.selecionado = selecionado;
    }

    public void filtrar() {
        View view = getView();
        view.setNomeRelatorio("Relatório de " + Persistencia.getNomeEntidade(classe));
        view.setColunas(Lists.newArrayList());
        String sql = Persistencia.adicionarColunas(view, "obj", classe);
        String sqlSelect = " select obj.id, " + sql.substring(0, sql.length() - 2) + "  from " + classe.getSimpleName() + " obj " +
            " where 1=1 ";

        for (Field field : Persistencia.getAtributosPesquisaveis(classe)) {
            criarParametroView(view, field);
        }

        montarParametroLoja(view);

        view.setSqlRecuperadorObjetos(sqlSelect);
        view.setSqlOrdenar(" order by obj.id desc");
        view.setSqlContadorObjetos(" select count(obj.id)  from " + classe.getSimpleName() + " obj where 1=1 ");

        view = viewRepository.recuperarObjetos(view);
        view.setTotalDeRegistrosExistentes(null);
    }

    public void montarParametroLoja(View view) {
        Loja loja = sistemaService.getLojaCorrente();
        Field field = getFieldLoja();
        if (esteSelecionadoPossuiLoja() && loja != null && field != null && !sistemaService.isLojaGestora()) {
            ParametroView parametroView = new ParametroView(Persistencia.getNomeCampo(field), getCondicao(field), loja.getId(), OperacaoCondicaoSql.IGUAL, null, field.getType());
            view.getParametros().add(parametroView);
        }
    }

    private String getCondicao(Field field) {
        return "obj." + (field.getType().isAnnotationPresent(Entity.class) ? field.getName() + "_id" : field.getName());
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public String getArtigoEntidade() {
        AbstractEntity obj;
        String PADRAO_INDEFINIDO = "o/a";
        try {
            obj = getClasseAsSuperEntidade();
            return obj.getArtigoDefinidoDaEntidade();
        } catch (ClassCastException cce) {
            return PADRAO_INDEFINIDO;
        }
    }

    public AbstractEntity getClasseAsSuperEntidade() {
        try {
            AbstractEntity obj = (AbstractEntity) classe.newInstance();
            return obj;
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AbstractController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
        return null;
    }

    public String getNomeEntidade() {
        if (classe.isAnnotationPresent(CRUD.class)) {
            return classe.getAnnotation(CRUD.class).label();
        }
        return classe.getSimpleName();
    }

    public String obterLabelCampo(Field f) {
        f.setAccessible(true);
        if (f.isAnnotationPresent(CRUD.class)) {
            return f.getAnnotation(CRUD.class).label();
        }
        return "";
    }

    public void alterarQuantidadeDeRegistrosPara(Long i) {
        view.setMaximoRegistrosTabela(i.intValue());
        view.setInicio(0);
        filtrar();
    }

    public void alterarQuantidadeDeRegistrosPara(Integer i) {
        view.setMaximoRegistrosTabela(i);
        view.setInicio(0);
        filtrar();
    }

    public void proximos() {
        view.setInicio(view.getInicio() + view.getMaximoRegistrosTabela());
        filtrar();
    }

    public void anteriores() {
        view.setInicio(view.getInicio() - view.getMaximoRegistrosTabela());
        if (view.getInicio() < 0) {
            view.setInicio(0);
        }
        filtrar();
    }

    public boolean isTemMaisResultados() {
        if (view.getObjetos() == null) {
            filtrar();
        }
        return view.getObjetos().size() >= view.getMaximoRegistrosTabela();
    }

    public boolean isTemAnterior() {
        return view.getInicio() > 0;
    }

    public void mostrarPrimeirosRegistros() {
        view.setInicio(0);
        filtrar();
    }


    public void mostrarUltimosRegistros() {
        view.setInicio(getTotalDeRegistrosExistentes());
        view.setInicio(view.getInicio() - view.getMaximoRegistrosTabela());
        Integer pgAtual = getPaginaAtual();
        if (view.getTotalDeRegistrosExistentes() - (view.getMaximoRegistrosTabela() * pgAtual) < view.getMaximoRegistrosTabela()) {
            view.setInicio(view.getMaximoRegistrosTabela() * pgAtual);
        }

        filtrar();
    }

    public boolean desabilitarBotaoUtilmo() {
        return getTotalDeRegistrosExistentes() <= (view.getInicio() + view.getMaximoRegistrosTabela());
    }

    public boolean desabilitarBotaoPrimeiro() {
        return view.getInicio() <= 0;
    }

    public Integer getPaginaAtual() {
        Double inicialD = new Double("" + view.getInicio());
        Double maximoD = new Double("" + view.getMaximoRegistrosTabela());
        Double totalD = new Double("" + getTotalDeRegistrosExistentes());

        Double pDivisao = totalD / maximoD;
        Double psoma = maximoD + inicialD;
        Double pMultiplicacao = pDivisao * psoma;
        pMultiplicacao = Math.ceil(pMultiplicacao);

        Double pResultado = pMultiplicacao / totalD;

        return pResultado.intValue();
    }

    public Integer getTotalDePaginas() {
        Double maximoD = new Double("" + view.getMaximoRegistrosTabela());
        Double totalD = new Double("" + getTotalDeRegistrosExistentes());
        Double totalDePaginas = Math.ceil(totalD / maximoD);

        return totalDePaginas.intValue();
    }

    public Integer getTotalDeRegistrosExistentes() {
        if (view.getTotalDeRegistrosExistentes() == null) {
            view.setTotalDeRegistrosExistentes(getObjetosViewComParametros(view.getParametros()));
        }
        return view.getTotalDeRegistrosExistentes();

    }

    public Integer getObjetosViewComParametros(List<ParametroView> parametros) {
        try {
            return getViewRepository().recuperarQuantidadeRegistroExistente(view).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public String botaoSelecionado(Integer i) {
        if (i != null && view != null) {
            if (i == view.getMaximoRegistrosTabela()) {
                return "btn btn-primary link-discreto m-xs";
            }
        }
        return "btn btn-success link-discreto m-xs ";
    }

    public List<T> completarEstaEntidade(String parte) {
        return getRepository().completarEstaEntidade(parte.trim());
    }

    public ViewRepository getViewRepository() {
        return viewRepository;
    }

    public void setConverterAutoComplete(ConverterAutoComplete converterAutoComplete) {
        this.converterAutoComplete = converterAutoComplete;
    }

    public void criarParametroView(View view, Field field) {
        field.setAccessible(true);
        CRUD crud = field.getAnnotation(CRUD.class);
        if (crud.pesquisavel()) {
            String condicao = getCondicao(field);
            String nomeCampo = Persistencia.getNomeCampo(field);
            OperacaoCondicaoSql operacao = getOperacao(field);
            TipoComponente tipoComponente = getTipoComponente(field);
            ParametroView parametroView = new ParametroView(nomeCampo, condicao, null, operacao, tipoComponente, field.getType());
            if (podeAdicionarParametro(view, parametroView)) {
                if (crud.controlador() != null) {
                    if (!crud.controlador().equals(AbstractController.class)) {
                        try {
                            ManagedBean annotation = (ManagedBean) crud.controlador().getAnnotation(ManagedBean.class);
                            parametroView.atribuirControlador((AbstractController) Util.getControladorPeloNome(annotation.name()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                view.getParametros().add(parametroView);
            }
        }
    }

    private TipoComponente getTipoComponente(Field field) {
        CRUD crud = field.getAnnotation(CRUD.class);
        if (crud.tipoComponente().equals(TipoComponente.PADRAO)) {
            if (field.getType().isAnnotationPresent(Entity.class)) {
                return TipoComponente.AUTO_COMPLETE;
            } else if (field.isAnnotationPresent(Enumerated.class)) {
                return TipoComponente.SELECT_MENU_ENUM;
            } else if (field.isAnnotationPresent(Temporal.class)) {
                if (field.getAnnotation(Temporal.class).value().equals(TemporalType.DATE)) {
                    return TipoComponente.DATA;
                } else if (field.getAnnotation(Temporal.class).value().equals(TemporalType.TIME)) {
                    return TipoComponente.HORA;
                }
                return TipoComponente.DATA_HORA;
            } else {
                return TipoComponente.STRING;
            }
        }
        return crud.tipoComponente();
    }

    private OperacaoCondicaoSql getOperacao(Field field) {
        if (field.getType().isAnnotationPresent(Entity.class)
            || field.isEnumConstant()
            || field.isAnnotationPresent(Temporal.class)) {
            CRUD crud = field.getAnnotation(CRUD.class);
            if (crud.tipoComponente().equals(TipoComponente.DATA_BETWEEN)) {
                return OperacaoCondicaoSql.BETWEEN;
            }
            return OperacaoCondicaoSql.IGUAL;
        }
        return OperacaoCondicaoSql.LIKE;
    }

    public void pesquisar() {
        view = viewRepository.recuperarObjetos(getView());
        view.setTotalDeRegistrosExistentes(null);
    }

    public void limparFiltros() {
        for (ParametroView parametroView : getView().getParametros()) {
//            if (!parametroView.getClasse().equals(Loja.class)) {
//                parametroView.setValor(null);
//            }
            parametroView.setValor(null);
        }
        pesquisar();

    }

    public boolean podeAdicionarParametro(View view, ParametroView parametroView) {
        for (ParametroView par : view.getParametros()) {
            if (par.getCondicao().equals(parametroView.getCondicao())) {
                return false;
            }
        }

        return true;
    }

    public void gerarRelatorioGenerico() {
        try {
            new PdfView().imprimirPDF(view);
            FacesUtil.addInfo("Gerado com sucesso.", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public Boolean mostrarBotaoExcluir(HttpServletRequest request) {
        PrettyContext prettyInstance = PrettyContext.getCurrentInstance(request);
        String currentePattern = prettyInstance.getCurrentMapping().getPattern();
        if (!ObjectUtils.isEmpty(sistemaService.getRecursos())) {
            try {
                for (Recurso recurso : sistemaService.getRecursos()) {
                    if (currentePattern.contains(recurso.getRecursoSistema().getPath())) {
                        return recurso.isExclusao();
                    }
                }

            } catch (Exception e) {
                return false;
            }

        }
        return Boolean.FALSE;
    }

    public Boolean mostrarBotaoVisualizacao(HttpServletRequest request) {
        PrettyContext prettyInstance = PrettyContext.getCurrentInstance(request);
        String currentePattern = prettyInstance.getCurrentMapping().getPattern();
        if (!ObjectUtils.isEmpty(sistemaService.getRecursos())) {
            try {
                for (Recurso recurso : sistemaService.getRecursos()) {
                    if (currentePattern.contains(recurso.getRecursoSistema().getPath())) {
                        return recurso.isVisualizar();
                    }
                }

            } catch (Exception e) {
                return false;
            }

        }
        return Boolean.FALSE;
    }

    public Boolean mostrarBotaoEdicao(HttpServletRequest request) {
        PrettyContext prettyInstance = PrettyContext.getCurrentInstance(request);
        String currentePattern = prettyInstance.getCurrentMapping().getPattern();
        if (!ObjectUtils.isEmpty(sistemaService.getRecursos())) {
            try {
                for (Recurso recurso : sistemaService.getRecursos()) {
                    if (currentePattern.contains(recurso.getRecursoSistema().getPath())) {
                        return recurso.isEditar();
                    }
                }

            } catch (Exception e) {
                return false;
            }

        }
        return Boolean.FALSE;
    }

    public Boolean mostrarBotaoSalvar(HttpServletRequest request) {
        PrettyContext prettyInstance = PrettyContext.getCurrentInstance(request);
        String currentePattern = prettyInstance.getCurrentMapping().getPattern();
        if (!ObjectUtils.isEmpty(sistemaService.getRecursos())) {
            try {
                for (Recurso recurso : sistemaService.getRecursos()) {
                    if (currentePattern.contains(recurso.getRecursoSistema().getPath())) {
                        return recurso.isCadastrar();
                    }
                }

            } catch (Exception e) {
                return false;
            }

        }
        return Boolean.FALSE;
    }

    public StreamedContent fileDownload() {
        try {
            List<Object[]> objetos = new ArrayList<>();
            List<String> titulos = new ArrayList<>();

            for (ObjetoView objetoView : view.getObjetos()) {
                for (ColunaView colunaView : objetoView.getColunasImpressao()) {
                    titulos.add(colunaView.getNomeColuna());
                }
                break;
            }

            for (ObjetoView objetoView : view.getObjetos()) {
                Object[] obj = new Object[objetoView.getColunasImpressao().size()];
                for (ColunaView colunaView : objetoView.getColunasImpressao()) {
                    if (colunaView == null || colunaView.getValor() == null) {

                    } else {
                        obj[objetoView.getColunasImpressao().indexOf(colunaView)] = colunaView.getValor().toString().replaceAll("\\<.*?>", "");
                    }
                }
                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            String titulo = "Relatório de " + Persistencia.getNomeEntidade(classe);
            excel.gerarExcel(titulo, "excel", titulos, objetos, "", sistemaService.getUsuarioCorrente().getNome());
            return excel.fileDownload();
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesUtil.addErrorOperacaoNaoPermitida(ex.getMessage());
        }
        return null;
    }

}
