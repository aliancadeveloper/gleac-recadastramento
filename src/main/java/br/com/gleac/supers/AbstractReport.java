package br.com.gleac.supers;

import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.service.SistemaService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by renat on 18/06/2016.
 */
@Component
public class AbstractReport implements Serializable {

    @Autowired
    @Qualifier("datasource")
    private DataSource dataSource;
    @Autowired
    protected SistemaService sistemaService;

    private Connection recuperaConexaoJDBC() {
        Connection conexao = null;
        try {
            DataSource ds = dataSource;
            conexao = ds.getConnection();
            return conexao;

        } catch (SQLException ex) {
            return conexao;
        } catch (Exception ex) {
            return conexao;
        }
    }

    public void gerarRelatorio(String arquivoJasper, HashMap parametros) throws JRException, IOException {
        //recupera informação do faces
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.responseComplete();
        ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();
        //gera relatorio com as classes do jasper
        JasperPrint jasperPrint = JasperFillManager.fillReport(scontext.getRealPath("/WEB-INF/relatorios/" + arquivoJasper), parametros, recuperaConexaoJDBC());
        ByteArrayOutputStream dadosByte = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(dadosByte));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCompressed(true);
        exporter.setConfiguration(configuration);
        exporter.exportReport();
        byte[] bytes = dadosByte.toByteArray();
        if (bytes != null && bytes.length > 0) {
            int recorte = arquivoJasper.indexOf(".");
            String nomePDF = arquivoJasper.substring(0, recorte);
            HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "inline; filename=\"" + nomePDF + ".pdf\"");
            response.setContentLength(bytes.length);
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(bytes, 0, bytes.length);
            outputStream.flush();
            outputStream.close();
        }
    }

    public JasperPrint gerarRelatorioRetornandoJasper(String arquivoJasper, HashMap parametros) throws JRException, IOException {
        Connection con = recuperaConexaoJDBC();
        String report = getCaminhoSemContexto();

        parametros.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(report + arquivoJasper, parametros, con);
        return jasperPrint;
    }

    public String getCaminhoSemContexto() {
        String separator = System.getProperty("file.separator");
        return getApplicationPath("/WEB-INF/") + separator + "relatorios" + separator;
    }

    public void gerarRelatorioComDadosEmCollectionAlterandoNomeArquivo(String nomeArquivo, String arquivoJasper, HashMap parametros, JRBeanCollectionDataSource jrbc) throws JRException, IOException {
        JasperPrint jasperPrint = JasperFillManager.fillReport(getServletContext().getRealPath("/WEB-INF/relatorios/" + arquivoJasper), parametros, jrbc);
        escreveNoResponse(nomeArquivo, preparaExportaReport(jasperPrint).toByteArray());
    }

    public void escreveNoResponse(String arquivoJasper, byte[] bytes) throws IOException {
        if (bytes != null && bytes.length > 0) {
            if (arquivoJasper.contains(".")) {
                int recorte = arquivoJasper.indexOf(".");
                arquivoJasper = arquivoJasper.substring(0, recorte);
            }
            HttpServletResponse response = (HttpServletResponse) getFacesContext().getExternalContext().getResponse();
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=" + arquivoJasper + ".pdf");
            response.setContentLength(bytes.length);
            ServletOutputStream outputStream = response.getOutputStream();
            outputStream.write(bytes, 0, bytes.length);
            outputStream.flush();
            outputStream.close();
            getFacesContext().responseComplete();
        }
    }

    public ServletContext getServletContext() {
        FacesContext facesContext = getFacesContext();
        ServletContext scontext = (ServletContext) facesContext.getExternalContext().getContext();
        return scontext;
    }

    private FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    protected ByteArrayOutputStream preparaExportaReport(JasperPrint jasperPrint) throws JRException {
        ByteArrayOutputStream dadosByte = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(dadosByte));
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCompressed(true);
        exporter.setConfiguration(configuration);
        exporter.exportReport();
        return dadosByte;
    }

    public String getCaminho() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String caminho = ((ServletContext) facesContext.getExternalContext().getContext()).getRealPath("/WEB-INF/relatorios");
        caminho += "/";
        return caminho;
    }

    public static String getApplicationPath(String path) {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath(path);
    }

    public String getCaminhoImagem() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        String caminho = ((ServletContext) facesContext.getExternalContext().getContext()).getRealPath("/resources/images");
        caminho += "/";
        return caminho;
    }

    public String getCaminhoImagemLogo() {
        return getCaminhoImagem() + "logo.jpg";
    }

    public void adicionarSubreportDir(HashMap parametros) {
        parametros.put("SUBREPORT_DIR", this.getCaminho());
    }

    public void adicionarcaminhoImagem(HashMap parametros) {
        parametros.put("imagem", this.getCaminhoImagemLogo());
    }

    public Usuario getUsuarioCorrente() {
        Usuario usuario = sistemaService.getUsuarioCorrente();
        return usuario;
    }

    public String getNomeUsuarioCorrente() {
        Usuario usuario = sistemaService.getUsuarioCorrente();
        if (usuario != null) {
            return usuario.getLogin();
        }
        return "ADMIN";
    }
}
