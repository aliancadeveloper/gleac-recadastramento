package br.com.gleac.supers;

/**
 * Created by renatoromanini on 28/08/15.
 */
public abstract class AbstractService {
    public abstract AbstractRepository getRepository();
}
