/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.supers;


import br.com.gleac.anotacao.CRUD;
import br.com.gleac.converters.MoneyConverter;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Romanini
 */
@MappedSuperclass
public abstract class AbstractEntity {

    public static String ADMIN = "/admin/";
    @Transient
    public Long criadoEm;

    public AbstractEntity() {
        this.criadoEm = System.nanoTime();
    }

    public abstract String getCaminhoPadrao();

    public Long getCriadoEm() {
        return criadoEm;
    }

    public void setCriadoEm(Long criadoEm) {
        this.criadoEm = criadoEm;
    }

    /*
     * Criado como método estático para evitar impor uma superclasse a todas as
     * entidades...
     */
    @Override
    public boolean equals(Object outro) {
        if (outro == null) {
            return false;
        }
        Object idObj = Persistencia.getId(this);
        Object idOutro = Persistencia.getId(outro);

        Long criadoEmObj = (Long) Persistencia.getAttributeValue(this, "criadoEm");
        Long criadoEmOutro = (Long) Persistencia.getAttributeValue(outro, "criadoEm");

        if (this.getClass() != outro.getClass()) {
            return false;
        }
        if (idObj == null) {
            if (!criadoEmObj.equals(criadoEmOutro)) {
                return false;
            }
        } else {
            if (!idObj.equals(idOutro)) {
                return false;
            }
        }
        return true;
    }

    /*
     * Criado como método estático para evitar impor uma superclasse a todas as
     * entidades...
     */
    @Override
    public int hashCode() {
        Object id = Persistencia.getId(this);
        Long criadoEm = (Long) Persistencia.getAttributeValue(this, "criadoEm");
        if (id == null) {
            int hash = 3;
            hash = 97 * hash + (criadoEm != null ? criadoEm.hashCode() : 0);
            return hash;
        } else {
            int hash = 7;
            hash = 71 * hash + (id != null ? id.hashCode() : 0);
            return hash;
        }
    }

    public String getArtigoDefinidoDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        final String PADRAO_DEFINIDO = "o/a";

        if (e == null) {
            return PADRAO_DEFINIDO;
        }

        if (e.sexo().equals("M")) {
            return "o";
        }

        if (e.sexo().equals("F")) {
            return "a";
        }

        return PADRAO_DEFINIDO;
    }

    public String getArtigoIndefinidoDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        final String PADRAO_INDEFINIDO = "um/uma";

        if (e == null) {
            return PADRAO_INDEFINIDO;
        }

        if (e.sexo().equals("M")) {
            return "um";
        }

        if (e.sexo().equals("F")) {
            return "uma";
        }

        return PADRAO_INDEFINIDO;
    }

    public String getTerceiraPessoaDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        final String PADRAO_INDEFINIDO = "ele/ela";

        if (e == null) {
            return PADRAO_INDEFINIDO;
        }

        if (e.sexo().equals("M")) {
            return "ele";
        }

        if (e.sexo().equals("F")) {
            return "ela";
        }

        return PADRAO_INDEFINIDO;
    }

    public String getEsteEstaDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        final String PADRAO_INDEFINIDO = "este/esta";

        if (e == null) {
            return PADRAO_INDEFINIDO;
        }

        if (e.sexo().equals("M")) {
            return "este";
        }

        if (e.sexo().equals("F")) {
            return "esta";
        }

        return PADRAO_INDEFINIDO;
    }

    public String getNomeDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        if (e == null) {
            return this.getClass().getSimpleName();
        }

        return e.label();
    }

    public String getNomePluralDaEntidade() {
        CRUD e = this.getClass().getAnnotation(CRUD.class);
        if (e == null) {
            return "s";
        }

        return e.plural();
    }


    private Object getValueFormatado(Object value, Field f) {
        if (f.getType().equals(BigDecimal.class)) {
            MoneyConverter mc = new MoneyConverter();
            try {
                value = mc.getAsString(null, null, f.get(this));
                return value;
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(AbstractEntity.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(AbstractEntity.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return value;
    }


    public void isCamposObrigatoriosPreenchidos() {
        int camposInvalidos = 0;
        for (Field field : Persistencia.getAtributos(this.getClass())) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(CRUD.class) && field.getAnnotation(CRUD.class).obrigatorio()) {
                if (Persistencia.getAttributeValue(this, field.getName()) == null || Persistencia.getAttributeValue(this, field.getName()).toString().trim().length() <= 0) {
                    String nomeCampo = field.getAnnotation(CRUD.class).label() == null || field.getAnnotation(CRUD.class).label().trim().length() <= 0 ? field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1, field.getName().length()) : field.getAnnotation(CRUD.class).label();
                    Util.addError("Campo obrigatório.", "O campo " + nomeCampo + " é obrigatório.");
                    camposInvalidos++;
                }
            }
        }

        if (camposInvalidos == 1) {
            throw new CampoObrigatorioException("Um campo obrigatório não foi informado.");
        }

        if (camposInvalidos > 1) {
            throw new CampoObrigatorioException("Existem campos obrigatórios que não foram informados");
        }
    }

    public void validarRegrasPrincipais()  {
        isCamposObrigatoriosPreenchidos();
    }

    public void validarCamposObrigatorios() {
        isCamposObrigatoriosPreenchidos();
    }

    public String getToStringHtml() {
        return "<a href='" + Util.getRequestContextPath() + this.getCaminhoPadrao() + "ver/" + Persistencia.getId(this) + "/'>" + this + "</a>";
    }
}
