package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.seguranca.GrupoAcesso;
import br.com.gleac.entidade.seguranca.GrupoUsuario;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.GrupoAcessoRepository;
import br.com.gleac.negocio.GrupoUsuarioRepository;
import br.com.gleac.negocio.UsuarioRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-grupo-usuario", pattern = "/admin/grupo-usuario/novo/", viewId = "/admin/seguranca/grupo-usuario/editar.xhtml"),
    @URLMapping(id = "editar-grupo-usuario", pattern = "/admin/grupo-usuario/editar/#{grupoUsuarioControlador.id}/", viewId = "/admin/seguranca/grupo-usuario/editar.xhtml"),
    @URLMapping(id = "ver-grupo-usuario", pattern = "/admin/grupo-usuario/ver/#{grupoUsuarioControlador.id}/", viewId = "/admin/seguranca/grupo-usuario/visualizar.xhtml"),
    @URLMapping(id = "listar-grupo-usuario", pattern = "/admin/grupo-usuario/listar/", viewId = "/admin/seguranca/grupo-usuario/listar.xhtml")

})
public class GrupoUsuarioControlador extends AbstractController<GrupoUsuario> {

    @Autowired
    private GrupoUsuarioRepository grupoUsuarioRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private GrupoAcessoRepository grupoAcessoRepository;

    private Usuario usuario;
    private GrupoAcesso grupoAcesso;


    // converter
    private ConverterAutoComplete converterGrupoAcesso;
    private ConverterAutoComplete converterUsuario;

    public GrupoUsuarioControlador() {
        super(GrupoUsuario.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return grupoUsuarioRepository;
    }

    @URLAction(mappingId = "novo-grupo-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-grupo-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-grupo-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-grupo-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        try {
            validarGrupoUsuario();
            grupoUsuarioRepository.salvar(selecionado);
            FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.selecionado.toString() + " foi <b> salvo </b> com sucesso!");
            navegarEmbora();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    private void validarGrupoUsuario() {
        ValidacaoException ve = new ValidacaoException();
        if (ObjectUtils.isEmpty(selecionado.getGruposAcessos())) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe um grupo de acesso.");
        }
        if (ObjectUtils.isEmpty(selecionado.getUsuarios())) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe um usuário.");
        }

        ve.lancarException();
    }

    public List<Usuario> completarUsuarioByLogin(String filter) {
        return usuarioRepository.completarUsuario(filter);
    }
    public List<GrupoAcesso> completarGrupoAcesso (String filter) {
        return grupoAcessoRepository.completarEstaEntidade(filter);
    }

    public void adicionarUsuario() {
        if (usuario != null) {
            usuario.setGruposUsuario(Util.adicionarObjetoEmLista(usuario.getGruposUsuario(), selecionado));
            selecionado.setUsuarios(Util.adicionarObjetoEmLista(selecionado.getUsuarios(), getUsuario()));
            FacesUtil.addInfo("Usuário adicionado.", "O Usuário " + usuario.toString() + " foi adicionado com sucesso.");
            usuario = new Usuario();
        } else {
            FacesUtil.addErrorOperacaoNaoPermitida("O Campo usuário é obrigatório.");
        }

    }

    public void adicionarGrupoAcesso () {
        try {
            validarGrupoAcesso();
            selecionado.setGruposAcessos(Util.adicionarObjetoEmLista(selecionado.getGruposAcessos(), getGrupoAcesso()));
            setGrupoAcesso(new GrupoAcesso());
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    private void validarGrupoAcesso() {
        ValidacaoException ve = new ValidacaoException();
        if (ObjectUtils.isEmpty(getGrupoAcesso())) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe um grupo de acesso.");
        }
        if (!ObjectUtils.isEmpty(getGrupoAcesso())) {
            if (!ObjectUtils.isEmpty(selecionado.getGruposAcessos())) {
                if (selecionado.getGruposAcessos().contains(getGrupoAcesso())) {
                    ve.adicionarMensagemDeOperacaoNaoPermitida("Esse grupo de acesso já foi adicionado.");
                }
            }
        }

        ve.lancarException();
    }

    public void removerGrupoAcesso(GrupoAcesso grupo) {
        selecionado.getGruposAcessos().remove(grupo);
    }

    public void removerUsuario (Usuario usuario) {
        selecionado.getUsuarios().remove(usuario);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public GrupoAcesso getGrupoAcesso() {
        return grupoAcesso;
    }

    public void setGrupoAcesso(GrupoAcesso grupoAcesso) {
        this.grupoAcesso = grupoAcesso;
    }

    public ConverterAutoComplete getConverterGrupoAcesso() {
        if (converterGrupoAcesso == null) {
            converterGrupoAcesso = new ConverterAutoComplete(GrupoAcesso.class, grupoAcessoRepository);
        }
        return converterGrupoAcesso;
    }

    public void setConverterGrupoAcesso(ConverterAutoComplete converterGrupoAcesso) {
        this.converterGrupoAcesso = converterGrupoAcesso;
    }

    public ConverterAutoComplete getConverterUsuario() {
        if (converterUsuario == null) {
            converterUsuario = new ConverterAutoComplete(Usuario.class, usuarioRepository);
        }
        return converterUsuario;
    }

    public void setConverterUsuario(ConverterAutoComplete converterUsuario) {
        this.converterUsuario = converterUsuario;
    }
}
