package br.com.gleac.controlador;

import br.com.gleac.entidade.auxiliares.TaxaPercapitaAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.configuracao.ConfiguracaoProduto;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.TaxaPercapita;
import br.com.gleac.entidade.financeiro.TaxaPercapitaParcela;
import br.com.gleac.entidade.financeiro.TaxaPercapitaProduto;
import br.com.gleac.enums.FormaPagamentoTaxa;
import br.com.gleac.enums.Situacao;
import br.com.gleac.enums.TipoServico;
import br.com.gleac.enums.TipoTaxaPercapita;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.*;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 14/10/17.
 */
@ManagedBean(name = "taxaPercapitaControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-taxa", pattern = "/admin/emissao-de-boletos/", viewId = "/admin/financeiro/taxa-percapita/editar.xhtml"),

})
public class TaxaPercapitaControlador implements Serializable {

    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ConfiguracaoServicoRepository configuracaoServicoRepository;
    @Autowired
    private ConfiguracaoProdutoRepository configuracaoProdutoRepository;
    @Autowired
    private TaxaPercapitaRepository taxaPercapitaRepository;
    private TaxaPercapitaAuxiliar selecionado;
    private BigDecimal valor;

    private TaxaPercapita taxaPercapitaPraImprimir;
    private String urlCarne;

    public TaxaPercapitaControlador() {
        injetarDependenciasSpring();
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    @URLAction(mappingId = "novo-taxa", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        instanciarCampos();
    }

    public String getMinimaDataVencimento() {
        return new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    }

    public void instanciarCampos() {
        selecionado = new TaxaPercapitaAuxiliar();
        DateTime dateTime = new DateTime();
        DateTime vencimento = dateTime.plusDays(30);
        selecionado.setDataVencimento(vencimento.toDate());
        valor = BigDecimal.ZERO;
    }

    public List<SelectItem> getTipos() {
        return Util.getListSelectItem(TipoTaxaPercapita.values());
    }

    public List<SelectItem> getTiposSolicitacao() {
        return Util.getListSelectItem(TipoSolicitacao.values());
    }

    public List<SelectItem> getFormas() {
        List<SelectItem> retorno = Lists.newArrayList();
        retorno.add(new SelectItem(FormaPagamentoTaxa.AVISTA_SEM_DESCONTO, FormaPagamentoTaxa.AVISTA_SEM_DESCONTO.getDescricao()));
        retorno.add(new SelectItem(FormaPagamentoTaxa.AVISTA_COM_DESCONTO, FormaPagamentoTaxa.AVISTA_COM_DESCONTO.getDescricao()));
        retorno.add(new SelectItem(FormaPagamentoTaxa.TODOS, FormaPagamentoTaxa.TODOS.getDescricao()));
        return retorno;
    }

    public List<Servico> completarServico(String filtro) {
        return configuracaoServicoRepository.completarServicosWithConfiguracaoByDescricaoAndTipo(filtro, TipoServico.TAXA);
    }

    public List<Produto> completarProduto(String filtro) {
        return configuracaoProdutoRepository.completarProdutoWithConfiguracaoByDescricao(filtro);
    }

    public void recuperarLoja() {
        try {
            if (selecionado.getLoja() != null) {
                selecionado.setLoja(lojaRepository.recuperar(Loja.class, selecionado.getLoja().getId()));
                Integer quantidade = maconRepository.contarMacomPorLojaAndSituacao("", selecionado.getLoja(), Situacao.REGULAR.name());
                if (selecionado.isServico()) {
                    if (selecionado.getServico() != null) {
                        ConfiguracaoServico configuracaoServico = configuracaoServicoRepository.buscarConfiguracao(selecionado.getDataVencimento(), selecionado.getServico());
                        if (configuracaoServico.getAnuidade()) {
                            this.valor = configuracaoServico.getValor().multiply(new BigDecimal(quantidade)).setScale(2, RoundingMode.HALF_EVEN);
                        } else {
                            this.valor = configuracaoServico.getValor();
                        }
                    }
                } else {
                    recuperarValorAtualProduto();
                }
            }
        } catch (Exception e) {
            FacesUtil.addAtencao("Erro ao recuperar valor total." + e.getMessage());
        }

    }

    private void recuperarValorAtualProduto() {
        if (selecionado.getProduto() != null) {
            ConfiguracaoProduto configuracaoProduto = configuracaoProdutoRepository.buscarConfiguracao(selecionado.getDataVencimento(), selecionado.getProduto());
            this.valor = configuracaoProduto.getValor();
            this.selecionado.getProduto().setValorAtual(this.valor);
        }
    }

    public void recuperarMacon() {
        if (selecionado.getTodasLojas()) {
            List<Loja> lojas = lojaRepository.listar();
            for (Loja loja : lojas) {
                List<Macon> macons = maconRepository.completarMacomPorLojaAndSituacao("", loja, Situacao.REGULAR.name());
                selecionado.setMacons(macons);
            }
        }
        if (selecionado.getLoja() != null) {
            List<Macon> macons = maconRepository.completarMacomPorLojaAndSituacao("", selecionado.getLoja(), Situacao.REGULAR.name());
            selecionado.setMacons(macons);
        }
        if (selecionado.getTodosMacon() && selecionado.getLoja() == null) {
            selecionado.setMacons(maconRepository.listar());
        }
        if (selecionado.getMacon() != null) {
            selecionado.getMacons().add(selecionado.getMacon());
        }


        Collections.sort(selecionado.getMacons(), new Comparator<Macon>() {
            @Override
            public int compare(Macon o1, Macon o2) {
                return o1.getIdentificacao().compareTo(o2.getIdentificacao());
            }
        });
    }

    public void adicionarMacon() {
        if (selecionado.getMacon() != null) {
            Util.adicionarObjetoEmLista(selecionado.getMacons(), selecionado.getMacon());
            selecionado.setMacon(null);
        }
    }

    public void removerMacon(Macon macon) {
        selecionado.getMacons().remove(macon);
    }

    public List<Macon> completarMacons(String parte) {
        List<Macon> macons = Lists.newArrayList();
        if (selecionado.getLoja() == null) {
            macons.addAll(maconRepository.completarMaconAtivoPorNomeSimples(parte));
            Collections.sort(macons);
            return macons;
        } else {
            macons.addAll(maconRepository.completarMacomPorLojaAndSituacao(parte, selecionado.getLoja(), Situacao.REGULAR.name()));
            Collections.sort(macons);
            return macons;
        }
    }

    public void imprimirBoletosAgrupados() {
        try {
            urlCarne = null;
            if (taxaPercapitaPraImprimir != null) {
                List<Boleto> boletos = Lists.newArrayList();
                boletos.add(taxaPercapitaPraImprimir.getContasReceber().getBoleto());

                for (TaxaPercapitaParcela parcela : taxaPercapitaPraImprimir.getParcelas()) {
                    if (parcela.getContasReceber().getBoleto() != null) {
                        boletos.add(parcela.getContasReceber().getBoleto());
                    }
                }
                urlCarne = taxaPercapitaRepository.gerarBoletosAgrupados(boletos);
            } else {
                FacesUtil.addOperacaoNaoPermitida("É obrigatório selecionar pelo menos um boleto.");
            }
        } catch (Exception e) {
            FacesUtil.addOperacaoNaoPermitida(e.getMessage());
        }
    }

    public void gerarTaxas() {
        try {
            validarTaxa();
            if (selecionado.isPorLoja()) {
                gerarTaxaPorLoja();
            } else {
                gerarTaxaPorMacom();
            }
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception e) {
            FacesUtil.addErrorOperacaoNaoPermitida(e.getLocalizedMessage());
        }
    }

    private void gerarTaxaPorLoja() {
        try {
            TaxaPercapita taxaPercapita = new TaxaPercapita();
            taxaPercapita.setTipoTaxaPercapita(TipoTaxaPercapita.POR_LOJA);
            taxaPercapita.setLoja(selecionado.getLoja());
            taxaPercapita.setData(selecionado.getDataVencimento());
            taxaPercapita.setServico(selecionado.getServico());
            taxaPercapita.setForma(selecionado.getFormaPagamentoTaxa());
            taxaPercapita.setTipoSolicitacao(selecionado.getTipoSolicitacao());
            adicionarProdutoTaxa(taxaPercapita);
            gerarAndImprimirCarne(taxaPercapita);
        } catch (Exception e) {
            e.printStackTrace();
            selecionado.getMensagens().add("Erro ao gerar Boleto para a loja " + selecionado.getLoja().toString() + ". ");
        }

        if (selecionado.getMensagens().isEmpty()) {
            this.selecionado.setLoja(null);
            FacesUtil.addOperacaoRealizada("Boleto Gerado com Sucesso!");
        } else {
            FacesUtil.addAtencao(selecionado.getMensagens().size() + " Boletos não foram gerados. Verifique os dados e tente novamente!");
        }
    }

    private void gerarAndImprimirCarne(TaxaPercapita taxaPercapita) {
        if (selecionado.getFormaPagamentoTaxa().equals(FormaPagamentoTaxa.TODOS)) {
            taxaPercapitaPraImprimir = taxaPercapitaRepository.gerarTaxa(taxaPercapita);
            imprimirBoletosAgrupados();
            FacesUtil.executaJavaScript("window.open('" + urlCarne + "', '_blank')");
        } else {
            taxaPercapitaPraImprimir = taxaPercapitaRepository.gerarTaxa(taxaPercapita);
            FacesUtil.executaJavaScript("window.open('" + taxaPercapita.getContasReceber().getBoleto().getUrlPdfPagamento() + "', '_blank')");
        }
    }

    private void adicionarProdutoTaxa(TaxaPercapita taxaPercapita) {
        if (!selecionado.isServico()) {
            for (Produto produto : selecionado.getProdutos()) {
                taxaPercapita.getProdutos().add(new TaxaPercapitaProduto(taxaPercapita, produto));
            }
        }
    }

    private void gerarTaxaPorMacom() {
        for (Macon macon : selecionado.getMacons()) {
            try {
                macon = maconRepository.recuperar(Macon.class, macon.getId());
                Loja loja = macon.getUltimaLojaInterna();
                TaxaPercapita taxaPercapita = new TaxaPercapita();
                taxaPercapita.setMacon(macon);
                taxaPercapita.setLoja(loja);
                taxaPercapita.setTipoTaxaPercapita(TipoTaxaPercapita.POR_MACOM);
                taxaPercapita.setData(selecionado.getDataVencimento());
                taxaPercapita.setServico(selecionado.getServico());
                taxaPercapita.setForma(selecionado.getFormaPagamentoTaxa());
                taxaPercapita.setTipoSolicitacao(selecionado.getTipoSolicitacao());
                adicionarProdutoTaxa(taxaPercapita);
                gerarAndImprimirCarne(taxaPercapita);
            } catch (Exception e) {
                selecionado.getMensagens().add("Erro ao gerar Boleto para o maçom " + macon.toString() + ". ");
            }
        }
        selecionado.getMacons().clear();
        if (selecionado.getMensagens().isEmpty()) {
            FacesUtil.addOperacaoRealizada("Boleto Gerado com Sucesso!");
        } else {
            FacesUtil.addAtencao(selecionado.getMensagens().size() + " Boletos não foram gerados. Verifique os dados e tente novamente!");
        }
    }

    private void validarTaxa() {
        ValidacaoException ve = new ValidacaoException();
        if (selecionado.isServico()) {
            if (selecionado.getServico() == null) {
                ve.adicionarMensagemDeCampoObrigatorio("O Campo Serviço é obrigatório!");
            }
        } else {
            if (selecionado.getProdutos() == null || selecionado.getProdutos().isEmpty()) {
                ve.adicionarMensagemDeCampoObrigatorio("É obrigatório ter pelo menos um produto adicionado!");
            }
        }

        if (selecionado.getDataVencimento() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("O Campo Data de Vencimento é obrigatório!");
        }
        if (selecionado.isPorLoja()) {
            if (selecionado.getLoja() == null) {
                ve.adicionarMensagemDeCampoObrigatorio("O Campo Loja é obrigatório!");
            }
        } else {
            if (selecionado.getMacons() == null || selecionado.getMacons().isEmpty()) {
                ve.adicionarMensagemDeCampoObrigatorio("É obrigatório ter pelo menos um Maçom na lista!");
            }
        }

        ve.lancarException();
    }

    public TaxaPercapitaAuxiliar getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(TaxaPercapitaAuxiliar selecionado) {
        this.selecionado = selecionado;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public void atribuirNullProduto() {
        selecionado.setProduto(null);
    }

    public void adicionarProduto() {
        if (selecionado.getProduto() == null) {
            FacesUtil.addOperacaoNaoPermitida("O Campo Produto é obrigatório!");
        } else {
            recuperarValorAtualProduto();
            selecionado.setProdutos(Util.adicionarObjetoEmLista(this.selecionado.getProdutos(), this.selecionado.getProduto()));
            atribuirNullProduto();
        }
    }

    public ConfiguracaoServico configuracaoServico(Servico servico) {
        return configuracaoServicoRepository.buscarConfiguracao(new Date(), servico);
    }

    public ConfiguracaoProduto configuracaoProduto(Produto produto) {
        return configuracaoProdutoRepository.buscarConfiguracao(new Date(), produto);
    }

    public Boolean permitirParcelamento() {
        if (selecionado.getServico() != null) {
            return configuracaoServico(selecionado.getServico()).getParcelamento();
        }

        if (selecionado.getProduto() != null) {
            return configuracaoProduto(selecionado.getProduto()).getParcelamento();
        }
        return Boolean.FALSE;
    }

    public void alterarProduto(Produto produto) {
        selecionado.setProduto(produto);
    }

    public void removerProduto(Produto produto) {
        selecionado.getProdutos().remove(produto);
    }

    public String getUrlCarne() {
        return urlCarne;
    }

    public void setUrlCarne(String urlCarne) {
        this.urlCarne = urlCarne;
    }
}
