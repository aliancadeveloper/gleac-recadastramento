package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.enums.OperacaoCondicaoSql;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.ProcessoFinanceiroRepository;
import br.com.gleac.negocio.SolicitacaoProcessoRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.view.ParametroView;
import br.com.gleac.util.view.View;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by renato on 08/07/2017.
 */
@ManagedBean(name = "processoFinanceiroControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-processoFinanceiro", pattern = "/admin/processo-financeiro/novo/", viewId = "/admin/financeiro/processo-financeiro/editar.xhtml"),
    @URLMapping(id = "editar-processoFinanceiro", pattern = "/admin/processo-financeiro/editar/#{processoFinanceiroControlador.id}/", viewId = "/admin/financeiro/processo-financeiro/editar.xhtml"),
    @URLMapping(id = "novo-processoFinanceiro-solicitacao", pattern = "/admin/processo-financeiro/nova-por-solicitacao/#{processoFinanceiroControlador.id}/", viewId = "/admin/financeiro/processo-financeiro/editar.xhtml"),
    @URLMapping(id = "listar-processoFinanceiro", pattern = "/admin/processo-financeiro/listar/", viewId = "/admin/financeiro/processo-financeiro/listar.xhtml"),
    @URLMapping(id = "ver-processoFinanceiro", pattern = "/admin/processo-financeiro/ver/#{processoFinanceiroControlador.id}/", viewId = "/admin/financeiro/processo-financeiro/visualizar.xhtml")
})
public class ProcessoFinanceiroControlador extends AbstractController<ProcessoFinanceiro> implements Serializable {

    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;
    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private LojaRepository lojaRepository;

    private ConverterAutoComplete converterSolicitacaoProcesso;

    @Override
    public AbstractRepository getRepository() {
        return processoFinanceiroRepository;
    }

    public ProcessoFinanceiroControlador() {
        super(ProcessoFinanceiro.class);
    }

    @Override
    public List<ProcessoFinanceiro> completarEstaEntidade(String parte) {
        return processoFinanceiroRepository.completarProcessoFinanceiroPorSituacao(parte);
    }

    @URLAction(mappingId = "listar-processoFinanceiro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-processoFinanceiro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "novo-processoFinanceiro-solicitacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novoPorSolicitacao() {
        super.novo();
        SolicitacaoProcesso sol = solicitacaoProcessoRepository.recuperar(SolicitacaoProcesso.class, getId());
        if (sol != null) {
            selecionado.setSolicitacaoProcesso(sol);
        }
    }

    @URLAction(mappingId = "editar-processoFinanceiro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        SolicitacaoProcesso sol = solicitacaoProcessoRepository.recuperar(SolicitacaoProcesso.class, selecionado.getSolicitacaoProcesso().getId());
        if (sol != null) {
            selecionado.setSolicitacaoProcesso(sol);
        }
    }

    @URLAction(mappingId = "ver-processoFinanceiro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @Override
    public void salvar() {
        try {
            if (Persistencia.getId(this.selecionado) == null) {
                getRepository().salvarNovo(this.selecionado);
            } else {
                getRepository().salvar(this.selecionado);
            }
            FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.selecionado.toString() + " foi <b> salvo </b> com sucesso!");
        } catch (RuntimeException ex) {

            if (ex instanceof ValidacaoException) {
                FacesUtil.printAllFacesMessages(((ValidacaoException) ex).getMensagens());
            } else {
                FacesUtil.addErrorPadrao(ex);
            }
            return;
        } catch (Exception ex) {

            FacesUtil.addErrorPadrao(ex);
            return;
        }
        FacesUtil.redirecionamentoInterno(getCaminhoPadrao() + "editar/" + selecionado.getId() + "/");
    }

    public ConverterAutoComplete getConverterSolicitacaoProcesso() {
        if (converterSolicitacaoProcesso == null) {
            converterSolicitacaoProcesso = new ConverterAutoComplete(SolicitacaoProcesso.class, solicitacaoProcessoRepository);
        }
        return converterSolicitacaoProcesso;
    }

    public List<SolicitacaoProcesso> completarSolicitacaoProcesso(String filtro) {
        return solicitacaoProcessoRepository.completarSolicitacaoProcessoPorDescricaoOrNumero(filtro, StatusProcesso.PARECER);
    }

    public void verificarBoleto() {
        processoFinanceiroRepository.verificarBoleto(selecionado);
    }

    public void cancelarBoleto() {
        processoFinanceiroRepository.cancelarBoleto(selecionado);
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    @Override
    public void filtrar() {
        if (sistemaService.getLojaCorrente().equals(getLojaRepository().buscarLojaGestora())) {
            super.filtrar();
        } else {
            View view = getView();
            view.setNomeRelatorio("Relatório de " + Persistencia.getNomeEntidade(classe));
            view.setColunas(Lists.newArrayList());
            String sql = Persistencia.adicionarColunas(view, "obj", classe);
            String sqlSelect = " select obj.id, " + sql.substring(0, sql.length() - 2) + "  from " + classe.getSimpleName() + " obj " +
                " inner join solicitacaoProcesso sol on obj.solicitacaoProcesso_id = sol.id" +
                " inner join departamento dep on sol.departamento_id = dep.id" +
                " inner join loja on dep.loja_id = loja.id " +
                " where 1=1 ";

            for (Field field : Persistencia.getAtributosPesquisaveis(classe)) {
                criarParametroView(view, field);
            }

            Loja loja = sistemaService.getLojaCorrente();

            ParametroView parametroView = new ParametroView("Loja", "loja.id", loja.getId(), OperacaoCondicaoSql.IGUAL, null, Loja.class);
            view.getParametros().add(parametroView);

            view.setSqlRecuperadorObjetos(sqlSelect);
            view.setSqlOrdenar(" order by obj.id desc");
            view.setSqlContadorObjetos(" select count(obj.id)  from " + classe.getSimpleName() + " obj " +
                " inner join solicitacaoProcesso sol on obj.solicitacaoProcesso_id = sol.id" +
                " inner join departamento dep on sol.departamento_id = dep.id" +
                " inner join loja on dep.loja_id = loja.id " +
                " where 1=1 ");

            view = super.getViewRepository().recuperarObjetos(view);
            view.setTotalDeRegistrosExistentes(null);
        }
    }

    public void atualizarDataVencimento() {
        DateTime dateTime = new DateTime(selecionado.getGeradoEm());
        DateTime vencimento = dateTime.plusDays(30);
        selecionado.setVencimentoBoleto(vencimento.toDate());
    }
}
