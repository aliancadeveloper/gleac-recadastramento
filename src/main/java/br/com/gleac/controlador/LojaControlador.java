package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.*;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.PessoaJuridicaRepository;
import br.com.gleac.negocio.PotenciaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by zaca on 06/10/16.
 */
@ManagedBean(name = "lojaControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-loja", pattern = "/admin/loja/novo/", viewId = "/admin/cadastros/loja/editar.xhtml"),
    @URLMapping(id = "editar-loja", pattern = "/admin/loja/editar/#{lojaControlador.id}/", viewId = "/admin/cadastros/loja/editar.xhtml"),
    @URLMapping(id = "ver-loja", pattern = "/admin/loja/ver/#{lojaControlador.id}/", viewId = "/admin/cadastros/loja/visualizar.xhtml"),
    @URLMapping(id = "listar-loja", pattern = "/admin/loja/listar/", viewId = "/admin/cadastros/loja/listar.xhtml")
})
public class LojaControlador extends AbstractController<Loja> implements Crud {

    @Autowired
    private LojaRepository lojaRepository;

    @Autowired
    private PotenciaRepository potenciaRepository;

    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;

    private Departamento departamento;
    private ContaCorrente contaCorrente;


    @Override
    public AbstractRepository getRepository() {
        return lojaRepository;
    }

    public LojaControlador() {
        super(Loja.class);
    }

    @URLAction(mappingId = "novo-loja", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        inicializarDepartamento();
        inicializarConta();
    }

    @URLAction(mappingId = "editar-loja", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        inicializarDepartamento();
        inicializarConta();
    }

    @URLAction(mappingId = "ver-loja", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-loja", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void filtrar() {
        super.filtrar();
    }

    public List<Potencia> completarPotencia(String filter) {
        return potenciaRepository.buscarPotenciaPorNome(filter);
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public void adicionarDepartamento() {
        try {
            departamento.setLoja(selecionado);
            validarDepartamento();

            selecionado.setDepartamentos(Util.adicionarObjetoEmLista(selecionado.getDepartamentos(), departamento));
            inicializarDepartamento();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void inicializarDepartamento() {
        departamento = new Departamento();
    }

    public void validarDepartamento() {
        Util.validarCampos(departamento);
    }

    public void editarDepartamento(Departamento departamento) {
        this.departamento = departamento;
        removerDepartamento(departamento);
    }

    public void removerDepartamento(Departamento end) {
        selecionado.getDepartamentos().remove(end);
    }

    public void adicionarConta() {
        try {
            validarConta();
            ajustarContaPrincipal();
            selecionado.setContas(Util.adicionarObjetoEmLista(selecionado.getContas(), new LojaConta(selecionado, contaCorrente)));
            inicializarConta();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void ajustarContaPrincipal() {
        if (contaCorrente.getPrincipal() && !selecionado.getContas().isEmpty()) {
            for (LojaConta pessoaConta : selecionado.getContas()) {
                pessoaConta.getContaCorrente().setPrincipal(Boolean.FALSE);
            }
        }
    }

    private void inicializarConta() {
        contaCorrente = new ContaCorrente();
    }

    public void validarConta() {
        Util.validarCampos(contaCorrente);
    }

    public void editarConta(LojaConta pessoaConta) {
        this.contaCorrente = pessoaConta.getContaCorrente();
        removerConta(pessoaConta);
    }

    public void removerConta(LojaConta pessoaConta) {
        selecionado.getContas().remove(pessoaConta);
    }

    public void recuperarPessoa() {
        selecionado.setPessoaJuridica(pessoaJuridicaRepository.recuperar(PessoaJuridica.class, selecionado.getPessoaJuridica().getId()));
        if (selecionado.getPessoaJuridica() != null && selecionado.getPessoaJuridica().getContas() != null) {
            for (PessoaConta conta : selecionado.getPessoaJuridica().getContas()) {
                Boolean acho = false;
                for (LojaConta selecionadoConta : selecionado.getContas()) {
                    if (selecionadoConta.getContaCorrente().getId().equals(conta.getContaCorrente().getId())) {
                        acho = true;
                    }
                }
                if (!acho) {
                    ContaCorrente novaConta = (ContaCorrente) Util.clonarObjeto(conta.getContaCorrente());
                    novaConta.setId(null);
                    selecionado.setContas(Util.adicionarObjetoEmLista(selecionado.getContas(), new LojaConta(selecionado, novaConta)));
                }
            }
        }
    }

}
