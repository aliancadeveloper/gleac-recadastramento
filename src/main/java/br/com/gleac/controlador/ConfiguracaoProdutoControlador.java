package br.com.gleac.controlador;


import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.configuracao.ConfiguracaoProduto;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.ConfiguracaoProdutoRepository;
import br.com.gleac.negocio.ProdutoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "configuracaoProdutoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "nova-configuracao-produto",
        pattern = "/admin/configuracao/produto/novo/",
        viewId = "/admin/cadastros/configuracao-produto/editar.xhtml"),
    @URLMapping(id = "editar-configuracao-produto",
        pattern = "/admin/configuracao/produto/editar/#{configuracaoProdutoControlador.id}/",
        viewId = "/admin/cadastros/configuracao-produto/editar.xhtml"),
    @URLMapping(id = "ver-configuracao-produto",
        pattern = "/admin/configuracao/produto/ver/#{configuracaoProdutoControlador.id}/",
        viewId = "/admin/cadastros/configuracao-produto/visualizar.xhtml"),
    @URLMapping(id = "listar-configuracao-produto",
        pattern = "/admin/configuracao/produto/listar/",
        viewId = "/admin/cadastros/configuracao-produto/listar.xhtml")
})
public class ConfiguracaoProdutoControlador extends AbstractController<ConfiguracaoProduto> implements Crud {

    @Autowired
    private ConfiguracaoProdutoRepository configuracaoProdutoRepository;
    @Autowired
    private ProdutoRepository produtoRepository;
    private ConverterAutoComplete converterProduto;

    public ConfiguracaoProdutoControlador() {
        super(ConfiguracaoProduto.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return getConfiguracaoProdutoRepository();
    }

    @URLAction(mappingId = "nova-configuracao-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-configuracao-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-configuracao-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-configuracao-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<Produto> completarProdutoLessConfiguracao(String filtro) {
        return produtoRepository.completarProdutoPorNome(filtro);
    }


    public ConfiguracaoProdutoRepository getConfiguracaoProdutoRepository() {
        return configuracaoProdutoRepository;
    }

    public ProdutoRepository getProdutoRepository() {
        return produtoRepository;
    }

    public ConverterAutoComplete getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterAutoComplete(Produto.class, getProdutoRepository());
        }
        return converterProduto;
    }
}
