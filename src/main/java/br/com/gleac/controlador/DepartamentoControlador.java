package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.negocio.DepartamentoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "departamentoControlador")
@ViewScoped
public class DepartamentoControlador extends AbstractController<Departamento> implements Serializable {
    @Autowired
    private DepartamentoRepository departamentoRepository;


    public DepartamentoControlador() {
        super(Departamento.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return getDepartamentoRepository();
    }

    public DepartamentoRepository getDepartamentoRepository() {
        return departamentoRepository;
    }

    @Override
    public List<Departamento> completarEstaEntidade(String parte) {
        return getDepartamentoRepository().buscarTodosDepartamentos(parte);
    }
}
