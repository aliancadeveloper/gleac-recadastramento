package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Profissao;
import br.com.gleac.negocio.ProfissaoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by zaca on 08/09/16.
 */
@ManagedBean(name = "profissaoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-profissao", pattern = "/admin/profissao/novo/", viewId = "/admin/cadastros/profissao/editar.xhtml"),
        @URLMapping(id = "editar-profissao", pattern = "/admin/profissao/editar/#{profissaoControlador.id}/", viewId = "/admin/cadastros/profissao/editar.xhtml"),
        @URLMapping(id = "ver-profissao", pattern = "/admin/profissao/ver/#{profissaoControlador.id}/", viewId = "/admin/cadastros/profissao/visualizar.xhtml"),
        @URLMapping(id = "listar-profissao", pattern = "/admin/profissao/listar/", viewId = "/admin/cadastros/profissao/listar.xhtml")
})
public class ProfissaoControlador extends AbstractController<Profissao> implements Serializable {


    @Autowired
    private ProfissaoRepository repository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public ProfissaoControlador() {
        super(Profissao.class);
    }

    @URLAction(mappingId = "novo-profissao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-profissao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-profissao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-profissao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }
}
