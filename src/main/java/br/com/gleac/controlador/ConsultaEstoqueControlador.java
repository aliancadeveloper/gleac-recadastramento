package br.com.gleac.controlador;


import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.estoque.SaldoEstoque;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.SaldoEstoqueRepository;
import br.com.gleac.util.FacesUtil;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Renato on 21/07/2017.
 */
@ManagedBean(name = "consultaEstoqueControlador")
@ViewScoped
@URLMapping(id = "novo-consulta-estoque", pattern = "/admin/consulta-estoque/", viewId = "/admin/estoque/consulta/editar.xhtml")
public class ConsultaEstoqueControlador implements Serializable {

    @Autowired
    private SaldoEstoqueRepository saldoEstoqueRepository;
    private Departamento departamento;
    private Produto produto;
    private Date dataInicial;
    private Date dataFinal;
    private List<SaldoEstoque> saldos;

    public ConsultaEstoqueControlador() {
        injetarDependenciasSpring();
    }

    @URLAction(mappingId = "novo-consulta-estoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        iniciarFiltros();
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    public void iniciarFiltros() {
        this.saldos = Lists.newArrayList();
        this.departamento = null;
        this.produto = null;
        this.dataInicial = new Date();
        this.dataFinal = new Date();
    }

    public void consultar() {
        try {
            validarCampos();
            List<SaldoEstoque> resultados = saldoEstoqueRepository.recuperarSaldos(dataInicial, dataFinal, departamento, produto);
            if (!resultados.isEmpty()) {
                this.saldos = resultados;
                return;
            }
            FacesUtil.addAtencao("Não foram localizados registros para os filtros informados.");
            this.saldos.clear();
        } catch (ValidacaoException ve) {
            ve.printStackTrace();
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addErrorOperacaoNaoPermitida(e.getMessage());
        }
    }

    private void validarCampos() {
        ValidacaoException ve = new ValidacaoException();
        if (this.dataInicial == null) {
            ve.adicionarMensagemDeCampoObrigatorio("O campo data inicial deve ser informado.");
        }
        if (this.dataFinal == null) {
            ve.adicionarMensagemDeCampoObrigatorio("O campo data final deve ser informado.");
        }
        ve.lancarException();
        if (this.dataFinal.before(this.dataInicial)) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("A data final deve ser superior ou igual a data inicial.");
        }
        ve.lancarException();
    }

    public Integer getQuantidadeTotal() {
        Integer total = 0;
        if (saldos != null && !saldos.isEmpty()) {
            for (SaldoEstoque saldo : saldos) {
                total = total + saldo.getQuantidade();
            }
        }
        return total;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public List<SaldoEstoque> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<SaldoEstoque> saldos) {
        this.saldos = saldos;
    }
}
