package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.negocio.ContasReceberRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "contasReceberControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-contas-receber", pattern = "/admin/contas-receber/novo/", viewId = "/admin/financeiro/contas-receber/editar.xhtml"),
        @URLMapping(id = "editar-contas-receber", pattern = "/admin/contas-receber/editar/#{contasReceberControlador.id}/", viewId = "/admin/financeiro/contas-receber/editar.xhtml"),
        @URLMapping(id = "listar-contas-receber", pattern = "/admin/contas-receber/listar/", viewId = "/admin/financeiro/contas-receber/listar.xhtml"),
        @URLMapping(id = "ver-contas-receber", pattern = "/admin/contas-receber/ver/#{contasReceberControlador.id}/", viewId = "/admin/financeiro/contas-receber/visualizar.xhtml")
})
public class ContasReceberControlador extends AbstractController<ContasReceber> implements Serializable {

    @Autowired
    private ContasReceberRepository repository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public ContasReceberControlador() {
        super(ContasReceber.class);
    }

    @URLAction(mappingId = "listar-contas-receber", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-contas-receber", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-contas-receber", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-contas-receber", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    public List<SelectItem> getTipos() {
        return Util.getListSelectItem(TipoContaPagar.values());
    }

    public List<SelectItem> getSituacoes() {
        return Util.getListSelectItem(SituacaoContaPagar.values());
    }

    public void estornar() {
        selecionado.setSituacaoContaPagar(SituacaoContaPagar.ESTORNADA);
        getRepository().salvar(selecionado);
        navegarEmbora();
        FacesUtil.addOperacaoRealizada("Registro foi estornado com sucesso.");
    }

    public boolean permtiEstornar() {
        return selecionado.getId() != null && selecionado.getSituacaoContaPagar().isPaga();
    }


    public Boolean mostrarBotaoExcluir() {
        return Boolean.FALSE;
    }


    public boolean isRegistroEditavel() {
        return !selecionado.getSituacaoContaPagar().isAberta();
    }

    public void receber() {
        FacesUtil.redirecionamentoInterno("/admin/receita/conta/" + selecionado.getId() + "/");
    }
}
