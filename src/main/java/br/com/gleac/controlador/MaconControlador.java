package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.*;
import br.com.gleac.enums.*;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.*;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import br.com.gleac.util.view.ParametroView;
import br.com.gleac.util.view.View;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by zaca on 05/09/16.
 */
@ManagedBean(name = "maconControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-macon", pattern = "/admin/macon/novo/", viewId = "/admin/cadastros/macon/editar.xhtml"),
    @URLMapping(id = "editar-macon", pattern = "/admin/macon/editar/#{maconControlador.id}/", viewId = "/admin/cadastros/macon/editar.xhtml"),
    @URLMapping(id = "ver-macon", pattern = "/admin/macon/ver/#{maconControlador.id}/", viewId = "/admin/cadastros/macon/visualizar.xhtml"),
    @URLMapping(id = "listar-macon", pattern = "/admin/macon/listar/", viewId = "/admin/cadastros/macon/listar.xhtml")

})
public class MaconControlador extends AbstractController<Macon> implements Serializable {
    private static final String POTENCIA = "GLEAC";
    private static String KEY_IMAGEM = "imagem-foto";

    @Autowired
    private MaconRepository repository;

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;

    @Autowired
    private ConjugeRepository conjugeRepository;

    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;

    @Autowired
    private CargoRepository cargoRepository;

    @Autowired
    private LojaRepository lojaRepository;

    @Autowired
    private SistemaService sistemaService;

    @Autowired
    private ArquivoRepository arquivoRepository;

    private ConverterAutoComplete converterPessoaFisica;

    private Conjuge conjuge;
    private TituloEleitor tituloEleitor;
    private Passaporte passaporte;
    private RG rg;
    private AreaFormacao areaFormacao;
    private Filho filho;
    private HistoricoObreiro historicoObreiro;
    private HistoricoMaconico historicoMaconico;
    private CarteiraTrabalho documentoTrabalho;
    private HistoricoCargo historicoCargo;
    private DocumentoMacon documentoMacon;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public MaconControlador() {
        super(Macon.class);
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/macon/";
    }

    @URLAction(mappingId = "novo-macon", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        selecionado.setGrau(Grau.APRENDIZ);
        selecionado.setSituacao(Situacao.REGULAR);
        carregarObjetos();
    }

    public void carregarObjetos() {
        conjuge = new Conjuge();
        historicoObreiro = new HistoricoObreiro();
        historicoMaconico = new HistoricoMaconico();
        historicoCargo = new HistoricoCargo();
        historicoMaconico.setPotencia(POTENCIA);
        historicoMaconico.setTipoLocalHistorico(TipoLocalHistorico.INTERNO);
        historicoMaconico.setLojaExterna("");
        setDocumentoMacon(new DocumentoMacon());
    }

    @URLAction(mappingId = "editar-macon", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        recuperarImagem();
        carregarObjetos();
    }

    private void recuperarImagem() {
        if (selecionado.getArquivo() != null) {
            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        }
    }

    @URLAction(mappingId = "ver-macon", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
        recuperarImagem();
        carregarObjetos();
    }

    @URLAction(mappingId = "listar-macon", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        try {
            realizarValidacao();
            validarRegistroMaconico();
            if (selecionado.getId() == null) {
                repository.salvarNovo(selecionado);
            } else {
                repository.salvar(selecionado);
            }
            FacesUtil.addInfo("Operação Realizada", "Registro salvo com sucesso!");
            navegarEmbora();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addErrorGenerico(ex);
        }

    }

    private void validarRegistroMaconico() {
        ValidacaoException ve = new ValidacaoException();

        if (repository.isNumeroMaconExistente(selecionado)) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Já existe um maçom cadastrado com esse registro! Por favor verificar.");
        }

        ve.lancarException();
    }

    public void navegarEmbora() {
        if (sistemaService.isMacon()) {
            FacesUtil.redirecionarHome();
        } else {
            FacesUtil.navegaEmbora(selecionado, getCaminhoPadrao());
        }

    }

    public List<Conjuge> completarConjuge(String filter) {
        return conjugeRepository.recuperarConjugesByNome(filter);
    }

    public List<PessoaFisica> completarPessoaFisica(String filter) {
        return pessoaFisicaRepository.buscarPessoaFisicaPorNome(filter);
    }

    public List<PessoaJuridica> completarPessoaJuridica(String filter) {
        return pessoaJuridicaRepository.buscarPessoaJuridica(filter);
    }

    public List<Loja> completarLojaByNome(String filter) {
        return lojaRepository.completarLojasByNome(filter);
    }

    public List<Cargo> completarCargo(String filter) {
        return cargoRepository.buscarCargoPorDescricao(filter);
    }

    public List<SelectItem> getSituacoes() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(Situacao.values());
    }

    public List<SelectItem> getTipoLocalHistorico() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(TipoLocalHistorico.values());
    }

    public List<SelectItem> getSituacoesHistorico() {
        return Util.getListSelectItemSemCampoVazioComOrdenacao(SituacaoHistorico.values());
    }

    public List<SelectItem> getFormacoes() {
        return Util.getListSelectItem(Escolaridade.values());
    }

    public List<SelectItem> getSexos() {
        return Util.getListSelectItem(Sexo.values());
    }

    public List<SelectItem> getAssociacao() {
        return Util.getListSelectItem(TipoAssociacao.values());
    }

    public List<SelectItem> getCargosExercidos() {
        return Util.getListSelectItem(TipoCargoExercido.values());
    }

    public List<SelectItem> getGraus() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(Grau.values());
    }

    public void validarConjuge() {
        ValidacaoException ex = new ValidacaoException();
        if (getConjuge() == null)
            ex.adicionarMensagemDeCampoObrigatorio("O Campo conjugê é obrigatório.");

        ex.lancarException();
    }

    public void adicionarHistoricoObreiro() {
        getHistoricoObreiro().setMacon(selecionado);
        selecionado.setHistoricosObreiro(Util.adicionarObjetoEmLista(selecionado.getHistoricosObreiro(), getHistoricoObreiro()));
        setHistoricoObreiro(new HistoricoObreiro());
    }

    public void editarHistoricoObreiro(HistoricoObreiro obreiro) {
        setHistoricoObreiro(obreiro);
        removerHistoricoObreiro(obreiro);
    }

    public void removerHistoricoObreiro(HistoricoObreiro obreiro) {
        selecionado.getHistoricosObreiro().remove(obreiro);
    }

    public void adicionarHistoricoMaconico() {
        try {
            validarAdicionarHistoricoMaconico();
            getHistoricoMaconico().setMacon(selecionado);
            selecionado.setHistoricoMaconicos(Util.adicionarObjetoEmLista(selecionado.getHistoricoMaconicos(), getHistoricoMaconico()));
            setHistoricoMaconico(new HistoricoMaconico());
            historicoMaconico.setPotencia(POTENCIA);
            historicoMaconico.setTipoLocalHistorico(TipoLocalHistorico.INTERNO);
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void validarAdicionarHistoricoMaconico() {
        ValidacaoException ve = new ValidacaoException();

        if (getHistoricoMaconico().getDataHistorico() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor informe a data ");
        }

        if (getHistoricoMaconico().getTipoLocalHistorico().equals(TipoLocalHistorico.EXTERNO)
            && (StringUtils.isEmpty(getHistoricoMaconico().getLojaExterna().trim()))) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor informe a loja");
        }

        if (getHistoricoMaconico().getTipoLocalHistorico().equals(TipoLocalHistorico.INTERNO) && getHistoricoMaconico().getLoja() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor informe a loja");
        }

        ve.lancarException();
    }

    public void editarHistoricoMaconico(HistoricoMaconico historico) {
        setHistoricoMaconico(historico);
        selecionado.getHistoricoMaconicos().remove(historico);
    }

    public void removerHistoricoMaconico(HistoricoMaconico historico) {
        selecionado.getHistoricoMaconicos().remove(historico);
    }

    public void validarHistoricoCargo() {
        Util.validarCampos(getHistoricoCargo());
    }

    public void adicionarHistoricoCargo() {
        try {
            validarHistoricoCargo();
            getHistoricoCargo().setMacon(selecionado);
            selecionado.setHistoricoCargos(Util.adicionarObjetoEmLista(selecionado.getHistoricoCargos(), getHistoricoCargo()));
            setHistoricoCargo(new HistoricoCargo());
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    public void editarHistoricoCargo(HistoricoCargo cargo) {
        setHistoricoCargo(cargo);
        removerHistoricoCargo(cargo);
    }

    public void removerHistoricoCargo(HistoricoCargo cargo) {
        selecionado.getHistoricoCargos().remove(cargo);
    }

    public void carregarFileupload(FileUploadEvent event) {
        try {
            Arquivo arq = new Arquivo();
            arq.setNome(event.getFile().getFileName());
            arq.setTipo(event.getFile().getContentType());
            arq.setArquivo(event.getFile().getContents());

            DocumentoMacon documentoMacon = new DocumentoMacon();
            documentoMacon.setArquivo(arq);
            documentoMacon.setMacon(selecionado);
            selecionado.getDocumentosMacon().add(documentoMacon);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        atualizarComponentesFormulario();
    }

    private void atualizarComponentesFormulario() {
        FacesUtil.atualizaComponete("Formulario:tabela-documentos");
    }

    public void removerDocumentoFornecedor(DocumentoMacon doc) {
        getSelecionado().getDocumentosMacon().remove(doc);
    }

    public StreamedContent realizarDownloadDocumentoMacon(DocumentoMacon doc) {
        if (doc.getArquivo().getId() != null) {
            doc.setArquivo(arquivoRepository.recuperar(Arquivo.class, doc.getArquivo().getId()));
        }
        StreamedContent s = null;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        for (byte a : doc.getArquivo().getArquivo()) {
            buffer.write(a);
        }
        InputStream is = new ByteArrayInputStream(buffer.toByteArray());

        s = new DefaultStreamedContent(is, doc.getArquivo().getTipo(), doc.getArquivo().getNome());
        return s;
    }

//    Converters

    public ConverterAutoComplete getConverterPessoaFisica() {
        if (converterPessoaFisica == null)
            converterPessoaFisica = new ConverterAutoComplete(PessoaFisica.class, pessoaFisicaRepository);
        return converterPessoaFisica;
    }

    public void setConverterPessoaFisica(ConverterAutoComplete converterPessoaFisica) {
        this.converterPessoaFisica = converterPessoaFisica;
    }

//    Gets and Sets


    public Conjuge getConjuge() {
        return conjuge;
    }

    public void setConjuge(Conjuge conjuge) {
        this.conjuge = conjuge;
    }

    public TituloEleitor getTituloEleitor() {
        return tituloEleitor;
    }

    public void setTituloEleitor(TituloEleitor tituloEleitor) {
        this.tituloEleitor = tituloEleitor;
    }

    public Passaporte getPassaporte() {
        return passaporte;
    }

    public void setPassaporte(Passaporte passaporte) {
        this.passaporte = passaporte;
    }

    public RG getRg() {
        return rg;
    }

    public void setRg(RG rg) {
        this.rg = rg;
    }

    public AreaFormacao getAreaFormacao() {
        return areaFormacao;
    }

    public void setAreaFormacao(AreaFormacao areaFormacao) {
        this.areaFormacao = areaFormacao;
    }

    public Filho getFilho() {
        return filho;
    }

    public void setFilho(Filho filho) {
        this.filho = filho;
    }

    public HistoricoObreiro getHistoricoObreiro() {
        return historicoObreiro;
    }

    public void setHistoricoObreiro(HistoricoObreiro historicoObreiro) {
        this.historicoObreiro = historicoObreiro;
    }

    public HistoricoMaconico getHistoricoMaconico() {
        return historicoMaconico;
    }

    public void setHistoricoMaconico(HistoricoMaconico historicoMaconico) {
        this.historicoMaconico = historicoMaconico;
    }

    public CarteiraTrabalho getDocumentoTrabalho() {
        return documentoTrabalho;
    }

    public void setDocumentoTrabalho(CarteiraTrabalho documentoTrabalho) {
        this.documentoTrabalho = documentoTrabalho;
    }

    public HistoricoCargo getHistoricoCargo() {
        return historicoCargo;
    }

    public void setHistoricoCargo(HistoricoCargo historicoCargo) {
        this.historicoCargo = historicoCargo;
    }

    public DocumentoMacon getDocumentoMacon() {
        return documentoMacon;
    }

    public void setDocumentoMacon(DocumentoMacon documentoMacon) {
        this.documentoMacon = documentoMacon;
    }

    @Override
    public List<Macon> completarEstaEntidade(String parte) {
        return repository.completarMaconAtivoPorNomeSimples(parte);
    }

    public boolean exibirSiluetaImagem() {
        try {
            return ((StreamedContent) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_IMAGEM)).getName().trim().length() <= 0;
        } catch (Exception e) {
            return true;
        }
    }

    private void colocarImagemSessao(StreamedContent streamedContent) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(KEY_IMAGEM, streamedContent);
    }

    public void carregarFoto(FileUploadEvent event) {
        try {
            Arquivo arquivo = new Arquivo();
            arquivo.setTipo(event.getFile().getContentType());
            arquivo.setNome(event.getFile().getFileName());
            byte[] content = IOUtils.toByteArray(event.getFile().getInputstream());
            arquivo.setArquivo(content);
            selecionado.setArquivo(arquivo);
            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        } catch (Exception ex) {
            FacesUtil.addError("Erro ao carregar o arquivo de imagem.", ex.getMessage());
        }
    }

    @Override
    public void filtrar() {
        if (sistemaService.getLojaCorrente().equals(lojaRepository.buscarLojaGestora())) {
            super.filtrar();
        } else {
            View view = getView();
            view.setNomeRelatorio("Relatório de " + Persistencia.getNomeEntidade(classe));
            view.setColunas(Lists.newArrayList());
            String sql = Persistencia.adicionarColunas(view, "obj", classe);
            String sqlSelect = " select obj.id, " + sql.substring(0, sql.length() - 2) + "  from " + classe.getSimpleName() + " obj " +
                " inner join historicomaconico hist on obj.id = hist.macon_id " +
                " inner join loja on hist.loja_id = loja.id " +
                " and hist.datahistorico = (SELECT max(x.datahistorico) from historicomaconico x " +
                "                                               where x.macon_id = hist.macon_id " +
                "                                               and x.loja_id is not null) " +
                " where 1=1 ";

            for (Field field : Persistencia.getAtributosPesquisaveis(classe)) {
                criarParametroView(view, field);
            }

            Loja loja = sistemaService.getLojaCorrente();

            ParametroView parametroView = new ParametroView("Loja", "loja.id", loja.getId(), OperacaoCondicaoSql.IGUAL, null, Loja.class);
            view.getParametros().add(parametroView);

            view.setSqlRecuperadorObjetos(sqlSelect);
            view.setSqlOrdenar(" order by obj.id desc");
            view.setSqlContadorObjetos(" select count(obj.id)  from " + classe.getSimpleName() + " obj  " +
                " inner join historicomaconico hist on obj.id = hist.macon_id " +
                " inner join loja on hist.loja_id = loja.id " +
                " and hist.datahistorico = (SELECT max(x.datahistorico) from historicomaconico x " +
                "                                               where x.macon_id = hist.macon_id " +
                "                                               and x.loja_id is not null) " +
                " where 1=1 ");

            view = super.getViewRepository().recuperarObjetos(view);
            view.setTotalDeRegistrosExistentes(null);
        }
    }
}
