package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContasPagar;
import br.com.gleac.entidade.financeiro.Pagamento;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.negocio.ContasPagarRepository;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.PagamentoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 06/07/17.
 */
@ManagedBean(name = "pagamentoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-pagamento", pattern = "/admin/pagamento/novo/", viewId = "/admin/financeiro/pagamento/editar.xhtml"),
    @URLMapping(id = "novo-pagamento-por-conta-pagar", pattern = "/admin/pagamento/conta/#{pagamentoControlador.id}/", viewId = "/admin/financeiro/pagamento/editar.xhtml"),
    @URLMapping(id = "editar-pagamento", pattern = "/admin/pagamento/editar/#{pagamentoControlador.id}/", viewId = "/admin/financeiro/pagamento/editar.xhtml"),
    @URLMapping(id = "listar-pagamento", pattern = "/admin/pagamento/listar/", viewId = "/admin/financeiro/pagamento/listar.xhtml"),
    @URLMapping(id = "ver-pagamento", pattern = "/admin/pagamento/ver/#{pagamentoControlador.id}/", viewId = "/admin/financeiro/pagamento/visualizar.xhtml")
})
public class PagamentoControlador extends AbstractController<Pagamento> implements Serializable {

    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ContasPagarRepository contasPagarRepository;

    @Override
    public AbstractRepository getRepository() {
        return pagamentoRepository;
    }

    public PagamentoControlador() {
        super(Pagamento.class);
    }

    @URLAction(mappingId = "listar-pagamento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-pagamento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        recuperarLoja();
    }

    @URLAction(mappingId = "novo-pagamento-por-conta-pagar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novoContaPagar() {
        super.novo();
        selecionado.setContasPagar(contasPagarRepository.recuperar(ContasPagar.class, getId()));
        ContasPagar contasPagar = selecionado.getContasPagar();
        if (contasPagar != null) {
            selecionado.setValor(contasPagar.getValor());
            selecionado.setDataLancamento(contasPagar.getDataVencimento());
            selecionado.setDescricao(contasPagar.getDescricao());
        }
        recuperarLoja();
    }


    @URLAction(mappingId = "editar-pagamento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        recuperarLoja();
    }

    @URLAction(mappingId = "ver-pagamento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    public void recuperarLoja() {
        if (selecionado.getLoja() != null) {
            selecionado.setLoja(lojaRepository.recuperar(Loja.class, selecionado.getLoja().getId()));
        }
    }

    public List<SelectItem> getContas() {
        return lojaRepository.getContasDaLojaForSelectItem(selecionado.getLoja());
    }

    public List<SelectItem> getFormasDePagamento() {
        return Util.getListSelectItem(FormaDePagamento.values());
    }


}
