package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.Agencia;
import br.com.gleac.negocio.AgenciaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "agenciaControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-agencia", pattern = "/admin/agencia/novo/", viewId = "/admin/financeiro/agencia/editar.xhtml"),
        @URLMapping(id = "editar-agencia", pattern = "/admin/agencia/editar/#{agenciaControlador.id}/", viewId = "/admin/financeiro/agencia/editar.xhtml"),
        @URLMapping(id = "listar-agencia", pattern = "/admin/agencia/listar/", viewId = "/admin/financeiro/agencia/listar.xhtml"),
        @URLMapping(id = "ver-agencia", pattern = "/admin/agencia/ver/#{agenciaControlador.id}/", viewId = "/admin/financeiro/agencia/visualizar.xhtml")
})
public class AgenciaControlador extends AbstractController<Agencia> implements Serializable {

    @Autowired
    private AgenciaRepository agenciaRepository;


    @Override
    public AbstractRepository getRepository() {
        return agenciaRepository;
    }

    public AgenciaControlador() {
        super(Agencia.class);
    }

    @URLAction(mappingId = "listar-agencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-agencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-agencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-agencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

}
