package br.com.gleac.controlador.relatorio;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioMaconPorSituacaoAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.relatorio.RelatorioMaconPorSituacaoRepository;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioMaconPorSituacaoControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-macon-situacao", pattern = "/admin/relatorio-macon-situacao/", viewId = "/admin/relatorios/macon-situacao.xhtml")
public class RelatorioMaconPorSituacaoControlador extends AbstractReportController implements Serializable {

    private Loja loja;
    @Autowired
    private LojaRepository lojaRepository;
    private ConverterAutoComplete converterLoja;
    private Grau grau;
    @Autowired
    private RelatorioMaconPorSituacaoRepository relatorioMaconPorSituacaoRepository;
    private List<RelatorioMaconPorSituacaoAuxiliar> relatorioMaconPorSituacaoAuxiliarList;
    private Future<List<RelatorioMaconPorSituacaoAuxiliar>> resultado;
    private Date dataReferencia;

    @URLAction(mappingId = "novo-relatorio-macon-grau", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setLoja(null);
        setGrau(null);
        relatorioMaconPorSituacaoAuxiliarList = Lists.newLinkedList();
        setDataReferencia(null);
    }

    @Override
    public String getNomeJasper() {
        return "macon-situacao.jasper";
    }

    @Override
    public String getNomePDF() {
        return "MACON-SITUAÇÃO";
    }

    public List<Loja> completarLoja(String filtro) {
        return getLojaRepository().completarLojasByNome(filtro.trim());
    }

    public List<SelectItem> getSituacoes() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Situacao item : Situacao.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getGraus() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Grau item : Grau.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        return parametros;
    }

    private String montarWhere() {
        String sql = " ";

        if (loja != null) {
            sql += (" AND L.ID = " + loja.getId());
        }

        if (grau != null) {
            sql += (" AND M.GRAU = '" + grau.name() + "'");
        }

        if (dataReferencia != null) {
            sql += (" AND HISTORICO.DATAHISTORICO >= TO_DATE('" + Util.sdf.format(dataReferencia) + "' , 'dd/MM/yyyy')");
        }

        if (!isLojaGestora()) {
            sql += (" AND L.ID = '" + getLojaCorrente().getId() + "'");
        }

        sql += " GROUP BY 'Loja - ' || l.numero " +
            " ORDER BY 1 ";
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioMaconPorSituacaoAuxiliarList = Lists.newLinkedList();
            resultado = relatorioMaconPorSituacaoRepository.buscarDadosRelatorio(relatorioMaconPorSituacaoAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Loja");
            titulos.add("Regular");
            titulos.add("Irregular");
            titulos.add("Cobertura de Direitos");
            titulos.add("Quit Placet");
            titulos.add("Emérito");
            titulos.add("Falecido");
            titulos.add("Certificado de Grau");
            titulos.add("Expulso");
            titulos.add("Past Grão-Mestre");
            titulos.add("Benemérito");
            titulos.add("Total");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioMaconPorSituacaoAuxiliar maconPorSituacaoAuxiliar : resultado.get()) {
                Object[] obj = new Object[12];
                obj[0] = maconPorSituacaoAuxiliar.getLoja();
                obj[1] = maconPorSituacaoAuxiliar.getRegular();
                obj[2] = maconPorSituacaoAuxiliar.getIrregular();
                obj[3] = maconPorSituacaoAuxiliar.getCoberturaDireito();
                obj[4] = maconPorSituacaoAuxiliar.getQuitPlacet();
                obj[5] = maconPorSituacaoAuxiliar.getEmerito();
                obj[6] = maconPorSituacaoAuxiliar.getFalecido();
                obj[7] = maconPorSituacaoAuxiliar.getCertificadoGrau();
                obj[8] = maconPorSituacaoAuxiliar.getExpulso();
                obj[9] = maconPorSituacaoAuxiliar.getPastGraoMestre();
                obj[10] = maconPorSituacaoAuxiliar.getBenemerito();
                obj[11] = maconPorSituacaoAuxiliar.getTotal();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Maçons - Situação Maçônico", "Relatório de Maçons - Situação Maçônico", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    public Date getDataReferencia() {
        return dataReferencia;
    }

    public void setDataReferencia(Date dataReferencia) {
        this.dataReferencia = dataReferencia;
    }
}
