package br.com.gleac.controlador.relatorio;


import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioMaconProfissaoAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Profissao;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.ProfissaoRepository;
import br.com.gleac.negocio.relatorio.RelatorioMaconProfissaoRepository;
import br.com.gleac.supers.AbstractReport;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioMaconProfissaoControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-macon-profissao", pattern = "/admin/relatorio-macon-profissao/", viewId = "/admin/relatorios/macon-profissao.xhtml")
public class RelatorioMaconProfissaoControlador extends AbstractReportController implements Serializable {

    @Autowired
    private AbstractReport abstractReport;
    private Loja loja;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ProfissaoRepository profissaoRepository;
    private Profissao profissao;
    private ConverterAutoComplete converterLoja;
    private ConverterAutoComplete converterProfissao;
    private Grau grau;
    private Situacao situacao;
    @Autowired
    private RelatorioMaconProfissaoRepository relatorioMaconProfissaoRepository;
    private List<RelatorioMaconProfissaoAuxiliar> relatorioMaconProfissaoAuxiliarList;
    private Future<List<RelatorioMaconProfissaoAuxiliar>> resultado;

    @URLAction(mappingId = "novo-relatorio-macon-profissao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setProfissao(null);
        setLoja(null);
        setGrau(null);
        setSituacao(null);
        relatorioMaconProfissaoAuxiliarList = Lists.newLinkedList();
    }

    @Override
    public String getNomeJasper() {
        return "macon-profissao.jasper";
    }

    @Override
    public String getNomePDF() {
        return "INFORMAÇÕES DO MAÇOM";
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        return parametros;
    }

    private String montarWhere() {
        String sql = " WHERE 1 = 1 ";

        if (profissao != null) {
            sql += (" AND PROFISSAO.ID = " + profissao.getId());
        }

        if (loja != null) {
            sql += (" AND L.ID = " + loja.getId());
        }

        if (grau != null) {
            sql += (" AND M.GRAU = '" + grau.name() + "'");
        }

        if (situacao != null) {
            sql += (" AND M.SITUACAO = '" + situacao.name() + "'");
        }

        if (!isLojaGestora()) {
            sql += (" AND L.ID = '" + getLojaCorrente().getId() + "'");
        }

        sql += " ORDER BY 1 ";
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioMaconProfissaoAuxiliarList = Lists.newLinkedList();
            resultado = relatorioMaconProfissaoRepository.buscarDadosRelatorio(relatorioMaconProfissaoAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Obreiro");
            titulos.add("Profissão");
            titulos.add("Telefone");
            titulos.add("E-mail");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioMaconProfissaoAuxiliar maconProfissaoAuxiliar : resultado.get()) {
                Object[] obj = new Object[4];
                obj[0] = maconProfissaoAuxiliar.getNome();
                obj[1] = maconProfissaoAuxiliar.getProfissao();
                obj[2] = maconProfissaoAuxiliar.getTelefone();
                obj[3] = maconProfissaoAuxiliar.getEmail();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Maçons - Profissão", "Relatório de Maçons - Profissão", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public List<Profissao> completarProfissao(String filtro) {
        return getProfissaoRepository().buscarProfissaoPorNome(filtro.trim());
    }

    public List<Loja> completarLoja(String filtro) {
        return getLojaRepository().completarLojasByNome(filtro.trim());
    }

    public List<SelectItem> getSituacoes() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Situacao item : Situacao.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getGraus() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Grau item : Grau.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public ProfissaoRepository getProfissaoRepository() {
        return profissaoRepository;
    }

    public ConverterAutoComplete getConverterProfissao() {
        if (converterProfissao == null) {
            converterProfissao = new ConverterAutoComplete(Profissao.class, getProfissaoRepository());
        }
        return converterProfissao;
    }
}
