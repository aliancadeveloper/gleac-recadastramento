package br.com.gleac.controlador.relatorio;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioMaconPorGrauAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.relatorio.RelatorioMaconProGrauRepository;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioMaconPorGrauControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-macon-grau", pattern = "/admin/relatorio-macon-grau/", viewId = "/admin/relatorios/macon-grau.xhtml")
public class RelatorioMaconPorGrauControlador extends AbstractReportController implements Serializable {

    private Loja loja;
    @Autowired
    private LojaRepository lojaRepository;
    private ConverterAutoComplete converterLoja;
    private Situacao situacao;
    @Autowired
    private RelatorioMaconProGrauRepository relatorioMaconProGrauRepository;
    private List<RelatorioMaconPorGrauAuxiliar> relatorioMaconPorGrauAuxiliarList;
    private Future<List<RelatorioMaconPorGrauAuxiliar>> resultado;
    private Date dataReferencia;

    @URLAction(mappingId = "novo-relatorio-macon-grau", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setLoja(null);
        setSituacao(null);
        relatorioMaconPorGrauAuxiliarList = Lists.newLinkedList();
        setDataReferencia(null);
    }

    @Override
    public String getNomeJasper() {
        return "macon-grau.jasper";
    }

    @Override
    public String getNomePDF() {
        return "MACON-GRAU";
    }

    public List<Loja> completarLoja(String filtro) {
        return getLojaRepository().completarLojasByNome(filtro.trim());
    }

    public List<SelectItem> getSituacoes() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Situacao item : Situacao.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getGraus() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Grau item : Grau.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        return parametros;
    }

    private String montarWhere() {
        String sql = " ";

        if (loja != null) {
            sql += (" AND L.ID = " + loja.getId());
        }

        if (situacao != null) {
            sql += (" AND M.SITUACAO = '" + situacao.name() + "'");
        }

        if (dataReferencia != null) {
            sql += (" AND HISTORICO.DATAHISTORICO >= TO_DATE('" + Util.sdf.format(dataReferencia) + "' , 'dd/MM/yyyy')");
        }

        if (!isLojaGestora()) {
            sql += (" AND L.ID = '" + getLojaCorrente().getId() + "'");
        }

        sql += " GROUP BY 'Loja - ' || l.numero " +
            " ORDER BY 1 ";
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioMaconPorGrauAuxiliarList = Lists.newLinkedList();
            resultado = relatorioMaconProGrauRepository.buscarDadosRelatorio(relatorioMaconPorGrauAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Loja");
            titulos.add("Aprendiz");
            titulos.add("Companheiro");
            titulos.add("Mestre");
            titulos.add("Mestre Instalado");
            titulos.add("Grão-Mestre");
            titulos.add("Total");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioMaconPorGrauAuxiliar maconPorGrauAuxiliar : resultado.get()) {
                Object[] obj = new Object[7];
                obj[0] = maconPorGrauAuxiliar.getLoja();
                obj[1] = maconPorGrauAuxiliar.getAprendiz();
                obj[2] = maconPorGrauAuxiliar.getCompanheiro();
                obj[3] = maconPorGrauAuxiliar.getMestre();
                obj[4] = maconPorGrauAuxiliar.getMestraInstalado();
                obj[5] = maconPorGrauAuxiliar.getGraoMestre();
                obj[6] = maconPorGrauAuxiliar.getTotal();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Maçons - Grau Maçônico", "Relatório de Maçons - Grau Maçônico", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Date getDataReferencia() {
        return dataReferencia;
    }

    public void setDataReferencia(Date dataReferencia) {
        this.dataReferencia = dataReferencia;
    }
}
