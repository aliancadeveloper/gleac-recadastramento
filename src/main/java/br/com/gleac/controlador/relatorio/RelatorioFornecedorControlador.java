package br.com.gleac.controlador.relatorio;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioFornecedorAuxiliar;
import br.com.gleac.entidade.comum.Fornecedor;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.FornecedorRepository;
import br.com.gleac.negocio.relatorio.RelatorioFornecedorRepository;
import br.com.gleac.supers.AbstractReport;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioFornecedorControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-fornecedor", pattern = "/admin/relatorio-fornecedor/", viewId = "/admin/relatorios/fornecedor.xhtml")
public class RelatorioFornecedorControlador extends AbstractReportController implements Serializable {

    @Autowired
    private AbstractReport abstractReport;
    @Autowired
    private FornecedorRepository fornecedorRepository;
    private Fornecedor fornecedor;
    private Date dataInicial;
    private Date dataFinal;
    private String numeroContrato;
    private ConverterAutoComplete converterFornecedor;
    private Boolean versaoSimplificada;
    @Autowired
    private RelatorioFornecedorRepository relatorioFornecedorRepository;
    private List<RelatorioFornecedorAuxiliar> relatorioFornecedorAuxiliarList;
    private Future<List<RelatorioFornecedorAuxiliar>> resultado;

    public RelatorioFornecedorControlador() {
    }

    @URLAction(mappingId = "novo-relatorio-fornecedor", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setFornecedor(null);
        setNumeroContrato(null);
        setDataInicial(null);
        setDataFinal(null);
        setVersaoSimplificada(Boolean.TRUE);
        relatorioFornecedorAuxiliarList = Lists.newLinkedList();
    }

    @Override
    public void gerarRelatorio() {
        try {
            validarFiltros();
            super.gerarRelatorio();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }

    }

    public void validarFiltros() {
        ValidacaoException ve = new ValidacaoException();
        if (dataInicial != null && dataFinal != null && (dataFinal.compareTo(dataInicial) < 0)) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("A Data Final deve ser igual ou superior a Data Inicial.");
        }

        ve.lancarException();
    }

    public List<Fornecedor> completarFornecedor(String filtro) {
        return getFornecedorRepository().completarFornecedorByNome(filtro.trim());
    }

    @Override
    public String getNomeJasper() {
        if (versaoSimplificada) {
            return "fornecedor_simplificado.jasper";
        } else {
            return "fornecedor.jasper";
        }
    }

    @Override
    public String getNomePDF() {
        return "FORNECEDOR";
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        parametros.put("SUBREPORT_DIR", abstractReport.getCaminho());
        return parametros;
    }

    private String montarWhere() {
        String sql = " WHERE 1 = 1";
        if (fornecedor != null) {
            sql += (" AND F.ID = " + fornecedor.getId());
        }

        if (!numeroContrato.trim().isEmpty()) {
            sql += (" AND LOWER(F.NUMEROCONTRATO) LIKE '%" + numeroContrato.toLowerCase() + "%'");
        }

        if (dataInicial != null) {
            sql += " AND  TO_DATE(TO_CHAR(F.DATAINICIAL, 'dd/MM/yyyy'),'dd/MM/yyyy') >= TO_DATE('" + Util.sdf.format(dataInicial) + "', 'dd/MM/yyyy') ";
        }
        if (dataFinal != null) {
            sql += " AND  TO_DATE(TO_CHAR(F.DATAFINAL, 'dd/MM/yyyy'),'dd/MM/yyyy') <= TO_DATE('" + Util.sdf.format(dataFinal) + "', 'dd/MM/yyyy')  ";
        }
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioFornecedorAuxiliarList = Lists.newLinkedList();
            resultado = relatorioFornecedorRepository.buscarDadosRelatorio(relatorioFornecedorAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Contrato");
            titulos.add("Fornecedor");
            titulos.add("CPF/CNPJ");
            titulos.add("Serviço/Produto");
            titulos.add("E-mail");
            titulos.add("Telefone");
            titulos.add("Contato");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioFornecedorAuxiliar fornecedorAuxiliar : resultado.get()) {
                Object[] obj = new Object[7];
                obj[0] = fornecedorAuxiliar.getContrato();
                obj[1] = fornecedorAuxiliar.getFornecedor();
                obj[2] = fornecedorAuxiliar.getIdentFornecedor();
                obj[3] = fornecedorAuxiliar.getProduto();
                obj[4] = fornecedorAuxiliar.getEmail();
                obj[5] = fornecedorAuxiliar.getTelefone();
                obj[6] = fornecedorAuxiliar.getContato();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Fornecedores", "Relatório de Fornecedores", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public FornecedorRepository getFornecedorRepository() {
        return fornecedorRepository;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public ConverterAutoComplete getConverterFornecedor() {
        if (converterFornecedor == null) {
            converterFornecedor = new ConverterAutoComplete(Fornecedor.class, getFornecedorRepository());
        }
        return converterFornecedor;
    }

    public Boolean getVersaoSimplificada() {
        return versaoSimplificada;
    }

    public void setVersaoSimplificada(Boolean versaoSimplificada) {
        this.versaoSimplificada = versaoSimplificada;
    }
}
