package br.com.gleac.controlador.relatorio;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioMaconAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.relatorio.RelatorioMaconRepository;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioMaconControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-macon", pattern = "/admin/relatorio-macon/", viewId = "/admin/relatorios/macon.xhtml")
public class RelatorioMaconControlador extends AbstractReportController implements Serializable {

    private Loja loja;
    @Autowired
    private LojaRepository lojaRepository;
    private ConverterAutoComplete converterLoja;
    private Grau grau;
    private Situacao situacao;
    private Boolean versaoSimplificada;
    @Autowired
    private RelatorioMaconRepository relatorioMaconRepository;
    private List<RelatorioMaconAuxiliar> relatorioMaconAuxiliarList;
    private Future<List<RelatorioMaconAuxiliar>> resultado;

    @URLAction(mappingId = "novo-relatorio-macon", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setLoja(null);
        setGrau(null);
        setSituacao(null);
        setVersaoSimplificada(Boolean.TRUE);
        relatorioMaconAuxiliarList = Lists.newLinkedList();
    }

    @Override
    public String getNomeJasper() {
        if (versaoSimplificada) {
            return "macon_simplificado.jasper";
        } else {
            return "macon.jasper";
        }
    }

    @Override
    public String getNomePDF() {
        return "MACON";
    }

    public List<Loja> completarLoja(String filtro) {
        return getLojaRepository().completarLojasByNome(filtro.trim());
    }

    public List<SelectItem> getSituacoes() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Situacao item : Situacao.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getGraus() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Grau item : Grau.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        return parametros;
    }

    private String montarWhere() {
        String sql = " WHERE 1 = 1 ";

        if (loja != null) {
            sql += (" AND L.ID = " + loja.getId());
        }

        if (grau != null) {
            sql += (" AND M.GRAU = '" + grau.name() + "'");
        }

        if (situacao != null) {
            sql += (" AND M.SITUACAO = '" + situacao.name() + "'");
        }

        if (!isLojaGestora()) {
            sql += (" AND L.ID = '" + getLojaCorrente().getId() + "'");
        }

        sql += " ORDER BY 2 ";
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioMaconAuxiliarList = Lists.newLinkedList();
            resultado = relatorioMaconRepository.buscarDadosRelatorio(relatorioMaconAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Cadastro");
            titulos.add("Obreiro");
            titulos.add("Loja");
            titulos.add("Grau");
            titulos.add("Situação do Maçon");
            titulos.add("Situação do Histórico");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioMaconAuxiliar maconAuxiliar : resultado.get()) {
                Object[] obj = new Object[6];
                obj[0] = maconAuxiliar.getCadastro();
                obj[1] = maconAuxiliar.getObreiro();
                obj[2] = maconAuxiliar.getLoja();
                obj[3] = maconAuxiliar.getGrau();
                obj[4] = maconAuxiliar.getSituacaoMacon();
                obj[5] = maconAuxiliar.getSituacaoHistorico();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Maçons", "Relatório de Maçons", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Boolean getVersaoSimplificada() {
        return versaoSimplificada;
    }

    public void setVersaoSimplificada(Boolean versaoSimplificada) {
        this.versaoSimplificada = versaoSimplificada;
    }
}
