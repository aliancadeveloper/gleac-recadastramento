package br.com.gleac.controlador.relatorio;


import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.auxiliares.RelatorioMaconDataNascimentoAuxiliar;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Mes;
import br.com.gleac.enums.Situacao;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.relatorio.RelatorioMaconDataNascimentoFacade;
import br.com.gleac.supers.AbstractReportController;
import br.com.gleac.util.ExcelUtil;
import br.com.gleac.util.FacesUtil;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

@ManagedBean(name = "relatorioMaconDataNascimentoControlador")
@ViewScoped
@URLMapping(id = "novo-relatorio-macon-nascimento", pattern = "/admin/relatorio-macon-nascimento/", viewId = "/admin/relatorios/macon-nascimento.xhtml")
public class RelatorioMaconDataNascimentoControlador extends AbstractReportController implements Serializable {

    private Loja loja;
    @Autowired
    private LojaRepository lojaRepository;
    private ConverterAutoComplete converterLoja;
    private ConverterAutoComplete converterProfissao;
    private Grau grau;
    private Situacao situacao;
    private Mes mes;
    private Integer ano;
    private List<RelatorioMaconDataNascimentoAuxiliar> relatorioMaconDataNascimentoAuxiliarList;
    private Future<List<RelatorioMaconDataNascimentoAuxiliar>> resultado;
    @Autowired
    private RelatorioMaconDataNascimentoFacade relatorioMaconDataNascimentoFacade;

    public RelatorioMaconDataNascimentoControlador() {
    }

    @URLAction(mappingId = "novo-relatorio-macon-nascimento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        limparCampos();
    }

    public void limparCampos() {
        setLoja(null);
        setGrau(null);
        setSituacao(null);
        setMes(null);
        setAno(null);
        relatorioMaconDataNascimentoAuxiliarList = Lists.newLinkedList();
    }

    @Override
    public String getNomeJasper() {
        return "macon-nascimento.jasper";
    }

    @Override
    public String getNomePDF() {
        return "INFORMAÇÕES DO MAÇOM";
    }

    @Override
    public HashMap getParametros() {
        HashMap parametros = new HashMap();
        parametros.put("TITULO", getConfiguracaoCabecalho().getTitulo());
        parametros.put("IMAGEM", getConfiguracaoCabecalho().getLogoInputStream());
        String sql = montarWhere();

        parametros.put("SQL", sql);
        return parametros;
    }

    private String montarWhere() {
        String sql = " WHERE 1 = 1 ";

        if (loja != null) {
            sql += (" AND L.ID = " + loja.getId());
        }

        if (grau != null) {
            sql += (" AND M.GRAU = '" + grau.name() + "'");
        }

        if (situacao != null) {
            sql += (" AND M.SITUACAO = '" + situacao.name() + "'");
        }

        if (mes != null) {
            sql += (" AND EXTRACT(MONTH FROM PF.DATANASCIMENTO) = " + getMes().getNumeroComZero());
        }

        if (ano != null) {
            sql += (" AND EXTRACT(YEAR FROM PF.DATANASCIMENTO) = " + getAno());
        }

        if (!isLojaGestora()) {
            sql += (" AND L.ID = '" + getLojaCorrente().getId() + "'");
        }

        sql += " ORDER BY 1 ";
        return sql;
    }

    public void gerarRelatorioExcel() {
        try {
            FacesUtil.executaJavaScript("PF('poll').start();");
            relatorioMaconDataNascimentoAuxiliarList = Lists.newLinkedList();
            resultado = relatorioMaconDataNascimentoFacade.buscarDadosRelatorio(relatorioMaconDataNascimentoAuxiliarList, montarWhere());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public StreamedContent fileDownload() throws FileNotFoundException, IOException {
        try {
            List<String> titulos = new ArrayList<>();
            titulos.add("Obreiro");
            titulos.add("Data de Nascimento");
            titulos.add("Idade");
            titulos.add("Telefone");
            titulos.add("E-mail");
            List<Object[]> objetos = new ArrayList<>();

            for (RelatorioMaconDataNascimentoAuxiliar relatorioMaconDataNascimentoAuxiliar : resultado.get()) {
                Object[] obj = new Object[5];
                obj[0] = relatorioMaconDataNascimentoAuxiliar.getObreiro();
                obj[1] = relatorioMaconDataNascimentoAuxiliar.getDataNascimento();
                obj[2] = relatorioMaconDataNascimentoAuxiliar.getIdade();
                obj[3] = relatorioMaconDataNascimentoAuxiliar.getTelefone();
                obj[4] = relatorioMaconDataNascimentoAuxiliar.getEmail();

                objetos.add(obj);
            }

            ExcelUtil excel = new ExcelUtil();
            excel.gerarExcel("Relatório de Maçons Por Data de Nascimento", "Relatório de Maçons Por Data de Nascimento", titulos, objetos, null, null);
            if (objetos.size() > 0) {
                return excel.fileDownload();
            } else {
                FacesUtil.addAtencao("Não foram encontrados registros para os parâmetros apresentados.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void verificarTerminoProcesso() {
        if (resultado != null && resultado.isDone()) {
            FacesUtil.executaJavaScript("PF('poll').stop();");
            FacesUtil.executaJavaScript("$('#imprimir').modal('show')");
        }
    }

    public List<Loja> completarLoja(String filtro) {
        return getLojaRepository().completarLojasByNome(filtro.trim());
    }

    public List<SelectItem> getSituacoes() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Situacao item : Situacao.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getGraus() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Grau item : Grau.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public List<SelectItem> getMeses() {
        List<SelectItem> itens = Lists.newArrayList();
        itens.add(new SelectItem("", ""));
        for (Mes item : Mes.values()) {
            itens.add(new SelectItem(item, item.getDescricao()));
        }

        return itens;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Mes getMes() {
        return mes;
    }

    public void setMes(Mes mes) {
        this.mes = mes;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Future<List<RelatorioMaconDataNascimentoAuxiliar>> getResultado() {
        return resultado;
    }

    public void setResultado(Future<List<RelatorioMaconDataNascimentoAuxiliar>> resultado) {
        this.resultado = resultado;
    }
}
