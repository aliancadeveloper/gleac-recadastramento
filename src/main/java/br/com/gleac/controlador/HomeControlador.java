package br.com.gleac.controlador;

import br.com.gleac.entidade.auxiliares.dto.SaldoPorLojaDTO;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.LojaConta;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.SaldoFinanceiro;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.seguranca.Notificacao;
import br.com.gleac.entidade.seguranca.Recurso;
import br.com.gleac.entidade.seguranca.RecursoSistema;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.enums.Modulo;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.negocio.*;
import br.com.gleac.service.SistemaService;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.PrettyContext;
import com.ocpsoft.pretty.faces.config.mapping.UrlMapping;
import com.ocpsoft.pretty.faces.url.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;

/**
 * Created by renatoromanini on 24/05/16.
 */
@ManagedBean(name = "homeControlador")
@ViewScoped
public class HomeControlador implements Serializable {
    private static final Integer QUANTIDADE_MOVIMENTOS = 5;
    public static final String LIST = "listar/";
    public static final String NEW = "novo/";
    public static final String VIEW = "visualizar/";
    public static final List<String> DEFAULT_PATHS = Arrays.asList(LIST, NEW, VIEW);
    @Autowired
    protected SistemaService sistemaService;
    @Autowired
    private PotenciaRepository potenciaRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private NotificacaoRepository notificacaoRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private ContasPagarRepository contasPagarRepository;
    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private PagamentoRepository pagamentoRepository;
    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private RecursoSistemaRepository recursoSistemaRepository;


    private Loja lojaCorrente;
    private List<SaldoPorLojaDTO> saldosDasLojas;

    public HomeControlador() {
        injetarDependenciasSpring();
    }


    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    public Usuario getUsuario() {
        Usuario usuario = sistemaService.getUsuarioCorrente();
        return usuario;
//        return null;
    }

    public Boolean isMacon() {
        return sistemaService.isMacon();
    }

    public Boolean isPerfilFinanceiro() {
        return sistemaService.isPerfilFinanceiro();
    }

    public Macon getMaconLogado() {
        return sistemaService.getMacon();
    }

    public String getImage() {
//        return "";
        if (getUsuario().getArquivo() != null) {
            return Util.getRequestContextPath().concat("/arquivo?id=").concat(getUsuario().getArquivo().getId() + "");
        }
        return Util.getRequestContextPath().concat("/resources/images/no_user.png");
    }

    public Loja getLojaCorrente() {
        if (lojaCorrente == null) {
            lojaCorrente = sistemaService.getLojaCorrente();
        }
        return lojaCorrente;
    }

    public void setLojaCorrente(Loja lojaCorrente) {
        this.lojaCorrente = lojaCorrente;
        sistemaService.setLojaCorrente(lojaCorrente);
    }

    public Integer getNumeroNotificacao() {
        return notificacaoRepository.buscarAndContarNotificacaoUsuarioAndVisualizado(getUsuario(), Boolean.FALSE);
    }

    public List<Notificacao> getNotificacoes() {
        return notificacaoRepository.buscarNotificacaoPorUsuarioAndVisualizacao(getUsuario(), Boolean.FALSE);
    }

    public void marcarNotificacaoVisualizada(Notificacao notificacao) {
        notificacao.setVisualizado(Boolean.TRUE);
        notificacaoRepository.salvar(notificacao);
    }

    public List<Loja> getLojasUsuario() {
        return sistemaService.getLojas();
    }

    public List<Field> getAtributosTabelaveisMacon() {
        return Persistencia.getAtributosTabelaveis(Macon.class);
    }


    public void redirecionarMacon() {
        FacesUtil.redirecionamentoInterno("/admin/macon/editar/" + getMaconLogado().getId() + "");
    }

    public Boolean isHome() {
        if (Util.localUsuarioAtualmenteCompleta().contains("index.xhtml")) {
            return true;
        }
        return false;
    }

    //perfil macon
    public List<ContasReceber> getContasReceberComBoletoDoMacon() {
        try {
            if (getLojaCorrente() != null && isMacon()) {
                return contasReceberRepository.recuperarContasReceberAbertaComBoleto(getLojaCorrente(), getMaconLogado().getPessoaFisica());
            }
            return Lists.newArrayList();
        } catch (Exception ex) {
            return Lists.newArrayList();
        }
    }

    public List<SolicitacaoProcesso> getSolicitacaoProcessosDoMacon() {
        try {
            if (getLojaCorrente() != null && isMacon()) {
                return solicitacaoProcessoRepository.recuperarSolicitacaoPorPessoa(getMaconLogado().getPessoaFisica());
            }
            return Lists.newArrayList();
        } catch (Exception ex) {
            return Lists.newArrayList();
        }
    }

    //dashboad
    public Date getDataAtual() {
        return new Date();
    }

    public Integer getAnoAtual() {
        return Util.recuperarExercicio();
    }

    public static Integer getQuantidadeMovimentos() {
        return QUANTIDADE_MOVIMENTOS;
    }

    public Long getCountLojas() {
        return lojaRepository.getLojaJPARepository().count();
    }

    public Long getCountMacons() {
        if (!lojaRepository.buscarLojaGestora().getId().equals(getLojaCorrente().getId())) {
            return maconRepository.contarMacomPorLoja(getLojaCorrente());
        } else {
            return maconRepository.getMaconJPARepository().count();
        }
    }

    public BigDecimal getContasReceber() {
        try {
            return contasReceberRepository.recuperarSomaPorDataAndSituacao(getDataAtual(), sistemaService.getLojaCorrente(), SituacaoContaPagar.ABERTA);
        } catch (Exception ex) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getContasPagar() {
        try {
            return contasPagarRepository.recuperarSomaPorDataAndSituacao(getDataAtual(), sistemaService.getLojaCorrente(), SituacaoContaPagar.ABERTA);
        } catch (Exception ex) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getReceita() {
        try {
            return receitaRepository.recuperarSoma(Util.recuperarExercicio(), null, null, sistemaService.getLojaCorrente());
        } catch (Exception ex) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getPagamento() {
        try {
            return pagamentoRepository.recuperarSoma(Util.recuperarExercicio(), null, null, sistemaService.getLojaCorrente());
        } catch (Exception ex) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getSaldoFinanceiro() {
        try {
            BigDecimal retorno = BigDecimal.ZERO;
            for (SaldoPorLojaDTO saldosDasLoja : getSaldosDasLojas()) {
                retorno = retorno.add(saldosDasLoja.getSaldoFinanceiro());
            }
            return retorno;
        } catch (Exception ex) {
            ex.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    public List<SaldoPorLojaDTO> getSaldosDasLojas() {
        if (saldosDasLojas == null) {
            saldosDasLojas = Lists.newArrayList();

            Loja lojaCorrente = getLojaCorrente();
            for (LojaConta conta : lojaCorrente.getContas()) {
                SaldoFinanceiro saldo = new SaldoFinanceiro(lojaCorrente, conta.getContaCorrente(), BigDecimal.ZERO, new Date());
                SaldoFinanceiro ultimoSaldo = lancamentoFinanceiroRepository.getUltimoSaldoPorDataLojaConta(saldo);
                if (ultimoSaldo != null) {
                    criarDTOSaldoPorLoja(ultimoSaldo);
                } else {
                    criarDTOSaldoPorLoja(saldo);
                }
            }
        }
        Collections.sort(saldosDasLojas, new Comparator<SaldoPorLojaDTO>() {
            @Override
            public int compare(SaldoPorLojaDTO o1, SaldoPorLojaDTO o2) {
                return o1.getLoja().getNumero().compareTo(o2.getLoja().getNumero());
            }
        });
        return saldosDasLojas;
    }

    private void criarDTOSaldoPorLoja(SaldoFinanceiro ultimoSaldo) {
        SaldoPorLojaDTO dto = new SaldoPorLojaDTO();
        dto.setContaCorrente(ultimoSaldo.getContaCorrente());
        dto.setData(ultimoSaldo.getData());
        dto.setSaldoFinanceiro(ultimoSaldo.getSaldo());
        dto.setLoja(ultimoSaldo.getLoja());

        dto.setContasPagar(contasPagarRepository.recuperarSomaPorDataAndSituacao(getDataAtual(), dto.getLoja(), SituacaoContaPagar.ABERTA));
        dto.setContasReceber(contasReceberRepository.recuperarSomaPorDataAndSituacao(getDataAtual(), dto.getLoja(), SituacaoContaPagar.ABERTA));
        saldosDasLojas.add(dto);
    }
    // menu

    @PostConstruct
    public void init() {
        /*getContasReceberComBoleto();*/
    }

    public List<Modulo> getModuloSistemaPorUsuario() {
        List<Modulo> retorno = Lists.newArrayList();
        List<Recurso> recursos = sistemaService.getRecursos();
        for (Modulo moduloSistema : Modulo.values()) {
            if (moduloSistema.getRecursos() == null) {
                moduloSistema.setRecursos(Lists.newArrayList());
            }
            moduloSistema.getRecursos().clear();
            for (Recurso recurso : recursos) {
                if (recurso.getRecursoSistema().getModulo().equals(moduloSistema)) {
                    if (!existeEsseRecurso(moduloSistema, recurso)) {
                        moduloSistema.getRecursos().add(recurso);
                    }
                }
            }
            if (!moduloSistema.getRecursos().isEmpty()) {
                retorno.add(moduloSistema);
            }
        }
        ordenarModulos(retorno);
        return retorno;
    }

    private boolean existeEsseRecurso(Modulo moduloSistema, Recurso recurso) {
        for (Recurso rec : moduloSistema.getRecursos()) {
            if (rec.getRecursoSistema().equals(recurso.getRecursoSistema())) {
                return true;
            }
        }
        return false;
    }


    private Function<RecursoSistema, Boolean> filterByModulo(Modulo modulo) {
        return x -> x.getModulo().equals(modulo);
    }

    private void ordenarModulos(List<Modulo> modulos) {
        Collections.sort(modulos, new Comparator<Modulo>() {
            @Override
            public int compare(Modulo o1, Modulo o2) {
                return o1.getPosicao().compareTo(o2.getPosicao());
            }
        });
        for (Modulo modulo : modulos) {
            Collections.sort(modulo.getRecursos(), new Comparator<Recurso>() {
                @Override
                public int compare(Recurso o1, Recurso o2) {
                    return o1.getRecursoSistema().getDescricao().compareTo(o2.getRecursoSistema().getDescricao());
                }
            });
        }

    }

    public void ordernarRecursos(List<RecursoSistema> recursos) {
        Collections.sort(recursos, new Comparator<RecursoSistema>() {
            @Override
            public int compare(RecursoSistema o1, RecursoSistema o2) {
                return o1.getDescricao().compareTo(o2.getDescricao());
            }
        });
    }

    public String getUrlMapped(String path) {

        for (String defaultPath : DEFAULT_PATHS) {
            String defaultPathRoute = getDefaultPathsRoutes(path, defaultPath);
            URL url = new URL(defaultPathRoute);
            UrlMapping mappingForUrl = getUrlMapping(url);
            if (!ObjectUtils.isEmpty(mappingForUrl)) {
                return mappingForUrl.getPattern();
            }
        }

        return path;

    }

    private String getDefaultPathsRoutes(String path, String defaultRoute) {
        return path.concat(defaultRoute);
    }

    private UrlMapping getUrlMapping(URL url) {
        return getCurrentInstancePretty().getConfig().getMappingForUrl(url);
    }

    private PrettyContext getCurrentInstancePretty() {
        return PrettyContext.getCurrentInstance();
    }


    public String getUsuarioComSaudacao() {
        return "Olá " + getUsuario().getLogin() + ", " + Util.getSaudacao();
    }
}
