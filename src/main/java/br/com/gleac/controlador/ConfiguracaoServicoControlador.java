package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Formulario;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.configuracao.ConfiguracaoServico;
import br.com.gleac.entidade.configuracao.ConfiguracaoServicoFormulario;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.ConfiguracaoServicoRepository;
import br.com.gleac.negocio.FormularioRepository;
import br.com.gleac.negocio.ServicoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

/**
 * Created by zaca.
 */
@ManagedBean(name = "configuracaoServicoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "nova-configuracao-servico",
                pattern = "/admin/configuracao/servico/novo/",
                viewId = "/admin/cadastros/configuracao-servico/editar.xhtml"),
        @URLMapping(id = "editar-configuracao-servico",
                pattern = "/admin/configuracao/servico/editar/#{configuracaoServicoControlador.id}/",
                viewId = "/admin/cadastros/configuracao-servico/editar.xhtml"),
        @URLMapping(id = "ver-configuracao-servico",
                pattern = "/admin/configuracao/servico/ver/#{configuracaoServicoControlador.id}/",
                viewId = "/admin/cadastros/configuracao-servico/visualizar.xhtml"),
        @URLMapping(id = "listar-configuracao-servico",
                pattern = "/admin/configuracao/servico/listar/",
                viewId = "/admin/cadastros/configuracao-servico/listar.xhtml")
})
public class ConfiguracaoServicoControlador extends AbstractController<ConfiguracaoServico> implements Crud {

    @Autowired
    private ConfiguracaoServicoRepository configuracaoServicoRepository;
    @Autowired
    private ServicoRepository servicoRepository;
    @Autowired
    private FormularioRepository formularioRepository;

    //converters
    private ConverterAutoComplete converterServico;
    private ConverterAutoComplete converterFormulario;

    //objects
    private ConfiguracaoServicoFormulario configuracaoServicoFormulario;

    @Override
    public AbstractRepository getRepository() {
        return getConfiguracaoServicoRepository();
    }

    public ConfiguracaoServicoControlador() {
        super(ConfiguracaoServico.class);
    }

    @URLAction(mappingId = "nova-configuracao-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        setConfiguracaoServicoFormulario(new ConfiguracaoServicoFormulario());
    }

    @URLAction(mappingId = "editar-configuracao-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        setConfiguracaoServicoFormulario(new ConfiguracaoServicoFormulario());
    }

    @URLAction(mappingId = "ver-configuracao-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-configuracao-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    // methods

    public List<Servico> completarServicosLessConfiguracao(String filtro) {
        return getConfiguracaoServicoRepository().completarServicosByDescricao(filtro);
    }

    public List<Formulario> completarFormularios(String filtro) {
        return getFormularioRepository().completarFormularioByDescricao(filtro);
    }

    public void adicionarFormulario() {
        try {
            validarFormulario();
            getConfiguracaoServicoFormulario().setConfiguracaoServico(getSelecionado());
            getSelecionado().getConfiguracaoServicoFormularios().add(getConfiguracaoServicoFormulario());
            setConfiguracaoServicoFormulario(new ConfiguracaoServicoFormulario());
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception e) {
            FacesUtil.addErrorGenerico(e);
        }
    }

    @Override
    public void salvar() {
        try {
            selecionado.validarRegrasPrincipais();
            validarConfiguracaoServicoFormulario();
            super.salvar();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addErrorGenerico(ex);
        }

    }

    private void validarConfiguracaoServicoFormulario() {
        ValidacaoException ve = new ValidacaoException();
        if (selecionado.getConfiguracaoServicoFormularios() == null || selecionado.getConfiguracaoServicoFormularios().isEmpty()) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor informe pelo menos um formulário");
        }

        ve.lancarException();
    }

    private void validarFormulario() {
        ValidacaoException ve = new ValidacaoException();

        if (getConfiguracaoServicoFormulario().getFormulario() == null) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe um formulário");
        }

        if (getConfiguracaoServicoFormulario().getFormulario() != null) {
            getSelecionado().getConfiguracaoServicoFormularios()
                    .stream()
                    .filter(config -> config.getFormulario()
                            .equals(getConfiguracaoServicoFormulario()
                                    .getFormulario()))
                    .findFirst()
                    .ifPresent(present -> {
                        ve.adicionarMensagemDeOperacaoNaoPermitida("Esta formulário já foi informado, por favor selecione outro.");
                    });
        }

        ve.lancarException();
    }

    public void removerFormulario(ConfiguracaoServicoFormulario form) {
        getSelecionado().getConfiguracaoServicoFormularios().remove(form);
    }

    public ConfiguracaoServicoRepository getConfiguracaoServicoRepository() {
        return configuracaoServicoRepository;
    }

    public ServicoRepository getServicoRepository() {
        return servicoRepository;
    }

    public FormularioRepository getFormularioRepository() {
        return formularioRepository;
    }

    public ConverterAutoComplete getConverterServico() {
        if (converterServico == null) {
            converterServico = new ConverterAutoComplete(Servico.class, getServicoRepository());
        }
        return converterServico;
    }

    public void setConverterServico(ConverterAutoComplete converterServico) {
        this.converterServico = converterServico;
    }

    public ConverterAutoComplete getConverterFormulario() {
        if (converterFormulario == null) {
            converterFormulario = new ConverterAutoComplete(Formulario.class, getFormularioRepository());
        }
        return converterFormulario;
    }

    public void setConverterFormulario(ConverterAutoComplete converterFormulario) {
        this.converterFormulario = converterFormulario;
    }

    public ConfiguracaoServicoFormulario getConfiguracaoServicoFormulario() {
        return configuracaoServicoFormulario;
    }

    public void setConfiguracaoServicoFormulario(ConfiguracaoServicoFormulario configuracaoServicoFormulario) {
        this.configuracaoServicoFormulario = configuracaoServicoFormulario;
    }
}
