package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Cidade;
import br.com.gleac.entidade.comum.Estado;
import br.com.gleac.negocio.CidadeRepository;
import br.com.gleac.negocio.EstadoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 02/09/16.
 */
@ManagedBean(name = "cidadeControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-cidade", pattern = "/admin/cidade/novo/", viewId = "/admin/cadastros/cidade/editar.xhtml"),
    @URLMapping(id = "editar-cidade", pattern = "/admin/cidade/editar/#{cidadeControlador.id}/", viewId = "/admin/cadastros/cidade/editar.xhtml"),
    @URLMapping(id = "ver-cidade", pattern = "/admin/cidade/ver/#{cidadeControlador.id}/", viewId = "/admin/cadastros/cidade/visualizar.xhtml"),
    @URLMapping(id = "listar-cidade", pattern = "/admin/cidade/listar/", viewId = "/admin/cadastros/cidade/listar.xhtml")
})
public class CidadeControlador extends AbstractController<Cidade> implements Serializable{

    @Autowired
    private CidadeRepository repository;

    @Autowired
    private EstadoRepository estadoRepository;

    private ConverterAutoComplete converterEstado;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public CidadeControlador() {
        super(Cidade.class);
    }

    @URLAction(mappingId = "novo-cidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-cidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-cidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-cidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        super.salvar();
    }

    public List<Estado> completarEstado(String filter){
        return estadoRepository.buscarEstadoPorNome(filter);
    }

    public ConverterAutoComplete getConverterEstado() {
        if (converterEstado == null)
            converterEstado = new ConverterAutoComplete(Estado.class, estadoRepository);
        return converterEstado;
    }

    public void setConverterEstado(ConverterAutoComplete converterEstado) {
        this.converterEstado = converterEstado;
    }
}
