package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Bairro;
import br.com.gleac.entidade.comum.Cidade;
import br.com.gleac.negocio.BairroRepository;
import br.com.gleac.negocio.CidadeRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 02/09/16.
 */
@ManagedBean(name = "bairroControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-bairro", pattern = "/admin/bairro/novo/", viewId = "/admin/cadastros/bairro/editar.xhtml"),
    @URLMapping(id = "editar-bairro", pattern = "/admin/bairro/editar/#{bairroControlador.id}/", viewId = "/admin/cadastros/bairro/editar.xhtml"),
    @URLMapping(id = "ver-bairro", pattern = "/admin/bairro/ver/#{bairroControlador.id}/", viewId = "/admin/cadastros/bairro/visualizar.xhtml"),
    @URLMapping(id = "listar-bairro", pattern = "/admin/bairro/listar/", viewId = "/admin/cadastros/bairro/listar.xhtml")

})
public class BairroControlador extends AbstractController<Bairro> implements Serializable{

    @Autowired
    private BairroRepository repository;

    @Autowired
    private CidadeRepository cidadeRepository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public BairroControlador() {
        super(Bairro.class);
    }

    @URLAction(mappingId = "novo-bairro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-bairro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-bairro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-bairro", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<Cidade> buscarCidade(String filter){
        return cidadeRepository.completarCidadeByNome(filter.trim(), null);
    }
}
