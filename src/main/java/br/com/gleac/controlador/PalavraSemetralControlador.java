package br.com.gleac.controlador;


import br.com.gleac.entidade.gerenciamentosite.PalavraSemestral;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.PalavraSemestralRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Created by renato on 11/02/17.
 */
@ManagedBean(name = "palavraSemestralControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-palavra-semestral", pattern = "/admin/palavra-semestral/novo/", viewId = "/admin/cadastros/palavra-semestral/editar.xhtml"),
    @URLMapping(id = "editar-palavra-semestral", pattern = "/admin/palavra-semestral/editar/#{palavraSemestralControlador.id}/", viewId = "/admin/cadastros/palavra-semestral/editar.xhtml"),
    @URLMapping(id = "ver-palavra-semestral", pattern = "/admin/palavra-semestral/ver/#{palavraSemestralControlador.id}/", viewId = "/admin/cadastros/palavra-semestral/visualizar.xhtml"),
    @URLMapping(id = "listar-palavra-semestral", pattern = "/admin/palavra-semestral/listar/", viewId = "/admin/cadastros/palavra-semestral/listar.xhtml")
})
public class PalavraSemetralControlador extends AbstractController<PalavraSemestral> implements Crud {

    @Autowired
    private PalavraSemestralRepository palavraSemestralRepository;


    @Override
    public AbstractRepository getRepository() {
        return palavraSemestralRepository;
    }

    public PalavraSemetralControlador() {
        super(PalavraSemestral.class);
    }

    @URLAction(mappingId = "novo-palavra-semestral", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-palavra-semestral", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-palavra-semestral", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-palavra-semestral", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

}
