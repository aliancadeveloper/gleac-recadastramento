package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.*;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.enums.TipoEndereco;
import br.com.gleac.enums.TipoInstituicao;
import br.com.gleac.enums.TipoTelefone;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.BairroRepository;
import br.com.gleac.negocio.CidadeRepository;
import br.com.gleac.negocio.PessoaJuridicaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 01/09/16.
 */
@ManagedBean(name = "pessoaJuridicaControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-pessoa-juridica", pattern = "/admin/pessoa-juridica/novo/", viewId = "/admin/cadastros/pessoa/juridica.xhtml"),
        @URLMapping(id = "editar-pessoa-juridica", pattern = "/admin/pessoa-juridica/editar/#{pessoaJuridicaControlador.id}/", viewId = "/admin/cadastros/pessoa/juridica.xhtml"),
        @URLMapping(id = "ver-pessoa-juridica", pattern = "/admin/pessoa-juridica/ver/#{pessoaJuridicaControlador.id}/", viewId = "/admin/cadastros/pessoa/visualizarjuridica.xhtml"),
        @URLMapping(id = "listar-juridica", pattern = "/admin/pessoa-juridica/listar/", viewId = "/admin/cadastros/pessoa/listar-juridica.xhtml")
})
public class PessoaJuridicaControlador extends AbstractController<PessoaJuridica> implements Serializable {

    @Autowired
    private PessoaJuridicaRepository repository;

    @Autowired
    private BairroRepository bairroRepository;

    @Autowired
    private CidadeRepository cidadeRepository;

    private Endereco endereco;
    private Telefone telefone;
    private ContaCorrenteBancaria contaCorrente;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public PessoaJuridicaControlador() {
        super(PessoaJuridica.class);
    }

    @URLAction(mappingId = "novo-pessoa-juridica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        endereco = new Endereco();
        telefone = new Telefone();
        inicializarConta();
    }

    @URLAction(mappingId = "editar-pessoa-juridica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        endereco = new Endereco();
        telefone = new Telefone();
        inicializarConta();
    }

    @URLAction(mappingId = "ver-pessoa-juridica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();

    }

    @URLAction(mappingId = "listar-juridica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }


    public void validarCnpj() {
        Boolean validado = Boolean.TRUE;
        if (selecionado.getId() == null ) {
            if (!Util.isValidCpfOrCnpj(selecionado.getCnpj())) {
                FacesUtil.addAtencao("O CNPJ " + selecionado.getCnpj() + " é inválido. ");
                validado = Boolean.FALSE;
            }

            if (!repository.getPessoasComMesmoCNPJ(selecionado.getCnpj()).isEmpty()) {
                for (PessoaJuridica pessoaJuridica : repository.getPessoasComMesmoCNPJ(selecionado.getCnpj())) {
                    FacesUtil.addAtencao("O CNPJ " + selecionado.getCnpj() + " já está cadastrado para " + pessoaJuridica.getNomePessoa());
                }
                validado = Boolean.FALSE;
            }
        }
        if (!validado) {
            selecionado.setCnpj(null);
        }
    }

    public List<SelectItem> getTipoInstituicoes(){
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(TipoInstituicao.values());
    }

    public List<Bairro> buscarBairro(String filter){
        if (endereco != null && endereco.getCidade() != null) {
            return bairroRepository.buscarBairroPorNomeCidade(filter.trim(), endereco.getCidade());
        }
        else {
            FacesUtil.addCampoObrigatorio("O campo Cidade deve ser informado.");
            return new ArrayList<>();
        }
    }

    public List<Cidade> buscarCidade(String filter){
        return cidadeRepository.completarCidadeByNome(filter, null);
    }

    public List<PessoaJuridica> buscarPessoaJuridica(String filter){
        return repository.buscarPessoaJuridica(filter);
    }


    public void adicionarTelefone() {

        try {
            validarTelefone();
            Util.isTamanhoTelefoneValido(getTelefone().getTelefone());
            getTelefone().setTelefone(Util.getTelefone(getTelefone().getTelefone()));
            selecionado.setTelefones(Util.adicionarObjetoEmLista(selecionado.getTelefones(), getTelefone()));
            getTelefone().setPessoa(selecionado);
            telefone = new Telefone();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }

    }

    public void validarTelefone() {
        Util.validarCampos(getTelefone());
    }


    public void removerTelefone(Telefone tel) {
        selecionado.getTelefones().remove(tel);
    }

    public void editarTelefone(Telefone tel) {
        telefone = tel;
        telefone.setTelefone(telefone.getTelefone().replaceAll("[^0-9]", ""));
        selecionado.getTelefones().remove(tel);
    }

    public void adicionarEndereco() {
        try {
            validarEndereco();
            selecionado.setEnderecos(Util.adicionarObjetoEmLista(selecionado.getEnderecos(), getEndereco()));
            getEndereco().setPessoa(selecionado);
            endereco = new Endereco();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    public void validarEndereco() {
        Util.validarCampos(getEndereco());
    }

    public void removerEndereco(Endereco end) {
        selecionado.getEnderecos().remove(end);

    }

    public void editarEndereco(Endereco end) {
        endereco = end;
        removerEndereco(end);
    }

    public void adicionarConta() {
        try {
            validarConta();
            ajustarContaPrincipal();
            selecionado.setContas(Util.adicionarObjetoEmLista(selecionado.getContas(), new PessoaConta(selecionado, contaCorrente)));
            inicializarConta();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void ajustarContaPrincipal() {
        if (contaCorrente.getPrincipal() && !selecionado.getContas().isEmpty()) {
            for (PessoaConta pessoaConta : selecionado.getContas()) {
                pessoaConta.getContaCorrente().setPrincipal(Boolean.FALSE);
            }
        }
    }

    private void inicializarConta() {
        contaCorrente = new ContaCorrenteBancaria();
    }

    public void validarConta() {
        Util.validarCampos(contaCorrente);
    }

    public void editarConta(PessoaConta pessoaConta) {
        this.contaCorrente = pessoaConta.getContaCorrente();
        removerConta(pessoaConta);
    }

    public void removerConta(PessoaConta pessoaConta) {
        selecionado.getContas().remove(pessoaConta);
    }

    public List<SelectItem> getTipoFoneSelectItem() {
        return Util.getListSelectItem(TipoTelefone.values());
    }

    public List<SelectItem> getTipoEnderecoSelectItem() {
        return Util.getListSelectItem(TipoEndereco.values());
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public ContaCorrenteBancaria getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrenteBancaria contaCorrente) {
        this.contaCorrente = contaCorrente;
    }
}
