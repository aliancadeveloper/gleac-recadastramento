package br.com.gleac.controlador;

import br.com.gleac.entidade.certificado.ModeloDocumento;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.processo.Placet;
import br.com.gleac.enums.TipoModeloDocumento;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.GestaoNotCurrentException;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.*;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.view.PdfView;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by zaca on 10/10/17.
 */
@ManagedBean
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "placet-novo", pattern = "/admin/placet/novo/", viewId = "/admin/cadastros/placet/editar.xhtml"),
        @URLMapping(id = "placet-editar", pattern = "/admin/placet/editar/#{placetControlador.id}/", viewId = "/admin/cadastros/placet/editar.xhtml"),
        @URLMapping(id = "placet-ver", pattern = "/admin/placet/ver/#{placetControlador.id}/", viewId = "/admin/cadastros/placet/visualizar.xhtml"),
        @URLMapping(id = "placet-listar", pattern = "/admin/placet/listar/", viewId = "/admin/cadastros/placet/listar.xhtml")
})
public class PlacetControlador extends AbstractController<Placet>{

    @Autowired
    private PlacetRepository placetRepository;

    @Autowired
    private GestaoRepository gestaoRepository;

    @Autowired
    private LojaRepository lojaRepository;

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;

    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;

    @Autowired
    private ModeloDocumentoRepository modeloDocumentoRepository;




    public PlacetControlador() {
        super(Placet.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return placetRepository;
    }

    @URLAction(mappingId = "placet-novo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        buscarGestaoVigente();
    }

    private void buscarGestaoVigente() {
        try {
            Gestao gestao = gestaoRepository.buscarGestaoVigente();
            if (gestao != null) {
                selecionado.setGestao(gestao);
            }
        } catch (GestaoNotCurrentException gex) {
            throw gex;
        }
    }

    @URLAction(mappingId = "placet-editar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "placet-ver", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "placet-listar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        Placet placet = placetRepository.gerarPlacet(selecionado);
        FacesUtil.redirecionamentoInterno("/admin/placet/ver/" + placet.getId() + "/");
    }

    public void emitirPlacet() {
        try {
            validarDocumentoPlacet();
            gerarDocumentoPlacet();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }

    }

    private void validarDocumentoPlacet() {
        ValidacaoException ve = new ValidacaoException();
        ModeloDocumento modeloDocumento = buscarModeloDocumentoPorPlacet();
        if (modeloDocumento == null) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, configure o modelo de documento para a emissão deste documento.");
        }
        ve.lancarException();
    }

    private void gerarDocumentoPlacet() {
        ModeloDocumento modeloDocumento = buscarModeloDocumentoPorPlacet();
        String conteudo = modeloDocumentoRepository.trocarTags(modeloDocumento, selecionado);
        PdfView.geraPDF("PLACET_"+ selecionado.getNumero(), conteudo, FacesContext.getCurrentInstance());
    }

    private ModeloDocumento buscarModeloDocumentoPorPlacet() {
        return modeloDocumentoRepository.buscarModeloDocumentoPorTipo(TipoModeloDocumento.PLACET);
    }

    public List<Loja> completarLojasPorNome(String filter) {
        return lojaRepository.completarLojasByNome(filter);
    }

    public List<PessoaFisica> completarPessoaFisicaComSolicitacaoPorStatus(String filter) {
        return pessoaFisicaRepository.completarPessoaFisicaComSolicitacaoPorStatus(StatusProcesso.PAGO, filter);
    }

    public List<ProcessoFinanceiro> completarProcessoFinanceiroPorSituacao(String filter) {
        return processoFinanceiroRepository.completarProcessoFinanceiroPorSituacao(StatusProcesso.PAGO, filter);
    }
}
