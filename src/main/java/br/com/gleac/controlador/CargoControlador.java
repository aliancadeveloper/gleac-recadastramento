package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Cargo;
import br.com.gleac.enums.TipoCargo;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.CargoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 02/09/16.
 */
@ManagedBean(name = "cargoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-cargo", pattern = "/admin/cargo/novo/", viewId = "/admin/cadastros/cargo/editar.xhtml"),
    @URLMapping(id = "editar-cargo", pattern = "/admin/cargo/editar/#{cargoControlador.id}/", viewId = "/admin/cadastros/cargo/editar.xhtml"),
    @URLMapping(id = "ver-cargo", pattern = "/admin/cargo/ver/#{cargoControlador.id}/", viewId = "/admin/cadastros/cargo/visualizar.xhtml"),
    @URLMapping(id = "listar-cargo", pattern = "/admin/cargo/listar/", viewId = "/admin/cadastros/cargo/listar.xhtml")
})
public class CargoControlador extends AbstractController<Cargo> implements Serializable {

    @Autowired
    private CargoRepository repository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public CargoControlador() {
        super(Cargo.class);
    }

    @URLAction(mappingId = "novo-cargo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-cargo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-cargo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-cargo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<SelectItem> getTipoCargos(){
        return Util.getListSelectItem(TipoCargo.values());
    }

    @Override
    public void salvar() {
        try {
            validarCargo();
            super.salvar();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (CampoObrigatorioException ex) {
            FacesUtil.addOperacaoNaoPermitida(ex.getMessage());
        } catch (Exception e) {
            FacesUtil.addErrorPadrao(e);
        }

    }

    private void validarCargo() {
        ValidacaoException ve = new ValidacaoException();
        selecionado.isCamposObrigatoriosPreenchidos();
        if (isExistsCargo()) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Já existe um cargo cadastrado com essa descrição.");
        }

        ve.lancarException();
    }

    private Boolean isExistsCargo() {
        return repository.isCargoExistente(selecionado);
    }
}
