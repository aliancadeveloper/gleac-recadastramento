package br.com.gleac.controlador;


import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.DocumentoFornecedor;
import br.com.gleac.entidade.comum.Fornecedor;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.FornecedorRepository;
import br.com.gleac.negocio.PessoaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean(name = "fornecedorControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-fornecedor", pattern = "/admin/fornecedor/novo/", viewId = "/admin/cadastros/fornecedor/editar.xhtml"),
    @URLMapping(id = "editar-fornecedor", pattern = "/admin/fornecedor/editar/#{fornecedorControlador.id}/", viewId = "/admin/cadastros/fornecedor/editar.xhtml"),
    @URLMapping(id = "ver-fornecedor", pattern = "/admin/fornecedor/ver/#{fornecedorControlador.id}/", viewId = "/admin/cadastros/fornecedor/visualizar.xhtml"),
    @URLMapping(id = "listar-fornecedor", pattern = "/admin/fornecedor/listar/", viewId = "/admin/cadastros/fornecedor/listar.xhtml")
})
public class FornecedorControlador extends AbstractController<Fornecedor> implements Crud {
    @Autowired
    private FornecedorRepository fornecedorRepository;
    private DocumentoFornecedor documentoFornecedor;
    @Autowired
    private PessoaRepository pessoaRepository;
    private ConverterAutoComplete converterPessoa;

    public FornecedorControlador() {
        super(Fornecedor.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return getFornecedorRepository();
    }

    @URLAction(mappingId = "novo-fornecedor", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        setDocumentoFornecedor(new DocumentoFornecedor());
    }

    @URLAction(mappingId = "editar-fornecedor", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        documentoFornecedor = new DocumentoFornecedor();
    }

    @URLAction(mappingId = "ver-fornecedor", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
        Pessoa pessoa = pessoaRepository.recuperar(Pessoa.class, selecionado.getPessoa().getId());
        selecionado.getPessoa().setContas(pessoa.getContas());
    }

    @URLAction(mappingId = "listar-fornecedor", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        try {
            super.salvar();
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addErrorPadrao(e);
        }
    }

    public List<Pessoa> completarPessoa(String filtro) {
        return getPessoaRepository().buscarPessoa(filtro.trim(), true);
    }

    public DocumentoFornecedor getDocumentoFornecedor() {
        return documentoFornecedor;
    }

    public void setDocumentoFornecedor(DocumentoFornecedor documentoFornecedor) {
        this.documentoFornecedor = documentoFornecedor;
    }

    public void carregarFileupload(FileUploadEvent event) {
        try {
            Arquivo arq = new Arquivo();
            arq.setNome(event.getFile().getFileName());
            arq.setTipo(event.getFile().getContentType());
            arq.setArquivo(event.getFile().getContents());

            DocumentoFornecedor documentoFornecedor = new DocumentoFornecedor();
            documentoFornecedor.setArquivo(arq);
            documentoFornecedor.setFornecedor(selecionado);
            selecionado.getDocumentosFornecedor().add(documentoFornecedor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        atualizarComponentesFormulario();
    }

    private void atualizarComponentesFormulario() {
        FacesUtil.atualizaComponete("Formulario:tabela-formulario");
        FacesUtil.atualizaComponete("Formulario:bt-upload-arquivo");
    }

    public void removerDocumentoFornecedor(DocumentoFornecedor doc) {
        getSelecionado().getDocumentosFornecedor().remove(doc);
    }

    private void validarAdicionarDocumento() {
        ValidacaoException ve = new ValidacaoException();
        if (getDocumentoFornecedor().getArquivo() == null) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, insira um arquivo. ");
        }
        ve.lancarException();
    }

    public FornecedorRepository getFornecedorRepository() {
        return fornecedorRepository;
    }

    public PessoaRepository getPessoaRepository() {
        return pessoaRepository;
    }

    public ConverterAutoComplete getConverterPessoa() {
        if (converterPessoa == null) {
            converterPessoa = new ConverterAutoComplete(Pessoa.class, getPessoaRepository());
        }
        return converterPessoa;
    }
}
