package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.Identificador;
import br.com.gleac.negocio.IdentificadorRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renato on 13/10/2018.
 */
@ManagedBean(name = "identificadorControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-identificador", pattern = "/admin/identificador/novo/", viewId = "/admin/cadastros/identificador/editar.xhtml"),
    @URLMapping(id = "editar-identificador", pattern = "/admin/identificador/editar/#{identificadorControlador.id}/", viewId = "/admin/cadastros/identificador/editar.xhtml"),
    @URLMapping(id = "ver-identificador", pattern = "/admin/identificador/ver/#{identificadorControlador.id}/", viewId = "/admin/cadastros/identificador/visualizar.xhtml"),
    @URLMapping(id = "listar-identificador", pattern = "/admin/identificador/listar/", viewId = "/admin/cadastros/identificador/listar.xhtml")
})
public class IdentificadorControlador extends AbstractController<Identificador> implements Serializable {

    @Autowired
    private IdentificadorRepository repository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public IdentificadorControlador() {
        super(Identificador.class);
    }

    @URLAction(mappingId = "novo-identificador", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-identificador", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-identificador", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-identificador", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }
}
