package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.ParecerSolicitacaoProcesso;
import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.processo.SolicitacaoProcessoFormulario;
import br.com.gleac.enums.processo.StatusParecer;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.ArquivoRepository;
import br.com.gleac.negocio.GestaoRepository;
import br.com.gleac.negocio.ParecerSolicitacaoProcessoRepository;
import br.com.gleac.negocio.SolicitacaoProcessoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by zaca.
 */
@ManagedBean(name = "parecerSolicitacaoProcessoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-parecer",
        pattern = "/admin/parecer-solicitacao-processo/novo/",
        viewId = "/admin/cadastros/parecer/editar.xhtml"),
    @URLMapping(id = "editar-parecer",
        pattern = "/admin/parecer-solicitacao-processo/editar/#{parecerSolicitacaoProcessoControlador.id}/",
        viewId = "/admin/cadastros/parecer/editar.xhtml"),
    @URLMapping(id = "ver-parecer",
        pattern = "/admin/parecer-solicitacao-processo/ver/#{parecerSolicitacaoProcessoControlador.id}/",
        viewId = "/admin/cadastros/parecer/visualizar.xhtml"),
    @URLMapping(id = "listar-parecer",
        pattern = "/admin/parecer-solicitacao-processo/listar/",
        viewId = "/admin/cadastros/parecer/listar.xhtml"),
    @URLMapping(id = "novo-parecer-solicitacao",
        pattern = "/admin/parecer-solicitacao-processo/nova-por-solicitacao/#{parecerSolicitacaoProcessoControlador.id}/",
        viewId = "/admin/cadastros/parecer/editar.xhtml"),
})
public class ParecerSolicitacaoProcessoControlador extends AbstractController<ParecerSolicitacaoProcesso> {

    @Autowired
    private ParecerSolicitacaoProcessoRepository parecerSolicitacaoProcessoRepository;
    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private ArquivoRepository arquivoRepository;
    @Autowired
    private GestaoRepository gestaoRepository;

    private List<SolicitacaoProcessoFormulario> documentosParecer;

    private ConverterAutoComplete converterSolicitacaoProcesso;

    private Boolean gerarProcessoFinanceiro;

    @Override
    public AbstractRepository getRepository() {
        return getParecerSolicitacaoProcessoRepository();
    }

    public ParecerSolicitacaoProcessoControlador() {
        super(ParecerSolicitacaoProcesso.class);
    }


    public ParecerSolicitacaoProcessoRepository getParecerSolicitacaoProcessoRepository() {
        return parecerSolicitacaoProcessoRepository;
    }

    @URLAction(mappingId = "novo-parecer", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        super.novo();
        setDocumentosParecer(Lists.newArrayList());
    }

    @URLAction(mappingId = "novo-parecer-solicitacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novoPorSolicitacao() {
        super.novo();
        SolicitacaoProcesso sol = solicitacaoProcessoRepository.recuperar(SolicitacaoProcesso.class, getId());
        if (sol != null) {
            selecionado.setSolicitacaoProcesso(sol);
            setDocumentosParecer(sol.getSolicitacaoProcessoFormularios());
        }
    }

    @URLAction(mappingId = "editar-parecer", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void editar() {
        super.editar();
        SolicitacaoProcesso solicitacaoProcesso = buscarSolicitacaoProcesso();
        setDocumentosParecer(solicitacaoProcesso.getSolicitacaoProcessoFormularios());
    }

    @URLAction(mappingId = "ver-parecer", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-parecer", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        try {
            if (isGerarProcessoFinanceiro()) {
                ProcessoFinanceiro processoFinanceiro = parecerSolicitacaoProcessoRepository.salvarNovoGerandoProcesso(selecionado);
                if (processoFinanceiro != null) {
                    FacesUtil.redirecionamentoInterno(processoFinanceiro.getCaminhoPadrao() + "editar/" + processoFinanceiro.getId() + "/");
                }
            } else {
                super.salvar();
            }
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ve) {
            FacesUtil.addOperacaoNaoPermitida(ve.getMessage());
        }
    }

    public Boolean isGerarProcessoFinanceiro() {
        if (gerarProcessoFinanceiro == null) {
            Gestao gestao = gestaoRepository.buscarGestaoVigente();
            gerarProcessoFinanceiro = gestao.getPermitirProcessoFinanceiro();
        }
        return gerarProcessoFinanceiro;
    }

    public List<SelectItem> getStatusDoParecer() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(StatusParecer.values());
    }

    public List<SolicitacaoProcesso> completarSolicitacaoProcesso(String filtro) {
        return getSolicitacaoProcessoRepository().completarSolicitacaoProcessoPorDescricaoOrNumero(filtro, StatusProcesso.ABERTO);
    }

    public void carregarDocumentosToConferencia() {
        try {
            SolicitacaoProcesso solicitacaoProcesso = buscarSolicitacaoProcesso();
            if (!solicitacaoProcesso.getSolicitacaoProcessoFormularios().isEmpty()) {
                solicitacaoProcesso.getSolicitacaoProcessoFormularios().forEach(buscarDocumentosToConferencia());
            }

            FacesUtil.atualizaComponete("Formulario");

        } catch (Exception e) {
            FacesUtil.addErrorGenerico(e);
        }
    }

    public StreamedContent realizarDownloadDocumentoParecer(SolicitacaoProcessoFormulario form) {
        if (form.getArquivo().getId() != null) {
            form.setArquivo(arquivoRepository.recuperar(Arquivo.class, form.getArquivo().getId()));
        }
        StreamedContent s = null;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        for (byte a : form.getArquivo().getArquivo()) {
            buffer.write(a);
        }
        InputStream is = new ByteArrayInputStream(buffer.toByteArray());

        s = new DefaultStreamedContent(is, form.getArquivo().getTipo(), form.getArquivo().getNome());
        return s;
    }

    private Consumer<SolicitacaoProcessoFormulario> buscarDocumentosToConferencia() {
        return form -> getDocumentosParecer().add(form);
    }

    private SolicitacaoProcesso buscarSolicitacaoProcesso() {
        return getSolicitacaoProcessoRepository().recuperar(SolicitacaoProcesso.class, getSelecionado().getSolicitacaoProcesso().getId());
    }


    public SolicitacaoProcessoRepository getSolicitacaoProcessoRepository() {
        return solicitacaoProcessoRepository;
    }

    public ConverterAutoComplete getConverterSolicitacaoProcesso() {
        if (converterSolicitacaoProcesso == null) {
            return new ConverterAutoComplete(SolicitacaoProcesso.class, getSolicitacaoProcessoRepository());
        }
        return converterSolicitacaoProcesso;
    }

    public void setConverterSolicitacaoProcesso(ConverterAutoComplete converterSolicitacaoProcesso) {
        this.converterSolicitacaoProcesso = converterSolicitacaoProcesso;
    }

    public List<SolicitacaoProcessoFormulario> getDocumentosParecer() {
        return documentosParecer;
    }

    public void setDocumentosParecer(List<SolicitacaoProcessoFormulario> documentosParecer) {
        this.documentosParecer = documentosParecer;
    }

    public ArquivoRepository getArquivoRepository() {
        return arquivoRepository;
    }
}
