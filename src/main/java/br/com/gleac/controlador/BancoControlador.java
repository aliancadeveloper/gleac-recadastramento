package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.Banco;
import br.com.gleac.negocio.BancoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "bancoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-banco", pattern = "/admin/banco/novo/", viewId = "/admin/financeiro/banco/editar.xhtml"),
        @URLMapping(id = "editar-banco", pattern = "/admin/banco/editar/#{bancoControlador.id}/", viewId = "/admin/financeiro/banco/editar.xhtml"),
        @URLMapping(id = "listar-banco", pattern = "/admin/banco/listar/", viewId = "/admin/financeiro/banco/listar.xhtml"),
        @URLMapping(id = "ver-banco", pattern = "/admin/banco/ver/#{bancoControlador.id}/", viewId = "/admin/financeiro/banco/visualizar.xhtml")
})
public class BancoControlador extends AbstractController<Banco> implements Serializable {

    @Autowired
    private BancoRepository bancoRepository;


    @Override
    public AbstractRepository getRepository() {
        return bancoRepository;
    }

    public BancoControlador() {
        super(Banco.class);
    }

    @URLAction(mappingId = "listar-banco", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-banco", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-banco", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-banco", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

}
