package br.com.gleac.controlador;


import br.com.gleac.entidade.mail.ConfiguracaoEmail;
import br.com.gleac.enums.TipoCriptografica;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.negocio.ConfiguracaoEmailRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.EmailUtil;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "configuracaoEmailControlador")
@ViewScoped
@URLMapping(id = "novo-configuracao", pattern = "/admin/configuracao/email/", viewId = "/admin/cadastros/configuracao-email/editar.xhtml")
public class ConfiguracaoEmailControlador extends AbstractController<ConfiguracaoEmail> implements Serializable {

    @Autowired
    private ConfiguracaoEmailRepository configuracaoEmailFacade;
    private ConfiguracaoEmail configuracaoEmail;
    private String destinatario;
    private TipoCriptografica tipoCriptografica;

    @Override
    public AbstractRepository getRepository() {
        return configuracaoEmailFacade;
    }

    public ConfiguracaoEmailControlador() {
        injetarDependenciasSpring();
    }

//    public final void injetarDependenciasSpring() {
//        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
//        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
//    }

    @URLAction(mappingId = "novo-configuracao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        configuracaoEmail = configuracaoEmailFacade.recuperarConfiguracaoAtual();
        if (configuracaoEmail == null) {
            configuracaoEmail = new ConfiguracaoEmail();
        } else {
            if(configuracaoEmail.isSsl()){
                tipoCriptografica = TipoCriptografica.SSL;
            }else if (configuracaoEmail.getTsl()){
                tipoCriptografica = TipoCriptografica.TSL;
            }else{
                tipoCriptografica = TipoCriptografica.NENHUM;
            }
        }
    }

    //GETTERS E SETTERS
    public ConfiguracaoEmail getConfiguracaoEmail() {
        return configuracaoEmail;
    }

    public void setConfiguracaoEmail(ConfiguracaoEmail configuracaoEmail) {
        this.configuracaoEmail = configuracaoEmail;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public TipoCriptografica getTipoCriptografica() {
        return tipoCriptografica;
    }

    public void setTipoCriptografica(TipoCriptografica tipoCriptografica) {
        this.tipoCriptografica = tipoCriptografica;
    }

    public List<SelectItem> getTiposCriptografia() {
        List<SelectItem> retorno = new ArrayList<SelectItem>();
        for (TipoCriptografica tipo : TipoCriptografica.values()) {
            retorno.add(new SelectItem(tipo, tipo.getDescricao()));
        }
        return Util.ordenaSelectItem(retorno);
    }

    /*
     * METODOS UTILIZADOS NA VIEW
     */
    public void enviarEmailTeste() {
        try {
            EmailUtil.enviaEmail(configuracaoEmail, destinatario);
            FacesUtil.addInfo("E-mail enviado com sucesso!", "E-mail enviado ao destinatário " + destinatario);
        } catch (Exception e) {
            FacesUtil.addError("Não foi possível enviar o E-mail!", e.getMessage());
        }
    }

    public void salvar() {
        try {
            if (configuracaoEmail.getId() == null) {
                configuracaoEmailFacade.salvarNovo(configuracaoEmail);
                FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.configuracaoEmail.toString() + " foi Inserido com sucesso!");
            } else {
                configuracaoEmailFacade.salvar(configuracaoEmail);
                FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.configuracaoEmail.toString() + " foi Alterado com sucesso!");
            }

        } catch (RuntimeException ex) {
            if (ex instanceof ExceptionGenerica) {
                FacesUtil.addError(ex.getMessage(), ex.getMessage());
            } else {
                FacesUtil.addErrorPadrao(ex);
            }
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesUtil.addErrorPadrao(ex);
            return;
        }
        navegarEmbora();
    }

    public void navegarEmbora() {
        FacesUtil.redirecionamentoInterno("/admin/home");
    }

    public void setarCriptografia() {
        if (tipoCriptografica.equals(TipoCriptografica.SSL)) {
            configuracaoEmail.setSsl(true);
            configuracaoEmail.setTsl(false);
        } else if (tipoCriptografica.equals(TipoCriptografica.TSL)) {
            configuracaoEmail.setTsl(true);
            configuracaoEmail.setSsl(false);
        } else {
            configuracaoEmail.setTsl(false);
            configuracaoEmail.setSsl(false);
        }
    }
}
