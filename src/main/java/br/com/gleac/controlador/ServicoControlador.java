package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.enums.TipoServico;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.ServicoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.util.List;

/**
 * Created by carlos.moreno on 06/08/2017.
 */
@ManagedBean(name = "servicoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-servico", pattern = "/admin/servico/novo/", viewId = "/admin/cadastros/servico/editar.xhtml"),
    @URLMapping(id = "editar-servico", pattern = "/admin/servico/editar/#{servicoControlador.id}/", viewId = "/admin/cadastros/servico/editar.xhtml"),
    @URLMapping(id = "ver-servico", pattern = "/admin/servico/ver/#{servicoControlador.id}/", viewId = "/admin/cadastros/servico/visualizar.xhtml"),
    @URLMapping(id = "listar-servico", pattern = "/admin/servico/listar/", viewId = "/admin/cadastros/servico/listar.xhtml")
})
public class ServicoControlador extends AbstractController<Servico> implements Crud {


    @Autowired
    private ServicoRepository servicoRepository;

    @Override
    public AbstractRepository getRepository() {
        return getServicoRepository();
    }

    public ServicoControlador() {
        super(Servico.class);
    }

    @URLAction(mappingId = "novo-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-servico", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void filtrar() {
        super.filtrar();
    }

    public List<SelectItem> getTiposServicos() {
        return Util.getListSelectItem(TipoServico.values(), false);
    }

    public ServicoRepository getServicoRepository() {
        return servicoRepository;
    }
}
