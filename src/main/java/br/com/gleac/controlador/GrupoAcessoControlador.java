package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.seguranca.*;
import br.com.gleac.enums.GrupoAcessoUsuario;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.GrupoAcessoRepository;
import br.com.gleac.negocio.RecursoSistemaRepository;
import br.com.gleac.negocio.UsuarioRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 11/05/16.
 */
@ManagedBean(name = "grupoAcessoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-grupo-acesso",
                pattern = "/admin/grupo-acesso/novo/",
                viewId = "/admin/seguranca/grupo-acesso/editar.xhtml"),
        @URLMapping(id = "editar-grupo-acesso",
                pattern = "/admin/grupo-acesso/editar/#{grupoAcessoControlador.id}/",
                viewId = "/admin/seguranca/grupo-acesso/editar.xhtml"),
        @URLMapping(id = "ver-grupo-acesso",
                pattern = "/admin/grupo-acesso/ver/#{grupoAcessoControlador.id}/",
                viewId = "/admin/seguranca/grupo-acesso/visualizar.xhtml"),
        @URLMapping(id = "listar-grupo-acesso",
                pattern = "/admin/grupo-acesso/listar/",
                viewId = "/admin/seguranca/grupo-acesso/listar.xhtml")
})
public class GrupoAcessoControlador extends AbstractController<GrupoAcesso> implements Serializable, Crud {

    @Autowired
    private GrupoAcessoRepository repository;
    @Autowired
    private RecursoSistemaRepository recursoSistemaRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;


    private Usuario usuario;
    private Recurso recurso;


    private ConverterAutoComplete converterRecursoSistema;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }


    public GrupoAcessoControlador() {
        super(GrupoAcesso.class);
        injetarDependenciasSpring();
    }



    @URLAction(mappingId = "novo-grupo-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        getSelecionado().setRecursos(Lists.newArrayList());
        setRecurso(new Recurso());
    }

    @Override
    public void salvar() {
        try {
            validarGrupoAcesso();
            super.salvar();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }

    }

    private void validarGrupoAcesso() {
        ValidacaoException ve = new ValidacaoException();
        if (ObjectUtils.isEmpty(selecionado.getRecursos())) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe ao menos um recurso.");
        }

        ve.lancarException();
    }

    @URLAction(mappingId = "editar-grupo-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        setRecurso(new Recurso());
    }

    @URLAction(mappingId = "ver-grupo-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    @URLAction(mappingId = "listar-grupo-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<SelectItem> getGrupos() {
        return Util.getListSelectItem(GrupoAcessoUsuario.values());
    }



    public void adicionarRecurso() {
        try {
            validarRecurso();
            getRecurso().setGrupoAcesso(selecionado);
            getSelecionado().setRecursos(Util.adicionarObjetoEmLista(selecionado.getRecursos(), getRecurso()));
            setRecurso(new Recurso());
            getRecurso().setRecursoSistema(new RecursoSistema());
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    private void validarRecurso() {
        ValidacaoException ve = new ValidacaoException();

        if (ObjectUtils.isEmpty(getRecurso())) {
            ve.adicionarMensagemDeCampoObrigatorio("O campo recurso deve ser informado.");
        }

        if (!ObjectUtils.isEmpty(getRecurso())) {
            if (!ObjectUtils.isEmpty(selecionado.getRecursos())) {
                for (Recurso rec : selecionado.getRecursos()) {
                    if (rec.getRecursoSistema().equals(recurso.getRecursoSistema())) {
                        ve.adicionarMensagemDeOperacaoNaoPermitida("O recurso já foi adicionado.");
                        break;
                    }
                }

            }

        }

        ve.lancarException();
    }

    public void removerRecurso (Recurso recurso) {
        getSelecionado().getRecursos().remove(recurso);
    }


    public List<RecursoSistema> completarRecursoSistema(String filter) {
        return recursoSistemaRepository.completarRecursoSistemaPorDescricao(filter);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Recurso getRecurso() {
        return recurso;
    }

    public void setRecurso(Recurso recurso) {
        this.recurso = recurso;
    }

    public ConverterAutoComplete getConverterRecursoSistema() {
        if (converterRecursoSistema == null) {
            converterRecursoSistema = new ConverterAutoComplete(RecursoSistema.class, recursoSistemaRepository);
        }
        return converterRecursoSistema;
    }

    public void setConverterRecursoSistema(ConverterAutoComplete converterRecursoSistema) {
        this.converterRecursoSistema = converterRecursoSistema;
    }
}
