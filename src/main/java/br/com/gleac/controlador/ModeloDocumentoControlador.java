package br.com.gleac.controlador;


import br.com.gleac.entidade.auxiliares.TagDocumento;
import br.com.gleac.entidade.certificado.ModeloDocumento;
import br.com.gleac.enums.TagDocumentoManual;
import br.com.gleac.enums.TagPlacet;
import br.com.gleac.enums.TipoModeloDocumento;
import br.com.gleac.negocio.GestaoRepository;
import br.com.gleac.negocio.ModeloDocumentoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "modeloDocumentoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-modelo-documento", pattern = "/admin/modelo-documento/novo/", viewId = "/admin/cadastros/modelo-documento/editar.xhtml"),
        @URLMapping(id = "editar-modelo-documento", pattern = "/admin/modelo-documento/editar/#{modeloDocumentoControlador.id}/", viewId = "/admin/cadastros/modelo-documento/editar.xhtml"),
        @URLMapping(id = "listar-modelo-documento", pattern = "/admin/modelo-documento/listar/", viewId = "/admin/cadastros/modelo-documento/listar.xhtml"),
        @URLMapping(id = "ver-modelo-documento", pattern = "/admin/modelo-documento/ver/#{modeloDocumentoControlador.id}/", viewId = "/admin/cadastros/modelo-documento/visualizar.xhtml")
})
public class ModeloDocumentoControlador extends AbstractController<ModeloDocumento> implements Serializable {

    @Autowired
    private ModeloDocumentoRepository modeloDocumentoRepository;
    @Autowired
    private GestaoRepository gestaoRepository;


    @Override
    public AbstractRepository getRepository() {
        return modeloDocumentoRepository;
    }

    public ModeloDocumentoControlador() {
        super(ModeloDocumento.class);
    }

    @URLAction(mappingId = "listar-modelo-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-modelo-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-modelo-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-modelo-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    public List<SelectItem> getTiposDocumentos() {
        List<SelectItem> toReturn = Lists.newArrayList();
        toReturn.add(new SelectItem(null, "Selecione um Tipo de Documento"));
        for (TipoModeloDocumento tipoModeloDocumento : TipoModeloDocumento.values()) {
            toReturn.add(new SelectItem(tipoModeloDocumento, tipoModeloDocumento.getDescricao()));
        }

        return toReturn;
    }

    public List<TagDocumento> getTags() {
        List<TagDocumento> tags = Lists.newArrayList();
        if (selecionado.getTipoModeloDocumento() != null) {

//            tags.addAll(getTagsManual());
            tags.addAll(getTagsPorTipo(selecionado.getTipoModeloDocumento()));

        }
        return tags;
    }

    private List<TagDocumento> getTagsPorTipo(TipoModeloDocumento tipo) {
        List<TagDocumento> tags = Lists.newArrayList();
        if (isTipoPlacet(tipo)) {
            getAllTagsPlacet(tags);
        }

        return tags;
    }

    private boolean isTipoPlacet(TipoModeloDocumento tipo) {
        try {
            return TipoModeloDocumento.PLACET.equals(tipo);
        } catch (Exception e) {
            return false;
        }
    }

    private void getAllTagsPlacet(List<TagDocumento> tags) {
        for (TagPlacet tagPlacet : TagPlacet.values()) {
            tags.add(new TagDocumento(tagPlacet.getDescricao(), "$" + tagPlacet.name()));
        }
    }

    private TagDocumento getTagSerenissimo() {
        return new TagDocumento("Sereníssimo", "$SERENISSIMO");
    }

    public List<TagDocumento> getTagsManual() {
        List<TagDocumento> tags = Lists.newArrayList();
        for (TagDocumentoManual tagDocumentoManual : TagDocumentoManual.values()) {
            tags.add(new TagDocumento(tagDocumentoManual.getDescricao(), "$" + tagDocumentoManual.name()));
        }
        return tags;
    }

}
