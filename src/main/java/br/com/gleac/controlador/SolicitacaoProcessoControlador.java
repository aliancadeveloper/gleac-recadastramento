package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.*;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.entidade.processo.SolicitacaoProcessoFormulario;
import br.com.gleac.entidade.processo.SolicitacaoProcessoProduto;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.OperacaoCondicaoSql;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.enums.processo.TipoPagamento;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.exception.FormularioNotFoundException;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.*;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import br.com.gleac.util.view.ParametroView;
import br.com.gleac.util.view.View;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

/**
 * Created by israeleriston on 26/06/17.
 */
@ManagedBean(name = "solicitacaoProcessoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-solicitacao-processo", pattern = "/admin/solicitacao-processo/novo/", viewId = "/admin/cadastros/solicitacao-processo/editar.xhtml"),
    @URLMapping(id = "editar-solicitacao-processo", pattern = "/admin/solicitacao-processo/editar/#{solicitacaoProcessoControlador.id}/", viewId = "/admin/cadastros/solicitacao-processo/editar.xhtml"),
    @URLMapping(id = "visualizar-solicitacao-processo", pattern = "/admin/solicitacao-processo/ver/#{solicitacaoProcessoControlador.id}/", viewId = "/admin/cadastros/solicitacao-processo/visualizar.xhtml"),
    @URLMapping(id = "listar-solicitacao-processo", pattern = "/admin/solicitacao-processo/listar/", viewId = "/admin/cadastros/solicitacao-processo/listar.xhtml")
})
public class SolicitacaoProcessoControlador extends AbstractController<SolicitacaoProcesso> implements Serializable {

    @Autowired
    private SolicitacaoProcessoRepository solicitacaoProcessoRepository;
    @Autowired
    private MaconRepository maconRepository;
    @Autowired
    private DepartamentoRepository departamentoRepository;
    @Autowired
    private ServicoRepository servicoRepository;
    @Autowired
    private ProdutoRepository produtoRepository;
    @Autowired
    private ConfiguracaoServicoRepository configuracaoServicoRepository;
    @Autowired
    private FormularioRepository formularioRepository;
    @Autowired
    private PessoaRepository pessoaRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private LojaRepository lojaRepository;


    private ConverterAutoComplete converterDepartamento;
    private ConverterAutoComplete converterPessoa;
    private ConverterAutoComplete converterProduto;
    private ConverterAutoComplete converterServico;
    private ConverterAutoComplete converterFormulario;

    private SolicitacaoProcessoProduto solicitacaoProcessoProduto;
    private SolicitacaoProcessoFormulario solicitacaoProcessoFormulario;
    private List<Formulario> formularios;

    @Override
    public AbstractRepository getRepository() {
        return getSolicitacaoProcessoRepository();
    }

    public SolicitacaoProcessoRepository getSolicitacaoProcessoRepository() {
        return solicitacaoProcessoRepository;
    }

    @Override
    public List<SolicitacaoProcesso> completarEstaEntidade(String parte) {
        return solicitacaoProcessoRepository.completarSolicitacaoProcessoPorDescricaoOrNumero(parte);
    }

    public SolicitacaoProcessoControlador() {
        super(SolicitacaoProcesso.class);
    }

    @URLAction(mappingId = "novo-solicitacao-processo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        getSelecionado().setSolicitadoEm(new Date());
        setSolicitacaoProcessoProduto(new SolicitacaoProcessoProduto());
        getSelecionado().setTipoSolicitacao(TipoSolicitacao.SERVICO);
        getSelecionado().setStatusProcesso(StatusProcesso.ABERTO);
        setSolicitacaoProcessoFormulario(new SolicitacaoProcessoFormulario());
        setFormularios(Lists.newArrayList());
    }

    @URLAction(mappingId = "editar-solicitacao-processo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        setSolicitacaoProcessoProduto(new SolicitacaoProcessoProduto());
        setSolicitacaoProcessoFormulario(new SolicitacaoProcessoFormulario());
        setFormularios(Lists.newArrayList());
    }

    @URLAction(mappingId = "visualizar-solicitacao-processo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-solicitacao-processo", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        try {
            validarSelecionado();
            super.salvar();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception e) {
            FacesUtil.addErrorPadrao(e);
        }
    }

    private void validarSelecionado() {
        ValidacaoException ve = new ValidacaoException();
        if (selecionado.getTipoSolicitacao().equals(TipoSolicitacao.SERVICO)) {
            if (selecionado.getSolicitacaoProcessoFormularios().isEmpty()) {
                ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe os formulários correspondentes para esse serviço");
            }
        }
        if (selecionado.getTipoSolicitacao().equals(TipoSolicitacao.PRODUTO)) {
            if (selecionado.getSolicitacaoProdutos().isEmpty()) {
                ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, informe os produtos");
            }
        }

        ve.lancarException();
    }

    public List<Departamento> completarDepartamento(String filtro) {
        return getDepartamentoRepository().buscarTodosDepartamentos(filtro, sistemaService.getLojaCorrente());
    }

    /**
     * @param filtro
     * @return lista de pessoas
     */
    public List<Pessoa> completarPessoa(String filtro) {
        try {
            return completarPessoaConformeSolicitacaoProcesso(filtro);
        } catch (ValidacaoException ex) {
            ex.printStackTrace();
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addErrorGenerico(e);
        }
        return Lists.newArrayList();
    }

    private List<Pessoa> completarPessoaConformeSolicitacaoProcesso(String filtro) {
        /**
         * @verify tipo da solicitação
         */
        if (selecionado.isSolicitacaoServico()) {
            /**
             * @valid validar se existe serviço informado
             */
            validarServicoToSolicitacaoProcesso();
            return pessoaRepository.buscarPessoa(filtro, selecionado.getServico().isServicoComum());
        }

        /**
         * @verify tipo da solicitação produto
         */
        return pessoaRepository.buscarPessoa(filtro, true);
    }

    /**
     * @validation validação para solicitar o maçom apenas quando for informado o serviço correspondente
     */
    private void validarServicoToSolicitacaoProcesso() {
        ValidacaoException ve = new ValidacaoException();
        if (getSelecionado().getServico() == null) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor informe o serviço para solicitar o processo. ");
        }

        ve.lancarException();
    }

    public List<Produto> completarProduto(String filtro) {
        return getProdutoRepository().completarProdutoPorNome(filtro);
    }

    public List<Servico> completarServico(String filtro) {
        return getConfiguracaoServicoRepository().completarServicosWithConfiguracaoByDescricaoAndTipo(filtro, null);
    }

    public List<SelectItem> getSolicitacaoProcessoFormularios() {
        List<SelectItem> items = Lists.newArrayList();
        items.add(new SelectItem(null, "Selecione um Formulário"));
        getFormularios().forEach(form -> items.add(new SelectItem(form, form.getDescricao())));
        return items;
    }

    public void carregarFormularios() {
        try {
            setFormularios(getFormularioRepository().buscarTodosFormulariosByServico(getSelecionado().getServico(), new Date()));
        } catch (FormularioNotFoundException fne) {
            FacesUtil.addAtencao(fne.getMessage());
        }
    }

    public void carregarFileupload(FileUploadEvent event) {
        try {
            Arquivo arq = new Arquivo();
            arq.setNome(event.getFile().getFileName());
            arq.setTipo(event.getFile().getContentType());
            arq.setArquivo(event.getFile().getContents());

            getSolicitacaoProcessoFormulario().setArquivo(arq);
            getSolicitacaoProcessoFormulario().setSolicitacaoProcesso(getSelecionado());
            getFormularios().remove(getSolicitacaoProcessoFormulario().getFormulario());
            getSelecionado().getSolicitacaoProcessoFormularios().add(getSolicitacaoProcessoFormulario());
            setSolicitacaoProcessoFormulario(new SolicitacaoProcessoFormulario());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        atualizarComponentesFormulario();
    }

    private void atualizarComponentesFormulario() {
        FacesUtil.atualizaComponete("Formulario:formulario");
        FacesUtil.atualizaComponete("Formulario:tabela-formulario");
        FacesUtil.atualizaComponete("Formulario:bt-upload-arquivo");
    }

    public void removerSolicitacaoProcessoFormulario(SolicitacaoProcessoFormulario sol) {
        getSelecionado().getSolicitacaoProcessoFormularios().remove(sol);
        getFormularios().add(sol.getFormulario());
    }

    public void recarregarSolicitacao() {
        if (selecionado.getTipoSolicitacao().equals(TipoSolicitacao.PRODUTO)) {
            selecionado.setSolicitacaoProdutos(Lists.newArrayList());
        }

    }

    public List<SelectItem> getTipoSolicitacaoProcesso() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(TipoSolicitacao.values());
    }

    public List<SelectItem> getTipoPagamento() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(TipoPagamento.values());
    }

    public List<SelectItem> getStatusSolicitacaoProcesso() {
        return Util.getListSelectItem(StatusProcesso.values());
    }

    public void adicionarItemSolicitacao() {

        try {
            ValidacaoException ve = new ValidacaoException();
            if (selecionado.getTipoSolicitacao().equals(TipoSolicitacao.PRODUTO)) {
                if (this.solicitacaoProcessoProduto.getProduto() == null) {
                    ve.adicionarMensagemDeOperacaoNaoPermitida("Campo produto é obrigatório.");
                }
                if (this.solicitacaoProcessoProduto.getQuantidade() == null) {
                    ve.adicionarMensagemDeOperacaoNaoPermitida("Campo quantidade é obrigatório.");
                }
            }
            ve.lancarException();
            getSolicitacaoProcessoProduto().setSolicitacaoProcesso(selecionado);
            getSelecionado().getSolicitacaoProdutos().add(getSolicitacaoProcessoProduto());
            setSolicitacaoProcessoProduto(new SolicitacaoProcessoProduto());
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception e) {
            FacesUtil.addErrorPadrao(e);
        }

    }

    public void removerItemSolicitacao(SolicitacaoProcessoProduto pro) {
        getSelecionado().getSolicitacaoProdutos().remove(pro);

    }

    public ConverterAutoComplete getConverterDepartamento() {
        if (converterDepartamento == null) {
            converterDepartamento = new ConverterAutoComplete(Departamento.class, getDepartamentoRepository());
        }
        return converterDepartamento;
    }

    public void setConverterDepartamento(ConverterAutoComplete converterDepartamento) {
        this.converterDepartamento = converterDepartamento;
    }

    public ConverterAutoComplete getConverterPessoa() {
        if (converterPessoa == null) {
            converterPessoa = new ConverterAutoComplete(Pessoa.class, pessoaRepository);
        }
        return converterPessoa;
    }


    public MaconRepository getMaconRepository() {
        return maconRepository;
    }

    public DepartamentoRepository getDepartamentoRepository() {
        return departamentoRepository;
    }

    public ConverterAutoComplete getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterAutoComplete(Produto.class, getProdutoRepository());
        }
        return converterProduto;
    }

    public void setConverterProduto(ConverterAutoComplete converterProduto) {
        this.converterProduto = converterProduto;
    }

    public ConverterAutoComplete getConverterServico() {
        if (converterServico == null) {
            converterServico = new ConverterAutoComplete(Servico.class, getServicoRepository());
        }
        return converterServico;
    }

    public void setConverterServico(ConverterAutoComplete converterServico) {
        this.converterServico = converterServico;
    }

    public SolicitacaoProcessoProduto getSolicitacaoProcessoProduto() {
        return solicitacaoProcessoProduto;
    }

    public void setSolicitacaoProcessoProduto(SolicitacaoProcessoProduto solicitacaoProcessoProduto) {
        this.solicitacaoProcessoProduto = solicitacaoProcessoProduto;
    }

    public ServicoRepository getServicoRepository() {
        return servicoRepository;
    }

    public ProdutoRepository getProdutoRepository() {
        return produtoRepository;
    }

    public ConfiguracaoServicoRepository getConfiguracaoServicoRepository() {
        return configuracaoServicoRepository;
    }

    public SolicitacaoProcessoFormulario getSolicitacaoProcessoFormulario() {
        return solicitacaoProcessoFormulario;
    }

    public void setSolicitacaoProcessoFormulario(SolicitacaoProcessoFormulario solicitacaoProcessoFormulario) {
        this.solicitacaoProcessoFormulario = solicitacaoProcessoFormulario;
    }

    public ConverterAutoComplete getConverterFormulario() {
        if (converterFormulario == null) {
            converterFormulario = new ConverterAutoComplete(Formulario.class, getFormularioRepository());
        }
        return converterFormulario;
    }

    public void setConverterFormulario(ConverterAutoComplete converterFormulario) {
        this.converterFormulario = converterFormulario;
    }

    public FormularioRepository getFormularioRepository() {
        return formularioRepository;
    }

    public void setFormularios(List<Formulario> formularios) {
        this.formularios = formularios;
    }

    public List<Formulario> getFormularios() {
        return formularios;
    }


    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    @Override
    public void filtrar() {
        if (sistemaService.getLojaCorrente().equals(getLojaRepository().buscarLojaGestora())) {
            super.filtrar();
        } else {
            View view = getView();
            view.setNomeRelatorio("Relatório de " + Persistencia.getNomeEntidade(classe));
            view.setColunas(Lists.newArrayList());
            String sql = Persistencia.adicionarColunas(view, "obj", classe);
            String sqlSelect = " select obj.id, " + sql.substring(0, sql.length() - 2) + "  from " + classe.getSimpleName() + " obj " +
                " inner join departamento dep on obj.departamento_id = dep.id" +
                " inner join loja on dep.loja_id = loja.id " +
                " where 1=1 ";

            for (Field field : Persistencia.getAtributosPesquisaveis(classe)) {
                criarParametroView(view, field);
            }

            Loja loja = sistemaService.getLojaCorrente();

            ParametroView parametroView = new ParametroView("Loja", "loja.id", loja.getId(), OperacaoCondicaoSql.IGUAL, null, Loja.class);
            view.getParametros().add(parametroView);

            view.setSqlRecuperadorObjetos(sqlSelect);
            view.setSqlOrdenar(" order by obj.id desc");
            view.setSqlContadorObjetos(" select count(obj.id)  from " + classe.getSimpleName() + " obj " +
                " inner join departamento dep on obj.departamento_id = dep.id" +
                " inner join loja on dep.loja_id = loja.id " +
                " where 1=1 ");

            view = super.getViewRepository().recuperarObjetos(view);
            view.setTotalDeRegistrosExistentes(null);
        }
    }

    @Override
    public void excluir() {
        if (selecionado.getStatusProcesso().name().equals(StatusProcesso.ABERTO.name())) {
            super.excluir();
        } else {
            FacesUtil.addOperacaoNaoPermitida("A Solicitação de Processo não está mais 'Em Aberto'!");
        }
    }
}
