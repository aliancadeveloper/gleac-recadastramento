package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Conjuge;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.negocio.ConjugeRepository;
import br.com.gleac.negocio.MaconRepository;
import br.com.gleac.negocio.PessoaFisicaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by zaca on 05/09/16.
 */
@ManagedBean(name = "conjugeControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-conjuge", pattern = "/admin/conjuge/novo/", viewId = "/admin/cadastros/conjuge/editar.xhtml"),
        @URLMapping(id = "editar-conjuge", pattern = "/admin/conjuge/editar/#{conjugeControlador.id}/", viewId = "/admin/cadastros/conjuge/editar.xhtml"),
        @URLMapping(id = "ver-conjuge", pattern = "/admin/conjuge/ver/#{conjugeControlador.id}/", viewId = "/admin/cadastros/conjuge/visualizar.xhtml"),
        @URLMapping(id = "listar-conjuge", pattern = "/admin/conjuge/listar/", viewId = "/admin/cadastros/conjuge/listar.xhtml")
})
public class ConjugeControlador extends AbstractController<Conjuge> implements Serializable{

    @Autowired
    private ConjugeRepository repository;

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;

    @Autowired
    private MaconRepository maconRepository;

    private ConverterAutoComplete converterMacon;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public ConjugeControlador() {
        super(Conjuge.class);
    }


    @URLAction(mappingId = "novo-conjuge", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-conjuge", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-conjuge", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-conjuge", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();

    }

    public List<Macon> completarMacon(String filter){
        return maconRepository.completarMaconAtivoPorNome(filter);
    }

    public List<PessoaFisica> completarPessoaFisica(String filter) {
        return pessoaFisicaRepository.buscarPessoaFisicaPorNome(filter);
    }

    public ConverterAutoComplete getConverterMacon() {
        if (converterMacon == null)
            converterMacon = new ConverterAutoComplete(Macon.class, maconRepository);
        return converterMacon;
    }

    public void setConverterMacon(ConverterAutoComplete converterMacon) {
        this.converterMacon = converterMacon;
    }
}
