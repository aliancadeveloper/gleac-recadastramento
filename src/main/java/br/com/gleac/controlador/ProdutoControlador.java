package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.ProdutoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Created by carlos.moreno on 06/08/2017.
 */
@ManagedBean(name = "produtoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-produto", pattern = "/admin/produto/novo/", viewId = "/admin/cadastros/produto/editar.xhtml"),
    @URLMapping(id = "editar-produto", pattern = "/admin/produto/editar/#{produtoControlador.id}/", viewId = "/admin/cadastros/produto/editar.xhtml"),
    @URLMapping(id = "ver-produto", pattern = "/admin/produto/ver/#{produtoControlador.id}/", viewId = "/admin/cadastros/produto/visualizar.xhtml"),
    @URLMapping(id = "listar-produto", pattern = "/admin/produto/listar/", viewId = "/admin/cadastros/produto/listar.xhtml")
})
public class ProdutoControlador extends AbstractController<Produto> implements Crud {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    public AbstractRepository getRepository() {
        return getProdutoRepository();
    }

    public ProdutoControlador() {
        super(Produto.class);
    }

    public ProdutoRepository getProdutoRepository() {
        return produtoRepository;
    }

    @URLAction(mappingId = "novo-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-produto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void filtrar() {
        super.filtrar();
    }
}
