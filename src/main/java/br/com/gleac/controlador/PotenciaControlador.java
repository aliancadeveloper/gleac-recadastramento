package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Potencia;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.PotenciaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


/**
 * Created by zaca on 06/10/16.
 */
@ManagedBean(name = "potenciaControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-potencia", pattern = "/admin/potencia/novo/", viewId = "/admin/cadastros/potencia/editar.xhtml"),
        @URLMapping(id = "editar-potencia", pattern = "/admin/potencia/editar/#{potenciaControlador.id}/", viewId = "/admin/cadastros/potencia/editar.xhtml"),
        @URLMapping(id = "ver-potencia", pattern = "/admin/potencia/ver/#{potenciaControlador.id}/", viewId = "/admin/cadastros/potencia/visualizar.xhtml"),
        @URLMapping(id = "listar-potencia", pattern = "/admin/potencia/listar/", viewId = "/admin/cadastros/potencia/listar.xhtml")
})
public class PotenciaControlador extends AbstractController<Potencia> implements Crud{


    @Autowired
    private PotenciaRepository repository;


    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public PotenciaControlador() {
        super(Potencia.class);
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/potencia/";
    }

    @URLAction(mappingId = "novo-potencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-potencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-potencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-potencia", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void filtrar() {
        super.filtrar();
    }
}
