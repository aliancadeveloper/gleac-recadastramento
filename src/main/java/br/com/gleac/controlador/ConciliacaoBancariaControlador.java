package br.com.gleac.controlador;

import br.com.gleac.entidade.auxiliares.MovimentoConciliacaoBancaria;
import br.com.gleac.entidade.financeiro.*;
import br.com.gleac.enums.SummaryMessages;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.ConciliacaoBancariaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "conciliacaoBancariaControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "nova-conciliacao", pattern = "/admin/conciliacao/novo/", viewId = "/admin/financeiro/conciliacao/editar.xhtml"),
    @URLMapping(id = "editar-conciliacao", pattern = "/admin/conciliacao/editar/#{conciliacaoBancariaControlador.id}/", viewId = "/admin/financeiro/conciliacao/editar.xhtml"),
    @URLMapping(id = "listar-conciliacao", pattern = "/admin/conciliacao/listar/", viewId = "/admin/financeiro/conciliacao/listar.xhtml"),
    @URLMapping(id = "ver-conciliacao", pattern = "/admin/conciliacao/ver/#{conciliacaoBancariaControlador.id}/", viewId = "/admin/financeiro/conciliacao/visualizar.xhtml")
})
public class ConciliacaoBancariaControlador extends AbstractController<ConciliacaoBancaria> implements Serializable {

    @Autowired
    private ConciliacaoBancariaRepository repository;
    private SaldoBancario saldoBancario;
    private List<SaldoBancario> historicoSaldoBancario;
    private Identificador identificador;
    private Boolean cadastrarNovoIdentificador;
    private List<MovimentoConciliacaoBancaria> movimentosConciliacao;
    private List<MovimentoConciliacaoBancaria> movimentosConciliacaoSelecionado;


    public ConciliacaoBancariaControlador() {
        super(ConciliacaoBancaria.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    @URLAction(mappingId = "listar-conciliacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "nova-conciliacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        selecionado.setUsuario(repository.getSistemaService().getUsuarioCorrente());
        historicoSaldoBancario = Lists.newArrayList();
        movimentosConciliacao = Lists.newArrayList();
        movimentosConciliacaoSelecionado = Lists.newArrayList();
    }

    @URLAction(mappingId = "editar-conciliacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        historicoSaldoBancario = Lists.newArrayList();
        movimentosConciliacao = Lists.newArrayList();
        movimentosConciliacaoSelecionado = Lists.newArrayList();
        atualizarSaldo();
    }

    @URLAction(mappingId = "ver-conciliacao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    private void validarRegrasEspecificas() {
        Util.validarCampos(selecionado);

        ValidacaoException ve = new ValidacaoException();
        if (movimentosConciliacaoSelecionado == null || movimentosConciliacaoSelecionado.isEmpty()) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Selecione um movimento para conciliar.");
        }
        ve.lancarException();
    }

    @Override
    public void salvar() {
        try {
            validarRegrasEspecificas();
            repository.concluirConciliacao(selecionado, movimentosConciliacaoSelecionado);
            FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Conciliação Bancária foi concluída com sucesso.");
            navegarEmbora();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ve) {
            FacesUtil.addErrorPadrao(ve);
        }
    }

    public void instanciarIdentificador() {
        if (cadastrarNovoIdentificador) {
            identificador = new Identificador();
        }
    }

    public void salvarIdentificador() {
        try {
            identificador.setData(new Date());
            Util.validarCampos(identificador);
            repository.getIdentificadorRepository().salvarNovo(identificador);
            this.identificador = repository.getIdentificadorRepository().recuperar(Identificador.class, identificador.getId());
            this.cadastrarNovoIdentificador = false;
            FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Identificador salvo com sucesso.");
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ve) {
            FacesUtil.addErrorPadrao(ve);
        }
    }

    public void identificarMovimentos() {
        try {
            if (identificador == null) {
                throw new ExceptionGenerica("Campo Identificador deve ser informado");
            }
            for (MovimentoConciliacaoBancaria movimento : movimentosConciliacaoSelecionado) {
                movimento.setIdentificador(identificador);
            }
            FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Movimentos identificados com sucesso.");
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ve) {
            FacesUtil.addErrorPadrao(ve);
        }
    }

    public void conciliarMovimentos() {
        try {
            validarConciliarMovimentos();
            adicionarMovimentosSelecionados();
            FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Movimentos conciliados com sucesso.");
            FacesUtil.addAtencao(" Para finalizar a conciliação, clique no botão 'Concluir Conciliação'.");
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ve) {
            FacesUtil.addErrorPadrao(ve);
        }
    }

    private void validarConciliarMovimentos() {
        ValidacaoException ve = new ValidacaoException();
        if (movimentosConciliacaoSelecionado.isEmpty()) {
            ve.adicionarMensagemDeOperacaoNaoPermitida("Selecione ao menos um movimento para ser conciliado.");
        }
        ve.lancarException();
    }

    public boolean hasLancamentoSelcionado() {
        return !movimentosConciliacaoSelecionado.isEmpty();
    }

    public Boolean mostrarBotaoSelecionarTodos() {
        try {
            return movimentosConciliacao.size() != movimentosConciliacaoSelecionado.size();
        } catch (NullPointerException ex) {
            return Boolean.FALSE;
        }
    }

    public void desmarcarTodos() {
        movimentosConciliacaoSelecionado.clear();
    }

    public Boolean itemSelecionado(MovimentoConciliacaoBancaria obj) {
        return movimentosConciliacaoSelecionado.contains(obj);
    }

    public void desmarcarItem(MovimentoConciliacaoBancaria obj) {
        movimentosConciliacaoSelecionado.remove(obj);
    }

    public void selecionarItem(MovimentoConciliacaoBancaria obj) {
        movimentosConciliacaoSelecionado.add(obj);
    }

    public void selecionarTodos() {
        try {
            desmarcarTodos();
            movimentosConciliacaoSelecionado.addAll(movimentosConciliacao);
        } catch (Exception e) {
            getLogger().error(e.getMessage());
        }
    }

    public void estornarConcilacao(MovimentoConciliacaoBancaria movimento) {
        movimento.setDataConciliacao(null);
        if (movimento.getTipoMovimentoConciliacao().isReceita()) {
            Receita receita = repository.getReceitaRepository().recuperarSemDependencia(movimento.getIdMovimento());
            repository.getReceitaRepository().estornarConciliacao(receita);
        } else {
            Pagamento despesa = repository.getPagamentoRepository().recuperarSemDependencia(movimento.getIdMovimento());
            repository.getPagamentoRepository().estornarConciliacao(despesa);
        }
        movimentosConciliacaoSelecionado.remove(movimento);
    }

    public void salvarSaldo() {
        try {
            if (saldoBancario.getId() == null) {
                repository.getSaldoBancarioRepository().salvarNovo(saldoBancario);
                FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Saldo foi salvo com sucesso.");
            } else {
                repository.getSaldoBancarioRepository().salvar(saldoBancario);
                FacesUtil.addInfo(SummaryMessages.OPERACAO_REALIZADA.getDescricao(), " Saldo foi atualizado com sucesso.");
            }
            atualizarSaldo();
        } catch (Exception ve) {
            getLogger().error(ve.getMessage());
        }
    }

    private void buscarHistoricoSaldo() {
        if (selecionado.getDataConciliacao() != null && selecionado.getContaCorrenteBancaria() != null) {
            historicoSaldoBancario = repository.getSaldoBancarioRepository().buscarUltimosSaldoBancarioPorContaCorrente(
                selecionado.getDataLancamento(), selecionado.getContaCorrenteBancaria());
        }
    }

    public void recuperarUltimoSaldo() {
        if (selecionado.getContaCorrenteBancaria() != null && selecionado.getDataLancamento() != null) {
            SaldoBancario saldoBancario = getUltimoSaldoBancario();
            if (saldoBancario == null) {
                novoSaldo();
            } else {
                this.saldoBancario = saldoBancario;
            }
        }
        buscarHistoricoSaldo();
        buscarMovimentosConciliacao();
    }

    private SaldoBancario getUltimoSaldoBancario() {
        return repository.getSaldoBancarioRepository().recuperarUltimoSaldo(selecionado.getDataLancamento(), selecionado.getContaCorrenteBancaria());
    }

    public BigDecimal getValorTotalCreditoAConciliar() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            for (MovimentoConciliacaoBancaria mov : getMovimentosConciliacao()) {
                if (mov.getTipoMovimentoConciliacao().isReceita() && mov.getDataConciliacao() == null) {
                    total = total.add(mov.getCredito());
                }
            }
        }
        return total;
    }

    public BigDecimal getValorTotalCreditoConciliado() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            for (MovimentoConciliacaoBancaria mov : getMovimentosConciliacao()) {
                if (mov.getTipoMovimentoConciliacao().isReceita() && mov.getDataConciliacao() != null) {
                    total = total.add(mov.getCredito());

                }
            }
        }
        return total;
    }

    public BigDecimal getValorTotalDebitoAConciliar() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            for (MovimentoConciliacaoBancaria mov : getMovimentosConciliacao()) {
                if (mov.getTipoMovimentoConciliacao().isDespesa() && mov.getDataConciliacao() == null) {
                    total = total.add(mov.getDebito());
                }
            }
        }
        return total;
    }

    public BigDecimal getValorTotalDebitoConciliado() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            for (MovimentoConciliacaoBancaria mov : getMovimentosConciliacao()) {
                if (mov.getTipoMovimentoConciliacao().isDespesa() && mov.getDataConciliacao() != null) {
                    total = total.add(mov.getDebito());
                }
            }
        }
        return total;
    }

    public BigDecimal getValorTotalGeralAConciliar() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            total = total.add(getValorTotalCreditoAConciliar().subtract(getValorTotalDebitoAConciliar()));
        }
        return total;
    }

    public BigDecimal getValorTotalGeralConciliado() {
        BigDecimal total = BigDecimal.ZERO;
        if (getMovimentosConciliacao() != null) {
            total = total.add(getValorTotalCreditoConciliado().subtract(getValorTotalDebitoConciliado()));
        }
        return total;
    }

    public BigDecimal getSaldoBancarioDoDia() {
        BigDecimal total = BigDecimal.ZERO;
        if (selecionado.getContaCorrenteBancaria() != null && selecionado.getDataLancamento() != null) {
            SaldoBancario saldoBancario = getUltimoSaldoBancario();
            if (saldoBancario != null) {
                total = saldoBancario.getSaldo();
            }
        }
        return total;
    }

    public boolean isMovimentosConcilados() {
        return getSaldoBancarioDoDia().compareTo(getValorTotalGeralConciliado()) == 0;
    }

    public boolean isMovimentosAConciliarConciliados() {
        return getValorTotalGeralAConciliar().compareTo(BigDecimal.ZERO) == 0;
    }


    public void novoSaldo() {
        this.saldoBancario = new SaldoBancario();
        this.saldoBancario.setDataSaldo(selecionado.getDataConciliacao());
        this.saldoBancario.setContaCorrenteBancaria(selecionado.getContaCorrenteBancaria());
    }

    public void atualizarSaldo() {
        recuperarUltimoSaldo();
        buscarHistoricoSaldo();
    }

    public void alterarSaldoBancario(SaldoBancario saldo) {
        this.saldoBancario = saldo;
    }

    public void removerSaldoBancario(SaldoBancario saldoBancario) {
        repository.getSaldoBancarioRepository().excluir(saldoBancario);
        buscarHistoricoSaldo();
        recuperarUltimoSaldo();
    }

    private void buscarMovimentosConciliacao() {
        if (selecionado.getContaCorrenteBancaria() != null && selecionado.getDataConciliacao() != null) {
            List<MovimentoConciliacaoBancaria> movimentos = repository.buscarMovimentosConciliacao(
                selecionado.getContaCorrenteBancaria(),
                selecionado.getDataLancamento());
            if (!movimentos.isEmpty()) {
                movimentosConciliacao = movimentos;
            } else {
                FacesUtil.addAtencao("Não existe movimentos para serem conciliados na data: " + Util.sdf.format(selecionado.getDataLancamento()));
            }
        }
    }

    public void adicionarMovimentosSelecionados() {
        try {
            validarRegrasEspecificas();
            conciliarMovimento();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    private void conciliarMovimento() {
        for (MovimentoConciliacaoBancaria movimento : movimentosConciliacaoSelecionado) {
            if (movimento.getTipoMovimentoConciliacao() == null) {
                throw new CampoObrigatorioException("Tipo de Movimento da Conciliação não recuperado para conciliar.");
            }
            switch (movimento.getTipoMovimentoConciliacao()) {
                case RECEITA:
                    movimento.setDataConciliacao(selecionado.getDataConciliacao());
                    break;
                case DESPESA:
                    movimento.setDataConciliacao(selecionado.getDataConciliacao());
                    break;
            }
        }
    }

    public boolean renderizarPanelConciliacao() {
        return selecionado.getDataConciliacao() != null && selecionado.getContaCorrenteBancaria() != null;
    }

    public SaldoBancario getSaldoBancario() {
        return saldoBancario;
    }

    public void setSaldoBancario(SaldoBancario saldoBancario) {
        this.saldoBancario = saldoBancario;
    }

    public List<SaldoBancario> getHistoricoSaldoBancario() {
        return historicoSaldoBancario;
    }

    public void setHistoricoSaldoBancario(List<SaldoBancario> historicoSaldoBancario) {
        this.historicoSaldoBancario = historicoSaldoBancario;
    }

    public List<MovimentoConciliacaoBancaria> getMovimentosConciliacaoSelecionado() {
        return movimentosConciliacaoSelecionado;
    }

    public void setMovimentosConciliacaoSelecionado(List<MovimentoConciliacaoBancaria> movimentosConciliacaoSelecionado) {
        this.movimentosConciliacaoSelecionado = movimentosConciliacaoSelecionado;
    }

    public List<MovimentoConciliacaoBancaria> getMovimentosConciliacao() {
        return movimentosConciliacao;
    }

    public void setMovimentosConciliacao(List<MovimentoConciliacaoBancaria> movimentosConciliacao) {
        this.movimentosConciliacao = movimentosConciliacao;
    }

    public Identificador getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Identificador identificador) {
        this.identificador = identificador;
    }

    public Boolean getCadastrarNovoIdentificador() {
        return cadastrarNovoIdentificador;
    }

    public void setCadastrarNovoIdentificador(Boolean cadastrarNovoIdentificador) {
        this.cadastrarNovoIdentificador = cadastrarNovoIdentificador;
    }
}
