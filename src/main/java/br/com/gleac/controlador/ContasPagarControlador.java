package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.ContasPagar;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.negocio.ContasPagarRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "contasPagarControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-contas-pagar", pattern = "/admin/contas-pagar/novo/", viewId = "/admin/financeiro/contas-pagar/editar.xhtml"),
    @URLMapping(id = "editar-contas-pagar", pattern = "/admin/contas-pagar/editar/#{contasPagarControlador.id}/", viewId = "/admin/financeiro/contas-pagar/editar.xhtml"),
    @URLMapping(id = "listar-contas-pagar", pattern = "/admin/contas-pagar/listar/", viewId = "/admin/financeiro/contas-pagar/listar.xhtml"),
    @URLMapping(id = "ver-contas-pagar", pattern = "/admin/contas-pagar/ver/#{contasPagarControlador.id}/", viewId = "/admin/financeiro/contas-pagar/visualizar.xhtml")
})
public class ContasPagarControlador extends AbstractController<ContasPagar> implements Serializable {

    @Autowired
    private ContasPagarRepository contasPagarRepository;

    @Override
    public AbstractRepository getRepository() {
        return contasPagarRepository;
    }

    public ContasPagarControlador() {
        super(ContasPagar.class);
    }

    @URLAction(mappingId = "listar-contas-pagar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-contas-pagar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-contas-pagar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-contas-pagar", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    public List<SelectItem> getTipos() {
        return Util.getListSelectItem(TipoContaPagar.values());
    }


    public void estornar() {
        selecionado.setSituacaoContaPagar(SituacaoContaPagar.ESTORNADA);
        getRepository().salvar(selecionado);
        navegarEmbora();
        FacesUtil.addOperacaoRealizada("Registro foi estornado.");
    }



    public void pagar() {
        FacesUtil.redirecionamentoInterno("/admin/pagamento/conta/" + selecionado.getId() + "/");
    }

    public boolean isRegistroEditavel() {
        return !selecionado.getSituacaoContaPagar().isAberta();
    }
}
