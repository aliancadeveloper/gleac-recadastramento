package br.com.gleac.controlador;

import br.com.gleac.entidade.auxiliares.FiltroBoleto;
import br.com.gleac.entidade.financeiro.Boleto;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.negocio.ContasReceberRepository;
import br.com.gleac.negocio.LancamentoFinanceiroRepository;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.ProcessoFinanceiroRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 06/08/2018.
 */
@ManagedBean(name = "boletoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-boleto", pattern = "/admin/boleto/novo/", viewId = "/admin/financeiro/boleto/editar.xhtml"),

})
public class BoletoControlador implements Serializable {

    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private ProcessoFinanceiroRepository processoFinanceiroRepository;
    private FiltroBoleto filtroBoleto;
    private List<ContasReceber> contasRecebersComBoleto;

    private List<ContasReceber> boletoSelecionados;
    private String urlCarne;

    public BoletoControlador() {
        injetarDependenciasSpring();
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    @URLAction(mappingId = "novo-boleto", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        instanciarCampos();
    }

    public void instanciarCampos() {
        limpar();
    }

    public List<SelectItem> getSituacoes() {
        return Util.getListSelectItem(SituacaoContaPagar.values());
    }

    public void recuperarLancamentos() {
        try {
            contasRecebersComBoleto = contasReceberRepository.recuperarContasReceberAbertaComBoleto(filtroBoleto);
        } catch (Exception ex) {
            FacesUtil.addErrorOperacaoNaoPermitida(ex.getMessage());
        }
    }

    public void limpar() {
        contasRecebersComBoleto = Lists.newArrayList();
        this.filtroBoleto = new FiltroBoleto();

    }

    public List<ContasReceber> getContasRecebersComBoleto() {
        return contasRecebersComBoleto;
    }

    public void setContasRecebersComBoleto(List<ContasReceber> contasRecebersComBoleto) {
        this.contasRecebersComBoleto = contasRecebersComBoleto;
    }

    public FiltroBoleto getFiltroBoleto() {
        return filtroBoleto;
    }

    public void setFiltroBoleto(FiltroBoleto filtroBoleto) {
        this.filtroBoleto = filtroBoleto;
    }

    public void cancelarBoleto(ContasReceber conta) {
        processoFinanceiroRepository.cancelarBoleto(conta);
    }

    public void imprimirBoletosAgrupados() {
        try {
            urlCarne = null;
            if (boletoSelecionados != null) {
                List<Boleto> boletos = Lists.newArrayList();
                for (ContasReceber conta : boletoSelecionados) {
                    if (conta.getBoleto() != null) {
                        boletos.add(conta.getBoleto());
                    }
                }
                urlCarne = processoFinanceiroRepository.gerarBoletosAgrupados(boletos);
            } else {
                FacesUtil.addOperacaoNaoPermitida("É obrigatório selecionar pelo menos um boleto.");
            }
        } catch (Exception e) {
            FacesUtil.addOperacaoNaoPermitida(e.getMessage());
        }
    }


    public List<ContasReceber> getBoletoSelecionados() {
        return boletoSelecionados;
    }

    public void setBoletoSelecionados(List<ContasReceber> boletoSelecionados) {
        this.boletoSelecionados = boletoSelecionados;
    }

    public String getUrlCarne() {
        return urlCarne;
    }

    public void setUrlCarne(String urlCarne) {
        this.urlCarne = urlCarne;
    }
}
