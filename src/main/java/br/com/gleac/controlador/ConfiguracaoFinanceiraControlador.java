package br.com.gleac.controlador;

import br.com.gleac.entidade.financeiro.ConfiguracaoFinanceira;
import br.com.gleac.negocio.ConfiguracaoFinanceiraRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renato on 30/09/17.
 */
@ManagedBean(name = "configuracaoFinanceiraControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-configuracao-financeira", pattern = "/admin/configuracao-financeira/novo/", viewId = "/admin/financeiro/configuracao-financeira/editar.xhtml"),
    @URLMapping(id = "editar-configuracao-financeira", pattern = "/admin/configuracao-financeira/editar/#{configuracaoFinanceiraControlador.id}/", viewId = "/admin/financeiro/configuracao-financeira/editar.xhtml"),
    @URLMapping(id = "ver-configuracao-financeira", pattern = "/admin/configuracao-financeira/ver/#{configuracaoFinanceiraControlador.id}/", viewId = "/admin/financeiro/configuracao-financeira/visualizar.xhtml"),
    @URLMapping(id = "listar-configuracao-financeira", pattern = "/admin/configuracao-financeira/listar/", viewId = "/admin/financeiro/configuracao-financeira/listar.xhtml")
})
public class ConfiguracaoFinanceiraControlador extends AbstractController<ConfiguracaoFinanceira> implements Serializable {

    @Autowired
    private ConfiguracaoFinanceiraRepository repository;


    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public ConfiguracaoFinanceiraControlador() {
        super(ConfiguracaoFinanceira.class);
    }

    @URLAction(mappingId = "novo-configuracao-financeira", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        ConfiguracaoFinanceira configuracaoFinanceira = repository.recuperarConfiguracao();
        if(configuracaoFinanceira != null){
            selecionado = configuracaoFinanceira;
        }
    }

    @URLAction(mappingId = "editar-configuracao-financeira", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-configuracao-financeira", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-configuracao-financeira", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }
}
