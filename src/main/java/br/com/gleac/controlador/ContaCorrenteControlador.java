package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.negocio.ContaCorrenteBancariaRepository;
import br.com.gleac.negocio.ContaCorrenteRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "contaCorrenteControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-contaCorrente", pattern = "/admin/conta-corrente/novo/", viewId = "/admin/financeiro/conta-corrente/editar.xhtml"),
    @URLMapping(id = "editar-contaCorrente", pattern = "/admin/conta-corrente/editar/#{contaCorrenteControlador.id}/", viewId = "/admin/financeiro/conta-corrente/editar.xhtml"),
    @URLMapping(id = "listar-contaCorrente", pattern = "/admin/conta-corrente/listar/", viewId = "/admin/financeiro/conta-corrente/listar.xhtml"),
    @URLMapping(id = "ver-contaCorrente", pattern = "/admin/conta-corrente/ver/#{contaCorrenteControlador.id}/", viewId = "/admin/financeiro/conta-corrente/visualizar.xhtml")
})
public class ContaCorrenteControlador extends AbstractController<ContaCorrenteBancaria> implements Serializable {

    @Autowired
    private ContaCorrenteBancariaRepository contaCorrenteBancariaRepository;
    @Autowired
    private ContaCorrenteRepository contaCorrenteRepository;
    private ConverterAutoComplete converterContaCorrente;

    @Override
    public AbstractRepository getRepository() {
        return contaCorrenteBancariaRepository;
    }

    public ContaCorrenteControlador() {
        super(ContaCorrenteBancaria.class);
    }

    @URLAction(mappingId = "listar-contaCorrente", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-contaCorrente", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-contaCorrente", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-contaCorrente", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    public ConverterAutoComplete getConverterContaCorrente() {
        if (converterContaCorrente == null) {
            converterContaCorrente = new ConverterAutoComplete(ContaCorrente.class, contaCorrenteRepository);
        }
        return converterContaCorrente;
    }
}
