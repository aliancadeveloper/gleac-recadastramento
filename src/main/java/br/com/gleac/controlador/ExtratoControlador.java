package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.LojaConta;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.entidade.financeiro.ContasPagar;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.SaldoFinanceiro;
import br.com.gleac.interfaces.ISaldoFinanceiro;
import br.com.gleac.negocio.*;
import br.com.gleac.service.SistemaService;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "extratoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-extrato", pattern = "/admin/extrato/novo/", viewId = "/admin/financeiro/extrato/editar.xhtml"),

})
public class ExtratoControlador implements Serializable {

    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ContasPagarRepository contasPagarRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;
    @Autowired
    private SistemaService sistemaService;
    @Autowired
    private ContaCorrenteRepository contaCorrenteRepository;
    private ConverterAutoComplete converterContaCorrente;
    private List<ContaCorrente> contas;
    private ContaCorrente contaCorrente;
    private Date dataInicial;
    private Date dataFinal;
    private Loja loja;
    private List<ISaldoFinanceiro> lancamentos;
    private List<ContasPagar> contasAPagar;
    private List<ContasReceber> contasReceber;


    public ExtratoControlador() {
        injetarDependenciasSpring();
    }

    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }

    @URLAction(mappingId = "novo-extrato", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        instanciarCampos();
    }

    public void instanciarCampos() {
        lancamentos = Lists.newArrayList();
        contasAPagar = Lists.newArrayList();
        contasReceber = Lists.newArrayList();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        dataInicial = new Date(c.getTime().getYear(), 0, 1);
        dataFinal = new Date(c.getTime().getYear(), 11, 31);
        contas = Lists.newArrayList();
        contaCorrente = null;
        if (sistemaService.getLojaCorrente() != null) {
            loja = sistemaService.getLojaCorrente();
            recuperarLoja();
        }
    }

    public void recuperarLoja() {
        if (loja != null) {
            loja = lojaRepository.recuperar(Loja.class, loja.getId());
            this.contas = Lists.newArrayList();
            for (LojaConta conta : loja.getContas()) {
                contas.add(conta.getContaCorrente());
            }
        }
    }


    public void recuperarLancamentos() {
        lancamentos = lancamentoFinanceiroRepository.recuperarLancamentos(loja, contaCorrente, dataInicial, dataFinal);
        contasAPagar = contasPagarRepository.recuperarLancamentos(loja, dataInicial, dataFinal);
        contasReceber = contasReceberRepository.recuperarLancamentos(loja, dataInicial, dataFinal);

    }


    public BigDecimal getSaldoFinal() {
        try {
            return getSaldoConta().add(getSomaContaReceber()).subtract(getSomaContaPagar());
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public String getDescricaoTipo1() {
        return "Contas a Pagar";
    }

    public String getDescricaoTipo2() {
        return "Contas a Receber";
    }

    public BigDecimal getSaldoConta() {
        try {
            SaldoFinanceiro ultimoSaldo = lancamentoFinanceiroRepository.getUltimoSaldoPorDataLojaConta(new SaldoFinanceiro(loja, contaCorrente, BigDecimal.ZERO, dataFinal));
            return ultimoSaldo.getSaldo();
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getSomaContaPagar() {
        try {
            BigDecimal retorno = BigDecimal.ZERO;
            for (ContasPagar contasPagar : contasAPagar) {
                retorno = retorno.add(contasPagar.getValor());
            }
            return retorno;
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getSomaContaReceber() {
        try {
            BigDecimal retorno = BigDecimal.ZERO;
            for (ContasReceber contasReceber : this.contasReceber) {
                retorno = retorno.add(contasReceber.getValor());
            }
            return retorno;
        } catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    public List<ContaCorrente> getContas() {
        return contas;
    }

    public void setContas(List<ContaCorrente> contas) {
        this.contas = contas;
    }

    public List<SelectItem> getContasAsSelectItem() {
        List<SelectItem> retorno = Lists.newArrayList();
        retorno.add(new SelectItem(null, ""));
        if (getContas() != null) {
            for (ContaCorrente conta : getContas()) {
                retorno.add(new SelectItem(conta, conta.toString()));
            }
        }
        return retorno;
    }

    public ConverterAutoComplete getConverterContaCorrente() {
        if (converterContaCorrente == null) {
            converterContaCorrente = new ConverterAutoComplete(ContaCorrente.class, contaCorrenteRepository);
        }
        return converterContaCorrente;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public List<ISaldoFinanceiro> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(List<ISaldoFinanceiro> lancamentos) {
        this.lancamentos = lancamentos;
    }

    public List<ContasPagar> getContasAPagar() {
        return contasAPagar;
    }

    public void setContasAPagar(List<ContasPagar> contasAPagar) {
        this.contasAPagar = contasAPagar;
    }

    public List<ContasReceber> getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(List<ContasReceber> contasReceber) {
        this.contasReceber = contasReceber;
    }
}
