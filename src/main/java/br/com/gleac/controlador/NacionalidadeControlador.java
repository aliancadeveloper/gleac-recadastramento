package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Nacionalidade;
import br.com.gleac.negocio.NacionalidadeRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

/**
 * Created by israeleriston on 02/09/16.
 */
@ManagedBean(name = "nacionalidadeControlador")
@ViewScoped
@URLMappings( mappings = {
    @URLMapping(id = "novo-nacionalidade", pattern = "/admin/nacionalidade/novo/", viewId = "/admin/cadastros/nacionalidade/editar.xhtml"),
    @URLMapping(id = "editar-nacionalidade", pattern = "/admin/nacionalidade/editar/#{nacionalidadeControlador.id}/", viewId = "/admin/cadastros/nacionalidade/editar.xhtml"),
    @URLMapping(id = "ver-nacionalidade", pattern = "/admin/nacionalidade/ver/#{nacionalidadeControlador.id}/", viewId = "/admin/cadastros/nacionalidade/visualizar.xhtml"),
    @URLMapping(id = "listar-nacionalidade", pattern = "/admin/nacionalidade/listar/", viewId = "/admin/cadastros/nacionalidade/listar.xhtml"),
})
public class NacionalidadeControlador extends AbstractController<Nacionalidade> {

    @Inject
    private NacionalidadeRepository repository;


    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public NacionalidadeControlador() {
        super(Nacionalidade.class);
    }

    @URLAction(mappingId = "novo-nacionalidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-nacionalidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-nacionalidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-nacionalidade", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        super.salvar();
    }
}
