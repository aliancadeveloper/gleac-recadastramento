package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.estoque.EntradaEstoque;
import br.com.gleac.entidade.estoque.ItemEntradaEstoque;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.EntradaEstoqueRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renato on 08/07/2017.
 */
@ManagedBean(name = "entradaEstoqueControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-entradaEstoque", pattern = "/admin/entrada-estoque/novo/", viewId = "/admin/estoque/entrada-estoque/editar.xhtml"),
    @URLMapping(id = "editar-entradaEstoque", pattern = "/admin/entrada-estoque/editar/#{entradaEstoqueControlador.id}/", viewId = "/admin/estoque/entrada-estoque/editar.xhtml"),
    @URLMapping(id = "listar-entradaEstoque", pattern = "/admin/entrada-estoque/listar/", viewId = "/admin/estoque/entrada-estoque/listar.xhtml"),
    @URLMapping(id = "ver-entradaEstoque", pattern = "/admin/entrada-estoque/ver/#{entradaEstoqueControlador.id}/", viewId = "/admin/estoque/entrada-estoque/visualizar.xhtml")
})
public class EntradaEstoqueControlador extends AbstractController<EntradaEstoque> implements Serializable {

    @Autowired
    private EntradaEstoqueRepository entradaEstoqueRepository;
    private ItemEntradaEstoque item;
    private ConverterAutoComplete converterProduto;
    private Integer quantidadeTotal;

    @Override
    public AbstractRepository getRepository() {
        return entradaEstoqueRepository;
    }

    public EntradaEstoqueControlador() {
        super(EntradaEstoque.class);
    }

    @URLAction(mappingId = "listar-entradaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-entradaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        item = new ItemEntradaEstoque();
        setQuantidadeTotal(0);
    }

    @URLAction(mappingId = "editar-entradaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        item = new ItemEntradaEstoque();
    }

    @URLAction(mappingId = "ver-entradaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    public ItemEntradaEstoque getItemEntradaEstoque() {
        return item;
    }

    public void setItemEntradaEstoque(ItemEntradaEstoque item) {
        this.item = item;
    }

    public void adicionarItemEntradaEstoque() {
        try {
            if (item == null) {
                return;
            }
            item.setEntradaEstoque(selecionado);
            Util.validarCampos(item);
//            verificarAndAdicionarItensNaLista();
            selecionado.setItens(Util.adicionarObjetoEmLista(selecionado.getItens(), item));
            selecionado.somarQuantidadeTotal();
            item = new ItemEntradaEstoque();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addError(ex.getMessage(), ex.getMessage());
        }
    }

    private void verificarAndAdicionarItensNaLista() {
        if ((getQuantidadeTotal() + item.getQuantidade()) <= selecionado.getQuantidade()) {
            selecionado.setItens(Util.adicionarObjetoEmLista(selecionado.getItens(), item));
            quantidadeTotal += item.getQuantidade();
        } else {
            FacesUtil.addErrorOperacaoNaoPermitida("A soma da quantidade dos itens não deve ser maior que a " +
                "quantidade de itens da nota.");
        }
    }

    public void alterarItemEntradaEstoque(ItemEntradaEstoque item) {
        this.item = item;
    }

    public void removerItemEntradaEstoque(ItemEntradaEstoque item) {
        selecionado.getItens().remove(item);
        selecionado.somarQuantidadeTotal();
    }

    public ItemEntradaEstoque getItem() {
        return item;
    }

    public void setItem(ItemEntradaEstoque item) {
        this.item = item;
    }

    public ConverterAutoComplete getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterAutoComplete(Produto.class, entradaEstoqueRepository.getProdutoRepository());
        }
        return converterProduto;
    }

    public List<Produto> completarProduto(String filtro) {
        return entradaEstoqueRepository.getProdutoRepository().completarProdutoPorNome(filtro);
    }

    public Integer getQuantidadeTotal() {
        return quantidadeTotal;
    }

    public void setQuantidadeTotal(Integer quantidadeTotal) {
        this.quantidadeTotal = quantidadeTotal;
    }
}
