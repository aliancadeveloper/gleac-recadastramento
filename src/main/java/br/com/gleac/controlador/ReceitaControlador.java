package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContasReceber;
import br.com.gleac.entidade.financeiro.Receita;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.negocio.ContasReceberRepository;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.ReceitaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renat on 06/08/17.
 */
@ManagedBean(name = "receitaControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-receita", pattern = "/admin/receita/novo/", viewId = "/admin/financeiro/receita/editar.xhtml"),
    @URLMapping(id = "novo-receita-por-conta-receber", pattern = "/admin/receita/conta/#{receitaControlador.id}/", viewId = "/admin/financeiro/receita/editar.xhtml"),
    @URLMapping(id = "editar-receita", pattern = "/admin/receita/editar/#{receitaControlador.id}/", viewId = "/admin/financeiro/receita/editar.xhtml"),
    @URLMapping(id = "listar-receita", pattern = "/admin/receita/listar/", viewId = "/admin/financeiro/receita/listar.xhtml"),
    @URLMapping(id = "ver-receita", pattern = "/admin/receita/ver/#{receitaControlador.id}/", viewId = "/admin/financeiro/receita/visualizar.xhtml")
})
public class ReceitaControlador extends AbstractController<Receita> implements Serializable {

    @Autowired
    private ReceitaRepository receitaRepository;
    @Autowired
    private LojaRepository lojaRepository;
    @Autowired
    private ContasReceberRepository contasReceberRepository;

    @Override
    public AbstractRepository getRepository() {
        return receitaRepository;
    }

    public ReceitaControlador() {
        super(Receita.class);
    }

    @URLAction(mappingId = "listar-receita", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-receita", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        recuperarLoja();
    }


    @URLAction(mappingId = "novo-receita-por-conta-receber", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novoContaReceber() {
        super.novo();
        selecionado.setContasReceber(contasReceberRepository.recuperar(ContasReceber.class, getId()));
        ContasReceber contasReceber = selecionado.getContasReceber();
        if (contasReceber != null) {
            selecionado.setValor(contasReceber.getValor());
            selecionado.setDataLancamento(contasReceber.getDataVencimento());
            selecionado.setOrigem(contasReceber.getDescricao());
            selecionado.setLoja(contasReceber.getLoja());


        }
        recuperarLoja();
    }

    @URLAction(mappingId = "editar-receita", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        recuperarLoja();
    }

    @URLAction(mappingId = "ver-receita", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    public void recuperarLoja() {
        if (selecionado.getLoja() != null) {
            selecionado.setLoja(lojaRepository.recuperar(Loja.class, selecionado.getLoja().getId()));
        }
    }

    public List<SelectItem> getContas() {
        return lojaRepository.getContasDaLojaForSelectItem(selecionado.getLoja());
    }

    public List<SelectItem> getFormasDePagamento() {
        return Util.getListSelectItem(FormaDePagamento.values());
    }


    public Boolean mostrarBotaoExcluir() {
        return Boolean.FALSE;
    }

    @Override
    public void salvar() {
        super.salvar();
    }
}
