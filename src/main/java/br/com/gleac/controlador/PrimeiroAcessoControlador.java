package br.com.gleac.controlador;

import br.com.gleac.entidade.auxiliares.PrimeiroAcesso;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.UsuarioRepository;
import br.com.gleac.util.FacesUtil;
import com.google.common.base.Strings;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 24/05/16.
 */
@ManagedBean(name = "primeiroAcessoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-primeiro-acesso", pattern = "/public/primeiro-acesso/", viewId = "/primeiro-acesso.xhtml"),
    @URLMapping(id = "novo-senha-primeiro-acesso", pattern = "/public/primeiro-acesso/senha/#{primeiroAcessoControlador.id}", viewId = "/primeiro-senha-acesso.xhtml")
})

public class PrimeiroAcessoControlador implements Serializable {

    @Autowired
    protected UsuarioRepository usuarioRepository;
    private PrimeiroAcesso primeiroAcesso;
    private Long id;
    private Usuario usuario;


    public PrimeiroAcessoControlador() {
        injetarDependenciasSpring();
    }


    public final void injetarDependenciasSpring() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }


    @URLAction(mappingId = "novo-primeiro-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novo() {
        primeiroAcesso = new PrimeiroAcesso();
    }

    @URLAction(mappingId = "novo-senha-primeiro-acesso", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void novaSenha() {
        usuario = usuarioRepository.recuperar(Usuario.class, id);
        usuario.setSenha("");
        usuario.setConfirmacaoSenha("");

    }

    public void salvarSenhaNova() {
        try {
            if (usuario.getExpiraPrimeiroAcesso().before(new Date())) {
                throw new ExceptionGenerica("A Senha do usuário expirou! Entre em contato com o administrador do sistema.");
            }
            usuario.isSenhasIguais();
            usuario.isTamanhoSenhaPermitido();
            usuario.criptografarSenha();
            usuarioRepository.salvar(usuario);
            FacesUtil.redirecionarLogin();

        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesUtil.addOperacaoNaoRealizada(ex.getMessage());
        }

    }

    public void verificarInformacoes() {
        try {
            if (primeiroAcesso != null) {
                if (Strings.isNullOrEmpty(primeiroAcesso.getLogin().trim())) {
                    throw new CampoObrigatorioException("O Campo Nome de Usuário é obrigatório.");
                }
                if (primeiroAcesso.getCpf().trim().length() == 0) {
                    throw new CampoObrigatorioException("O Campo CPF é obrigatório.");
                }
                if (primeiroAcesso.getEmail().trim().length() == 0) {
                    throw new CampoObrigatorioException("O Campo E-mail é obrigatório.");
                }
                usuarioRepository.verificarPrimeiroAcesso(primeiroAcesso);
                usuarioRepository.verificarNomeDeUsuario(primeiroAcesso.getLogin());
                usuarioRepository.criarNovoUsuarioEnviandoEmailPrimeiroAcesso(primeiroAcesso);

                FacesUtil.addInfo("E-mail enviado com sucesso!", " Acesse seu e-mail e utilize o link para acesso ao sistema.");
                primeiroAcesso = new PrimeiroAcesso();
                FacesUtil.redirecionarLogin();
            }
        } catch (Exception e) {
            e.printStackTrace();
            FacesUtil.addOperacaoNaoPermitida(e.getMessage());
        }
    }

    public PrimeiroAcesso getPrimeiroAcesso() {
        return primeiroAcesso;
    }

    public void setPrimeiroAcesso(PrimeiroAcesso primeiroAcesso) {
        this.primeiroAcesso = primeiroAcesso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
