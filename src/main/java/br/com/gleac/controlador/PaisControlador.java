package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Pais;
import br.com.gleac.negocio.PaisRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "paisControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-pais", pattern = "/admin/pais/novo/", viewId = "/admin/cadastros/pais/editar.xhtml"),
        @URLMapping(id = "editar-pais", pattern = "/admin/pais/editar/#{paisControlador.id}/", viewId = "/admin/cadastros/pais/editar.xhtml"),
        @URLMapping(id = "listar-pais", pattern = "/admin/pais/listar/", viewId = "/admin/cadastros/pais/listar.xhtml"),
        @URLMapping(id = "ver-pais", pattern = "/admin/pais/ver/#{paisControlador.id}/", viewId = "/admin/cadastros/pais/visualizar.xhtml")
})
public class PaisControlador extends AbstractController<Pais> implements Serializable {

    @Autowired
    private PaisRepository paisRepository;


    @Override
    public AbstractRepository getRepository() {
        return paisRepository;
    }

    public PaisControlador() {
        super(Pais.class);
    }

    @URLAction(mappingId = "listar-pais", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-pais", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-pais", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-pais", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @Override
    public void salvar() {
        super.salvar();
    }


    @Override
    public void filtrar() {

        super.filtrar();
    }
}
