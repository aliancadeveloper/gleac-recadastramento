package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.DocumentoSite;
import br.com.gleac.enums.TipoDocumentoSite;
import br.com.gleac.negocio.DocumentoSiteRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carlos on 28/09/16.
 */
@ManagedBean(name = "documentoSiteControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-documento", pattern = "/admin/documento-site/novo/", viewId = "/admin/cadastros/documentosite/editar.xhtml"),
    @URLMapping(id = "editar-documento", pattern = "/admin/documento-site/editar/#{documentoSiteControlador.id}/", viewId = "/admin/cadastros/documentosite/editar.xhtml"),
    @URLMapping(id = "ver-documento", pattern = "/admin/documento-site/ver/#{documentoSiteControlador.id}/", viewId = "/admin/cadastros/documentosite/visualizar.xhtml"),
    @URLMapping(id = "listar-documento", pattern = "/admin/documento-site/listar/", viewId = "/admin/cadastros/documentosite/listar.xhtml")
})
public class DocumentoSiteControlador extends AbstractController<DocumentoSite> implements Serializable {

    @Autowired
    private DocumentoSiteRepository repository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/documento-site/";
    }

    public DocumentoSiteControlador() {
        super(DocumentoSite.class);
    }

    @URLAction(mappingId = "novo-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-documento", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<SelectItem> getTipoDocumento() {
        List<SelectItem> tipo = new ArrayList<>();
        tipo.add(new SelectItem(null, ""));
        for (TipoDocumentoSite documentoSite : TipoDocumentoSite.values()) {
            tipo.add(new SelectItem(documentoSite, documentoSite.toString()));
        }
        return Util.ordenaSelectItem(tipo);
    }

    public void handleFileUpload(FileUploadEvent event) {
        try {
            Arquivo a = new Arquivo();
            a.setNome(event.getFile().getFileName());
            a.setTipo(event.getFile().getContentType());
            a.setArquivo(event.getFile().getContents());
            selecionado.setArquivo(a);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public StreamedContent recuperarArquivoParaDownload(Arquivo arq) {
        if (arq.getId() != null) {
            arq = repository.getArquivoRepository().recuperar(Arquivo.class, arq.getId());
        }
        StreamedContent s = null;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        for (byte a : arq.getArquivo()) {
            buffer.write(a);
        }
        InputStream is = new ByteArrayInputStream(buffer.toByteArray());

        s = new DefaultStreamedContent(is, arq.getTipo(), arq.getNome());
        return s;
    }
}
