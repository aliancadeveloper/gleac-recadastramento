package br.com.gleac.controlador;

import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Entity;
import javax.servlet.ServletContext;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean
@RequestScoped
public class Web implements Serializable {

    public static final String SESSION_KEY_CONVERSATION = "objectConversation";
    public static final String SESSION_KEY_CAMINHO = "objectCaminho";


    /**
     * Creates a new instance of initControlador
     */
    public Web() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
    }


    private boolean possuiAnotacao(Class classe, Class anotacao) {
        return classe.isAnnotationPresent(anotacao);
    }

    private boolean isEntidade(Class c) {
        return possuiAnotacao(c, Entity.class);
    }

    private boolean isAtributoDeEntidade(Field atributo) {
        if (isEntidade(atributo.getType())) {
            return true;
        }

        if (atributo.getType().equals(List.class)) {
            ParameterizedType pt = (ParameterizedType) atributo.getGenericType();
            for (Type t : pt.getActualTypeArguments()) {
                if (isEntidade((Class) t)) {
                    return true;
                }
            }
        }

        return false;
    }


    private static Map<String, Object> getSessionMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    private static Stack getStackCaminhos() {
        return (Stack) getSessionMap().get(SESSION_KEY_CAMINHO);
    }

    public static void setCaminhoOrigem(String caminho) {
        if (getStackCaminhos() == null) {
            getSessionMap().put(SESSION_KEY_CAMINHO, new Stack<Object>());
        }
        getStackCaminhos().add(caminho);
    }

    public static String getCaminhoOrigem() {
        Stack<String> origens = getStackCaminhos();
        if (origens != null && !origens.isEmpty()) {
            String origem = origens.peek();
            origens.remove(origem);
            return origem;
        }
        return "";
    }

    public void navegacao(Object obj, String origem, String destino) {
        poeNaSessao(obj);
        setCaminhoOrigem(origem);
        FacesUtil.redirecionamentoInterno(destino);
    }

    public void navegacao(Object selecionado, Object parente, String origem, String destino) {
        poeNaSessao(selecionado);
        poeNaSessao(parente);
        setCaminhoOrigem(origem);
        FacesUtil.redirecionamentoInterno(destino);
    }

    public void navegacaoPadrao(String origem, String padrao, String destino) {
        setCaminhoOrigem(origem);
        FacesUtil.redirecionamentoInterno(padrao + destino);
    }

    public static void poeNaSessao(Object object) {
        if (getMapConversation() == null) {
            getSessionMap().put(SESSION_KEY_CONVERSATION, new HashMap<Class, Object>());
        }
        getMapConversation().put(object.getClass(), object);
    }

    public static Map<Class, Object> getMapConversation() {
        return (Map<Class, Object>) getSessionMap().get(SESSION_KEY_CONVERSATION);
    }

    public List<Field> getAtributosTabelaveis(Class classe) {
        return Persistencia.getAtributos(classe);
    }


    public String getNomeCampo(Field field) {
        return Persistencia.getNomeCampo(field);
    }

    public String getValorCampo(Field field, Object objeto) {
        return Persistencia.getValorCampo(field, objeto);
    }

}
