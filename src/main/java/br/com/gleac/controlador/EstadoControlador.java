package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Estado;
import br.com.gleac.entidade.comum.Pais;
import br.com.gleac.negocio.EstadoRepository;
import br.com.gleac.negocio.PaisRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 02/09/16.
 */
@ManagedBean(name = "estadoControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-estado", pattern = "/admin/estado/novo/", viewId = "/admin/cadastros/estado/editar.xhtml"),
    @URLMapping(id = "editar-estado", pattern = "/admin/estado/editar/#{estadoControlador.id}/", viewId = "/admin/cadastros/estado/editar.xhtml"),
    @URLMapping(id = "ver-estado", pattern = "/admin/estado/ver/#{estadoControlador.id}/", viewId = "/admin/cadastros/estado/visualizar.xhtml"),
    @URLMapping(id = "listar-estado", pattern = "/admin/estado/listar/", viewId = "/admin/cadastros/estado/listar.xhtml")
})
public class EstadoControlador extends AbstractController<Estado> implements Serializable {

    @Autowired
    private EstadoRepository repository;
    @Autowired
    private PaisRepository paisRepository;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    public EstadoControlador() {
        super(Estado.class);
    }

    @URLAction(mappingId = "novo-estado", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-estado", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-estado", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-estado", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @Override
    public void salvar() {
        super.salvar();
    }

    public List<Pais> completarPais(String filter){
        return paisRepository.buscarPaisPorNome(filter);
    }
}
