package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.GestaoLoja;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.GestaoRepository;
import br.com.gleac.negocio.LojaRepository;
import br.com.gleac.negocio.MaconRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by zaca on 30/09/17.
 */
@ManagedBean
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-gestao", pattern = "/admin/gestao/novo/", viewId = "/admin/cadastros/gestao/editar.xhtml"),
    @URLMapping(id = "editar-gestao", pattern = "/admin/gestao/editar/#{gestaoControlador.id}/", viewId = "/admin/cadastros/gestao/editar.xhtml"),
    @URLMapping(id = "visualizar-gestao", pattern = "/admin/gestao/ver/#{gestaoControlador.id}/", viewId = "/admin/cadastros/gestao/visualizar.xhtml"),
    @URLMapping(id = "listar-gestao", pattern = "/admin/gestao/listar/", viewId = "/admin/cadastros/gestao/listar.xhtml")
})
public class GestaoControlador extends AbstractController<Gestao> implements Serializable {

    private static String KEY_IMAGEM = "imagem-foto";

    @Autowired
    private GestaoRepository gestaoRepository;
    @Autowired
    private MaconRepository maconRepository;

    @Autowired
    private LojaRepository lojaRepository;

    private GestaoLoja gestaoLoja;

    private ConverterAutoComplete converterMacon;
    private ConverterAutoComplete converterLoja;

    @Override
    public AbstractRepository getRepository() {
        return getGestaoRepository();
    }

    public GestaoRepository getGestaoRepository() {
        return gestaoRepository;
    }

    public GestaoControlador() {
        super(Gestao.class);
    }

    @URLAction(mappingId = "novo-gestao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        colocarImagemSessao(null);
        getSelecionado().setNumeroPlacet(0L);
        setGestaoLoja(new GestaoLoja());
    }

    @URLAction(mappingId = "editar-gestao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        if (selecionado.getArquivo() != null) {
            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        }
        setGestaoLoja(new GestaoLoja());
    }

    @URLAction(mappingId = "visualizar-gestao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-gestao", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public List<Macon> completarSerenissimo(String filter) {
        return maconRepository.completarMaconAtivoPorNomeSimples(filter);
    }

    public List<Loja> completarLojaByNome(String filter) {
        return lojaRepository.completarLojasByNome(filter);
    }

    public ConverterAutoComplete getConverterMacon() {
        if (converterMacon == null) {
            converterMacon = new ConverterAutoComplete(Macon.class, getMaconRepository());
        }
        return converterMacon;
    }

    public void carregarFoto(FileUploadEvent event) {
        try {
            Arquivo arquivo = new Arquivo();
            arquivo.setTipo(event.getFile().getContentType());
            arquivo.setNome(event.getFile().getFileName());
            byte[] content = IOUtils.toByteArray(event.getFile().getInputstream());
            arquivo.setArquivo(content);
            selecionado.setArquivo(arquivo);
            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        } catch (Exception ex) {
            FacesUtil.addError("Erro ao carregar o arquivo.", ex.getMessage());
        }
    }

    public void adicionarLoja() {
        try {
            validarLojaGestao();
            getGestaoLoja().setGestao(selecionado);
            selecionado.setGestaoLojas(Util.adicionarObjetoEmLista(selecionado.getGestaoLojas(), getGestaoLoja()));
            setGestaoLoja(new GestaoLoja());
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    private void validarLojaGestao() {
        ValidacaoException ve = new ValidacaoException();
        if (getGestaoLoja().getLoja() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor, informe a Loja.");
        }

        if (getGestaoLoja().getMacon() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor, informe o venerável mestre.");
        }
        if (getGestaoLoja().getDataInicial() == null) {
            ve.adicionarMensagemDeCampoObrigatorio("Por favor, informe a data inicial de vigência");
        }


        ve.lancarException();
    }

    public void removerLojaGestao(GestaoLoja gestaoLoja) {
        selecionado.getGestaoLojas().remove(gestaoLoja);
    }

    public boolean exibirSiluetaImagem() {
        try {
            return ((StreamedContent) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_IMAGEM)).getName().trim().length() <= 0;
        } catch (Exception e) {
            return true;
        }
    }

    private void colocarImagemSessao(StreamedContent streamedContent) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(KEY_IMAGEM, streamedContent);
    }

    public void setConverterMacon(ConverterAutoComplete converterMacon) {
        this.converterMacon = converterMacon;
    }

    public MaconRepository getMaconRepository() {
        return maconRepository;
    }

    public GestaoLoja getGestaoLoja() {
        return gestaoLoja;
    }

    public void setGestaoLoja(GestaoLoja gestaoLoja) {
        this.gestaoLoja = gestaoLoja;
    }

    public LojaRepository getLojaRepository() {
        return lojaRepository;
    }

    public ConverterAutoComplete getConverterLoja() {
        if (converterLoja == null) {
            converterLoja = new ConverterAutoComplete(Loja.class, getLojaRepository());
        }
        return converterLoja;
    }
}
