package br.com.gleac.controlador;


import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.*;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.*;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.*;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by renat on 10/05/2016.
 */
@ManagedBean(name = "pessoaFisicaControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-pessoa-fisica", pattern = "/admin/pessoa-fisica/novo/", viewId = "/admin/cadastros/pessoa/fisica.xhtml"),
    @URLMapping(id = "editar-pessoa-fisica", pattern = "/admin/pessoa-fisica/editar/#{pessoaFisicaControlador.id}/", viewId = "/admin/cadastros/pessoa/fisica.xhtml"),
    @URLMapping(id = "ver-pessoa-fisica", pattern = "/admin/pessoa-fisica/ver/#{pessoaFisicaControlador.id}/", viewId = "/admin/cadastros/pessoa/visualizarfisica.xhtml"),
    @URLMapping(id = "listar-pessoa", pattern = "/admin/pessoa-fisica/listar/", viewId = "/admin/cadastros/pessoa/listar.xhtml")
})
public class PessoaFisicaControlador extends AbstractController<PessoaFisica> implements Serializable {

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;
    @Autowired
    private PessoaRepository pessoaRepository;
    @Autowired
    private CidadeRepository cidadeRepository;
    @Autowired
    private EstadoRepository estadoRepository;
    @Inject
    private NacionalidadeRepository nacionalidadeRepository;
    @Autowired
    private BairroRepository bairroRepository;
    @Autowired
    private CargoRepository cargoRepository;
    @Autowired
    private PessoaJuridicaRepository pessoaJuridicaRepository;
    @Autowired
    private ProfissaoRepository profissaoRepository;
    @Autowired
    private AreaFormacaoRepository areaFormacaoRepository;

    private ConverterAutoComplete converterNacionalidade;
    private ConverterAutoComplete converterCidade;
    private ConverterAutoComplete converterProfissao;
    private ConverterAutoComplete converterPessoa;
    private ConverterAutoComplete converterAreaFormacao;

    private Telefone telefone;
    private Endereco endereco;
    private CarteiraTrabalho carteiraTrabalho;
    private TituloEleitor tituloEleitor;
    private RG rg;
    private AreaFormacao areaFormacao;
    private Passaporte passaporte;
    private Filho filho;
    private List<CarteiraTrabalho> carteirasTrabalho;
    private Conjuge conjuge;
    private ContaCorrenteBancaria contaCorrente;

    private Estado estado;
    private Estado estadoParaEndereco;


    @Override
    public String getCaminhoPadrao() {
        return "/admin/pessoa-fisica/";
    }


    @Override
    public AbstractRepository getRepository() {
        return pessoaFisicaRepository;
    }

    public PessoaFisicaControlador() {
        super(PessoaFisica.class);
    }


    @URLAction(mappingId = "listar-pessoa", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }


    @URLAction(mappingId = "novo-pessoa-fisica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        selecionado.setTipoSanguineo(TipoSanguineo.NAO_INFORMADO);
        selecionado.setRacaCor(RacaCor.NAO_INFORMADA);
        selecionado.setEstadoCivil(EstadoCivil.CASADO);
        selecionado.setSexo(Sexo.MASCULINO);
        carregarOperacaoToSelecionado();

    }

    private void carregarOperacaoToSelecionado() {
        endereco = new Endereco();
        telefone = new Telefone();
        tituloEleitor = new TituloEleitor();
        passaporte = new Passaporte();
        rg = new RG();
        areaFormacao = new AreaFormacao();
        filho = new Filho();
        carteiraTrabalho = new CarteiraTrabalho();
        carteirasTrabalho = Lists.newArrayList();
        conjuge = new Conjuge();
        inicializarConta();
    }


    @URLAction(mappingId = "editar-pessoa-fisica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        carregarOperacaoToSelecionado();
        carregarDocumentoPessoal();
        if (selecionado.getCidade() != null) {
            estado = selecionado.getCidade().getEstado();
        }
    }


    @URLAction(mappingId = "ver-pessoa-fisica", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
        tituloEleitor = new TituloEleitor();
        passaporte = new Passaporte();
        rg = new RG();
        carteirasTrabalho = Lists.newArrayList();
        carregarDocumentoPessoal();
    }

    private void carregarDocumentoPessoal() {
        if (selecionado.getDocumentosPessoais() != null) {
            for (DocumentoPessoal documentoPessoal : selecionado.getDocumentosPessoais()) {
                if (documentoPessoal instanceof RG) {
                    setRg((RG) documentoPessoal);
                } else if (documentoPessoal instanceof Passaporte) {
                    setPassaporte((Passaporte) documentoPessoal);
                } else if (documentoPessoal instanceof TituloEleitor) {
                    setTituloEleitor((TituloEleitor) documentoPessoal);
                } else if (documentoPessoal instanceof CarteiraTrabalho) {
                    setCarteirasTrabalho(Util.adicionarObjetoEmLista(getCarteirasTrabalho(), (CarteiraTrabalho) documentoPessoal));
                }

            }

        }
    }


    // Metodos

    @Override
    public void salvar() {
        try {
            realizarValidacao();
            carregarDocumentosPessoais();
            if (selecionado.getId() == null) {
                pessoaFisicaRepository.salvarNovo(selecionado);
            } else {
                pessoaFisicaRepository.salvar(selecionado);
            }
            FacesUtil.addOperacaoRealizada("Registros Salvo Com Sucesso!");
            navegarEmbora();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesUtil.addErrorGenerico(ex);
        }

    }

    private void carregarDocumentosPessoais() {
        definirDocumentoPessoalToSelecionado();
        carregarCarteirasTrabalho();


    }

    private void carregarCarteirasTrabalho() {
        if (getCarteirasTrabalho() != null) {
            for (CarteiraTrabalho carteira : getCarteirasTrabalho()) {
                selecionado.setDocumentosPessoais(Util.adicionarObjetoEmLista(selecionado.getDocumentosPessoais(), carteira));
            }

        }
    }

    private void definirDocumentoPessoalToSelecionado() {
        getRg().setPessoaFisica(selecionado);
        getPassaporte().setPessoaFisica(selecionado);
        getTituloEleitor().setPessoaFisica(selecionado);
        selecionado.setDocumentosPessoais(Util.adicionarObjetoEmLista(selecionado.getDocumentosPessoais(), getRg()));
        selecionado.setDocumentosPessoais(Util.adicionarObjetoEmLista(selecionado.getDocumentosPessoais(), getPassaporte()));
        selecionado.setDocumentosPessoais(Util.adicionarObjetoEmLista(selecionado.getDocumentosPessoais(), getTituloEleitor()));
    }

    public List<Cargo> completarCargo(String filter) {
        return cargoRepository.buscarCargoPorDescricao(filter);
    }


    public List<PessoaFisica> completarPessoaFisica(String filter) {
        return pessoaFisicaRepository.buscarPessoaFisicaPorNome(filter);
    }

    public List<PessoaJuridica> completarPessoaJuridica(String filter) {
        return pessoaJuridicaRepository.buscarPessoaJuridica(filter);
    }

    public List<Pessoa> completarTodasPessoa(String filter) {
        return pessoaFisicaRepository.findAllPessoasByNome(filter);
    }

    public List<Profissao> completarProfissao(String filter) {
        return profissaoRepository.buscarProfissaoPorNome(filter);
    }

    public List<AreaFormacao> completarAreaFormacao(String filter) {
        return areaFormacaoRepository.buscarAreaFormacaoPorNome(filter);
    }

    public void adicionarCarteiraTrabalho() {
        getCarteiraTrabalho().setPessoaFisica(selecionado);
        setCarteirasTrabalho(Util.adicionarObjetoEmLista(getCarteirasTrabalho(), getCarteiraTrabalho()));
        setCarteiraTrabalho(new CarteiraTrabalho());
    }

    public void editarCarteiraTrabalho(CarteiraTrabalho doc) {
        setCarteiraTrabalho(doc);
        removerCarteiraTrabalho(doc);
    }

    public void removerCarteiraTrabalho(CarteiraTrabalho doc) {
        getCarteirasTrabalho().remove(doc);
    }

    public void adicionarFormacao() {
        getAreaFormacao().setPessoaFisica(selecionado);
        selecionado.setFormacoes(Util.adicionarObjetoEmLista(selecionado.getFormacoes(), getAreaFormacao()));
        setAreaFormacao(new AreaFormacao());
    }

    public void editarFormacao(AreaFormacao formacao) {
        setAreaFormacao(formacao);
        removerFormacao(formacao);
    }

    public void removerFormacao(AreaFormacao formacao) {
        selecionado.getFormacoes().remove(formacao);
    }

    public void validarFilho() {
        Util.validarCampos(getFilho());
    }

    public void validarConjuge() {
        Util.validarCampos(getConjuge());
    }

    public void adicionarFilho() {
        try {
            validarFilho();
            getFilho().setPessoaFisica(selecionado);
            selecionado.setFilhos(Util.adicionarObjetoEmLista(selecionado.getFilhos(), getFilho()));
            setFilho(new Filho());
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    public void adicionarConjuge() {
        try {
            validarConjuge();
            verificarConjugeAtivoAndAtualizarStatusCasamento();
            getConjuge().setPessoaFisica(selecionado);
            selecionado.setConjuges(Util.adicionarObjetoEmLista(selecionado.getConjuges(), getConjuge()));
            setConjuge(new Conjuge());
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void verificarConjugeAtivoAndAtualizarStatusCasamento() {
        if (!selecionado.getConjuges().isEmpty()) {
            for (Conjuge conj : selecionado.getConjuges()) {
                if (conjuge.getCasado() && conj.getCasado()) {
                    conj.setCasado(Boolean.FALSE);
                }
            }
        }
    }

    public void editarFilho(Filho fi) {
        setFilho(fi);
        removerFilho(fi);
    }

    public void editarConjuge(Conjuge conjuge) {
        setConjuge(conjuge);
        removerConjuge(conjuge);
    }

    public void removerFilho(Filho fi) {
        selecionado.getFilhos().remove(fi);
    }

    public void removerConjuge(Conjuge conjuge) {
        selecionado.getConjuges().remove(conjuge);
    }


    // Select Itens
    public List<SelectItem> getSexoSelectItem() {
        return Util.getListSelectItem(Sexo.values());
    }

    public List<SelectItem> getRacaCorSelectItem() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(RacaCor.values());
    }

    public List<SelectItem> getTipoFoneSelectItem() {
        return Util.getListSelectItem(TipoTelefone.values());
    }

    public List<SelectItem> getEstadoCivilSelectItem() {
        return Util.getListSelectItem(EstadoCivil.values());
    }

    public List<SelectItem> getTipoSanguineoSelectItem() {
        return Util.getListSelectItemSemCampoVazioSemOrdenacao(TipoSanguineo.values());
    }

    public List<SelectItem> getSituacoes() {
        return Util.getListSelectItem(Situacao.values());
    }

    public List<SelectItem> getFormacoes() {
        return Util.getListSelectItem(Escolaridade.values());
    }

    public List<SelectItem> getSexos() {
        return Util.getListSelectItem(Sexo.values());
    }

    public List<SelectItem> getAssociacao() {
        return Util.getListSelectItem(TipoAssociacao.values());
    }

    public List<SelectItem> getCargosExercidos() {
        return Util.getListSelectItem(TipoCargoExercido.values());
    }

    public List<SelectItem> getTipoEnderecoSelectItem() {
        return Util.getListSelectItem(TipoEndereco.values());
    }


    public void validarCpf() {
        Boolean validado = Boolean.TRUE;
        if (selecionado.getId() == null) {
            if (!Util.isValidCpfOrCnpj(((PessoaFisica) selecionado).getCpf().trim())) {
                FacesUtil.addAtencao("O CPF " + ((PessoaFisica) selecionado).getCpf() + " é inválido.");
                validado = Boolean.FALSE;
            }

            if (!pessoaFisicaRepository.getPessoasComMesmoCPF(selecionado.getCpf()).isEmpty()) {
                for (Pessoa pessoa : pessoaFisicaRepository.getPessoasComMesmoCPF(selecionado.getCpf())) {
                    FacesUtil.addAtencao("O CPF " + selecionado.getCpf() + " já está cadastrado para " + pessoa.getNomePessoa());
                }
                validado = Boolean.FALSE;
            }
        }
        if (!validado) {
            selecionado.setCpf(null);
        }
    }


    public List<Estado> completarEstado(String filter) {
        return estadoRepository.buscarEstadoPorNome(filter);
    }

    public List<Cidade> completarCidade(String filter) {
        return cidadeRepository.completarCidadeByNome(filter, estado);
    }

    public List<Cidade> completarCidadeParaEndereco(String filter) {
        return cidadeRepository.completarCidadeByNome(filter, estadoParaEndereco);
    }

    public List<Nacionalidade> completarNacionalidade(String filter) {
        return nacionalidadeRepository.findAllNacionalidadeByDescricaoOrCodigo(filter);
    }

    public List<Bairro> completarBairro(String filter) {
        if (endereco != null && endereco.getCidade() != null) {
            return bairroRepository.buscarBairroPorNomeCidade(filter.trim(), endereco.getCidade());
        } else {
            FacesUtil.addCampoObrigatorio("O campo Cidade deve ser informado.");
            return new ArrayList<>();
        }
    }

    public void adicionarTelefone() {
        try {
            validarTelefone();
            Util.isTamanhoTelefoneValido(getTelefone().getTelefone());
            getTelefone().setTelefone(Util.getTelefone(getTelefone().getTelefone()));
            getTelefone().setPessoa(selecionado);
            selecionado.setTelefones(Util.adicionarObjetoEmLista(selecionado.getTelefones(), getTelefone()));
            telefone = new Telefone();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    public void editarTelefone(Telefone fone) {
        telefone = fone;
        telefone.setTelefone(telefone.getTelefone().replaceAll("[^0-9]", ""));
        removerTelefone(fone);
    }

    public void removerTelefone(Telefone fone) {
        selecionado.getTelefones().remove(fone);
    }

    public void adicionarEndereco() {
        try {
            validarEndereco();
            getEndereco().setPessoa(selecionado);
            selecionado.setEnderecos(Util.adicionarObjetoEmLista(selecionado.getEnderecos(), getEndereco()));
            endereco = new Endereco();
            estadoParaEndereco = null;
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    public void validarEndereco() {
        Util.validarCampos(getEndereco());
    }


    public void validarTelefone() {
        Util.validarCampos(getTelefone());
    }

    public void editarEndereco(Endereco end) {
        endereco = end;
        if (end != null && end.getCidade() != null) {
            estadoParaEndereco = end.getCidade().getEstado();
        }
        removerEndereco(end);
    }

    public void removerEndereco(Endereco end) {
        selecionado.getEnderecos().remove(end);
    }


    public ConverterAutoComplete getConverterNacionalidade() {
        return converterNacionalidade;
    }

    public void setConverterNacionalidade(ConverterAutoComplete converterNacionalidade) {
        this.converterNacionalidade = converterNacionalidade;
    }

    public ConverterAutoComplete getConverterCidade() {
        return converterCidade;
    }

    public void setConverterCidade(ConverterAutoComplete converterCidade) {
        this.converterCidade = converterCidade;
    }

    public void adicionarConta() {
        try {
            validarConta();
            ajustarContaPrincipal();
            selecionado.setContas(Util.adicionarObjetoEmLista(selecionado.getContas(), new PessoaConta(selecionado, contaCorrente)));
            inicializarConta();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        }
    }

    private void ajustarContaPrincipal() {
        if (contaCorrente.getPrincipal() && !selecionado.getContas().isEmpty()) {
            for (PessoaConta pessoaConta : selecionado.getContas()) {
                pessoaConta.getContaCorrente().setPrincipal(Boolean.FALSE);
            }
        }
    }

    private void inicializarConta() {
        contaCorrente = new ContaCorrenteBancaria();
    }

    public void validarConta() {
        Util.validarCampos(contaCorrente);
    }

    public void editarConta(PessoaConta pessoaConta) {
        this.contaCorrente = pessoaConta.getContaCorrente();
        removerConta(pessoaConta);
    }

    public void removerConta(PessoaConta pessoaConta) {
        selecionado.getContas().remove(pessoaConta);
    }


    // Gets and Sets

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public CarteiraTrabalho getCarteiraTrabalho() {
        return carteiraTrabalho;
    }

    public void setCarteiraTrabalho(CarteiraTrabalho carteiraTrabalho) {
        this.carteiraTrabalho = carteiraTrabalho;
    }

    public TituloEleitor getTituloEleitor() {
        return tituloEleitor;
    }

    public void setTituloEleitor(TituloEleitor tituloEleitor) {
        this.tituloEleitor = tituloEleitor;
    }

    public RG getRg() {
        return rg;
    }

    public void setRg(RG rg) {
        this.rg = rg;
    }

    public AreaFormacao getAreaFormacao() {
        return areaFormacao;
    }

    public void setAreaFormacao(AreaFormacao areaFormacao) {
        this.areaFormacao = areaFormacao;
    }

    public Passaporte getPassaporte() {
        return passaporte;
    }

    public void setPassaporte(Passaporte passaporte) {
        this.passaporte = passaporte;
    }

    public Filho getFilho() {
        return filho;
    }

    public void setFilho(Filho filho) {
        this.filho = filho;
    }

    public List<CarteiraTrabalho> getCarteirasTrabalho() {
        return carteirasTrabalho;
    }

    public void setCarteirasTrabalho(List<CarteiraTrabalho> carteirasTrabalho) {
        this.carteirasTrabalho = carteirasTrabalho;
    }

    public ConverterAutoComplete getConverterProfissao() {
        if (converterProfissao == null) {
            converterProfissao = new ConverterAutoComplete(Profissao.class, profissaoRepository);
        }
        return converterProfissao;
    }

    public void setConverterProfissao(ConverterAutoComplete converterProfissao) {
        this.converterProfissao = converterProfissao;
    }

    public ConverterAutoComplete getConverterPessoa() {
        if (converterPessoa == null) {
            converterPessoa = new ConverterAutoComplete(Pessoa.class, pessoaRepository);
        }
        return converterPessoa;
    }

    public ConverterAutoComplete getConverterAreaFormacao() {
        if (converterAreaFormacao == null) {
            converterAreaFormacao = new ConverterAutoComplete(AreaFormacao.class, areaFormacaoRepository);
        }
        return converterAreaFormacao;
    }

    public Conjuge getConjuge() {
        return conjuge;
    }

    public void setConjuge(Conjuge conjuge) {
        this.conjuge = conjuge;
    }

    public ContaCorrenteBancaria getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrenteBancaria contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstadoParaEndereco() {
        return estadoParaEndereco;
    }

    public void setEstadoParaEndereco(Estado estadoParaEndereco) {
        this.estadoParaEndereco = estadoParaEndereco;
    }
}
