package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.seguranca.GrupoAcesso;
import br.com.gleac.entidade.seguranca.GrupoUsuario;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.PessoaFisicaRepository;
import br.com.gleac.negocio.UsuarioRepository;
import br.com.gleac.service.SistemaService;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

/**
 * Created by israeleriston on 11/05/16.
 */
@ManagedBean(name = "usuarioControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-usuario",
        pattern = "/admin/usuario/novo/",
        viewId = "/admin/seguranca/usuario/editar.xhtml"),
    @URLMapping(id = "editar-usuario",
        pattern = "/admin/usuario/editar/#{usuarioControlador.id}/",
        viewId = "/admin/seguranca/usuario/editar.xhtml"),
    @URLMapping(id = "ver-usuario",
        pattern = "/admin/usuario/ver/#{usuarioControlador.id}/",
        viewId = "/admin/seguranca/usuario/visualizar.xhtml"),
    @URLMapping(id = "listar-usuario",
        pattern = "/admin/usuario/listar/",
        viewId = "/admin/seguranca/usuario/listar.xhtml"),
    @URLMapping(id = "alterar-senha",
        pattern = "/admin/usuario/alterar-senha/#{usuarioControlador.id}/",
        viewId = "/admin/seguranca/usuario/alterar-senha.xhtml")
})
public class UsuarioControlador extends AbstractController<Usuario> implements Serializable, Crud {

    private static String KEY_IMAGEM = "imagem-foto";
    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private PessoaFisicaRepository pessoaFisicaRepository;

    @Autowired
    private SistemaService sistemaService;

    private GrupoAcesso grupoAcesso;

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }


    public UsuarioControlador() {
        super(Usuario.class);
    }

    @URLAction(mappingId = "alterar-senha", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    public void alterarSenha() {
        super.editar();
        selecionado.setSenha("");
        selecionado.setConfirmacaoSenha("");
    }

    public void redirecionarParaAlterarSenha() {
        FacesUtil.redirecionamentoInterno(selecionado.getCaminhoPadrao() + "alterar-senha/" + selecionado.getId());
    }

    @URLAction(mappingId = "novo-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        colocarImagemSessao(null);
    }

    @Override
    public void salvar() {
        try {
            if (selecionado.getId() == null) {
                validarUsername();
                validarPessoaUsuario();
                validarSelecionado();
                getRepository().salvarNovo(selecionado);
            }

            if (selecionado.getId() != null) {
                getRepository().salvar(selecionado);
            }
            navegarEmbora();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addOperacaoNaoRealizada(ex.getMessage());
        }

    }

    private void validarPessoaUsuario() {
        ValidacaoException ve = new ValidacaoException();
        if (selecionado.getPessoa() != null) {
            PessoaFisica pessoaFisica = pessoaFisicaRepository.buscarPessoaFisicaWithUsuario(selecionado.getPessoa());
            if (pessoaFisica != null) {
                ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, verifique a pessoa física informada, já existe um usuário atribuido a essa pessoa física.");
            }
        }
        ve.lancarException();
    }

    private void validarUsername() {
        ValidacaoException ve = new ValidacaoException();
        if (StringUtils.isNotEmpty(selecionado.getLogin())) {
            Usuario usuario = repository.recuperarUsuarioPorLogin(selecionado.getLogin());
            if (usuario != null) {
                ve.adicionarMensagemDeOperacaoNaoPermitida("Por favor, verifique o login, já existe um usuário com esse login.");
            }
        }
        ve.lancarException();
    }

    private void validarSelecionado() {
        selecionado.isSenhasIguais();
        selecionado.isTamanhoSenhaPermitido();
        if(selecionado.getId() == null) {
            selecionado.criptografarSenha();
        }
    }

    public void resetSenha() {
        selecionado.setSenha(selecionado.getUsername().trim());
        selecionado.setConfirmacaoSenha(selecionado.getUsername().trim());
        selecionado.criptografarSenha();
    }

    public void adicionarGrupoAcesso() {
        if (this.getGrupoAcesso() != null) {
//            selecionado.setGruposUsuario(Util.adicionarObjetoEmLista(selecionado.getGruposUsuario(), new GrupoUsuario(selecionado, grupoAcesso)));
            this.setGrupoAcesso(new GrupoAcesso());
        }
    }

    private void validarGrupoAcesso() {
        ValidacaoException ex = new ValidacaoException();
        if (this.selecionado.getGruposUsuario() == null || this.selecionado.getGruposUsuario().isEmpty()) {
            ex.adicionarMensagemDeOperacaoNaoPermitida(" È necessário informar um grupo de acesso para o usuário ");
        }

        ex.lancarException();
    }

    public void redefinirSenha() {
        try {
            validarSelecionado();
            repository.salvar(selecionado);
            FacesUtil.addOperacaoRealizada("O registro: " + this.selecionado.toString() + " foi alterado com sucesso!");
            navegarEmbora();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addErrorOperacaoNaoPermitida(ex.getMessage());
        }
    }

    @URLAction(mappingId = "editar-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        if (selecionado.getArquivo() != null){
            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        }

    }

    @URLAction(mappingId = "ver-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    @URLAction(mappingId = "listar-usuario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    public void carregarFoto(FileUploadEvent event) {
        try {
            Arquivo arquivo = new Arquivo();
            arquivo.setTipo(event.getFile().getContentType());
            arquivo.setNome(event.getFile().getFileName());
            byte[] content = IOUtils.toByteArray(event.getFile().getInputstream());
            arquivo.setArquivo(content);
            selecionado.setArquivo(arquivo);

            colocarImagemSessao(selecionado.getArquivo().getStreamedContent());
        } catch (Exception ex) {
            ex.printStackTrace();
            FacesUtil.addError("Erro ao carregar o arquivo.", ex.getMessage());
        }
    }

    public List<PessoaFisica> completarPessoa(String filter) {
        return pessoaFisicaRepository.buscarPessoaFisicaPorNome(filter);
    }

    private void colocarImagemSessao(StreamedContent streamedContent) {
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(KEY_IMAGEM, streamedContent);
    }

    public void removerGrupo(GrupoUsuario grupoUsuario) {
        selecionado.getGruposUsuario().remove(grupoUsuario);
    }

    public GrupoAcesso getGrupoAcesso() {
        return grupoAcesso;
    }

    public void setGrupoAcesso(GrupoAcesso grupoAcesso) {
        this.grupoAcesso = grupoAcesso;
    }

    public Boolean mostrarImagem() {
        try {
            return ((StreamedContent) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_IMAGEM)).getName().trim().length() <= 0;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean exibirSiluetaImagem() {
        try {
            return ((StreamedContent) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(KEY_IMAGEM)).getName().trim().length() <= 0;
        } catch (Exception e) {
            return true;
        }
    }


}
