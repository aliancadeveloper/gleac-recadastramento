package br.com.gleac.controlador;

import br.com.gleac.converters.ConverterAutoComplete;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.estoque.ItemSaidaEstoque;
import br.com.gleac.entidade.estoque.SaidaEstoque;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.SaidaEstoqueRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renato on 08/07/2017.
 */
@ManagedBean(name = "saidaEstoqueControlador")
@ViewScoped
@URLMappings(mappings = {
    @URLMapping(id = "novo-saidaEstoque", pattern = "/admin/saida-estoque/novo/", viewId = "/admin/estoque/saida-estoque/editar.xhtml"),
    @URLMapping(id = "editar-saidaEstoque", pattern = "/admin/saida-estoque/editar/#{saidaEstoqueControlador.id}/", viewId = "/admin/estoque/saida-estoque/editar.xhtml"),
    @URLMapping(id = "listar-saidaEstoque", pattern = "/admin/saida-estoque/listar/", viewId = "/admin/estoque/saida-estoque/listar.xhtml"),
    @URLMapping(id = "ver-saidaEstoque", pattern = "/admin/saida-estoque/ver/#{saidaEstoqueControlador.id}/", viewId = "/admin/estoque/saida-estoque/visualizar.xhtml")
})
public class SaidaEstoqueControlador extends AbstractController<SaidaEstoque> implements Serializable {

    @Autowired
    private SaidaEstoqueRepository saidaEstoqueRepository;
    private ItemSaidaEstoque item;
    private ConverterAutoComplete converterProduto;

    @Override
    public AbstractRepository getRepository() {
        return saidaEstoqueRepository;
    }

    public SaidaEstoqueControlador() {
        super(SaidaEstoque.class);
    }

    @URLAction(mappingId = "listar-saidaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "novo-saidaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
        item = new ItemSaidaEstoque();
    }

    @URLAction(mappingId = "editar-saidaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
        item = new ItemSaidaEstoque();
    }

    @URLAction(mappingId = "ver-saidaEstoque", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }


    public ItemSaidaEstoque getItemSaidaEstoque() {
        return item;
    }

    public void setItemSaidaEstoque(ItemSaidaEstoque item) {
        this.item = item;
    }

    public void adicionarItemSaidaEstoque() {
        try {
            if (item == null) {
                return;
            }
            item.setSaidaEstoque(selecionado);
            Util.validarCampos(item);
            selecionado.setItens(Util.adicionarObjetoEmLista(selecionado.getItens(), item));
            item = new ItemSaidaEstoque();
        } catch (ValidacaoException ex) {
            FacesUtil.printAllFacesMessages(ex.getMensagens());
        } catch (Exception ex) {
            FacesUtil.addError(ex.getMessage(), ex.getMessage());
        }
    }

    public void alterarItemSaidaEstoque(ItemSaidaEstoque item) {
        this.item = item;
    }

    public void removerItemSaidaEstoque(ItemSaidaEstoque item) {
        selecionado.getItens().remove(item);
    }

    public ItemSaidaEstoque getItem() {
        return item;
    }

    public void setItem(ItemSaidaEstoque item) {
        this.item = item;
    }

    public ConverterAutoComplete getConverterProduto() {
        if (converterProduto == null) {
            converterProduto = new ConverterAutoComplete(Produto.class, saidaEstoqueRepository.getProdutoRepository());
        }
        return converterProduto;
    }

    public List<Produto> completarProduto(String filtro) {
        return saidaEstoqueRepository.getProdutoRepository().completarProdutoPorNome(filtro);
    }

    @Override
    public void salvar() {
        try {
            if (Persistencia.getId(this.selecionado) == null) {
                saidaEstoqueRepository.salvarNovo(this.selecionado);
            } else {
                saidaEstoqueRepository.salvar(this.selecionado);
            }
            FacesUtil.addInfo("Operação realizada com sucesso!", "O " + this.selecionado.toString() + " foi <b> salvo </b> com sucesso!");
        } catch (RuntimeException ex) {
            if (ex instanceof ValidacaoException) {
                FacesUtil.printAllFacesMessages(((ValidacaoException) ex).getMensagens());
            } else {
                FacesUtil.addErrorPadrao(ex);
            }
            return;
        } catch (Exception ex) {
            FacesUtil.addErrorPadrao(ex);
            return;
        }

        navegarEmbora();
    }
}
