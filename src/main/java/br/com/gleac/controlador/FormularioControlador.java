package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.Formulario;
import br.com.gleac.interfaces.Crud;
import br.com.gleac.negocio.FormularioRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Created by zaca.
 */
@ManagedBean(name = "formularioControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "novo-formulario", pattern = "/admin/formulario/novo/", viewId = "/admin/cadastros/formulario/editar.xhtml"),
        @URLMapping(id = "editar-formulario", pattern = "/admin/formulario/editar/#{formularioControlador.id}/", viewId = "/admin/cadastros/formulario/editar.xhtml"),
        @URLMapping(id = "ver-formulario", pattern = "/admin/formulario/ver/#{formularioControlador.id}/", viewId = "/admin/cadastros/formulario/visualizar.xhtml"),
        @URLMapping(id = "listar-formulario", pattern = "/admin/formulario/listar/", viewId = "/admin/cadastros/formulario/listar.xhtml")
})
public class FormularioControlador extends AbstractController<Formulario> implements Crud {

    @Autowired
    private FormularioRepository formularioRepository;

    @Override
    public AbstractRepository getRepository() {
        return getFormularioRepository();
    }

    public FormularioControlador() {
        super(Formulario.class);
    }

    public FormularioRepository getFormularioRepository() {
        return formularioRepository;
    }

    @URLAction(mappingId = "novo-formulario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }
    @URLAction(mappingId = "editar-formulario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-formulario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @URLAction(mappingId = "listar-formulario", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }
}
