package br.com.gleac.controlador;

import br.com.gleac.entidade.comum.ConfiguracaoCabecalho;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.negocio.ConfiguracaoCabecalhoRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import br.com.gleac.util.FacesUtil;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Edi on 30/07/2017.
 */

@ManagedBean(name = "configuracaoCabecalhoControlador")
@ViewScoped
@URLMappings(mappings = {
        @URLMapping(id = "nova-config-cabecalho", pattern = "/admin/configuracao-cabecalho/novo/", viewId = "/admin/cadastros/configuracao-cabecalho/editar.xhtml"),
        @URLMapping(id = "editar-config-cabecalho", pattern = "/admin/configuracao-cabecalho/editar/#{configuracaoCabecalhoControlador.id}/", viewId = "/admin/cadastros/configuracao-cabecalho/editar.xhtml"),
        @URLMapping(id = "listar-config-cabecalho", pattern = "/admin/configuracao-cabecalho/listar/", viewId = "/admin/cadastros/configuracao-cabecalho/listar.xhtml"),
        @URLMapping(id = "ver-config-cabecalho", pattern = "/admin/configuracao-cabecalho/ver/#{configuracaoCabecalhoControlador.id}/", viewId = "/admin/cadastros/configuracao-cabecalho/visualizar.xhtml")
})
public class ConfiguracaoCabecalhoControlador extends AbstractController<ConfiguracaoCabecalho> implements Serializable {

    @Autowired
    private ConfiguracaoCabecalhoRepository repository;

    public ConfiguracaoCabecalhoControlador() {
        super(ConfiguracaoCabecalho.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return repository;
    }

    @URLAction(mappingId = "listar-config-cabecalho", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void listar() {
        super.listar();
    }

    @URLAction(mappingId = "nova-config-cabecalho", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void novo() {
        super.novo();
    }

    @URLAction(mappingId = "editar-config-cabecalho", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void editar() {
        super.editar();
    }

    @URLAction(mappingId = "ver-config-cabecalho", phaseId = URLAction.PhaseId.RENDER_RESPONSE, onPostback = false)
    @Override
    public void visualizar() {
        super.visualizar();
    }

    @Override
    public void salvar() {
        try {
            super.salvar();
        } catch (ValidacaoException ve) {
            FacesUtil.printAllFacesMessages(ve.getMensagens());
        }
    }

    public void uploadArquivo(FileUploadEvent event) {
        try {
            criarArquivo(event);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void criarArquivo(FileUploadEvent event) throws IOException {
        selecionado.setArquivo(repository.getArquivoRepository().criarArquivo(event));
    }

    public void removerArquivo() {
        selecionado.setArquivo(null);
    }
}
