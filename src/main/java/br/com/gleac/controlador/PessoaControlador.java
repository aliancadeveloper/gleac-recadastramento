package br.com.gleac.controlador;

import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.negocio.PessoaRepository;
import br.com.gleac.supers.AbstractController;
import br.com.gleac.supers.AbstractRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "pessoaControlador")
@ViewScoped
public class PessoaControlador extends AbstractController<Pessoa> implements Serializable {
    @Autowired
    private PessoaRepository pessoaRepository;


    public PessoaControlador() {
        super(Pessoa.class);
    }

    @Override
    public AbstractRepository getRepository() {
        return getPessoaRepository();
    }

    public PessoaRepository getPessoaRepository() {
        return pessoaRepository;
    }


    @Override
    public List<Pessoa> completarEstaEntidade(String parte) {
        return pessoaRepository.buscarPessoa(parte, true);
    }
}
