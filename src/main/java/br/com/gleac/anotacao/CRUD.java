/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.gleac.anotacao;

import br.com.gleac.enums.TipoComponente;
import br.com.gleac.supers.AbstractController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Roma
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE, ElementType.FIELD})
public @interface CRUD {
    public String label() default "";

    public boolean obrigatorio() default true;

    public boolean aceitaNegativo() default true;

    public boolean visualizavel() default true;

    public boolean pesquisavel() default true;

    public boolean pesquisavelAutoComplete() default false;

    public boolean tabelavel() default true;

    public String sexo() default "M";

    public String plural() default "";

    public TipoComponente tipoComponente() default TipoComponente.PADRAO;

    public Class controlador() default AbstractController.class;
}
