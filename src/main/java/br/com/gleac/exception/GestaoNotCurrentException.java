package br.com.gleac.exception;

/**
 * Created by zaca.
 */
public class GestaoNotCurrentException  extends RuntimeException {
    public GestaoNotCurrentException(String s) {
        super(s);
    }

}
