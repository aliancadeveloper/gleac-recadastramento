/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author Felipe Marinzeck
 */
@ApplicationException(rollback = true)
public class ExceptionGenerica extends RuntimeException {

    public ExceptionGenerica(String message) {
        super(message);
    }
}
