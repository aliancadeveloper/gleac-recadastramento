/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.exception;

/**
 *
 * @author Romanini
 */
public class RegistroJaExistenteException extends RuntimeException{
    
    public RegistroJaExistenteException(String message) {
        super(message);
    }
}
