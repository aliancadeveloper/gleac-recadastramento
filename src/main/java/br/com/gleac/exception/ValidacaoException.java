package br.com.gleac.exception;

import br.com.gleac.enums.SummaryMessages;

import javax.ejb.ApplicationException;
import javax.faces.application.FacesMessage;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 30/08/16.
 */
@ApplicationException(rollback = true)
public class ValidacaoException extends RuntimeException {

    private final List<FacesMessage> mensagens;
    public boolean validou;

    public ValidacaoException() {
        mensagens = new ArrayList<>();
        validou = true;
    }

    public ValidacaoException(String message) {
        super(message);
        mensagens = new ArrayList<>();
        validou = true;
    }

    public ValidacaoException(String message, Throwable e) {
        super(message, e);
        mensagens = new ArrayList<>();
        validou = true;
    }

    public void adicionarMensagemError(SummaryMessages summary, String detail) {
        if (summary.equals(SummaryMessages.ATENCAO) || summary.equals(SummaryMessages.OPERACAO_REALIZADA)) {
            throw new IllegalArgumentException("O SummaryMessage do tipo " + summary + " não pode ser utilizado em uma mensagem de erro.");
        }

        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, summary.getDescricao(), detail));
        validou = false;
    }

    public void adicionarMensagemWarn(SummaryMessages summary, String detail) {
        if (!summary.equals(SummaryMessages.ATENCAO)) {
            throw new IllegalArgumentException("O SummaryMessage do tipo " + summary + " não pode ser utilizado em uma mensagem de alerta.");
        }

        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_WARN, summary.getDescricao(), detail));
    }

    public void adicionarMensagemInfo(SummaryMessages summary, String detail) {
        if (!summary.equals(SummaryMessages.OPERACAO_REALIZADA)) {
            throw new IllegalArgumentException("O SummaryMessage do tipo " + summary + " não pode ser utilizado em uma mensagem de informação.");
        }

        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_INFO, summary.getDescricao(), detail));
    }

    public void adicionarMensagensError(List<FacesMessage> msgs) {
        for (FacesMessage msg : msgs) {
            mensagens.add(msg);
        }

        validou = false;
    }

    public void adicionarMensagemDeCampoObrigatorio(String detail) {
        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, SummaryMessages.CAMPO_OBRIGATORIO.getDescricao(), detail));
    }

    public void adicionarMensagemDeOperacaoNaoPermitida(String detail) {
        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, SummaryMessages.OPERACAO_NAO_PERMITIDA.getDescricao(), detail));
    }

    public void adicionarMensagemDeOperacaoNaoRealizada(String detail) {
        mensagens.add(new FacesMessage(FacesMessage.SEVERITY_ERROR, SummaryMessages.OPERACAO_NAO_REALIZADA.getDescricao(), detail));
    }

    public boolean temMensagens() {
        return !mensagens.isEmpty();
    }

    public List<FacesMessage> getMensagens() {
        return mensagens;
    }

    public void lancarException(){
        if (temMensagens())
            throw this;
    }
}


