package br.com.gleac.exception;

/**
 * Created by zaca.
 */
public class FormularioNotFoundException extends RuntimeException {
    public FormularioNotFoundException(String s) {
        super(s);
    }

    public FormularioNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
