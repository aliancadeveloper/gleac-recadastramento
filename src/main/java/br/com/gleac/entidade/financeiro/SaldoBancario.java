package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.ContaCorrenteControlador;
import br.com.gleac.exception.CampoObrigatorioException;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Audited
@Entity
@CRUD(label = "Saldo Bancário", sexo = "F", plural = "Saldos Bancários")
public class SaldoBancario extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "saldo-bancario/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date dataSaldo;

    @CRUD(label = "Saldo", aceitaNegativo = false)
    private BigDecimal saldo;

    @CRUD(label = "Histórico", obrigatorio = false)
    private String historico;

    @CRUD(label = "Conta Corrente", controlador = ContaCorrenteControlador.class)
    @ManyToOne
    private ContaCorrenteBancaria contaCorrenteBancaria;

    public SaldoBancario() {
        saldo = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataSaldo() {
        return dataSaldo;
    }

    public void setDataSaldo(Date dataSaldo) {
        this.dataSaldo = dataSaldo;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public ContaCorrenteBancaria getContaCorrenteBancaria() {
        return contaCorrenteBancaria;
    }

    public void setContaCorrenteBancaria(ContaCorrenteBancaria contaCorrenteBancaria) {
        this.contaCorrenteBancaria = contaCorrenteBancaria;
    }

    @Override
    public void validarRegrasPrincipais() throws ExceptionGenerica {
        super.validarRegrasPrincipais();
        if (saldo.compareTo(BigDecimal.ZERO) <= 0) {
            throw new CampoObrigatorioException("O campo saldo deve ser maior que zero(0)");
        }
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }
}
