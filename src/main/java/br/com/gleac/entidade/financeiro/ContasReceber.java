package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.LojaControlador;
import br.com.gleac.controlador.PessoaControlador;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoComponente;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Contas a Receber", sexo = "F", plural = "Contas a Receber")
public class ContasReceber extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "contas-receber/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Loja", pesquisavelAutoComplete = true, controlador = LojaControlador.class, tabelavel = false)
    private Loja loja;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data do Vencimento", tipoComponente = TipoComponente.DATA_BETWEEN)
    private Date dataVencimento;
    @ManyToOne
    @CRUD(label = "Pessoa", obrigatorio = false, pesquisavelAutoComplete = true, controlador = PessoaControlador.class)
    private Pessoa pessoa;
    @CRUD(label = "Descrição")
    private String descricao;
    @CRUD(label = "Tipo")
    @Enumerated(EnumType.STRING)
    private TipoContaPagar tipoContaPagar;
    @CRUD(label = "Situação")
    @Enumerated(EnumType.STRING)
    private SituacaoContaPagar situacaoContaPagar;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;

    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Boleto", obrigatorio = false, pesquisavel = false)
    private Boleto boleto;

    public ContasReceber() {
        dataVencimento = new Date();
        situacaoContaPagar = SituacaoContaPagar.ABERTA;
        tipoContaPagar = TipoContaPagar.DESPESA_DIVERSA;
        valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoContaPagar getTipoContaPagar() {
        return tipoContaPagar;
    }

    public void setTipoContaPagar(TipoContaPagar tipoContaPagar) {
        this.tipoContaPagar = tipoContaPagar;
    }

    public SituacaoContaPagar getSituacaoContaPagar() {
        return situacaoContaPagar;
    }

    public void setSituacaoContaPagar(SituacaoContaPagar situacaoContaPagar) {
        this.situacaoContaPagar = situacaoContaPagar;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public void validarRegrasPrincipais() throws ExceptionGenerica {
        super.validarRegrasPrincipais();
    }

    @Override
    public String toString() {
        return descricao;
    }
}
