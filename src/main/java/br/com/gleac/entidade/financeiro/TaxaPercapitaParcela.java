package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.enums.FormaPagamentoTaxa;
import br.com.gleac.enums.TipoTaxaPercapita;
import br.com.gleac.enums.TipoTaxaPercapitaParcela;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Taxa Per Capita", sexo = "F", plural = "Taxas Per Capita")
public class TaxaPercapitaParcela extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "emissao-de-boletos/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private TaxaPercapita taxaPercapita;

    @CRUD(label = "Tipo")
    private TipoTaxaPercapitaParcela tipo;

    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Contas a Receber")
    private ContasReceber contasReceber;


    public TaxaPercapitaParcela() {
        this.tipo = TipoTaxaPercapitaParcela.PRIMEIRA_PARCELA;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaxaPercapita getTaxaPercapita() {
        return taxaPercapita;
    }

    public void setTaxaPercapita(TaxaPercapita taxaPercapita) {
        this.taxaPercapita = taxaPercapita;
    }

    public TipoTaxaPercapitaParcela getTipo() {
        return tipo;
    }

    public void setTipo(TipoTaxaPercapitaParcela tipo) {
        this.tipo = tipo;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return contasReceber.toString();
    }

    public Boolean isComDesconto() {
        return TipoTaxaPercapitaParcela.UNICA_COM_DESCONTO.equals(tipo);
    }

}
