package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by renatoromanini on 27/06/2017.
 */
@Audited
@Entity
@CRUD(label = "Conta Corrente", sexo = "F", plural = "Contas Correntes")
public class ContaCorrente extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "conta-corrente/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Número", pesquisavelAutoComplete = true)
    private String numero;

    @CRUD(label = "Conta Bancária")
    @Identificador
    @ManyToOne
    private ContaCorrenteBancaria contaCorrenteBancaria;

    @CRUD(label = "Nome", pesquisavelAutoComplete = true)
    private String nome;

    @CRUD(label = "Principal ", obrigatorio = false)
    private Boolean principal;

    public ContaCorrente() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public ContaCorrenteBancaria getContaCorrenteBancaria() {
        return contaCorrenteBancaria;
    }

    public void setContaCorrenteBancaria(ContaCorrenteBancaria contaCorrenteBancaria) {
        this.contaCorrenteBancaria = contaCorrenteBancaria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return numero + "- " + nome;
    }
}
