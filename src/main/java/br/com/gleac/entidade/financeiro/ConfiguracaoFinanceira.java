package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by renatoromanini on 27/06/2017.
 */
@Audited
@Entity
@Document(indexName = "configuracao-financeira")
@CRUD(label = "Configuração Financeira", sexo = "M", plural = "Configurações Financeiras")
public class ConfiguracaoFinanceira extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "configuracao-financeira/";

    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Email")
    @Identificador
    private String email;
    @CRUD(label = "Token")
    @Identificador
    private String token;
    @CRUD(label = "ApiKey")
    @Identificador
    private String apiKey;
    @CRUD(label = "Partners_id", tabelavel = false)
    @Identificador
    private String partners_id;
    @CRUD(label = "Url Notificação", tabelavel = false, obrigatorio = false)
    @Identificador
    private String urlNotificacao;

    public ConfiguracaoFinanceira() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


    public String getPartners_id() {
        return partners_id;
    }

    public void setPartners_id(String partners_id) {
        this.partners_id = partners_id;
    }

    public String getUrlNotificacao() {
        return urlNotificacao;
    }

    public void setUrlNotificacao(String urlNotificacao) {
        this.urlNotificacao = urlNotificacao;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return email;
    }

}
