package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Paramiter;
import br.com.gleac.anotacao.ParamiterIgnore;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renato on 30/09/17.
 */

@Audited
@Entity
@CRUD(label = "Boleto", sexo = "F", plural = "Boletos")
@Paramiter
public class DadosRetornoPgHiper extends AbstractEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @Column(name = "pgtransc_id")
    private Integer id;

    @Column(name = "pg_hiper_host")
    @ParamiterIgnore
    private String ipHost;

    @Column(name = "pg_hiper_uri")
    @ParamiterIgnore
    private String uri;

    @Column(name = "pg_hiper_verificado")
    @ParamiterIgnore
    private Boolean verificado;

    @Column(name = "pg_hiper_dt_post")
    @ParamiterIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPost = new Date();

    @Column(name = "pg_hiper_resposta")
    @ParamiterIgnore
    private String respostaPGHIper = "VAZIO";
    @Column(name = "pg_hiper_urlConfirme")
    @ParamiterIgnore
    private String urlConfirmPGHIper = "VAZIO";
    @Column(name = "pg_hiper_paransConfirme")
    @ParamiterIgnore
    private String paransConfirmPGHIper = "VAZIO";

    @Column(name = "pg_hiper_idTransacao")
    private String idTransacao;

    @Column(name = "pg_hiper_dataTransacao")
    private String dataTransacao;

    @Column(name = "pg_hiper_dataCredito")
    private String dataCredito;
    @Column(name = "pg_hiper_valorOriginal")
    private String valorOriginal;
    @Column(name = "pg_hiper_valorLoja")
    private String valorLoja;
    @Column(name = "pg_hiper_valorTotal")
    private String valorTotal;
    @Column(name = "pg_hiper_numeroParcelas")
    private String numeroParcelas;
    @Column(name = "pg_hiper_status")
    private String status;
    @Column(name = "pg_hiper_nomecliente")
    private String nomeCliente;
    @Column(name = "pg_hiper_emailcliente")
    private String emailCliente;
    @Column(name = "pg_hiper_rgcliente")
    private String rgCliente;
    @Column(name = "pg_hiper_cpfcliente")
    private String cpfCliente;
    @Column(name = "pg_hiper_sexocliente")
    private String sexoCliente;
    @Column(name = "pg_hiper_razaosocialcliente")
    private String razaoSocialCliente;
    @Column(name = "pg_hiper_cnpjcliente")
    private String cnpjCliente;
    @Column(name = "pg_hiper_notaFiscal")
    private String notaFiscal;
    @Column(name = "pg_hiper_frasefixa")
    private String fraseFixa;
    @Column(name = "pg_hiper_enderecocliente")
    private String enderecoCliente;
    @Column(name = "pg_hiper_complementocliente")
    private String complementoCliente;
    @Column(name = "pg_hiper_bairrocliente")
    private String bairroCliente;
    @Column(name = "pg_hiper_cidadecliente")
    private String cidadeCliente;
    @Column(name = "pg_hiper_estadocliente")
    private String estadoCliente;
    @Column(name = "pg_hiper_cepcliente")
    private String cepCliente;
    @Column(name = "pg_hiper_frete")
    private String frete;
    @Column(name = "pg_hiper_tipoFrete")
    private String tipoFrete;
    @Column(name = "pg_hiper_vendedorEmail")
    private String vendedorEmail;
    @Column(name = "pg_hiper_numitem")
    private String numItem;

    @Column(name = "pg_hiper_idPlataforma")
    private String idPlataforma;

    @Column(name = "pg_hiper_cod_retorno")
    private String codRetorno;
    @Column(name = "pg_hiper_tp_pgto")
    private String tipoPagamento;
    @Column(name = "pg_hiper_cod_pagto")
    private String codPagamento;
    @Column(name = "pg_hiper_url_pgto")
    private String urlPagamento;
    @Column(name = "pg_hiper_linha_digitavel")
    private String linhaDigitavel;
    @Column(name = "pg_hiper_stractrace", columnDefinition = "TEXT")
    private String stracTrace;

    public DadosRetornoPgHiper() {
    }

    @Override
    public String getCaminhoPadrao() {
        return "";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIpHost() {
        return ipHost;
    }

    public void setIpHost(String ipHost) {
        this.ipHost = ipHost;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Boolean getVerificado() {
        return verificado;
    }

    public void setVerificado(Boolean verificado) {
        this.verificado = verificado;
    }

    public Date getDataPost() {
        return dataPost;
    }

    public void setDataPost(Date dataPost) {
        this.dataPost = dataPost;
    }

    public String getRespostaPGHIper() {
        return respostaPGHIper;
    }

    public void setRespostaPGHIper(String respostaPGHIper) {
        this.respostaPGHIper = respostaPGHIper;
    }

    public String getUrlConfirmPGHIper() {
        return urlConfirmPGHIper;
    }

    public void setUrlConfirmPGHIper(String urlConfirmPGHIper) {
        this.urlConfirmPGHIper = urlConfirmPGHIper;
    }

    public String getParansConfirmPGHIper() {
        return paransConfirmPGHIper;
    }

    public void setParansConfirmPGHIper(String paransConfirmPGHIper) {
        this.paransConfirmPGHIper = paransConfirmPGHIper;
    }

    public String getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(String idTransacao) {
        this.idTransacao = idTransacao;
    }

    public String getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(String dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    public String getDataCredito() {
        return dataCredito;
    }

    public void setDataCredito(String dataCredito) {
        this.dataCredito = dataCredito;
    }

    public String getValorOriginal() {
        return valorOriginal;
    }

    public void setValorOriginal(String valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    public String getValorLoja() {
        return valorLoja;
    }

    public void setValorLoja(String valorLoja) {
        this.valorLoja = valorLoja;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getNumeroParcelas() {
        return numeroParcelas;
    }

    public void setNumeroParcelas(String numeroParcelas) {
        this.numeroParcelas = numeroParcelas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public String getRgCliente() {
        return rgCliente;
    }

    public void setRgCliente(String rgCliente) {
        this.rgCliente = rgCliente;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getSexoCliente() {
        return sexoCliente;
    }

    public void setSexoCliente(String sexoCliente) {
        this.sexoCliente = sexoCliente;
    }

    public String getRazaoSocialCliente() {
        return razaoSocialCliente;
    }

    public void setRazaoSocialCliente(String razaoSocialCliente) {
        this.razaoSocialCliente = razaoSocialCliente;
    }

    public String getCnpjCliente() {
        return cnpjCliente;
    }

    public void setCnpjCliente(String cnpjCliente) {
        this.cnpjCliente = cnpjCliente;
    }

    public String getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(String notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public String getFraseFixa() {
        return fraseFixa;
    }

    public void setFraseFixa(String fraseFixa) {
        this.fraseFixa = fraseFixa;
    }

    public String getEnderecoCliente() {
        return enderecoCliente;
    }

    public void setEnderecoCliente(String enderecoCliente) {
        this.enderecoCliente = enderecoCliente;
    }

    public String getComplementoCliente() {
        return complementoCliente;
    }

    public void setComplementoCliente(String complementoCliente) {
        this.complementoCliente = complementoCliente;
    }

    public String getBairroCliente() {
        return bairroCliente;
    }

    public void setBairroCliente(String bairroCliente) {
        this.bairroCliente = bairroCliente;
    }

    public String getCidadeCliente() {
        return cidadeCliente;
    }

    public void setCidadeCliente(String cidadeCliente) {
        this.cidadeCliente = cidadeCliente;
    }

    public String getEstadoCliente() {
        return estadoCliente;
    }

    public void setEstadoCliente(String estadoCliente) {
        this.estadoCliente = estadoCliente;
    }

    public String getCepCliente() {
        return cepCliente;
    }

    public void setCepCliente(String cepCliente) {
        this.cepCliente = cepCliente;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public String getVendedorEmail() {
        return vendedorEmail;
    }

    public void setVendedorEmail(String vendedorEmail) {
        this.vendedorEmail = vendedorEmail;
    }

    public String getNumItem() {
        return numItem;
    }

    public void setNumItem(String numItem) {
        this.numItem = numItem;
    }

    public String getIdPlataforma() {
        return idPlataforma;
    }

    public void setIdPlataforma(String idPlataforma) {
        this.idPlataforma = idPlataforma;
    }

    public String getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public String getCodPagamento() {
        return codPagamento;
    }

    public void setCodPagamento(String codPagamento) {
        this.codPagamento = codPagamento;
    }

    public String getUrlPagamento() {
        return urlPagamento;
    }

    public void setUrlPagamento(String urlPagamento) {
        this.urlPagamento = urlPagamento;
    }

    public String getLinhaDigitavel() {
        return linhaDigitavel;
    }

    public void setLinhaDigitavel(String linhaDigitavel) {
        this.linhaDigitavel = linhaDigitavel;
    }

    public String getStracTrace() {
        return stracTrace;
    }

    public void setStracTrace(String stracTrace) {
        this.stracTrace = stracTrace;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
