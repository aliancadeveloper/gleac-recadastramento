package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.enums.FormaPagamentoTaxa;
import br.com.gleac.enums.TipoTaxaPercapita;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Taxa Per Capita", sexo = "F", plural = "Taxas Per Capita")
public class TaxaPercapita extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "emissao-de-boletos/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date data;
    @ManyToOne
    @CRUD(label = "Loja")
    private Loja loja;
    @ManyToOne
    @CRUD(label = "Maçom")
    private Macon macon;
    @ManyToOne
    @CRUD(label = "Serviço")
    private Servico servico;
    @CRUD(label = "Tipo de taxa")
    private TipoTaxaPercapita tipoTaxaPercapita;
    @CRUD(label = "Forma de Pagamento")
    private FormaPagamentoTaxa forma;
    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Contas a Receber")
    private ContasReceber contasReceber;
    @Transient
    private String retorno;

    @CRUD(label = "Tipo da Solicitação")
    @Enumerated(EnumType.STRING)
    private TipoSolicitacao tipoSolicitacao;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxaPercapita", orphanRemoval = true)
    private List<TaxaPercapitaParcela> parcelas;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taxaPercapita", orphanRemoval = true)
    private List<TaxaPercapitaProduto> produtos;

    public TaxaPercapita() {
        data = new Date();
        this.tipoTaxaPercapita = TipoTaxaPercapita.POR_LOJA;
        this.forma = FormaPagamentoTaxa.TODOS;
        this.tipoSolicitacao = TipoSolicitacao.SERVICO;
        this.parcelas = Lists.newArrayList();
        this.produtos = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public TipoTaxaPercapita getTipoTaxaPercapita() {
        return tipoTaxaPercapita;
    }

    public void setTipoTaxaPercapita(TipoTaxaPercapita tipoTaxaPercapita) {
        this.tipoTaxaPercapita = tipoTaxaPercapita;
    }

    public List<TaxaPercapitaParcela> getParcelas() {
        return parcelas;
    }

    public void setParcelas(List<TaxaPercapitaParcela> parcelas) {
        this.parcelas = parcelas;
    }

    public FormaPagamentoTaxa getForma() {
        return forma;
    }

    public void setForma(FormaPagamentoTaxa forma) {
        this.forma = forma;
    }

    public TipoSolicitacao getTipoSolicitacao() {
        return tipoSolicitacao;
    }

    public void setTipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
        this.tipoSolicitacao = tipoSolicitacao;
    }

    public List<TaxaPercapitaProduto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<TaxaPercapitaProduto> produtos) {
        this.produtos = produtos;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return servico.toString();
    }

    public Boolean isPorLoja() {
        return TipoTaxaPercapita.POR_LOJA.equals(tipoTaxaPercapita);
    }

    public Boolean isParcelado() {
        return FormaPagamentoTaxa.PARCELADO.equals(forma);
    }

    public Boolean isComDesconto() {
        return FormaPagamentoTaxa.AVISTA_COM_DESCONTO.equals(forma);
    }

    public Boolean isTodos() {
        return FormaPagamentoTaxa.TODOS.equals(forma);
    }

    public Boolean isServico() {
        return TipoSolicitacao.SERVICO.equals(tipoSolicitacao);
    }
}
