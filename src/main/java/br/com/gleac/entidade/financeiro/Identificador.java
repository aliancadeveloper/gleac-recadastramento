package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 13/10/2018.
 */

@Audited
@Entity
@CRUD(label = "Identificador", sexo = "F", plural = "Identificadores")
public class Identificador extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "identificador/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date data;
    @CRUD(label = "Descrição", pesquisavelAutoComplete = true)
    private String descricao;

    public Identificador() {
        this.data = new Date();
    }

    public Identificador(Long id, String descricao) {
        this.descricao = descricao;
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
