package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.LojaControlador;
import br.com.gleac.controlador.PessoaControlador;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.SituacaoContaPagar;
import br.com.gleac.enums.TipoContaPagar;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Contas a Pagar", sexo = "F", plural = "Contas a Pagar")
public class ContasPagar extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "contas-pagar/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Loja", pesquisavelAutoComplete = true, controlador = LojaControlador.class, tabelavel = false)
    private Loja loja;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data do vencimento")
    private Date dataVencimento;
    @ManyToOne
    @CRUD(label = "Pessoa", obrigatorio = false, pesquisavelAutoComplete = true, controlador = PessoaControlador.class)
    private Pessoa pessoa;
    @CRUD(label = "Descrição", pesquisavelAutoComplete = true)
    private String descricao;
    @CRUD(label = "Tipo")
    @Enumerated(EnumType.STRING)
    private TipoContaPagar tipoContaPagar;
    @CRUD(label = "Situação")
    @Enumerated(EnumType.STRING)
    private SituacaoContaPagar situacaoContaPagar;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;


    public ContasPagar() {
        dataVencimento = new Date();
        situacaoContaPagar = SituacaoContaPagar.ABERTA;
        tipoContaPagar = TipoContaPagar.DESPESA_DIVERSA;
        valor = BigDecimal.ZERO;
    }

    public ContasPagar(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoContaPagar getTipoContaPagar() {
        return tipoContaPagar;
    }

    public void setTipoContaPagar(TipoContaPagar tipoContaPagar) {
        this.tipoContaPagar = tipoContaPagar;
    }

    public SituacaoContaPagar getSituacaoContaPagar() {
        return situacaoContaPagar;
    }

    public void setSituacaoContaPagar(SituacaoContaPagar situacaoContaPagar) {
        this.situacaoContaPagar = situacaoContaPagar;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public void validarRegrasPrincipais() throws ExceptionGenerica {
        super.validarRegrasPrincipais();
    }

    public Boolean isContaVenceHoje() {
        if (dataVencimento != null) {
            return dataVencimento.compareTo(new Date()) == 0;
        }
        return false;
    }

    public String getStyleClassHome() {
        if (isContaVenceHoje()) {
            return "fa fa-exclamation-circle text-red";
        }
        return "fa fa-warning text-yellow";
    }

    @Override
    public String toString() {
        return descricao;
    }
}
