package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.enums.TipoOperacao;
import br.com.gleac.interfaces.ISaldoFinanceiro;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Pagamento", sexo = "F", plural = "Pagamentos")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pagamento extends AbstractEntity implements Serializable, ISaldoFinanceiro {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "pagamento/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Loja", pesquisavel = false, tabelavel = false)
    private Loja loja;
    @CRUD(label = "Conta")
    @ManyToOne
    private ContaCorrente contaCorrente;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date dataLancamento;
    @CRUD(label = "Descrição")
    private String descricao;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;
    @CRUD(label = "Contas a pagar", obrigatorio = false, pesquisavel = false, tabelavel = false)
    @ManyToOne
    private ContasPagar contasPagar;

    @CRUD(label = "Tipo de Pagamento")
    @Enumerated(EnumType.STRING)
    private FormaDePagamento formaDePagamento;
    @CRUD(label = "Observação", obrigatorio = false)
    private String observacao;

    /*Conciliacao*/
    @Temporal(TemporalType.DATE)
    private Date dataConciliacao;
    @ManyToOne
    private Identificador identificador;

    public Pagamento() {
        dataLancamento = new Date();
        valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public FormaDePagamento getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(FormaDePagamento formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDataConciliacao() {
        return dataConciliacao;
    }

    public void setDataConciliacao(Date dataConciliacao) {
        this.dataConciliacao = dataConciliacao;
    }

    public Identificador getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Identificador identificador) {
        this.identificador = identificador;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return descricao;
    }

    @Override
    public TipoOperacao getTipoOperacao() {
        return TipoOperacao.DEBITO;
    }

    public String getReferencia() {
        return "Pagamento";
    }

    @Override
    public String getHistorico() {
        return descricao;
    }
}
