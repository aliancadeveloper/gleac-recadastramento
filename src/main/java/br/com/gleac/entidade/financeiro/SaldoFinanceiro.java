package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.interfaces.ISaldoFinanceiro;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 06/08/17.
 */

@Audited
@Entity
@CRUD(label = "Saldo Financeiro", sexo = "M", plural = "Saldo Financeiros")
public class SaldoFinanceiro extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "lancamento/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date data;
    @CRUD(label = "Loja")
    @ManyToOne
    private Loja loja;
    @CRUD(label = "Conta corrente")
    @ManyToOne
    private ContaCorrente contaCorrente;
    @CRUD(label = "Débito")
    private BigDecimal saldo;
    @Version
    private Long versao;


    public SaldoFinanceiro() {
    }

    public SaldoFinanceiro(ISaldoFinanceiro iSaldoFinanceiro) {
        this.loja = iSaldoFinanceiro.getLoja();
        this.data = iSaldoFinanceiro.getDataLancamento();
        this.contaCorrente = iSaldoFinanceiro.getContaCorrente();
        this.saldo = iSaldoFinanceiro.getValor();
    }

    public SaldoFinanceiro(Loja loja, ContaCorrente contaCorrente, BigDecimal saldo, Date data) {
        this.loja = loja;
        this.data = data;
        this.contaCorrente = contaCorrente;
        this.saldo = saldo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Long getVersao() {
        return versao;
    }

    public void setVersao(Long versao) {
        this.versao = versao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return "Data: " + Util.sdf.format(data) + "; Valor:" + Util.getValorFormatado(getSaldo()) + "; Conta:" + contaCorrente.toString();
    }
}
