package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.SolicitacaoProcessoControlador;
import br.com.gleac.entidade.estoque.SaidaEstoque;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 08/07/17.
 */
@Audited
@Entity
@CRUD(label = "Processo Financeiro", sexo = "F", plural = "Processos Financeiros")
public class ProcessoFinanceiro extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "processo-financeiro/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Gerado Em")
    @Temporal(TemporalType.DATE)
    private Date geradoEm;
    @CRUD(label = "Vencimento do Boleto")
    @Temporal(TemporalType.DATE)
    private Date vencimentoBoleto;
    @ManyToOne
    @CRUD(label = "Solicitação", pesquisavelAutoComplete = true, controlador = SolicitacaoProcessoControlador.class)
    private SolicitacaoProcesso solicitacaoProcesso;
    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Boleto", obrigatorio = false, pesquisavel = false)
    private Boleto boleto;
    @Transient
    private String retorno;

    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Saida do estoque", obrigatorio = false, pesquisavel = false)
    private SaidaEstoque saidaEstoque;

    public ProcessoFinanceiro() {
        geradoEm = new Date();
        DateTime dateTime = new DateTime();
        DateTime vencimento = dateTime.plusDays(30);
        vencimentoBoleto = vencimento.toDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGeradoEm() {
        return geradoEm;
    }

    public void setGeradoEm(Date geradoEm) {
        this.geradoEm = geradoEm;
    }

    public SolicitacaoProcesso getSolicitacaoProcesso() {
        return solicitacaoProcesso;
    }

    public void setSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso) {
        this.solicitacaoProcesso = solicitacaoProcesso;
    }

    public Boleto getBoleto() {
        return boleto;
    }

    public void setBoleto(Boleto boleto) {
        this.boleto = boleto;
    }

    public String getRetorno() {
        return retorno;
    }

    public void setRetorno(String retorno) {
        this.retorno = retorno;
    }

    public Date getVencimentoBoleto() {
        return vencimentoBoleto;
    }

    public void setVencimentoBoleto(Date vencimentoBoleto) {
        this.vencimentoBoleto = vencimentoBoleto;
    }

    public SaidaEstoque getSaidaEstoque() {
        return saidaEstoque;
    }

    public void setSaidaEstoque(SaidaEstoque saidaEstoque) {
        this.saidaEstoque = saidaEstoque;
    }

    @Override
    public String toString() {
        return "Solicitação de processo " + getSolicitacaoProcesso().getDescricao() + " em " + Util.sdf.format(getGeradoEm());
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

}
