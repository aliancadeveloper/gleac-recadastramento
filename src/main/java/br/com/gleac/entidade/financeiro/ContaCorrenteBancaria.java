package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 27/06/2017.
 */
@Audited
@Entity
@CRUD(label = "Conta Corrente", sexo = "F", plural = "Contas Correntes")
public class ContaCorrenteBancaria extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "conta-corrente/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Número", pesquisavelAutoComplete = true)
    private String numero;

    @CRUD(label = "Agência")
    @Identificador
    @ManyToOne
    private Agencia agencia;

    @CRUD(label = "Nome", pesquisavelAutoComplete = true)
    private String nome;

    @CRUD(label = "Principal ", obrigatorio = false)
    private Boolean principal;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contaCorrenteBancaria", orphanRemoval = true)
    private List<ContaCorrente> contas;

    public ContaCorrenteBancaria() {
        this.contas = Lists.newArrayList();
    }

    public ContaCorrenteBancaria(Long id, String numero, Agencia agencia, String nome) {
        this.id = id;
        this.numero = numero;
        this.nome = nome;
        this.agencia = agencia;
    }

    public ContaCorrenteBancaria(Long id, String numero, Agencia agencia, String nome, Boolean principal) {
        this.id = id;
        this.numero = numero;
        this.agencia = agencia;
        this.nome = nome;
        this.principal = Boolean.FALSE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    public List<ContaCorrente> getContas() {
        return contas;
    }

    public void setContas(List<ContaCorrente> contas) {
        this.contas = contas;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return numero + "- " + nome + "/" + agencia.getNumero() + "/" + agencia.getBanco().getNumero();
    }
}
