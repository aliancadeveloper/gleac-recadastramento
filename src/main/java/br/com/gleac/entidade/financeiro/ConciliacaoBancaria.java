package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.ContaCorrenteControlador;
import br.com.gleac.entidade.seguranca.Usuario;
import br.com.gleac.supers.AbstractEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/*@Audited*/
@Entity
@CRUD(label = "Conciliação Bancária", sexo = "F", plural = "Conciliações")
public class ConciliacaoBancaria extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "conciliacao/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data de Lançamento")
    private Date dataLancamento;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data de Conciliação")
    private Date dataConciliacao;

    @ManyToOne
    @CRUD(label = "Usuário")
    private Usuario usuario;

    @CRUD(label = "Conta Corrente", controlador = ContaCorrenteControlador.class)
    @ManyToOne
    private ContaCorrenteBancaria contaCorrenteBancaria;

    public ConciliacaoBancaria() {
        dataLancamento = new Date();
        dataConciliacao = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataConciliacao() {
        return dataConciliacao;
    }

    public void setDataConciliacao(Date dataConciliacao) {
        this.dataConciliacao = dataConciliacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ContaCorrenteBancaria getContaCorrenteBancaria() {
        return contaCorrenteBancaria;
    }

    public void setContaCorrenteBancaria(ContaCorrenteBancaria contaCorrenteBancaria) {
        this.contaCorrenteBancaria = contaCorrenteBancaria;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }
}
