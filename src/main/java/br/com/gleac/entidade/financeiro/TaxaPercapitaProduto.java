package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.enums.TipoTaxaPercapitaParcela;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Taxa Per Capita", sexo = "F", plural = "Taxas Per Capita")
public class TaxaPercapitaProduto extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "emissao-de-boletos/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private TaxaPercapita taxaPercapita;

    @OneToOne
    @CRUD(label = "Produto")
    private Produto produto;


    public TaxaPercapitaProduto() {

    }

    public TaxaPercapitaProduto(TaxaPercapita taxaPercapita, Produto produto) {
        this.taxaPercapita = taxaPercapita;
        this.produto = produto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TaxaPercapita getTaxaPercapita() {
        return taxaPercapita;
    }

    public void setTaxaPercapita(TaxaPercapita taxaPercapita) {
        this.taxaPercapita = taxaPercapita;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return produto.toString();
    }

}
