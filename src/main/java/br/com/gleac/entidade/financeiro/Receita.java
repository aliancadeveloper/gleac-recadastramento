package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.FormaDePagamento;
import br.com.gleac.enums.TipoOperacao;
import br.com.gleac.interfaces.ISaldoFinanceiro;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 01/08/17.
 */

@Audited
@Entity
@CRUD(label = "Lançamento de Receita", sexo = "M", plural = "Lançamentos de Receitas")
public class Receita extends AbstractEntity implements Serializable, ISaldoFinanceiro {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "receita/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Loja", pesquisavel = false, tabelavel = false)
    private Loja loja;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date dataLancamento;
    @CRUD(label = "Origem")
    private String origem;
    @CRUD(label = "Conta")
    @ManyToOne
    private ContaCorrente contaCorrente;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;
    @CRUD(label = "Contas a receber", obrigatorio = false, pesquisavel = false, tabelavel = false)
    @ManyToOne
    private ContasReceber contasReceber;

    @CRUD(label = "Tipo de Pagamento")
    @Enumerated(EnumType.STRING)
    private FormaDePagamento formaDePagamento;
    @CRUD(label = "Observação", obrigatorio = false)
    private String observacao;

    /*Conciliacao*/
    @Temporal(TemporalType.DATE)
    private Date dataConciliacao;
    @ManyToOne
    private Identificador identificador;

    public Receita() {
        this.dataLancamento = new Date();
        this.valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public FormaDePagamento getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(FormaDePagamento formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getDataConciliacao() {
        return dataConciliacao;
    }

    public void setDataConciliacao(Date dataConciliacao) {
        this.dataConciliacao = dataConciliacao;
    }

    public Identificador getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Identificador identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return origem;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public TipoOperacao getTipoOperacao() {
        return TipoOperacao.CREDITO;
    }

    @Override
    public String getReferencia() {
        return "Receita";
    }

    @Override
    public String getHistorico() {
        return origem;
    }
}
