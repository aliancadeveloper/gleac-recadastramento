package br.com.gleac.entidade.financeiro;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 08/07/17.
 */
@Audited
@Entity
@CRUD(label = "Boleto", sexo = "F", plural = "Boletos")
public class Boleto extends AbstractEntity implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long numero;
    private String idTransacao;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataTransacao;
    private BigDecimal valorTotal;
    private String status;
    private String idPlataforma;
    private String urlPagamento;
    private String urlPdfPagamento;
    private String linhaDigitavel;
    @Temporal(TemporalType.DATE)
    private Date dataVencimento;

    @OneToOne(mappedBy = "boleto")
    private ContasReceber contasReceber;

    /* STATUS BOLETO
            pending aguardando
            canceled cancelado
            completed completo
            paid aprovado
            processing analise
            refunded estornado*/
    public Boolean isPago() {
        return status.equals("paid") || status.equals("aprovado") || status.equals("completed") || status.equals("completo");
    }

    public Boolean isCancelado() {
        return status.equals("canceled") || status.equals("cancelado");
    }

    public Boolean isAberto() {
        return status.equals("pending") || status.equals("aguardando");
    }

    public Boleto() {

    }

    @Override
    public String getCaminhoPadrao() {
        return "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdTransacao() {
        return idTransacao;
    }

    public void setIdTransacao(String idTransacao) {
        this.idTransacao = idTransacao;
    }

    public Date getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(Date dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdPlataforma() {
        return idPlataforma;
    }

    public void setIdPlataforma(String idPlataforma) {
        this.idPlataforma = idPlataforma;
    }

    public String getUrlPagamento() {
        return urlPagamento;
    }

    public void setUrlPagamento(String urlPagamento) {
        this.urlPagamento = urlPagamento;
    }

    public String getUrlPdfPagamento() {
        return urlPdfPagamento;
    }

    public void setUrlPdfPagamento(String urlPdfPagamento) {
        this.urlPdfPagamento = urlPdfPagamento;
    }

    public String getLinhaDigitavel() {
        return linhaDigitavel;
    }

    public void setLinhaDigitavel(String linhaDigitavel) {
        this.linhaDigitavel = linhaDigitavel;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }


    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {

        String retorno = "";
        if (valorTotal != null) {
            retorno += Util.getValorFormatado(valorTotal);
        }
        if (dataVencimento != null) {
            retorno += " - Vencimento : " + Util.sdf.format(dataVencimento);
        }
        if (contasReceber != null) {
            retorno += " (" + contasReceber.getSituacaoContaPagar().getDescricao() + ")";
        }
        return retorno;
    }

    @Override
    public String getToStringHtml() {
        return toString();
    }
}
