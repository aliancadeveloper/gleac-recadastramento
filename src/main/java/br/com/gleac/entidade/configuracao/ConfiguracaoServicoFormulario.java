package br.com.gleac.entidade.configuracao;

import br.com.gleac.entidade.comum.Formulario;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by zaca.
 */
@Entity
@Audited
public class ConfiguracaoServicoFormulario extends AbstractEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Formulario formulario;

    @ManyToOne
    private ConfiguracaoServico configuracaoServico;

    public ConfiguracaoServicoFormulario(Formulario formulario, ConfiguracaoServico configuracaoServico) {
        this.formulario = formulario;
        this.configuracaoServico = configuracaoServico;
    }

    public ConfiguracaoServicoFormulario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Formulario getFormulario() {
        return formulario;
    }

    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public ConfiguracaoServico getConfiguracaoServico() {
        return configuracaoServico;
    }

    public void setConfiguracaoServico(ConfiguracaoServico configuracaoServico) {
        this.configuracaoServico = configuracaoServico;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
