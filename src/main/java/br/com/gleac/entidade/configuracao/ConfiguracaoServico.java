package br.com.gleac.entidade.configuracao;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.ServicoControlador;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca.
 */
@Entity
@Audited
@CRUD(label = "Configuração de Serviço", plural = "Configuração de Serviços")
public class ConfiguracaoServico extends AbstractEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição da Configuração do Serviço")
    private String descricao;

    @CRUD(label = "Descrição da Lei Orçamentária", obrigatorio = false)
    private String lei;

    @CRUD(label = "Serviço", pesquisavelAutoComplete = true, controlador = ServicoControlador.class)
    @ManyToOne
    private Servico servico;

    @CRUD(label = "Multiplica Pela Quantidade de Maçons")
    private Boolean anuidade;

    @CRUD(label = "Permite Parcelamento")
    private Boolean parcelamento;

    @CRUD(label = "Qtd. de Dias Para Dar Desconto", aceitaNegativo = false)
    private Integer quantidadeDiasDesconto;

    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valorDesconto;

    @CRUD(label = "Data Inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;

    @CRUD(label = "Data Final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    @OneToMany(mappedBy = "configuracaoServico", cascade = CascadeType.ALL, orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<ConfiguracaoServicoFormulario> configuracaoServicoFormularios;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;

    public ConfiguracaoServico(String descricao, Servico servico, List<ConfiguracaoServicoFormulario> configuracaoServicoFormularios, BigDecimal valor) {
        this.descricao = descricao;
        this.servico = servico;
        this.configuracaoServicoFormularios = configuracaoServicoFormularios;
        this.valor = BigDecimal.ZERO;
        this.valorDesconto = BigDecimal.ZERO;
        this.quantidadeDiasDesconto = 0;
    }

    public ConfiguracaoServico() {
        this.setConfiguracaoServicoFormularios(Lists.newArrayList());
        this.valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public List<ConfiguracaoServicoFormulario> getConfiguracaoServicoFormularios() {
        return configuracaoServicoFormularios;
    }

    public void setConfiguracaoServicoFormularios(List<ConfiguracaoServicoFormulario> configuracaoServicoFormularios) {
        this.configuracaoServicoFormularios = configuracaoServicoFormularios;
    }

    public String getLei() {
        return lei;
    }

    public void setLei(String lei) {
        this.lei = lei;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boolean getAnuidade() {
        if (anuidade == null) {
            anuidade = Boolean.FALSE;
        }
        return anuidade;
    }

    public void setAnuidade(Boolean anuidade) {
        this.anuidade = anuidade;
    }

    public Integer getQuantidadeDiasDesconto() {
        return quantidadeDiasDesconto;
    }

    public void setQuantidadeDiasDesconto(Integer quantidadeDiasDesconto) {
        this.quantidadeDiasDesconto = quantidadeDiasDesconto;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public Boolean getParcelamento() {
        if (parcelamento == null) {
            parcelamento = Boolean.FALSE;
        }
        return parcelamento;
    }

    public void setParcelamento(Boolean parcelamento) {
        this.parcelamento = parcelamento;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/configuracao/servico/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
