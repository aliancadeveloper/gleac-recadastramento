package br.com.gleac.entidade.configuracao;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.*;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca.
 */
@Entity
@Audited
@CRUD(label = "Gestão", plural = "Gestões")
public class Gestao extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Descrição")
    private String descricao;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data Inicial")
    private Date inicioEm;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data Final")
    private Date fimEm;

    @CRUD(label = "Sereníssimo")
    @ManyToOne
    private Macon macon;

    @CRUD(label = "Secretário")
    @ManyToOne
    private Macon secretario;

    @CRUD(label = "Tesoureiro")
    @ManyToOne
    private Macon tesoureiro;

    @CRUD(label = "Número Placet", tabelavel = false, obrigatorio = false)
    private Long numeroPlacet;

    @NotAudited
    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Logo", pesquisavel = false, tabelavel = false)
    private Arquivo arquivo;

    @OneToMany(mappedBy = "gestao", cascade = CascadeType.ALL, orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<GestaoLoja> gestaoLojas;

    @OneToOne
    @CRUD(label = "Loja Gestora")
    private Loja loja;

    @OneToOne
    @CRUD(label = "Departamento de estoque")
    private Departamento departamentoEstoque;

    @OneToOne
    @CRUD(label = "Conta")
    private ContaCorrenteBancaria contaCorrenteBancaria;

    @CRUD(label = "Processo Financeiro Automático")
    private Boolean permitirProcessoFinanceiro;

    public Gestao() {
    }

    public Gestao(String descricao, Date inicioEm, Date fimEm, Macon macon, Loja loja) {
        this.descricao = descricao;
        this.inicioEm = inicioEm;
        this.fimEm = fimEm;
        this.macon = macon;
        this.loja = loja;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getInicioEm() {
        return inicioEm;
    }

    public void setInicioEm(Date inicioEm) {
        this.inicioEm = inicioEm;
    }

    public Date getFimEm() {
        return fimEm;
    }

    public void setFimEm(Date fimEm) {
        this.fimEm = fimEm;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public Long getNumeroPlacet() {
        return numeroPlacet;
    }

    public void setNumeroPlacet(Long numeroPlacet) {
        this.numeroPlacet = numeroPlacet;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public List<GestaoLoja> getGestaoLojas() {
        return gestaoLojas;
    }

    public void setGestaoLojas(List<GestaoLoja> gestaoLojas) {
        this.gestaoLojas = gestaoLojas;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Macon getSecretario() {
        return secretario;
    }

    public void setSecretario(Macon secretario) {
        this.secretario = secretario;
    }

    public Macon getTesoureiro() {
        return tesoureiro;
    }

    public void setTesoureiro(Macon tesoureiro) {
        this.tesoureiro = tesoureiro;
    }

    public ContaCorrenteBancaria getContaCorrenteBancaria() {
        return contaCorrenteBancaria;
    }

    public void setContaCorrenteBancaria(ContaCorrenteBancaria contaCorrenteBancaria) {
        this.contaCorrenteBancaria = contaCorrenteBancaria;
    }

    public Boolean getPermitirProcessoFinanceiro() {
        if (permitirProcessoFinanceiro == null) {
            this.permitirProcessoFinanceiro = Boolean.FALSE;
        }
        return permitirProcessoFinanceiro;
    }

    public void setPermitirProcessoFinanceiro(Boolean permitirProcessoFinanceiro) {
        this.permitirProcessoFinanceiro = permitirProcessoFinanceiro;
    }

    public Departamento getDepartamentoEstoque() {
        return departamentoEstoque;
    }

    public void setDepartamentoEstoque(Departamento departamentoEstoque) {
        this.departamentoEstoque = departamentoEstoque;
    }

    @Override
    public String toString() {
        return getDescricao();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/gestao/";
    }
}
