package br.com.gleac.entidade.configuracao;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.ProdutoControlador;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Audited
@CRUD(label = "Configuração de Produto", plural = "Configuração de Produtos")
public class ConfiguracaoProduto extends AbstractEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição da Configuração do Produto")
    private String descricao;

    @CRUD(label = "Descrição da Lei Orçamentária", obrigatorio = false)
    private String lei;

    @CRUD(label = "Produto", pesquisavelAutoComplete = true, controlador = ProdutoControlador.class)
    @ManyToOne
    private Produto produto;

    @CRUD(label = "Data Inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;

    @CRUD(label = "Data Final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;

    @CRUD(label = "Permite Parcelamento")
    private Boolean parcelamento;

    public ConfiguracaoProduto(String descricao, Produto produto, BigDecimal valor) {
        this.descricao = descricao;
        this.produto = produto;
        this.valor = BigDecimal.ZERO;
    }

    public ConfiguracaoProduto() {
        this.valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public String getLei() {
        return lei;
    }

    public void setLei(String lei) {
        this.lei = lei;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Boolean getParcelamento() {
        if (parcelamento == null) {
            parcelamento = Boolean.FALSE;
        }
        return parcelamento;
    }

    public void setParcelamento(Boolean parcelamento) {
        this.parcelamento = parcelamento;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/configuracao/produto/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
