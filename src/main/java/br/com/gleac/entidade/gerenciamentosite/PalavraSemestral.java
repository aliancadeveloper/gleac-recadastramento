package br.com.gleac.entidade.gerenciamentosite;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.exception.ExceptionGenerica;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by renatoromanini on 26/05/16.
 */
@Entity
@Audited
@CRUD(label = "Palavra Semestral", sexo = "M", plural = "Palavras Semestral")
public class PalavraSemestral extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Palavra", pesquisavelAutoComplete = true)
    private String palavra;
    @CRUD(label = "Data Inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;
    @CRUD(label = "Data Final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;
    @CRUD(label = "Código", tabelavel = false, pesquisavel = false, obrigatorio = false)
    @Type(type = "uuid-char")
    private UUID codigo = UUID.randomUUID();


    public PalavraSemestral() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public UUID getCodigo() {
        return codigo;
    }

    public void setCodigo(UUID codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/palavra-semestral/";
    }

    @Override
    public String toString() {
        return palavra + " vigência : " + Util.sdf.format(getDataInicial()) + " e " + Util.sdf.format(getDataFinal());
    }

    @Override
    public void validarRegrasPrincipais(){
        super.validarRegrasPrincipais();
        if (getDataFinal().before(getDataInicial())) {
            throw new ExceptionGenerica("A Data final não pode ser inferior a data inicial.");
        }
    }
}
