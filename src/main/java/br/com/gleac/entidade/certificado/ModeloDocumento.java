package br.com.gleac.entidade.certificado;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoModeloDocumento;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by renatoromanini on 26/05/16.
 */
@Entity
@Audited
@CRUD(label = "Modelo de Documento", sexo = "M", plural = "Modelos de Documentos")
public class ModeloDocumento extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Nome",pesquisavelAutoComplete = true)
    private String nome;
    @CRUD(label = "Tipo de Modelo de Documento")
    @Enumerated(EnumType.STRING)
    private TipoModeloDocumento tipoModeloDocumento;
    @CRUD(label = "Conteudo",pesquisavel = false,visualizavel = false,tabelavel = false)
    @Lob
    private String conteudo;

    public ModeloDocumento() {
    }

    public ModeloDocumento(String nome, TipoModeloDocumento tipoModeloDocumento, String conteudo) {
        this.nome = nome;
        this.tipoModeloDocumento = tipoModeloDocumento;
        this.conteudo = conteudo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoModeloDocumento getTipoModeloDocumento() {
        return tipoModeloDocumento;
    }

    public void setTipoModeloDocumento(TipoModeloDocumento tipoModeloDocumento) {
        this.tipoModeloDocumento = tipoModeloDocumento;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/modelo-documento/";
    }

}
