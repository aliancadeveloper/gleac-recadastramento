package br.com.gleac.entidade.estoque;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Entrada de Estoque", sexo = "F", plural = "Entradas de Estoques")
public class ItemEntradaEstoque extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "entrada-estoque/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Entrada")
    private EntradaEstoque entradaEstoque;
    @ManyToOne
    @CRUD(label = "Produto")
    private Produto produto;
    @CRUD(label = "Quantidade")
    private Integer quantidade;

    public ItemEntradaEstoque() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntradaEstoque getEntradaEstoque() {
        return entradaEstoque;
    }

    public void setEntradaEstoque(EntradaEstoque entradaEstoque) {
        this.entradaEstoque = entradaEstoque;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }
}
