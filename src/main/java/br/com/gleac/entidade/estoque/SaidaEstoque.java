package br.com.gleac.entidade.estoque;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 22/07/17.
 */

@Audited
@Entity
@CRUD(label = "Saída de Estoque", sexo = "F", plural = "Saídas de Estoques")
public class SaidaEstoque extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "saida-estoque/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date data;
    @ManyToOne
    @CRUD(label = "Departamento")
    private Departamento departamento;
    @CRUD(label = "Observação", obrigatorio = false)
    private String observacao;

    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data de retirada", obrigatorio = false)
    private Date retiradaEm;
    @CRUD(label = "Retirada Por", obrigatorio = false)
    private String retiradaPor;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "saidaEstoque", orphanRemoval = true)
    private List<ItemSaidaEstoque> itens;


    public SaidaEstoque() {
        itens = Lists.newArrayList();
        data = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public List<ItemSaidaEstoque> getItens() {
        return itens;
    }

    public void setItens(List<ItemSaidaEstoque> itens) {
        this.itens = itens;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getRetiradaEm() {
        return retiradaEm;
    }

    public void setRetiradaEm(Date retiradaEm) {
        this.retiradaEm = retiradaEm;
    }

    public String getRetiradaPor() {
        return retiradaPor;
    }

    public void setRetiradaPor(String retiradaPor) {
        this.retiradaPor = retiradaPor;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return departamento.toString() + " - " + Util.sdf.format(getData());
    }
}
