package br.com.gleac.entidade.estoque;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by renatoromanini on 14/08/17.
 */

@Audited
@Entity
@CRUD(label = "Saída de Estoque", sexo = "F", plural = "Saídas de Estoques")
public class ItemSaidaEstoque extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "saida-estoque/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @CRUD(label = "Saída")
    private SaidaEstoque saidaEstoque;
    @ManyToOne
    @CRUD(label = "Produto")
    private Produto produto;
    @CRUD(label = "Quantidade")
    private Integer quantidade;

    public ItemSaidaEstoque() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SaidaEstoque getSaidaEstoque() {
        return saidaEstoque;
    }

    public void setSaidaEstoque(SaidaEstoque saidaEstoque) {
        this.saidaEstoque = saidaEstoque;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }
}
