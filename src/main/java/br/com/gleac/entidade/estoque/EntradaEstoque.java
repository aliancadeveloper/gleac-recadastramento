package br.com.gleac.entidade.estoque;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 14/08/15.
 */

@Audited
@Entity
@CRUD(label = "Entrada de Estoque", sexo = "F", plural = "Entradas de Estoques")
public class EntradaEstoque extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "entrada-estoque/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data de Entrada")
    private Date data;
    @ManyToOne
    @CRUD(label = "Departamento")
    private Departamento departamento;
    @CRUD(label = "Observação", obrigatorio = false, tabelavel = false)
    private String observacao;
    @CRUD(label = "Descrição")
    private String descricao;
    @CRUD(label = "Quantidade")
    private Integer quantidade;
    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;
    @CRUD(label = "Número da Nota", obrigatorio = false)
    private String numeroNota;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entradaEstoque", orphanRemoval = true)
    private List<ItemEntradaEstoque> itens;


    public EntradaEstoque() {
        itens = Lists.newArrayList();
        data = new Date();
        valor = BigDecimal.ZERO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public List<ItemEntradaEstoque> getItens() {
        return itens;
    }

    public void setItens(List<ItemEntradaEstoque> itens) {
        this.itens = itens;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return "Nota Nº " + getNumeroNota();
    }

    public void somarQuantidadeTotal() {
        this.quantidade = 0;
        for (ItemEntradaEstoque itemEntradaEstoque : getItens()) {
            this.quantidade = quantidade + itemEntradaEstoque.getQuantidade();
        }
    }
}
