package br.com.gleac.entidade.estoque;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 08/07/17.
 */
@Audited
@Entity
@CRUD(label = "Saldo Estoque", sexo = "F", plural = "Saldos estoques")
public class SaldoEstoque extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "saldo-estoque/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @ManyToOne
    @CRUD(label = "Produto")
    private Produto produto;
    @ManyToOne
    @CRUD(label = "Departamento")
    private Departamento departamento;
    @CRUD(label = "Quantidade")
    private Integer quantidade;

    public SaldoEstoque() {
        quantidade = 0;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return quantidade + " - " + produto;
    }
}
