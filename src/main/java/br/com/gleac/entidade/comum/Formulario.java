package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by zaca.
 */
@Entity
@Audited
@CRUD(label = "Formulário ", plural = "Formulários")
public class Formulario extends AbstractEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição")
    private String descricao;

    public Formulario(String descricao) {
        this.descricao = descricao;
    }

    public Formulario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return getDescricao();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/formulario/";
    }
}
