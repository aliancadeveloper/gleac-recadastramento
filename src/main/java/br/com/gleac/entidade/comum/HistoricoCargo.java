package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoCargoExercido;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by israeleriston on 12/07/16.
 */
@Entity
@Audited
@CRUD(label = "Histórico de Cargo", plural = "Histórico de Cargos")
public class HistoricoCargo extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Tipo de Cargo Exercido")
    @Enumerated(EnumType.STRING)
    private TipoCargoExercido tipoCargoExercido;

    @CRUD(label = "Inicio de Vigência", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date inicioEm;

    @CRUD(label = "Final de Vigência", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date fimEm;

    @ManyToOne
    private Macon macon;

    @CRUD(label = "Cargo ")
    @ManyToOne
    private Cargo cargo;

    public HistoricoCargo(TipoCargoExercido tipoCargoExercido, Date inicioEm, Date fimEm, Macon macon, Cargo cargo) {
        this.tipoCargoExercido = tipoCargoExercido;
        this.inicioEm = inicioEm;
        this.fimEm = fimEm;
        this.macon = macon;
        this.cargo = cargo;
    }

    public HistoricoCargo() {
        this.cargo = new Cargo();
        this.macon = new Macon();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoCargoExercido getTipoCargoExercido() {
        return tipoCargoExercido;
    }

    public void setTipoCargoExercido(TipoCargoExercido tipoCargoExercido) {
        this.tipoCargoExercido = tipoCargoExercido;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Date getInicioEm() {
        return inicioEm;
    }

    public void setInicioEm(Date inicioEm) {
        this.inicioEm = inicioEm;
    }

    public Date getFimEm() {
        return fimEm;
    }

    public void setFimEm(Date fimEm) {
        this.fimEm = fimEm;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/historico-de-cargo/";
    }
}
