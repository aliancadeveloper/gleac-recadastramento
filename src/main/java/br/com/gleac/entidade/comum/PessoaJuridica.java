package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.seguranca.Pessoa;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 17/05/16.
 */
@Entity
@Audited
@CRUD(sexo = "F", label = "Pessoa Jurídica", plural = "Pessoas Jurídicas")
public class PessoaJuridica extends Pessoa implements Serializable {

    @CRUD(label = "Nome Fantasia", pesquisavelAutoComplete = true)
    private String nomeFantasia;
    @CRUD(label = "Razão Social", pesquisavelAutoComplete = true, obrigatorio = false)
    private String razaoSocial;
    @CRUD(label = "CNPJ", pesquisavelAutoComplete = true, obrigatorio = false)
    private String cnpj;
    @CRUD(label = "Inscrição Estadual", obrigatorio = false)
    private String inscricaoEstadual;
    @CRUD(label = "Email",obrigatorio = false, tabelavel = false)
    private String email;



    public PessoaJuridica(String nomeFantasia, String razaoSocial, String cnpj, String inscricaoEstadual, String email) {
        this.nomeFantasia = nomeFantasia;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.email = email;
        this.setEnderecos(new ArrayList<>());
        this.setTelefones(new ArrayList<>());
        this.setContas(new ArrayList<>());
    }

    public PessoaJuridica(List<Endereco> enderecos, List<Telefone> telefones, List<PessoaConta> contas) {
        super(enderecos, telefones, contas);
    }

    public PessoaJuridica() {
        this.setEnderecos(new ArrayList<>());
        this.setTelefones(new ArrayList<>());
        this.setContas(new ArrayList<>());
    }

    public PessoaJuridica(Long id, String nomeFantasia, String razaoSocial, String cnpj) {
        super(id);
        this.nomeFantasia = nomeFantasia;
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
    }

    @Override
    public String getNomePessoa() {
        return getNomeFantasia();
    }


    @Override
    public String getCpf_Cnpj() {
        return getCnpj();
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return getNomeFantasia() + " - " + getCnpj();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/pessoa-juridica/";
    }
}
