package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.EstadoControlador;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Cidade", plural = "Cidades")
public class Cidade extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Estado", pesquisavelAutoComplete = true, controlador = EstadoControlador.class)
    @ManyToOne
    private Estado estado;

    @CRUD(label = "Cidade", pesquisavelAutoComplete = true)
    private String nome;

    @OneToMany(mappedBy = "cidade", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Bairro> bairros;

    public Cidade(Estado estado, String nome, List<Bairro> bairros) {
        this.estado = estado;
        this.nome = nome;
        this.bairros = bairros;
    }

    public Cidade() {
        this.estado = new Estado();
        this.bairros = new ArrayList<>();
    }
    public Cidade(Long id, Estado estado, String nome) {
        this.id = id;
        this.estado = estado;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Bairro> getBairros() {
        return bairros;
    }

    public void setBairros(List<Bairro> bairros) {
        this.bairros = bairros;
    }

    @Override
    public String toString() {
        return getNome() + " - " + getEstado().getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/cidade/";
    }
}
