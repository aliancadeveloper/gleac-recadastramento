package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca on 06/10/16.
 */
@Entity
@Audited
@CRUD(label = "Loja", plural = "Lojas")
public class Loja extends AbstractEntity implements Serializable, Comparable<Loja> {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Número", pesquisavelAutoComplete = true)
    private String numero;

    @CRUD(label = "Nome", pesquisavelAutoComplete = true)
    private String nome;

    @OneToOne
    @CRUD(label = "Pessoa Jurídica")
    private PessoaJuridica pessoaJuridica;


    @CRUD(label = "Fundada em")
    @Temporal(TemporalType.DATE)
    private Date fundadaEm;

    @ManyToOne
    @CRUD(label = "Venerável Mestre")
    private Macon macon;

    @ManyToOne
    @CRUD(label = "Potência")
    private Potencia potencia;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loja", orphanRemoval = true)
    private List<Departamento> departamentos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loja", orphanRemoval = true)
    private List<LojaConta> contas;

    public Loja(PessoaJuridica pessoaJuridica, Potencia potencia) {
        this.pessoaJuridica = pessoaJuridica;
        this.potencia = potencia;
    }

    public Loja(Long id, String numero, String nome) {
        this.id = id;
        this.numero = numero;
        this.nome = nome;
    }

    public Loja() {
        departamentos = Lists.newArrayList();
        contas = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public Potencia getPotencia() {
        return potencia;
    }

    public void setPotencia(Potencia potencia) {
        this.potencia = potencia;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/loja/";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getFundadaEm() {
        return fundadaEm;
    }

    public void setFundadaEm(Date fundadaEm) {
        this.fundadaEm = fundadaEm;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public List<Departamento> getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public List<LojaConta> getContas() {
        return contas;
    }

    public void setContas(List<LojaConta> contas) {
        this.contas = contas;
    }

    @Override
    public String toString() {
        return this.getNumero() + " - " + this.getNome();
    }

    @Override
    public int compareTo(Loja o) {
        return Integer.valueOf(this.getNumero()).compareTo(Integer.valueOf(o.getNumero()));
    }
}
