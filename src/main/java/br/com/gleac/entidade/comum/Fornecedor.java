package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.PessoaControlador;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Audited
@CRUD(label = "Fornecedor", plural = "Fornecedores")
public class Fornecedor extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Fornecedor", pesquisavelAutoComplete = true, obrigatorio = false, controlador = PessoaControlador.class)
    @ManyToOne
    private Pessoa pessoa;

    @CRUD(label = "Contrato", pesquisavelAutoComplete = true)
    private String numeroContrato;

    @CRUD(label = "Serviço/Produto", pesquisavelAutoComplete = true)
    private String produto;

    @CRUD(label = "Data Inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;

    @CRUD(label = "Data Final", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fornecedor", orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<DocumentoFornecedor> documentosFornecedor;

    public Fornecedor() {
        this.documentosFornecedor = Lists.newArrayList();
    }

    public Fornecedor(Pessoa pessoa, String numeroContrato, String produto, Date dataInicial, Date dataFinal) {
        this.pessoa = pessoa;
        this.numeroContrato = numeroContrato;
        this.produto = produto;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.documentosFornecedor = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public List<DocumentoFornecedor> getDocumentosFornecedor() {
        return documentosFornecedor;
    }

    public void setDocumentosFornecedor(List<DocumentoFornecedor> documentosFornecedor) {
        this.documentosFornecedor = documentosFornecedor;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/fornecedor/";
    }

    @Override
    public String toString() {
        return getPessoa().toString();
    }
}
