package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Histórico do Obreiro")
public class HistoricoObreiro extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descriminação", obrigatorio = false)
    private String descriminacao;

    @CRUD(label = "Data histórico", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataHistorico;

    @CRUD(label = "Conferido Por ", obrigatorio = false)
    @ManyToOne
    private PessoaFisica conferidor;


    @ManyToOne
    private Macon macon;


    public HistoricoObreiro(String descriminacao, Date dataHistorico, PessoaFisica conferidor, Macon macon) {
        this.descriminacao = descriminacao;
        this.dataHistorico = dataHistorico;
        this.conferidor = conferidor;
        this.macon = macon;
    }

    public HistoricoObreiro() {
        this.macon = new Macon();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescriminacao() {
        return descriminacao;
    }

    public void setDescriminacao(String descriminacao) {
        this.descriminacao = descriminacao;
    }

    public Date getDataHistorico() {
        return dataHistorico;
    }

    public void setDataHistorico(Date dataHistorico) {
        this.dataHistorico = dataHistorico;
    }

    public PessoaFisica getConferidor() {
        return conferidor;
    }

    public void setConferidor(PessoaFisica conferidor) {
        this.conferidor = conferidor;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/historicoobreiro/";
    }
}
