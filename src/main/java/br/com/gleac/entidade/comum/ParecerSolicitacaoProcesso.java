package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.SolicitacaoProcessoControlador;
import br.com.gleac.entidade.processo.SolicitacaoProcesso;
import br.com.gleac.enums.processo.StatusParecer;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by zaca.
 */
@Entity
@Audited
@CRUD(label = "Parecer da Solicitação de Processo ")
public class ParecerSolicitacaoProcesso extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @CRUD(label = "Solicitação de Processo", pesquisavelAutoComplete = true, controlador = SolicitacaoProcessoControlador.class)
    private SolicitacaoProcesso solicitacaoProcesso;

    @CRUD(label = "Descrição do Parecer")
    private String descricao;

    @ManyToOne
    private Macon macon;

    @CRUD(label = "Status do Parecer")
    @Enumerated(EnumType.STRING)
    private StatusParecer statusParecer;

    @CRUD(label = "Emissão do Parecer")
    @Temporal(TemporalType.DATE)
    private Date emitidoEm;

    @CRUD(label = "Número do Parecer", obrigatorio = false)
    private Long numero;

    /*usado quando a gestão gera processo financeiro automatico*/
    @Transient
    private Date vencimentoBoleto;

    public ParecerSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso, Macon macon, StatusParecer statusParecer) {
        this.solicitacaoProcesso = solicitacaoProcesso;
        this.macon = macon;
        this.statusParecer = statusParecer;
    }

    public ParecerSolicitacaoProcesso() {
        this.setEmitidoEm(new Date());
        DateTime dateTime = new DateTime();
        DateTime vencimento = dateTime.plusDays(30);
        vencimentoBoleto = vencimento.toDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SolicitacaoProcesso getSolicitacaoProcesso() {
        return solicitacaoProcesso;
    }

    public void setSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso) {
        this.solicitacaoProcesso = solicitacaoProcesso;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public StatusParecer getStatusParecer() {
        return statusParecer;
    }

    public void setStatusParecer(StatusParecer statusParecer) {
        this.statusParecer = statusParecer;
    }

    public Date getEmitidoEm() {
        return emitidoEm;
    }

    public void setEmitidoEm(Date emitidoEm) {
        this.emitidoEm = emitidoEm;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getVencimentoBoleto() {
        return vencimentoBoleto;
    }

    public void setVencimentoBoleto(Date vencimentoBoleto) {
        this.vencimentoBoleto = vencimentoBoleto;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/parecer-solicitacao-processo/";
    }

    @Override
    public String toString() {
        if (solicitacaoProcesso != null) {
            return solicitacaoProcesso.toString();
        } else {
            return getSolicitacaoProcesso().getDescricao();
        }
    }
}
