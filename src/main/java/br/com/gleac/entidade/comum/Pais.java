package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by renatoromanini on 14/08/15.
 */
@Audited
@Entity
@CRUD(label = "País", sexo = "M", plural = "Países")
public class Pais extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "pais/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Nome")
    @Identificador
    private String nome;
    @Identificador
    @CRUD(label = "Sigla")
    private String sigla;
    @Identificador
    @CRUD(label = "Código")
    private String codigo;

    @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Estado> estados;

    public Pais(String nome, String sigla, String codigo, List<Estado> estados) {
        this.nome = nome;
        this.sigla = sigla;
        this.codigo = codigo;
        this.estados = estados;
    }

    public Pais() {
        this.estados = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return codigo + " - " + nome;
    }
}
