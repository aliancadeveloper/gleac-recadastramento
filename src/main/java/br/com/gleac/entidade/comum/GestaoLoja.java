package br.com.gleac.entidade.comum;

import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by zaca on 18/10/17.
 */
@Entity
@Audited
public class GestaoLoja extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date dataInicial;

    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    @ManyToOne
    private Macon macon;

    @ManyToOne
    private Gestao gestao;

    @ManyToOne
    private Loja loja;

    public GestaoLoja(Macon macon, Date dataInicial, Date dataFinal, Gestao gestao, Loja loja) {
        this.macon = macon;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.gestao = gestao;
        this.loja = loja;
    }

    public GestaoLoja() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public Gestao getGestao() {
        return gestao;
    }

    public void setGestao(Gestao gestao) {
        this.gestao = gestao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
