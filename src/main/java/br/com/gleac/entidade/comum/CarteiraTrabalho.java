package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by israeleriston on 02/09/16.
 */
@Entity
@Audited
@CRUD(label = "Documento de Trabalho", plural = "Documentos de Trabalho")
public class CarteiraTrabalho extends DocumentoPessoal implements Serializable {


    @ManyToOne
    private Profissao profissao;

    @ManyToOne
    private PessoaJuridica localTrabalho;

    @ManyToOne
    private Cargo cargo;

    @CRUD(label = "Data inicial", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date inicio;

    @CRUD(label = " Data Final", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date fim;


    public CarteiraTrabalho(Profissao profissao, PessoaJuridica localTrabalho, Cargo cargo) {
        this.profissao = profissao;
        this.localTrabalho = localTrabalho;
        this.cargo = cargo;
    }

    public CarteiraTrabalho() {
        this.cargo = new Cargo();
        this.localTrabalho = new PessoaJuridica();
        this.profissao = new Profissao();
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public PessoaJuridica getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(PessoaJuridica localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getFim() {
        return fim;
    }

    public void setFim(Date fim) {
        this.fim = fim;
    }


    @Override
    public String getCaminhoPadrao() {
        return "";
    }
}
