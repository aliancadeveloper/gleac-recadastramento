package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.persistence.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by renatoromanini on 24/05/16.
 */
@Entity
//@Audited(targetAuditMode = NOT_AUDITED)
@CRUD(label = "arquivo", sexo = "M", plural = "Arquivos")
public class Arquivo extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = "/arquivo/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Nome")
    private String nome;
    @CRUD(label = "Tipo")
    private String tipo;
    @Lob
    @CRUD(label = "Bytes", obrigatorio = false)
    private byte arquivo[];
    @Transient
    private StreamedContent streamedContent;

    public Arquivo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public byte[] getArquivo() {
        return arquivo;
    }

    public void setArquivo(byte[] arquivo) {
        this.arquivo = arquivo;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return nome;
    }

    public StreamedContent getStreamedContent() {
        if (streamedContent == null) {
            carregarImagem();
        }
        return streamedContent;
    }

    public void setStreamedContent(StreamedContent streamedContent) {
        this.streamedContent = streamedContent;
    }

    public void carregarImagem() {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        try {
            buffer.write(this.getArquivo());
        } catch (IOException ex) {
            Logger.getLogger(Arquivo.class.getName()).log(Level.SEVERE, null, ex);
        }

        InputStream is = new ByteArrayInputStream(buffer.toByteArray());
        this.streamedContent = new DefaultStreamedContent(is, this.getTipo(), this.getNome());
    }
}
