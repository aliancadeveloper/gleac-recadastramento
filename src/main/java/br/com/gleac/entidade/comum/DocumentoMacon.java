package br.com.gleac.entidade.comum;

import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

@Entity
@Audited
public class DocumentoMacon extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Macon macon;

    @OneToOne(cascade = CascadeType.ALL)
    @NotAudited
    private Arquivo arquivo;

    public DocumentoMacon() {
    }

    public DocumentoMacon(Macon macon, Arquivo arquivo) {
        this.macon = macon;
        this.arquivo = arquivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
