package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.financeiro.ContaCorrente;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by zaca on 06/10/16.
 */
@Entity
@Audited
@CRUD(label = "Loja", plural = "Lojas")
public class LojaConta extends AbstractEntity implements Serializable{

    @Id
    @GeneratedValue
    private Long id;


    @ManyToOne
    @CRUD(label = "Loja")
    private Loja loja;

    @ManyToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Conta")
    private ContaCorrente contaCorrente;

    public LojaConta() {

    }

    public LojaConta(Loja selecionado, ContaCorrente contaCorrente) {
        this.loja = selecionado;
        this.contaCorrente = contaCorrente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/loja/";
    }


    @Override
    public String toString() {
        return this.contaCorrente.toString();
    }
}
