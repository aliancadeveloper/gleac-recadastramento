package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.Escolaridade;
import br.com.gleac.enums.TipoSanguineo;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Conjugê")
public class Conjuge extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Nome do Cônjuge")
    private String nome;

    @CRUD(label = "Data de Nascimento", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @CRUD(label = "Tipo Sanguineo")
    @Enumerated(EnumType.STRING)
    private TipoSanguineo tipoSanguineo;

    @CRUD(label = "Escolaridade ", obrigatorio = false)
    @Enumerated(EnumType.STRING)
    private Escolaridade escolaridade;

    @CRUD(label = "Área de Formação", obrigatorio = false, pesquisavelAutoComplete = true)
    @OneToOne
    private AreaFormacao areaFormacao;

    @CRUD(label = "Profissão", obrigatorio = false)
    @OneToOne
    private Profissao profissao;

    @CRUD(label = "Local de Trabalho", obrigatorio = false)
    private String localTrabalho;

    @CRUD(label = "Casado ", obrigatorio = false)
    private Boolean casado;

    @ManyToOne
    private PessoaFisica pessoaFisica;


    public Conjuge(String nome, Date dataNascimento, TipoSanguineo tipoSanguineo, Escolaridade escolaridade, AreaFormacao areaFormacao, Profissao profissao, String localTrabalho, Boolean casado, PessoaFisica pessoaFisica) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.tipoSanguineo = tipoSanguineo;
        this.escolaridade = escolaridade;
        this.areaFormacao = areaFormacao;
        this.profissao = profissao;
        this.localTrabalho = localTrabalho;
        this.casado = casado;
        this.pessoaFisica = pessoaFisica;
    }

    public Conjuge() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public TipoSanguineo getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public Escolaridade getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(Escolaridade escolaridade) {
        this.escolaridade = escolaridade;
    }

    public AreaFormacao getAreaFormacao() {
        return areaFormacao;
    }

    public void setAreaFormacao(AreaFormacao areaFormacao) {
        this.areaFormacao = areaFormacao;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public String getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(String localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Boolean getCasado() {
        return casado;
    }

    public void setCasado(Boolean casado) {
        this.casado = casado;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/conjuge/";
    }
}
