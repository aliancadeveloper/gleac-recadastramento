package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.TipoTelefone;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Telefone", plural = "Telefones")
public class Telefone extends AbstractEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Pessoa pessoa;

    @CRUD(label = "Telefone")
    private String telefone;

    @CRUD(label = "Tipo do Telefone")
    @Enumerated(EnumType.STRING)
    private TipoTelefone tipoFone;

    @CRUD(label = "Principal ", obrigatorio = false)
    private Boolean principal;


    @CRUD(label = "Pessoa para Contato " , obrigatorio = false)
    private String pessoaContato;

    public Telefone(Pessoa pessoa, String telefone, TipoTelefone tipoFone, Boolean principal, String pessoaContato) {
        this.pessoa = pessoa;
        this.telefone = telefone;
        this.tipoFone = tipoFone;
        this.principal = Boolean.FALSE;
        this.pessoaContato = pessoaContato;
    }

    public Telefone() {
        this.principal = Boolean.FALSE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public TipoTelefone getTipoFone() {
        return tipoFone;
    }

    public void setTipoFone(TipoTelefone tipoFone) {
        this.tipoFone = tipoFone;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    public String getPessoaContato() {
        return pessoaContato;
    }

    public void setPessoaContato(String pessoaContato) {
        this.pessoaContato = pessoaContato;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/telefone/";
    }
}
