package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.CidadeControlador;
import br.com.gleac.controlador.NacionalidadeControlador;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.EstadoCivil;
import br.com.gleac.enums.RacaCor;
import br.com.gleac.enums.Sexo;
import br.com.gleac.enums.TipoSanguineo;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Pessoa Física", plural = "Pessoas Físicas")
public class PessoaFisica extends Pessoa implements Serializable {

    @CRUD(label = "CPF", pesquisavelAutoComplete = true)
    private String cpf;

    @CRUD(label = "Nome ", pesquisavelAutoComplete = true)
    private String nome;

    @CRUD(label = "Data de Nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @CRUD(label = "Aposentado", obrigatorio = false, tabelavel = false, pesquisavel = false)
    private Boolean aposentado;

    @CRUD(label = "Doador de Orgão ", obrigatorio = false, tabelavel = false, pesquisavel = false)
    private Boolean doadorOrgao;

    @CRUD(label = "Nome do Pai", obrigatorio = false, tabelavel = false)
    private String pai;

    @CRUD(label = "Nome da Mãe", obrigatorio = false, tabelavel = false)
    private String mae;

    @CRUD(label = "Sexo", tabelavel = false)
    @Enumerated(EnumType.STRING)
    private Sexo sexo;

    @CRUD(label = "Raça", tabelavel = false)
    @Enumerated(EnumType.STRING)
    private RacaCor racaCor;

    @CRUD(label = "Estado Civil", tabelavel = false)
    @Enumerated(EnumType.STRING)
    private EstadoCivil estadoCivil;


    @CRUD(label = "Tipo Sanguineo", obrigatorio = false, tabelavel = false)
    @Enumerated(EnumType.STRING)
    private TipoSanguineo tipoSanguineo;

    @CRUD(label = "Naturalidade", controlador = CidadeControlador.class)
    @ManyToOne
    private Cidade cidade;


    @CRUD(label = "Nacionalidade", tabelavel = false, controlador = NacionalidadeControlador.class)
    @ManyToOne
    private Nacionalidade nacionalidade;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaFisica", orphanRemoval = true)
    private List<DocumentoPessoal> documentosPessoais;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaFisica", orphanRemoval = true)
    private List<AreaFormacao> formacoes;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaFisica", orphanRemoval = true)
    private List<Filho> filhos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaFisica", orphanRemoval = true)
    private List<Conjuge> conjuges;

    public PessoaFisica(List<Endereco> enderecos, List<Telefone> telefones, List<PessoaConta> contas, String cpf, String nome, Date dataNascimento, Boolean aposentado, Boolean doadorOrgao, String pai, String mae, Sexo sexo, RacaCor racaCor, EstadoCivil estadoCivil, TipoSanguineo tipoSanguineo, Cidade cidade, Nacionalidade nacionalidade, List<DocumentoPessoal> documentosPessoais, List<AreaFormacao> formacoes, List<Filho> filhos, List<Conjuge> conjuges) {
        super(enderecos, telefones, contas);
        this.cpf = cpf;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.aposentado = aposentado;
        this.doadorOrgao = doadorOrgao;
        this.pai = pai;
        this.mae = mae;
        this.sexo = sexo;
        this.racaCor = racaCor;
        this.estadoCivil = estadoCivil;
        this.tipoSanguineo = tipoSanguineo;
        this.cidade = cidade;
        this.nacionalidade = nacionalidade;
        this.documentosPessoais = documentosPessoais;
        this.formacoes = formacoes;
        this.filhos = filhos;
        this.conjuges = conjuges;
    }

    public PessoaFisica() {
        this.setEnderecos(new ArrayList<>());
        this.setTelefones(new ArrayList<>());
        this.setContas(new ArrayList<>());
        this.cidade = new Cidade();
        this.nacionalidade = new Nacionalidade();
        this.documentosPessoais = new ArrayList<>();
        this.formacoes = new ArrayList<>();
        this.filhos = new ArrayList<>();
        this.conjuges = new ArrayList<>();
    }

    public PessoaFisica(Long id) {
        this.setId(id);
    }

    public PessoaFisica(Long id, String cpf, String nome) {
        super(id);
        this.cpf = cpf;
        this.nome = nome;
    }

    @Override
    public String getNomePessoa() {
        return getNome();
    }

    @Override
    public String getCpf_Cnpj() {
        return getCpf();
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public RacaCor getRacaCor() {
        return racaCor;
    }

    public void setRacaCor(RacaCor racaCor) {
        this.racaCor = racaCor;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Nacionalidade getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(Nacionalidade nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public TipoSanguineo getTipoSanguineo() {
        return tipoSanguineo;
    }

    public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
        this.tipoSanguineo = tipoSanguineo;
    }

    public Boolean getAposentado() {
        return aposentado;
    }

    public void setAposentado(Boolean aposentado) {
        this.aposentado = aposentado;
    }

    public Boolean getDoadorOrgao() {
        return doadorOrgao;
    }

    public void setDoadorOrgao(Boolean doadorOrgao) {
        this.doadorOrgao = doadorOrgao;
    }

    public List<DocumentoPessoal> getDocumentosPessoais() {
        return documentosPessoais;
    }

    public void setDocumentosPessoais(List<DocumentoPessoal> documentosPessoais) {
        this.documentosPessoais = documentosPessoais;
    }

    public List<AreaFormacao> getFormacoes() {
        return formacoes;
    }

    public void setFormacoes(List<AreaFormacao> formacoes) {
        this.formacoes = formacoes;
    }

    public List<Filho> getFilhos() {
        return filhos;
    }

    public void setFilhos(List<Filho> filhos) {
        this.filhos = filhos;
    }

    public List<Conjuge> getConjuges() {
        return conjuges;
    }

    public void setConjuges(List<Conjuge> conjuges) {
        this.conjuges = conjuges;
    }

    @Override
    public String toString() {
        return getNome() + " - " + getCpf();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/pessoa-fisica/";
    }


}
