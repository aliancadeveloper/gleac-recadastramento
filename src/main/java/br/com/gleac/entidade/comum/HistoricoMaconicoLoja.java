package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 31/08/16.
 */
@Entity
@Audited
@CRUD(label = "Historico Maconico de Loja", plural = "Histórico Maçonico De Lojas ")
public class HistoricoMaconicoLoja extends AbstractEntity implements Serializable {


    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @CRUD(label = " Loja ")
    private PessoaJuridica loja;

    @CRUD(label = " Data de Mudança")
    private Date dataMudanca;

    @CRUD(label = "Potência ")
    private String potencia;

    @ManyToOne
    private Macon macon;

    public HistoricoMaconicoLoja(PessoaJuridica loja, Date dataMudanca, String potencia, Macon macon) {
        this.loja = loja;
        this.dataMudanca = dataMudanca;
        this.potencia = potencia;
        this.macon = macon;
    }

    public HistoricoMaconicoLoja() {
        this.loja = new PessoaJuridica();
        this.macon = new Macon();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaJuridica getLoja() {
        return loja;
    }

    public void setLoja(PessoaJuridica loja) {
        this.loja = loja;
    }

    public Date getDataMudanca() {
        return dataMudanca;
    }

    public void setDataMudanca(Date dataMudanca) {
        this.dataMudanca = dataMudanca;
    }

    public String getPotencia() {
        return potencia;
    }

    public void setPotencia(String potencia) {
        this.potencia = potencia;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    @Override
    public String getCaminhoPadrao() {
        return "";
    }
}
