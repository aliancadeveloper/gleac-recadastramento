package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.Sexo;
import br.com.gleac.enums.TipoAssociacao;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Filho", plural = "Filhos")
public class Filho extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Nome do Filho")
    private String nome;

    @CRUD(label = "Data de Nascimento", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @CRUD(label = "Sexo")
    @Enumerated(EnumType.STRING)
    private Sexo sexo;

    @CRUD(label = "Matricula ", obrigatorio = false)
    private String matricula;

    @CRUD(label = "Ordens Pertencentes", obrigatorio = false)
    private TipoAssociacao tipoAssociacao;

    @ManyToOne
    private PessoaFisica pessoaFisica;

    public Filho(String nome, Date dataNascimento, Sexo sexo, String matricula, TipoAssociacao tipoAssociacao, PessoaFisica pessoaFisica) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.matricula = matricula;
        this.tipoAssociacao = tipoAssociacao;
        this.pessoaFisica = pessoaFisica;
    }

    public Filho() {
       this.pessoaFisica = new PessoaFisica();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public TipoAssociacao getTipoAssociacao() {
        return tipoAssociacao;
    }

    public void setTipoAssociacao(TipoAssociacao tipoAssociacao) {
        this.tipoAssociacao = tipoAssociacao;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/filho/";
    }
}
