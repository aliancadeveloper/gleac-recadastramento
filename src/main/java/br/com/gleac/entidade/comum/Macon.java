package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.anotacao.Identificador;
import br.com.gleac.controlador.PessoaFisicaControlador;
import br.com.gleac.enums.Grau;
import br.com.gleac.enums.Situacao;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca on 05/09/16.
 */
@Entity
@Audited
@CRUD(label = "Maçom", plural = "Maçons")
public class Macon extends AbstractEntity implements Serializable, Comparable<Macon> {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Código Cadastral")
    @Identificador
    private Integer identificacao;

    @OneToOne
    @CRUD(label = " Pessoa Física", pesquisavelAutoComplete = true, controlador = PessoaFisicaControlador.class)
    private PessoaFisica pessoaFisica;

    @CRUD(label = "Grau")
    @Enumerated(EnumType.STRING)
    private Grau grau;

    @CRUD(label = "Situação", obrigatorio = false)
    @Enumerated(EnumType.STRING)
    private Situacao situacao;

    @CRUD(label = "Licenciamento sem ônus", obrigatorio = false, tabelavel = false)
    private Boolean licenciamento;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "macon", orphanRemoval = true)
    private List<HistoricoObreiro> historicosObreiro;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "macon", orphanRemoval = true)
    private List<HistoricoMaconico> historicoMaconicos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "macon", orphanRemoval = true)
    private List<HistoricoCargo> historicoCargos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "macon", orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<DocumentoMacon> documentosMacon;

    @NotAudited
    @OneToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Imagem", pesquisavel = false, tabelavel = false)
    private Arquivo arquivo;


    public Macon(Integer identificacao, PessoaFisica pessoaFisica, Situacao situacao, List<HistoricoObreiro> historicosObreiro, List<HistoricoMaconico> historicoMaconicos, List<HistoricoMaconicoLoja> historicoMaconicoLoja, List<HistoricoCargo> historicoCargos, List<DocumentoMacon> documentosMacon) {
        this.identificacao = identificacao;
        this.pessoaFisica = pessoaFisica;
        this.situacao = situacao;
        this.historicoMaconicos = historicoMaconicos;
        this.historicoCargos = historicoCargos;
        this.documentosMacon = documentosMacon;
    }

    public Macon() {
        this.pessoaFisica = new PessoaFisica();
        this.historicosObreiro = new ArrayList<>();
        this.historicoMaconicos = new ArrayList<>();
        this.historicoCargos = new ArrayList<>();
        this.documentosMacon = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(Integer identificacao) {
        this.identificacao = identificacao;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }


    public List<HistoricoObreiro> getHistoricosObreiro() {
        return historicosObreiro;
    }

    public void setHistoricosObreiro(List<HistoricoObreiro> historicosObreiro) {
        this.historicosObreiro = historicosObreiro;
    }

    public List<HistoricoMaconico> getHistoricoMaconicos() {
        return historicoMaconicos;
    }

    public void setHistoricoMaconicos(List<HistoricoMaconico> historicoMaconicos) {
        this.historicoMaconicos = historicoMaconicos;
    }

    public List<HistoricoCargo> getHistoricoCargos() {
        return historicoCargos;
    }

    public void setHistoricoCargos(List<HistoricoCargo> historicoCargos) {
        this.historicoCargos = historicoCargos;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public Grau getGrau() {
        return grau;
    }

    public void setGrau(Grau grau) {
        this.grau = grau;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public List<DocumentoMacon> getDocumentosMacon() {
        return documentosMacon;
    }

    public void setDocumentosMacon(List<DocumentoMacon> documentosMacon) {
        this.documentosMacon = documentosMacon;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public Boolean getLicenciamento() {
        if (licenciamento == null) {
            licenciamento = Boolean.FALSE;
        }
        return licenciamento;
    }

    public void setLicenciamento(Boolean licenciamento) {
        this.licenciamento = licenciamento;
    }

    @Override
    public String toString() {
        return identificacao + " - " + this.getPessoaFisica().getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/macon/";
    }

    public Loja getUltimaLojaInterna() {
        Loja ultimaLoja = null;
        Date ultimoHistorico = null;
        for (HistoricoMaconico historicoMaconico : historicoMaconicos) {
            if (ultimaLoja == null) {
                ultimaLoja = historicoMaconico.getLoja();
                ultimoHistorico = historicoMaconico.getDataHistorico();
            } else {
                if (historicoMaconico.getDataHistorico().after(ultimoHistorico)) {
                    ultimaLoja = historicoMaconico.getLoja();
                    ultimoHistorico = historicoMaconico.getDataHistorico();
                }
            }
        }
        return ultimaLoja;
    }

    @Override
    public int compareTo(Macon o) {
        return getPessoaFisica().getNomePessoa().compareTo(o.getPessoaFisica().getNomePessoa());
    }
}
