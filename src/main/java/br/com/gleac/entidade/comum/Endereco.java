package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.TipoEndereco;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Endereço", plural = "Endereços")
public class Endereco extends AbstractEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Tipo do Endereço")
    @Enumerated(EnumType.STRING)
    private TipoEndereco tipoEndereco;

    @CRUD(label = "CEP", obrigatorio = false)
    private String cep;

    @CRUD(label = "Logradouro")
    private String logradouro;

    @CRUD(label = "Número", obrigatorio = false)
    private String numero;

    @CRUD(label = "Complemento", obrigatorio = false)
    private String complemento;

    @ManyToOne
    private Pessoa pessoa;

    @CRUD(label = "Cidade")
    @ManyToOne
    private Cidade cidade;

    @CRUD(label = "Bairro")
    @ManyToOne
    private Bairro bairro;

    public Endereco(TipoEndereco tipoEndereco, String cep, String logradouro, String numero, String complemento, Pessoa pessoa, Bairro bairro, Cidade cidade) {
        this.tipoEndereco = tipoEndereco;
        this.cep = cep;
        this.logradouro = logradouro;
        this.numero = numero;
        this.complemento = complemento;
        this.pessoa = pessoa;
        this.cidade = cidade;
        this.bairro = bairro;
    }

    public Endereco() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEndereco getTipoEndereco() {
        return tipoEndereco;
    }

    public void setTipoEndereco(TipoEndereco tipoEndereco) {
        this.tipoEndereco = tipoEndereco;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/endereco/";
    }
}
