package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Profissão")
public class Profissao extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Nome da Profissão")
    private String nome;


    public Profissao(String nome) {
        this.nome = nome;
    }

    public Profissao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/profissao/";
    }
}
