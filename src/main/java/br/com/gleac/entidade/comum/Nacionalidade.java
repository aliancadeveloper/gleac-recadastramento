package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Nacionalidade", plural = "Nacionalidades")
public class Nacionalidade extends AbstractEntity{

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição")
    private String descricao;

    @CRUD(label = "Código")
    private Integer codigo;

    public Nacionalidade(String descricao, Integer codigo) {
        this.descricao = descricao;
        this.codigo = codigo;
    }

    public Nacionalidade() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return getDescricao();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/nacionalidade/";
    }
}
