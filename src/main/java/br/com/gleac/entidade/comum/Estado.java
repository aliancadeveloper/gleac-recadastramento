package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Estado", plural = "Estados")
public class Estado extends AbstractEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "País")
    @ManyToOne
    private Pais pais;

    @CRUD(label = "Estado")
    private String nome;

    @CRUD(label = "Sigla")
    private String sigla;

    @OneToMany(mappedBy = "estado", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Cidade> cidades;

    public Estado(Pais pais, String nome, List<Cidade> cidades) {
        this.pais = pais;
        this.nome = nome;
        this.cidades = cidades;
    }

    public Estado() {
        this.pais = new Pais();
        this.cidades = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @Override
    public String toString() {
        return getNome() + " - " + getPais().getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/estado/";
    }
}
