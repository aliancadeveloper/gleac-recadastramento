package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.InputStream;
import java.io.Serializable;

/**
 * Created by Edi on 30/07/2017.
 */
@Entity
@Audited
@Document(indexName = "configuracaoCabecalho")
@CRUD(label = "Configuração Cabeçalho", plural = "Configurações Cabeçalhos", sexo = "M")
public class ConfiguracaoCabecalho extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "configuracao-cabecalho/";
    @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Título", pesquisavelAutoComplete = true)
    private String titulo;

    @CRUD(label = "Subtítulo", pesquisavelAutoComplete = true)
    private String subTitulo;

    @NotAudited
    @CRUD(label = "Imagem", obrigatorio = false)
    @OneToOne(cascade = CascadeType.ALL)
    private Arquivo arquivo;

    public ConfiguracaoCabecalho() {
    }

    public ConfiguracaoCabecalho(Long id) {
        this.id = id;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubTitulo() {
        return subTitulo;
    }

    public void setSubTitulo(String subTitulo) {
        this.subTitulo = subTitulo;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }

    @Override
    public String toString() {
        return getTitulo();
    }

    public InputStream getLogoInputStream() {
        if (this.arquivo != null) {
            return new Util().getImagemInputStream(this.arquivo);
        }
        return null;
    }

}
