package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Passaporte")
public class Passaporte extends DocumentoPessoal implements Serializable {

    @Id
    @CRUD(label = "Número do Passaporte", obrigatorio = false)
    private String numero;

    @ManyToOne
    @CRUD(label = "Pais de Origem", obrigatorio = false)
    private Pais pais;

    @CRUD(label = "Data de Expedição", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataExpedicao;

    @CRUD(label = "Data de Validade", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date validade;


    public Passaporte(String numero, Pais paisOrigem, Date dataExpedicao, Date validade) {
        this.numero = numero;
        this.pais = paisOrigem;
        this.dataExpedicao = dataExpedicao;
        this.validade = validade;
    }

    public Passaporte() {
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Pais getPaisOrigem() {
        return pais;
    }

    public void setPaisOrigem(Pais paisOrigem) {
        this.pais = paisOrigem;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }
}
