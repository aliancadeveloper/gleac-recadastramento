package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "RG")
public class RG extends DocumentoPessoal implements Serializable {

    @Id
    @CRUD(label = "Número RG" , obrigatorio = false)
    private String numero;

    @CRUD(label = "Orgão Expeditor " , obrigatorio = false)
    private String orgaoExpeditor;

    @CRUD(label = "Data Expedição", obrigatorio = false)
    private Date dataExpedicao;

    public RG(String numero, String orgaoExpeditor) {
        this.numero = numero;
        this.orgaoExpeditor = orgaoExpeditor;
    }

    public RG() {
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getOrgaoExpeditor() {
        return orgaoExpeditor;
    }

    public void setOrgaoExpeditor(String orgaoExpeditor) {
        this.orgaoExpeditor = orgaoExpeditor;
    }

    public Date getDataExpedicao() {
        return dataExpedicao;
    }

    public void setDataExpedicao(Date dataExpedicao) {
        this.dataExpedicao = dataExpedicao;
    }
}
