package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoServico;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by israeleriston on 26/06/17
 */
@Entity
@Audited
@CRUD(label = "Serviço")
public class Servico extends AbstractEntity {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição", pesquisavelAutoComplete = true)
    private String descricao;

    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;

    @CRUD(label = "Número", obrigatorio = false)
    private Integer numero;

    @Enumerated(EnumType.STRING)
    @CRUD(label = "Tipo de Serviço")
    private TipoServico tipoServico;

    public Servico(String descricao, Integer numero, TipoServico tipoServico) {
        this.descricao = descricao;
        this.valor = BigDecimal.ZERO;
        this.numero = numero;
        this.tipoServico = tipoServico;
    }

    public Servico() {
        this.valor = BigDecimal.ZERO;
    }

    public Servico(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Servico(Long id, String descricao, BigDecimal valor) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public TipoServico getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
    }

    public Boolean isServicoComum() {
        try {
            return TipoServico.COMUM.equals(this.getTipoServico()) || TipoServico.INICIACAO.equals(this.getTipoServico());
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/servico/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
