package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.Escolaridade;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Àrea de Formação", plural = "Àrea de Formações")
public class AreaFormacao extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Nome", obrigatorio = false)
    private String nome;

    @CRUD(label = "Escolaridade ", obrigatorio = false)
    @Enumerated(EnumType.STRING)
    private Escolaridade escolaridade;

    @CRUD(label = "Especializado ? ", obrigatorio = false)
    private Boolean especializacao;

    @CRUD(label = "Descricao Especialização", obrigatorio = false)
    private String descricaoEspecializacao;

    @ManyToOne
    private PessoaFisica pessoaFisica;

    public AreaFormacao(String nome, Escolaridade escolaridade, Boolean especializacao, String descricaoEspecializacao, PessoaFisica pessoaFisica) {
        this.nome = nome;
        this.escolaridade = escolaridade;
        this.especializacao = especializacao;
        this.descricaoEspecializacao = descricaoEspecializacao;
        this.pessoaFisica = pessoaFisica;
    }

    public AreaFormacao() {
       this.pessoaFisica = new PessoaFisica();
        this.especializacao = Boolean.FALSE;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Escolaridade getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(Escolaridade escolaridade) {
        this.escolaridade = escolaridade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getEspecializacao() {
        return especializacao;
    }

    public void setEspecializacao(Boolean especializacao) {
        this.especializacao = especializacao;
    }

    public String getDescricaoEspecializacao() {
        return descricaoEspecializacao;
    }

    public void setDescricaoEspecializacao(String descricaoEspecializacao) {
        this.descricaoEspecializacao = descricaoEspecializacao;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    @Override
    public String toString() {
        return getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/areaformacao/";
    }

}
