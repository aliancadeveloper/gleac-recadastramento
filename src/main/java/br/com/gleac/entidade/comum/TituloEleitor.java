package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Titulo Eleitor")
public class TituloEleitor extends DocumentoPessoal implements Serializable {

    @Id
    @CRUD(label = "Número Eleitor", obrigatorio = false)
    private String numero;

    @CRUD(label = "Sessão", obrigatorio = false)
    private String sessao;

    @CRUD(label = "Zona Eleitoral", obrigatorio = false)
    private String zona;

    @CRUD(label = "Data de Emissão", obrigatorio = false)
    @Temporal(TemporalType.DATE)
    private Date dataEmissao;

    public TituloEleitor(String numero, String sessao, String zona, Date dataEmissao) {
        this.numero = numero;
        this.sessao = sessao;
        this.zona = zona;
        this.dataEmissao = new Date();
    }

    public TituloEleitor() {
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSessao() {
        return sessao;
    }

    public void setSessao(String sessao) {
        this.sessao = sessao;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }
}
