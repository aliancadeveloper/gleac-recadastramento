package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoDocumentoSite;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by carlos on 28/09/16.
 */
@Entity
@Audited
@CRUD(label = "Documentos do Site", plural = "Documentos")
public class DocumentoSite extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Descrição")
    private String descricao;

    @Enumerated(EnumType.STRING)
    @CRUD(label = "Tipo de Documento")
    private TipoDocumentoSite tipoDocumentoSite;

    @ManyToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Arquivo", tabelavel = false, pesquisavel = false)
    @NotAudited
    private Arquivo arquivo;

    @CRUD(label = "Data de Publicação")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataPublicacao;

    @CRUD(label = "Código", tabelavel = false, pesquisavel = false, obrigatorio = false)
    @Type(type = "uuid-char")
    private UUID codigo = UUID.randomUUID();

    @Override
    public String getCaminhoPadrao() {
        return "/admin/documento-site/";
    }

    public DocumentoSite() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoDocumentoSite getTipoDocumentoSite() {
        return tipoDocumentoSite;
    }

    public void setTipoDocumentoSite(TipoDocumentoSite tipoDocumentoSite) {
        this.tipoDocumentoSite = tipoDocumentoSite;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public UUID getCodigo() {
        return codigo;
    }

    public void setCodigo(UUID codigo) {
        this.codigo = codigo;
    }
}
