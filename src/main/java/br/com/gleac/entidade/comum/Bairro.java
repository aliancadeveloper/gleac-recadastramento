package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by israeleriston on 28/05/16.
 */
@Entity
@Audited
@CRUD(label = "Bairro", plural = "Bairros")
public class Bairro extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CRUD(label = "Cidade")
    @ManyToOne
    private Cidade cidade;

    @CRUD(label = "Bairro")
    private String nome;

    @OneToMany(mappedBy = "bairro", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Endereco> enderecos;

    public Bairro(Cidade cidade, String nome, List<Endereco> enderecos) {
        this.cidade = cidade;
        this.nome = nome;
        this.enderecos = enderecos;
    }

    public Bairro(String nome) {
        this.nome = nome;
    }

    public Bairro(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public Bairro(Cidade cidade) {
        this.cidade = cidade;
    }

    public Bairro() {
        this.enderecos = new ArrayList<>();
        this.cidade = new Cidade();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/bairro/";
    }

    @Override
    public String toString() {
        return getNome() + " - " + getCidade().getNome();
    }
}
