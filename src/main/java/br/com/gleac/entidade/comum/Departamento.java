package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Persistencia;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by zaca on 06/10/16.
 */
@Entity
@Audited
@CRUD(label = "Loja", plural = "Lojas")
public class Departamento extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Número")
    private String numero;

    @CRUD(label = "Nome")
    private String nome;

    @ManyToOne
    @CRUD(label = "Loja")
    private Loja loja;

    public Departamento() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/loja/";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }


    @Override
    public String toString() {
        return this.getNome();
    }

    @Override
    public String getToStringHtml() {
        return "<a href='" + Util.getRequestContextPath() + this.getCaminhoPadrao() + "ver/" + Persistencia.getId(getLoja()) + "/'>" + this + "</a>";
    }
}
