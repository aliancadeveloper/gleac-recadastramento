package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.SituacaoHistorico;
import br.com.gleac.enums.TipoLocalHistorico;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@CRUD(label = "Histórico Maçônico")
public class HistoricoMaconico extends AbstractEntity {


    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Loja ", obrigatorio = false)
    @ManyToOne
    private Loja loja;

    @CRUD(label = " Potência", obrigatorio = false)
    private String potencia;

    @CRUD(label = " Data Histórico ", obrigatorio = false)
    private Date dataHistorico;

    @CRUD(label = "Loja externa", obrigatorio = false)
    private String lojaExterna;

    @CRUD(label = "Situação ", obrigatorio = false)
    @Enumerated(EnumType.STRING)
    private SituacaoHistorico situacaoHistorico;


    @CRUD(label = "Local Histórico")
    @Enumerated(EnumType.STRING)
    private TipoLocalHistorico tipoLocalHistorico;

    @CRUD(label = "Observação", obrigatorio = false)
    private String observacao;


    @ManyToOne
    private Macon macon;


    public HistoricoMaconico() {
    }

    public HistoricoMaconico(Loja loja, String potencia, Date dataHistorico, String lojaExterna, SituacaoHistorico situacaoHistorico, TipoLocalHistorico tipoLocalHistorico, Macon macon, String observacao) {
        this.loja = loja;
        this.potencia = potencia;
        this.dataHistorico = dataHistorico;
        this.lojaExterna = lojaExterna;
        this.situacaoHistorico = situacaoHistorico;
        this.tipoLocalHistorico = tipoLocalHistorico;
        this.macon = macon;
        this.observacao = observacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public String getPotencia() {
        return potencia;
    }

    public void setPotencia(String potencia) {
        this.potencia = potencia;
    }

    public Date getDataHistorico() {
        return dataHistorico;
    }

    public void setDataHistorico(Date dataHistorico) {
        this.dataHistorico = dataHistorico;
    }

    public String getLojaExterna() {
        return lojaExterna;
    }

    public void setLojaExterna(String lojaExterna) {
        this.lojaExterna = lojaExterna;
    }

    public SituacaoHistorico getSituacaoHistorico() {
        return situacaoHistorico;
    }

    public void setSituacaoHistorico(SituacaoHistorico situacaoHistorico) {
        this.situacaoHistorico = situacaoHistorico;
    }

    public TipoLocalHistorico getTipoLocalHistorico() {
        return tipoLocalHistorico;
    }

    public void setTipoLocalHistorico(TipoLocalHistorico tipoLocalHistorico) {
        this.tipoLocalHistorico = tipoLocalHistorico;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/historicomaconico/";
    }
}
