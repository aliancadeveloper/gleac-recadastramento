package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;

/**
 * Created by israeleriston on 26/06/17.
 */
@Entity
@Audited
@CRUD(label = "Produto")
public class Produto  extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição", pesquisavelAutoComplete = true)
    private String descricao;

    @CRUD(label = "Valor", aceitaNegativo = false)
    private BigDecimal valor;

    @Transient
    private BigDecimal valorAtual;

    public Produto(String descricao) {
        this.descricao = descricao;
        this.valor = BigDecimal.ZERO;
    }

    public Produto() {
        this.valor = BigDecimal.ZERO;
        this.valorAtual = BigDecimal.ZERO;
    }

    public Produto(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Produto(Long id, String descricao, BigDecimal valor) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorAtual() {
        return valorAtual;
    }

    public void setValorAtual(BigDecimal valorAtual) {
        this.valorAtual = valorAtual;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/produto/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
