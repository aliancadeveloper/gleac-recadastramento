package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zaca on 06/10/16.
 */
@Entity
@Audited
@CRUD(label = "Potência")
public class Potencia extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @CRUD(label = "Pessoa Jurídica")
    private PessoaJuridica pessoaJuridica;

    @CRUD(label = "Nome da Potência")
    private String nome;

    @OneToMany(cascade =  CascadeType.ALL, mappedBy = "potencia", orphanRemoval = true)
    private List<Loja> lojas;

    public Potencia(PessoaJuridica pessoaJuridica, List<Loja> lojas) {
        this.pessoaJuridica = pessoaJuridica;
        this.lojas = lojas;
    }

    public Potencia() {
        this.lojas = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaJuridica getPessoaJuridica() {
        return pessoaJuridica;
    }

    public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
        this.pessoaJuridica = pessoaJuridica;
    }

    public List<Loja> getLojas() {
        return lojas;
    }

    public void setLojas(List<Loja> lojas) {
        this.lojas = lojas;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    @Override
    public String toString() {
        return this.getNome();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/potencia/";
    }
}
