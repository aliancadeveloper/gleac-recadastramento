package br.com.gleac.entidade.comum;

import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

@Entity
@Audited
public class DocumentoFornecedor extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Fornecedor fornecedor;

    @OneToOne(cascade = CascadeType.ALL)
    @NotAudited
    private Arquivo arquivo;

    public DocumentoFornecedor() {
    }

    public DocumentoFornecedor(Fornecedor fornecedor, Arquivo arquivo) {
        this.fornecedor = fornecedor;
        this.arquivo = arquivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
