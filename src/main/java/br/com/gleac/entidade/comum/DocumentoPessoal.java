package br.com.gleac.entidade.comum;

import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by israeleriston on 11/07/16.
 */
@Entity
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class DocumentoPessoal extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private PessoaFisica pessoaFisica;

    public DocumentoPessoal() {
        this.pessoaFisica = new PessoaFisica();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }
}
