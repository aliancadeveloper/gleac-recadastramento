package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.financeiro.ContaCorrenteBancaria;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Audited
@CRUD(label = "Conta", plural = "Contas")
public class PessoaConta extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;


    @ManyToOne
    @CRUD(label = "Pessoa")
    private Pessoa pessoa;

    @ManyToOne(cascade = CascadeType.ALL)
    @CRUD(label = "Conta")
    private ContaCorrenteBancaria contaCorrente;

    public PessoaConta() {
    }

    public PessoaConta(Pessoa pessoa, ContaCorrenteBancaria contaCorrente) {
        this.pessoa = pessoa;
        this.contaCorrente = contaCorrente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public ContaCorrenteBancaria getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrenteBancaria contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
