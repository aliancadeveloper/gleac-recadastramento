package br.com.gleac.entidade.comum;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoCargo;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by israeleriston on 12/07/16.
 */
@Entity
@Audited
@CRUD(label = "Cargo", plural = "Cargos")
public class Cargo extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Descrição do Cargo")
    private String descricao;

    @CRUD(label = "Tipo de Cargo")
    @Enumerated(EnumType.STRING)
    private TipoCargo tipoCargo;


    public Cargo(String descricao, TipoCargo tipoCargo) {
        this.descricao = descricao;
        this.tipoCargo = tipoCargo;
    }

    public Cargo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoCargo getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(TipoCargo tipoCargo) {
        this.tipoCargo = tipoCargo;
    }


    @Override
    public String getCaminhoPadrao() {
        return "/admin/cargo/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
