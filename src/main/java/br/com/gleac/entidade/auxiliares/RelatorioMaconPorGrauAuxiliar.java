package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconPorGrauAuxiliar {
    private String loja;
    private Integer aprendiz;
    private Integer companheiro;
    private Integer mestre;
    private Integer mestraInstalado;
    private Integer graoMestre;
    private Integer total;

    public RelatorioMaconPorGrauAuxiliar() {
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public Integer getAprendiz() {
        return aprendiz;
    }

    public void setAprendiz(Integer aprendiz) {
        this.aprendiz = aprendiz;
    }

    public Integer getCompanheiro() {
        return companheiro;
    }

    public void setCompanheiro(Integer companheiro) {
        this.companheiro = companheiro;
    }

    public Integer getMestre() {
        return mestre;
    }

    public void setMestre(Integer mestre) {
        this.mestre = mestre;
    }

    public Integer getMestraInstalado() {
        return mestraInstalado;
    }

    public void setMestraInstalado(Integer mestraInstalado) {
        this.mestraInstalado = mestraInstalado;
    }

    public Integer getGraoMestre() {
        return graoMestre;
    }

    public void setGraoMestre(Integer graoMestre) {
        this.graoMestre = graoMestre;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
