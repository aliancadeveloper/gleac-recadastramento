package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconPorSituacaoAuxiliar {
    private String loja;
    private Integer regular;
    private Integer irregular;
    private Integer coberturaDireito;
    private Integer quitPlacet;
    private Integer emerito;
    private Integer falecido;
    private Integer certificadoGrau;
    private Integer expulso;
    private Integer pastGraoMestre;
    private Integer benemerito;
    private Integer total;

    public RelatorioMaconPorSituacaoAuxiliar() {
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public Integer getRegular() {
        return regular;
    }

    public void setRegular(Integer regular) {
        this.regular = regular;
    }

    public Integer getIrregular() {
        return irregular;
    }

    public void setIrregular(Integer irregular) {
        this.irregular = irregular;
    }

    public Integer getCoberturaDireito() {
        return coberturaDireito;
    }

    public void setCoberturaDireito(Integer coberturaDireito) {
        this.coberturaDireito = coberturaDireito;
    }

    public Integer getQuitPlacet() {
        return quitPlacet;
    }

    public void setQuitPlacet(Integer quitPlacet) {
        this.quitPlacet = quitPlacet;
    }

    public Integer getEmerito() {
        return emerito;
    }

    public void setEmerito(Integer emerito) {
        this.emerito = emerito;
    }

    public Integer getFalecido() {
        return falecido;
    }

    public void setFalecido(Integer falecido) {
        this.falecido = falecido;
    }

    public Integer getCertificadoGrau() {
        return certificadoGrau;
    }

    public void setCertificadoGrau(Integer certificadoGrau) {
        this.certificadoGrau = certificadoGrau;
    }

    public Integer getExpulso() {
        return expulso;
    }

    public void setExpulso(Integer expulso) {
        this.expulso = expulso;
    }

    public Integer getPastGraoMestre() {
        return pastGraoMestre;
    }

    public void setPastGraoMestre(Integer pastGraoMestre) {
        this.pastGraoMestre = pastGraoMestre;
    }

    public Integer getBenemerito() {
        return benemerito;
    }

    public void setBenemerito(Integer benemerito) {
        this.benemerito = benemerito;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
