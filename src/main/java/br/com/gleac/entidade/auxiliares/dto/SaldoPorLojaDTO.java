package br.com.gleac.entidade.auxiliares.dto;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.financeiro.ContaCorrente;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SaldoPorLojaDTO implements Serializable {

    private Date data;
    private Loja loja;
    private ContaCorrente contaCorrente;
    private BigDecimal saldoFinanceiro;
    private BigDecimal contasReceber;
    private BigDecimal contasPagar;

    public SaldoPorLojaDTO() {
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public BigDecimal getSaldoFinanceiro() {
        return saldoFinanceiro;
    }

    public void setSaldoFinanceiro(BigDecimal saldoFinanceiro) {
        this.saldoFinanceiro = saldoFinanceiro;
    }

    public BigDecimal getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(BigDecimal contasReceber) {
        this.contasReceber = contasReceber;
    }

    public BigDecimal getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(BigDecimal contasPagar) {
        this.contasPagar = contasPagar;
    }
}
