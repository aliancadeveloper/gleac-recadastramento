package br.com.gleac.entidade.auxiliares.dto;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by renatoromanini on 08/07/17.
 */
public class IntegracaoBoletoDTO {
    private String emailLoja;
    private String urlRetorno;
    private TipoBoleto tipoBoleto;
    private Integer vencimentoBoleto;
    private Long id_plataforma;
    private String produto_codigo_1;
    private BigDecimal produto_valor_1;
    private String produto_descricao_1;
    private Integer produto_qtde_1;
    private BigDecimal descontoBoleto;
    private BigDecimal frete;
    private TipoFrete tipoFrete;
    private String email;
    private String nome;
    private String cpf;
    private String rg;
    private Date dataNascimento;
    private String razaoSocial;
    private String cnpj;
    private String numeroNotaFiscal;
    private Boolean frase_fixa_boleto;
    private String telefone;
    private String celular;
    private String endereco;
    private String bairro;
    private String cidade;
    private String estado;
    private String cep;
    private String numero_casa;
    private String complemento;
    private String pagamento;

    public IntegracaoBoletoDTO() {
        pagamento = "pagamento";
    }

    public String getEmailLoja() {
        return emailLoja;
    }

    public void setEmailLoja(String emailLoja) {
        this.emailLoja = emailLoja;
    }

    public String getUrlRetorno() {
        return urlRetorno;
    }

    public void setUrlRetorno(String urlRetorno) {
        this.urlRetorno = urlRetorno;
    }

    public TipoBoleto getTipoBoleto() {
        return tipoBoleto;
    }

    public void setTipoBoleto(TipoBoleto tipoBoleto) {
        this.tipoBoleto = tipoBoleto;
    }

    public Integer getVencimentoBoleto() {
        return vencimentoBoleto;
    }

    public void setVencimentoBoleto(Integer vencimentoBoleto) {
        this.vencimentoBoleto = vencimentoBoleto;
    }

    public Long getId_plataforma() {
        return id_plataforma;
    }

    public void setId_plataforma(Long id_plataforma) {
        this.id_plataforma = id_plataforma;
    }

    public String getProduto_codigo_1() {
        return produto_codigo_1;
    }

    public void setProduto_codigo_1(String produto_codigo_1) {
        this.produto_codigo_1 = produto_codigo_1;
    }

    public BigDecimal getProduto_valor_1() {
        return produto_valor_1;
    }

    public void setProduto_valor_1(BigDecimal produto_valor_1) {
        this.produto_valor_1 = produto_valor_1;
    }

    public String getProduto_descricao_1() {
        return produto_descricao_1;
    }

    public void setProduto_descricao_1(String produto_descricao_1) {
        this.produto_descricao_1 = produto_descricao_1;
    }

    public Integer getProduto_qtde_1() {
        return produto_qtde_1;
    }

    public void setProduto_qtde_1(Integer produto_qtde_1) {
        this.produto_qtde_1 = produto_qtde_1;
    }

    public BigDecimal getDescontoBoleto() {
        return descontoBoleto;
    }

    public void setDescontoBoleto(BigDecimal descontoBoleto) {
        this.descontoBoleto = descontoBoleto;
    }

    public BigDecimal getFrete() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete = frete;
    }

    public TipoFrete getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(TipoFrete tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public Boolean getFrase_fixa_boleto() {
        return frase_fixa_boleto;
    }

    public void setFrase_fixa_boleto(Boolean frase_fixa_boleto) {
        this.frase_fixa_boleto = frase_fixa_boleto;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNumero_casa() {
        return numero_casa;
    }

    public void setNumero_casa(String numero_casa) {
        this.numero_casa = numero_casa;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public enum TipoBoleto {
        BOLETO("boletoA4"),
        CARNE("boletoCarne");

        private String descricao;

        TipoBoleto(String descricao) {
            this.descricao = descricao;
        }
    }

    public enum TipoFrete {
        SEDEX("SEDEX"),
        SEDEX10("SEDEX10"),
        PAC("PAC"),
        TRANSPORTADORA("TRANSPORTADORA");

        private String descricao;

        TipoFrete(String descricao) {
            this.descricao = descricao;
        }
    }
}
