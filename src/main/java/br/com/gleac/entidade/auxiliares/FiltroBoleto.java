package br.com.gleac.entidade.auxiliares;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.SituacaoContaPagar;

import java.util.Date;

public class FiltroBoleto {

    private Date dataInicial;
    private Date dataFinal;

    private Loja loja;
    private Pessoa pessoa;
    private SituacaoContaPagar situacaoContaPagar;

    public FiltroBoleto() {
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public SituacaoContaPagar getSituacaoContaPagar() {
        return situacaoContaPagar;
    }

    public void setSituacaoContaPagar(SituacaoContaPagar situacaoContaPagar) {
        this.situacaoContaPagar = situacaoContaPagar;
    }
}
