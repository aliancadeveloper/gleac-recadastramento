package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconEndTelEmailAuxiliar {
    private String obreiro;
    private String endereco;
    private String telefone;
    private String email;

    public RelatorioMaconEndTelEmailAuxiliar() {
    }

    public String getObreiro() {
        return obreiro;
    }

    public void setObreiro(String obreiro) {
        this.obreiro = obreiro;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
