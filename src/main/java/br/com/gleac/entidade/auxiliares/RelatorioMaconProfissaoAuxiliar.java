package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconProfissaoAuxiliar {
    private String nome;
    private String profissao;
    private String telefone;
    private String email;

    public RelatorioMaconProfissaoAuxiliar() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
