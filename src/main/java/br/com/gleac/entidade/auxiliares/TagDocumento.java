package br.com.gleac.entidade.auxiliares;

import java.lang.reflect.Field;

/**
 * Created by renatoromanini on 26/05/16.
 */
public class TagDocumento {
    private String label;
    private String tag;
    private Field campo;
    private Object valor;

    public TagDocumento() {
    }

    public TagDocumento(String labelCampo, String tag) {
        label = labelCampo;
        this.tag = tag;
    }
    public TagDocumento(String labelCampo, Object valor, Field atributo) {
        label = labelCampo;
        campo = atributo;
        this.valor = valor;
        tag = "$"+atributo.getName().toUpperCase();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Field getCampo() {
        return campo;
    }

    public void setCampo(Field campo) {
        this.campo = campo;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

}
