package br.com.gleac.entidade.auxiliares;


import br.com.gleac.entidade.financeiro.Identificador;
import br.com.gleac.enums.TipoMovimentoConciliacao;

import java.math.BigDecimal;
import java.util.Date;

public class MovimentoConciliacaoBancaria {

    private Long idMovimento;
    private BigDecimal credito;
    private BigDecimal debito;
    private Date dataLancamento;
    private Date dataConciliacao;
    private String tipoLancamento;
    private TipoMovimentoConciliacao tipoMovimentoConciliacao;
    private Identificador identificador;

    public MovimentoConciliacaoBancaria() {
        credito = BigDecimal.ZERO;
        debito = BigDecimal.ZERO;
    }

    public String getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(String tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public Long getIdMovimento() {
        return idMovimento;
    }

    public void setIdMovimento(Long idMovimento) {
        this.idMovimento = idMovimento;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public Date getDataConciliacao() {
        return dataConciliacao;
    }

    public void setDataConciliacao(Date dataConciliacao) {
        this.dataConciliacao = dataConciliacao;
    }

    public TipoMovimentoConciliacao getTipoMovimentoConciliacao() {
        return tipoMovimentoConciliacao;
    }

    public void setTipoMovimentoConciliacao(TipoMovimentoConciliacao tipoMovimentoConciliacao) {
        this.tipoMovimentoConciliacao = tipoMovimentoConciliacao;
    }

    public BigDecimal getCredito() {
        return credito;
    }

    public void setCredito(BigDecimal credito) {
        this.credito = credito;
    }

    public BigDecimal getDebito() {
        return debito;
    }

    public void setDebito(BigDecimal debito) {
        this.debito = debito;
    }

    public Identificador getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Identificador identificador) {
        this.identificador = identificador;
    }
}
