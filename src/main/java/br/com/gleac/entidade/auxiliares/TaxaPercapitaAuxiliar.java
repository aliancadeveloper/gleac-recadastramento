package br.com.gleac.entidade.auxiliares;

import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.Macon;
import br.com.gleac.entidade.comum.Produto;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.enums.FormaPagamentoTaxa;
import br.com.gleac.enums.TipoTaxaPercapita;
import br.com.gleac.enums.processo.TipoSolicitacao;
import com.google.common.collect.Lists;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by renatoromanini on 14/10/2017.
 */
public class TaxaPercapitaAuxiliar {

    private TipoTaxaPercapita tipoTaxaPercapita;
    private FormaPagamentoTaxa formaPagamentoTaxa;
    private Servico servico;
    private Boolean todasLojas;
    private Loja loja;
    private Boolean todosMacon;
    private Macon macon;
    private Date dataVencimento;
    private List<Macon> macons;
    private List<String> mensagens;
    private TipoSolicitacao tipoSolicitacao;
    private Produto produto;
    private List<Produto> produtos;

    public TaxaPercapitaAuxiliar() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.set(Calendar.DAY_OF_MONTH, 1);
        dataVencimento = c.getTime();
        todasLojas = Boolean.TRUE;
        todosMacon = Boolean.TRUE;
        macons = Lists.newArrayList();
        mensagens = Lists.newArrayList();
        this.tipoTaxaPercapita = TipoTaxaPercapita.POR_LOJA;
        this.formaPagamentoTaxa = FormaPagamentoTaxa.AVISTA_SEM_DESCONTO;
        this.tipoSolicitacao = TipoSolicitacao.SERVICO;
        this.produtos = Lists.newArrayList();
    }

    public TipoTaxaPercapita getTipoTaxaPercapita() {
        return tipoTaxaPercapita;
    }

    public void setTipoTaxaPercapita(TipoTaxaPercapita tipoTaxaPercapita) {
        this.tipoTaxaPercapita = tipoTaxaPercapita;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public Boolean getTodasLojas() {
        return todasLojas;
    }

    public void setTodasLojas(Boolean todasLojas) {
        this.todasLojas = todasLojas;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Boolean getTodosMacon() {
        return todosMacon;
    }

    public void setTodosMacon(Boolean todosMacon) {
        this.todosMacon = todosMacon;
    }

    public Macon getMacon() {
        return macon;
    }

    public void setMacon(Macon macon) {
        this.macon = macon;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public List<Macon> getMacons() {
        Collections.sort(macons);
        return macons;
    }

    public void setMacons(List<Macon> macons) {
        this.macons = macons;
    }

    public List<String> getMensagens() {
        return mensagens;
    }

    public void setMensagens(List<String> mensagens) {
        this.mensagens = mensagens;
    }

    public Boolean isPorLoja() {
        return TipoTaxaPercapita.POR_LOJA.equals(tipoTaxaPercapita);
    }

    public FormaPagamentoTaxa getFormaPagamentoTaxa() {
        return formaPagamentoTaxa;
    }

    public void setFormaPagamentoTaxa(FormaPagamentoTaxa formaPagamentoTaxa) {
        this.formaPagamentoTaxa = formaPagamentoTaxa;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public TipoSolicitacao getTipoSolicitacao() {
        return tipoSolicitacao;
    }

    public void setTipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
        this.tipoSolicitacao = tipoSolicitacao;
    }

    public Boolean isServico() {
        return TipoSolicitacao.SERVICO.equals(this.getTipoSolicitacao());
    }
}
