package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconAuxiliar {
    private Integer cadastro;
    private String obreiro;
    private String loja;
    private String grau;
    private String situacaoMacon;
    private String situacaoHistorico;

    public RelatorioMaconAuxiliar() {
    }

    public Integer getCadastro() {
        return cadastro;
    }

    public void setCadastro(Integer cadastro) {
        this.cadastro = cadastro;
    }

    public String getObreiro() {
        return obreiro;
    }

    public void setObreiro(String obreiro) {
        this.obreiro = obreiro;
    }

    public String getLoja() {
        return loja;
    }

    public void setLoja(String loja) {
        this.loja = loja;
    }

    public String getGrau() {
        return grau;
    }

    public void setGrau(String grau) {
        this.grau = grau;
    }

    public String getSituacaoMacon() {
        return situacaoMacon;
    }

    public void setSituacaoMacon(String situacaoMacon) {
        this.situacaoMacon = situacaoMacon;
    }

    public String getSituacaoHistorico() {
        return situacaoHistorico;
    }

    public void setSituacaoHistorico(String situacaoHistorico) {
        this.situacaoHistorico = situacaoHistorico;
    }
}
