package br.com.gleac.entidade.auxiliares;

public class RelatorioMaconDataNascimentoAuxiliar {
    private String obreiro;
    private String dataNascimento;
    private Integer idade;
    private String telefone;
    private String email;

    public RelatorioMaconDataNascimentoAuxiliar() {
    }

    public String getObreiro() {
        return obreiro;
    }

    public void setObreiro(String obreiro) {
        this.obreiro = obreiro;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
