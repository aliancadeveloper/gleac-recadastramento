package br.com.gleac.entidade.auxiliares;

/**
 * Created by renatoromanini on 11/02/17.
 */
public class PrimeiroAcesso {

    private String cpf;
    private String email;
    private String login;

    public PrimeiroAcesso() {

    }

    public PrimeiroAcesso(String cpf, String email, String login) {
        this.cpf = cpf;
        this.email = email;
        this.login = login;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
