package br.com.gleac.entidade.auxiliares;

public class RelatorioFornecedorAuxiliar {
    private String contrato;
    private String fornecedor;
    private String identFornecedor;
    private String produto;
    private String email;
    private String telefone;
    private String contato;

    public RelatorioFornecedorAuxiliar() {
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getIdentFornecedor() {
        return identFornecedor;
    }

    public void setIdentFornecedor(String identFornecedor) {
        this.identFornecedor = identFornecedor;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }
}
