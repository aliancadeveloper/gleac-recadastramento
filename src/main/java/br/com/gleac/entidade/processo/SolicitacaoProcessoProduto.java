package br.com.gleac.entidade.processo;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Produto;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * Created by zaca.
 */
@Entity
@Audited
public class SolicitacaoProcessoProduto  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Produto produto;

    @ManyToOne
    private SolicitacaoProcesso solicitacaoProcesso;

    @CRUD(label = "Quantidade")
    private Integer quantidade;

    public SolicitacaoProcessoProduto(Produto produto, SolicitacaoProcesso solicitacaoProcesso) {
        this.produto = produto;
        this.solicitacaoProcesso = solicitacaoProcesso;
    }

    public SolicitacaoProcessoProduto() {
        this.produto = new Produto();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public SolicitacaoProcesso getSolicitacaoProcesso() {
        return solicitacaoProcesso;
    }

    public void setSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso) {
        this.solicitacaoProcesso = solicitacaoProcesso;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

}
