package br.com.gleac.entidade.processo;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.LojaControlador;
import br.com.gleac.controlador.PessoaFisicaControlador;
import br.com.gleac.controlador.ProcessoFinanceiroControlador;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.configuracao.Gestao;
import br.com.gleac.entidade.financeiro.ProcessoFinanceiro;
import br.com.gleac.supers.AbstractEntity;
import br.com.gleac.util.Util;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * Created by zaca.
 */
@Entity
@Audited
@CRUD(label = "Placet", plural = "Placets")
public class Placet extends AbstractEntity {

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Número", obrigatorio = false)
    private Long numero;

    @CRUD(label = "Gestão", pesquisavel = false)
    @ManyToOne
    private Gestao gestao;

    @CRUD(label = "Loja", pesquisavelAutoComplete = true, controlador = LojaControlador.class)
    @ManyToOne
    private Loja loja;

    @CRUD(label = "Requerido", pesquisavelAutoComplete = true, controlador = PessoaFisicaControlador.class)
    @ManyToOne
    private PessoaFisica pessoaFisica;

    @CRUD(label = "Processo", pesquisavelAutoComplete = true, controlador = ProcessoFinanceiroControlador.class)
    @OneToOne
    private ProcessoFinanceiro processoFinanceiro;

    public Placet(Long numero, Gestao gestao, Loja loja, PessoaFisica pessoaFisica, ProcessoFinanceiro processoFinanceiro) {
        this.numero = numero;
        this.gestao = gestao;
        this.loja = loja;
        this.pessoaFisica = pessoaFisica;
        this.processoFinanceiro = processoFinanceiro;
    }

    public Placet() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Gestao getGestao() {
        return gestao;
    }

    public void setGestao(Gestao gestao) {
        this.gestao = gestao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public PessoaFisica getPessoaFisica() {
        return pessoaFisica;
    }

    public void setPessoaFisica(PessoaFisica pessoaFisica) {
        this.pessoaFisica = pessoaFisica;
    }

    public ProcessoFinanceiro getProcessoFinanceiro() {
        return processoFinanceiro;
    }

    public void setProcessoFinanceiro(ProcessoFinanceiro processoFinanceiro) {
        this.processoFinanceiro = processoFinanceiro;
    }

    @Override
    public String toString() {
        return pessoaFisica + " Placet "
                + numero + "/" + Util.sfta.format(getGestao().getInicioEm())  + "-" + Util.sfta.format(getGestao().getFimEm());
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/placet/";
    }
}
