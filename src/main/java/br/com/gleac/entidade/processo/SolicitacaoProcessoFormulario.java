package br.com.gleac.entidade.processo;

import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.entidade.comum.Formulario;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

/**
 * Created by zaca.
 */
@Entity
@Audited
public class SolicitacaoProcessoFormulario extends AbstractEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Formulario formulario;

    @ManyToOne
    private SolicitacaoProcesso solicitacaoProcesso;

    @OneToOne(cascade = CascadeType.ALL)
    @NotAudited
    private Arquivo arquivo;

    public SolicitacaoProcessoFormulario(Formulario formulario, SolicitacaoProcesso solicitacaoProcesso, Arquivo arquivo) {
        this.formulario = formulario;
        this.solicitacaoProcesso = solicitacaoProcesso;
        this.arquivo = arquivo;
    }

    public SolicitacaoProcessoFormulario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Formulario getFormulario() {
        return formulario;
    }

    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public SolicitacaoProcesso getSolicitacaoProcesso() {
        return solicitacaoProcesso;
    }

    public void setSolicitacaoProcesso(SolicitacaoProcesso solicitacaoProcesso) {
        this.solicitacaoProcesso = solicitacaoProcesso;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    @Override
    public String getCaminhoPadrao() {
        return null;
    }
}
