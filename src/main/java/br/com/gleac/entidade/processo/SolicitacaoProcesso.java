package br.com.gleac.entidade.processo;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.controlador.DepartamentoControlador;
import br.com.gleac.controlador.PessoaControlador;
import br.com.gleac.controlador.PessoaFisicaControlador;
import br.com.gleac.entidade.comum.Departamento;
import br.com.gleac.entidade.comum.PessoaFisica;
import br.com.gleac.entidade.comum.Servico;
import br.com.gleac.entidade.seguranca.Pessoa;
import br.com.gleac.enums.processo.StatusProcesso;
import br.com.gleac.enums.processo.TipoPagamento;
import br.com.gleac.enums.processo.TipoSolicitacao;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by zaca
 */
@Entity
@Audited
@CRUD(label = "Solicitação de Processo")
public class SolicitacaoProcesso extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @CRUD(label = "Departamento Requerente ", pesquisavelAutoComplete = true, controlador = DepartamentoControlador.class)
    @ManyToOne
    private Departamento departamento;

    @CRUD(label = "Descrição Solicitação")
    private String descricao;

    @CRUD(label = "Pessoa", pesquisavelAutoComplete = true, controlador = PessoaControlador.class)
    @ManyToOne
    private Pessoa pessoa;

    @CRUD(label = "Número do Processo", obrigatorio = false, pesquisavelAutoComplete = true)
    private Integer numero;

    @CRUD(label = "Tipo da Solicitação de Processo")
    @Enumerated(EnumType.STRING)
    private TipoSolicitacao tipoSolicitacao;

    @CRUD(label = "Tipo de pagamento")
    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;

    @CRUD(label = "Status")
    @Enumerated(EnumType.STRING)
    private StatusProcesso statusProcesso;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitacaoProcesso", orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<SolicitacaoProcessoProduto> solicitacaoProdutos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitacaoProcesso", orphanRemoval = true)
    //@Fetch(FetchMode.JOIN)
    private List<SolicitacaoProcessoFormulario> solicitacaoProcessoFormularios;

    @ManyToOne
    private Servico servico;


    @CRUD(label = "Data Solicitação")
    @Temporal(TemporalType.TIMESTAMP)
    private Date solicitadoEm;


    public SolicitacaoProcesso() {
        this.statusProcesso = StatusProcesso.ABERTO;
        this.solicitadoEm = new Date();
        this.solicitacaoProdutos = Lists.newArrayList();
        this.solicitacaoProcessoFormularios = Lists.newArrayList();
    }

    public SolicitacaoProcesso(Long id, Integer numero) {
        this.id = id;
        this.numero = numero;
        this.statusProcesso = StatusProcesso.ABERTO;
        this.solicitadoEm = new Date();
        this.solicitacaoProdutos = Lists.newArrayList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public StatusProcesso getStatusProcesso() {
        return statusProcesso;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public TipoSolicitacao getTipoSolicitacao() {
        return tipoSolicitacao;
    }

    public void setTipoSolicitacao(TipoSolicitacao tipoSolicitacao) {
        this.tipoSolicitacao = tipoSolicitacao;
    }

    public void setStatusProcesso(StatusProcesso statusProcesso) {
        this.statusProcesso = statusProcesso;
    }

    public List<SolicitacaoProcessoProduto> getSolicitacaoProdutos() {
        return solicitacaoProdutos;
    }

    public void setSolicitacaoProdutos(List<SolicitacaoProcessoProduto> solicitacaoProdutos) {
        this.solicitacaoProdutos = solicitacaoProdutos;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public Date getSolicitadoEm() {
        return solicitadoEm;
    }

    public void setSolicitadoEm(Date solicitadoEm) {
        this.solicitadoEm = solicitadoEm;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<SolicitacaoProcessoFormulario> getSolicitacaoProcessoFormularios() {
        return solicitacaoProcessoFormularios;
    }

    public void setSolicitacaoProcessoFormularios(List<SolicitacaoProcessoFormulario> solicitacaoProcessoFormularios) {
        this.solicitacaoProcessoFormularios = solicitacaoProcessoFormularios;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public Boolean isSolicitacaoServico() {
        try {
            return this.tipoSolicitacao.equals(TipoSolicitacao.SERVICO);
        } catch (Exception ex) {
            return false;
        }
    }

    public Boolean isSolicitacaoProduto() {
        try {
            return this.tipoSolicitacao.equals(TipoSolicitacao.PRODUTO);
        } catch (Exception ex) {
            return false;
        }
    }

    public Boolean isTipoPagamentoBoleto() {
        try {
            return TipoPagamento.BOLETO.equals(this.tipoPagamento);
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/solicitacao-processo/";
    }

    @Override
    public String toString() {
        return getPessoa() +
            " - " + getDepartamento().getLoja().getNome() +
            " - " + getDescricao() +
            " - SOLICITAÇÃO Nº " + getNumero();
    }
}
