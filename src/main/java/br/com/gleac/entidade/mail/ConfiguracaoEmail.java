package br.com.gleac.entidade.mail;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by renatoromanini on 14/02/17.
 */
@Entity
@Audited
@CRUD(label = "Palavra Semestral", sexo = "M", plural = "Palavras Semestral")
public class ConfiguracaoEmail extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Host SMTP")
    private String hostSmtp;
    @CRUD(label = "Remetente")
    private String emailRemetente;
    @CRUD(label = "Senha")
    private String senha;
    @CRUD(label = "Porta")
    private String porta;
    private Boolean ssl;
    private Boolean tsl;

    public ConfiguracaoEmail() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostSmtp() {
        return hostSmtp;
    }

    public void setHostSmtp(String hostSmtp) {
        this.hostSmtp = hostSmtp;
    }

    public String getEmailRemetente() {
        return emailRemetente;
    }

    public void setEmailRemetente(String emailRemetente) {
        this.emailRemetente = emailRemetente;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public Boolean getSsl() {
        return ssl;
    }

    public void setSsl(Boolean ssl) {
        this.ssl = ssl;
    }

    public Boolean getTsl() {
        return tsl;
    }

    public void setTsl(Boolean tsl) {
        this.tsl = tsl;
    }

    public boolean isSsl() {
        return ssl == null ? false : ssl;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/configuracao-email/";
    }
}
