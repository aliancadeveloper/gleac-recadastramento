package br.com.gleac.entidade.seguranca;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.TipoNotificacao;
import br.com.gleac.supers.AbstractEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by renatoromanini on 14/08/15.
 */

/*@Audited*/
@Entity
@CRUD(label = "Notificação", sexo = "F", plural = "Notificações")
public class Notificacao extends AbstractEntity implements Serializable {

    private static String CAMINHO_PADRAO = AbstractEntity.ADMIN + "notificacao/";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    @CRUD(label = "Data")
    private Date data;
    @ManyToOne
    @CRUD(label = "Usuario")
    private Usuario usuario;
    @Enumerated(EnumType.STRING)
    @CRUD(label = "Tipo")
    private TipoNotificacao tipoNotificacao;
    @CRUD(label = "Mensagem", obrigatorio = false, tabelavel = false)
    private String mensagem;
    @CRUD(label = "Link", obrigatorio = false, tabelavel = false)
    private String link;
    private Boolean visualizado;

    public Notificacao() {
        data = new Date();
    }

    public Notificacao(Date data, Usuario usuario, TipoNotificacao tipoNotificacao, String mensagem, String link) {
        this.data = data;
        this.usuario = usuario;
        this.tipoNotificacao = tipoNotificacao;
        this.mensagem = mensagem;
        this.link = link;
        this.visualizado = Boolean.FALSE;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoNotificacao getTipoNotificacao() {
        return tipoNotificacao;
    }

    public void setTipoNotificacao(TipoNotificacao tipoNotificacao) {
        this.tipoNotificacao = tipoNotificacao;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getVisualizado() {
        return visualizado;
    }

    public void setVisualizado(Boolean visualizado) {
        this.visualizado = visualizado;
    }

    @Override
    public String getCaminhoPadrao() {
        return CAMINHO_PADRAO;
    }
}
