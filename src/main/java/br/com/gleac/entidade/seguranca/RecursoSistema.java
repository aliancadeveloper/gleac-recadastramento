package br.com.gleac.entidade.seguranca;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.enums.Modulo;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@CRUD(label = "Recurso Sistema")
public class RecursoSistema implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @JsonIgnore
    private Long id;
    private String checksum;
    private String ref;
    private String path;
    private String icone;
    private String descricao;
    @Enumerated(EnumType.STRING)
    private Modulo modulo;

    public RecursoSistema(String checksum, String path, String icone, String descricao) {
        this.checksum = checksum;
        this.path = path;
        this.icone = icone;
        this.descricao = descricao;
    }

    public RecursoSistema() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIcone() {
        return icone;
    }

    public void setIcone(String icone) {
        this.icone = icone;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecursoSistema that = (RecursoSistema) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (checksum != null ? !checksum.equals(that.checksum) : that.checksum != null) return false;
        if (path != null ? !path.equals(that.path) : that.path != null) return false;
        if (icone != null ? !icone.equals(that.icone) : that.icone != null) return false;
        return descricao != null ? descricao.equals(that.descricao) : that.descricao == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (checksum != null ? checksum.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (icone != null ? icone.hashCode() : 0);
        result = 31 * result + (descricao != null ? descricao.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RecursoSistema{" +
            "checksum='" + checksum + '\'' +
            ", path='" + path + '\'' +
            ", icone='" + icone + '\'' +
            ", descricao='" + descricao + '\'' +
            '}';
    }
}
