package br.com.gleac.entidade.seguranca;

import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Loja;
import br.com.gleac.enums.GrupoAcessoUsuario;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by renatoromanini on 21/05/16.
 */
@Entity
@CRUD(label = "Grupo de Acesso")
//@Audited
public class GrupoAcesso extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Descrição",pesquisavelAutoComplete = true)
    private String descricao;
    @ManyToOne
    @CRUD(label = "Loja")
    private Loja loja;
    @Enumerated(EnumType.STRING)
    @CRUD(label = "Grupo")
    private GrupoAcessoUsuario grupoAcessoUsuario;

    @ManyToMany
    @JoinTable(name = "GRUPOUSUARIOACESSO", joinColumns =
    @JoinColumn(name = "GRUPOACESSO_ID", referencedColumnName = "ID"), inverseJoinColumns =
    @JoinColumn(name = "GRUPOUSUARIO_ID", referencedColumnName = "ID"))
    private List<GrupoUsuario> gruposUsuarios;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grupoAcesso", orphanRemoval = true)
    private List<Recurso> recursos;



    public GrupoAcesso() {
        recursos = Lists.newArrayList();

    }

    public GrupoAcesso(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public GrupoAcessoUsuario getGrupoAcessoUsuario() {
        return grupoAcessoUsuario;
    }

    public void setGrupoAcessoUsuario(GrupoAcessoUsuario grupoAcessoUsuario) {
        this.grupoAcessoUsuario = grupoAcessoUsuario;
    }

    public List<GrupoUsuario> getGruposUsuarios() {
        return gruposUsuarios;
    }

    public void setGruposUsuarios(List<GrupoUsuario> gruposUsuarios) {
        this.gruposUsuarios = gruposUsuarios;
    }

    public List<Recurso> getRecursos() {
        return recursos;
    }

    public void setRecursos(List<Recurso> recursos) {
        this.recursos = recursos;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/grupo-acesso/";
    }

    @Override
    public String toString() {
        return descricao;
    }
}
