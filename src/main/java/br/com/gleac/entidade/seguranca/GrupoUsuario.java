/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.entidade.seguranca;


import br.com.gleac.anotacao.CRUD;
import br.com.gleac.supers.AbstractEntity;
import com.google.common.collect.Lists;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Renato
 */
@Entity
@CRUD(label = "Grupo de Usuário")
public class GrupoUsuario extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToMany
    @JoinTable(name = "GRUPOUSUARIOSISTEMA", joinColumns =
    @JoinColumn(name = "GRUPOUSUARIO_ID", referencedColumnName = "ID"), inverseJoinColumns =
    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID"))
    private List<Usuario> usuarios;
    @CRUD(label = "Descrição")
    private String descricao;
    @ManyToMany
    @JoinTable(name = "GRUPOUSUARIOACESSO", joinColumns =
    @JoinColumn(name = "GRUPOUSUARIO_ID", referencedColumnName = "ID"), inverseJoinColumns =
    @JoinColumn(name = "GRUPOACESSO_ID", referencedColumnName = "ID"))
    private List<GrupoAcesso> gruposAcessos;

    public GrupoUsuario() {
        this.setGruposAcessos(Lists.newArrayList());
    }



    public GrupoUsuario(List<Usuario> usuarios, String descricao, List<GrupoAcesso> gruposAcessos) {
        this.usuarios = usuarios;
        this.descricao = descricao;
        this.gruposAcessos = gruposAcessos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<GrupoAcesso> getGruposAcessos() {
        return gruposAcessos;
    }

    public void setGruposAcessos(List<GrupoAcesso> gruposAcessos) {
        this.gruposAcessos = gruposAcessos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/grupo-usuario/";
    }

    @Override
    public String toString() {
        return getDescricao();
    }
}
