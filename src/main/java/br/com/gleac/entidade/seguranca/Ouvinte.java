package br.com.gleac.entidade.seguranca;

import br.com.gleac.service.SistemaService;
import org.hibernate.envers.RevisionListener;

import java.util.Date;

/**
 * Created by israeleriston on 14/05/16.
 */
public class Ouvinte implements RevisionListener {
    @Override
    public void newRevision(Object auditing) {
        RevisionAuditing revision = (RevisionAuditing) auditing;
        revision.setIp(SistemaService.getIP());
        revision.setUsuario(SistemaService.getLoginUsuarioCorrente());
        revision.setDataHora(new Date());
    }
}
