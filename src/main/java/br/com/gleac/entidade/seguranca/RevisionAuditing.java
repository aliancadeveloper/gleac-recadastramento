package br.com.gleac.entidade.seguranca;

import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by israeleriston on 14/05/16.
 */
@Entity
@RevisionEntity(value = Ouvinte.class)
public class RevisionAuditing extends AbstractEntity implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @RevisionNumber
    private Long id;
    @RevisionTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date tempohora;
    private String usuario;
    private String ip;

    public RevisionAuditing(Date dataHora, String usuario, String ip) {
        this.tempohora = dataHora;
        this.usuario = usuario;
        this.ip = ip;
    }

    public RevisionAuditing() {
    }

    @Override
    public String getCaminhoPadrao() {
        return "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataHora() {
        return tempohora;
    }

    public void setDataHora(Date dataHora) {
        this.tempohora = dataHora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
