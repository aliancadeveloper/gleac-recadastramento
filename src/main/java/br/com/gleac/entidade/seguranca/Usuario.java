/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.entidade.seguranca;


import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.Arquivo;
import br.com.gleac.exception.ValidacaoException;
import br.com.gleac.seguranca.Seguranca;
import br.com.gleac.supers.AbstractEntity;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Marinzeck
 */
@Entity
/*@Audited*/
@CRUD(label = "Usuário", plural = "Usuários", sexo = "M")
public class Usuario extends AbstractEntity implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;
    public static final long TAMANHO_SENHA = 6;
    private static final List<GrantedAuthority> DEFAULT_AUTHORITIES = AuthorityUtils.createAuthorityList("ROLE_USER");
    private static final ShaPasswordEncoder PASSWORD_ENCODER = new ShaPasswordEncoder();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @CRUD(label = "Login", pesquisavelAutoComplete = true)
    private String login;
    @CRUD(label = "Senha", visualizavel = false, pesquisavel = false, obrigatorio = false, tabelavel = false)
    private String senha;
    @Transient
    @CRUD(visualizavel = false, pesquisavel = false, obrigatorio = false, tabelavel = false)
    private String confirmacaoSenha;
    @CRUD(label = "Ativo?", pesquisavel = false)
    private Boolean ativo;
    @ManyToMany
    @JoinTable(name = "GRUPOUSUARIOSISTEMA", joinColumns =
    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID"), inverseJoinColumns =
    @JoinColumn(name = "GRUPOUSUARIO_ID", referencedColumnName = "ID"))
    private List<GrupoUsuario> gruposUsuario;
    @CRUD(label = "Pessoa")
    @OneToOne
    private Pessoa pessoa;
    @CRUD(label = "Imagem", obrigatorio = false, tabelavel = false, pesquisavel = false)
    @OneToOne(cascade = CascadeType.ALL)
    private Arquivo arquivo;
    private Date expiraPrimeiroAcesso;
    private String token;

    public Usuario() {
        ativo = Boolean.TRUE;
        gruposUsuario = new ArrayList<GrupoUsuario>();
    }

    public Usuario(Long id, String login) {
        this.id = id;
        this.login = login;
    }

    public List<GrupoUsuario> getGruposUsuario() {
        return gruposUsuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setGruposUsuario(List<GrupoUsuario> grupoUsuario) {
        this.gruposUsuario = grupoUsuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getConfirmacaoSenha() {
        return confirmacaoSenha;
    }

    public void setConfirmacaoSenha(String confirmacaoSenha) {
        this.confirmacaoSenha = confirmacaoSenha;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Arquivo getArquivo() {
        return arquivo;
    }

    public void setArquivo(Arquivo arquivo) {
        this.arquivo = arquivo;
    }

    public Date getExpiraPrimeiroAcesso() {
        return expiraPrimeiroAcesso;
    }

    public void setExpiraPrimeiroAcesso(Date expiraPrimeiroAcesso) {
        this.expiraPrimeiroAcesso = expiraPrimeiroAcesso;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSenhasIguais() {
        String mensagem = "A senha deve ser igual a senha de confirmação.";
        try {
            if (!senha.equals(confirmacaoSenha)) {
                throw new ValidacaoException(mensagem);
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(mensagem);
        }
    }

    public boolean isTamanhoSenhaPermitido() {
        String mensagem = "A senha deve possuir no mínimo 6(seis) caracteres.";
        try {
            if (senha.length() < TAMANHO_SENHA) {
                throw new ValidacaoException(mensagem);
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(mensagem);
        }
    }


    public boolean possuiNoMinimoUmGrupoDeAcesso() {
        String mensagem = "O usuário deve possuir no mínimo um grupo de acesso.";
        try {
            if (this.gruposUsuario.isEmpty()) {
                throw new ValidacaoException(mensagem);
            }
            return true;
        } catch (Exception e) {
            throw new RuntimeException(mensagem);
        }
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/usuario/";
    }

    public void criptografarSenha() {
        this.senha = Seguranca.encodedSenha(this.senha);
    }

    @Override
    public String toString() {
        return login;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return DEFAULT_AUTHORITIES;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getNome() {
        if (this.pessoa != null) {
            return this.pessoa.getNomePessoa();
        }
        return login;
    }
}
