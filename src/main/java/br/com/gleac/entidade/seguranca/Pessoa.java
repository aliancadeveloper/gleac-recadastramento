/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gleac.entidade.seguranca;


import br.com.gleac.anotacao.CRUD;
import br.com.gleac.entidade.comum.*;
import br.com.gleac.supers.AbstractEntity;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roma
 */
@Entity
@Audited
@Inheritance(strategy = InheritanceType.JOINED)
@CRUD(label = "Pessoa", sexo = "F", plural = "Pessoas")
public class Pessoa extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(mappedBy = "pessoa", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Endereco> enderecos;
    @OneToMany(mappedBy = "pessoa", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Telefone> telefones;
    @CRUD(label = "E-mail", obrigatorio = false, tabelavel = false)
    private String email;

    @OneToMany(mappedBy = "pessoa", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PessoaConta> contas;


    public Pessoa(List<Endereco> enderecos, List<Telefone> telefones, List<PessoaConta> contas) {
        this.enderecos = enderecos;
        this.telefones = telefones;
        this.contas = contas;
    }

    public Pessoa() {
        this.enderecos = new ArrayList<>();
        this.telefones = new ArrayList<>();
        this.contas = new ArrayList<>();
    }

    public Pessoa(Long id) {
        this.id = id;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNomePessoa() {
        return this instanceof PessoaFisica ? ((PessoaFisica) this).getNomePessoa() : ((PessoaJuridica) this).getNomePessoa();
    }

    public List<PessoaConta> getContas() {
        return contas;
    }

    public void setContas(List<PessoaConta> contas) {
        this.contas = contas;
    }

    public String getCpf_Cnpj() {
        return this instanceof PessoaFisica ? ((PessoaFisica) this).getCpf_Cnpj() : ((PessoaJuridica) this).getCpf_Cnpj();
    }

    @Override
    public String getCaminhoPadrao() {
        return "/admin/pessoa/";
    }

    @Override
    public String toString() {
        return getNomePessoa();
    }
}
