package br.com.gleac.entidade.seguranca;

import br.com.gleac.anotacao.CRUD;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@CRUD(label = "Recurso")
public class Recurso implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @CRUD(label = "Grupo Acesso")
    private GrupoAcesso grupoAcesso;
    @OneToOne
    private RecursoSistema recursoSistema;
    @CRUD(label = "Pode Editar?")
    private Boolean podeEditar;
    @CRUD(label = "Pode Visualizar?")
    private Boolean podeVisualizar;
    @CRUD(label = "Pode Cadastrar?")
    private Boolean podeCadastrar;
    @CRUD(label = "Pode Excluir?")
    private Boolean podeExcluir;
    private Boolean relatorio;

    public Recurso() {
        podeEditar = true;
        podeVisualizar = true;
        podeCadastrar = true;
        podeExcluir = true;
    }

    public Recurso(GrupoAcesso grupoAcesso, RecursoSistema recursoSistema, Boolean podeEditar, Boolean podeVisualizar, Boolean podeCadastrar, Boolean podeExcluir) {
        this.grupoAcesso = grupoAcesso;
        this.recursoSistema = recursoSistema;
        this.podeEditar = podeEditar;
        this.podeVisualizar = podeVisualizar;
        this.podeCadastrar = podeCadastrar;
        this.podeExcluir = podeExcluir;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GrupoAcesso getGrupoAcesso() {
        return grupoAcesso;
    }

    public void setGrupoAcesso(GrupoAcesso grupoAcesso) {
        this.grupoAcesso = grupoAcesso;
    }

    public RecursoSistema getRecursoSistema() {
        return recursoSistema;
    }

    public void setRecursoSistema(RecursoSistema recursoSistema) {
        this.recursoSistema = recursoSistema;
    }

    public Boolean getPodeEditar() {
        return podeEditar;
    }

    public void setPodeEditar(Boolean podeEditar) {
        this.podeEditar = podeEditar;
    }

    public Boolean getPodeVisualizar() {
        return podeVisualizar;
    }

    public void setPodeVisualizar(Boolean podeVisualizar) {
        this.podeVisualizar = podeVisualizar;
    }

    public Boolean getPodeCadastrar() {
        return podeCadastrar;
    }

    public void setPodeCadastrar(Boolean podeCadastrar) {
        this.podeCadastrar = podeCadastrar;
    }

    public Boolean getPodeExcluir() {
        return podeExcluir;
    }

    public void setPodeExcluir(Boolean podeExcluir) {
        this.podeExcluir = podeExcluir;
    }

    public Boolean getRelatorio() {
        return relatorio;
    }

    public void setRelatorio(Boolean relatorio) {
        this.relatorio = relatorio;
    }

    public Boolean isExclusao() {
        return getPodeExcluir() == null ? false : getPodeExcluir();
    }

    public Boolean isCadastrar() {
        return this.podeCadastrar == null ? false : this.podeCadastrar;
    }

    public Boolean isVisualizar() {
        return getPodeVisualizar() == null ? false : getPodeVisualizar();
    }

    public Boolean isEditar() {
        return getPodeEditar() == null ? false : getPodeEditar();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recurso recurso = (Recurso) o;

        if (id != null ? !id.equals(recurso.id) : recurso.id != null) return false;
        if (grupoAcesso != null ? !grupoAcesso.equals(recurso.grupoAcesso) : recurso.grupoAcesso != null) return false;
        if (recursoSistema != null ? !recursoSistema.equals(recurso.recursoSistema) : recurso.recursoSistema != null)
            return false;
        if (podeEditar != null ? !podeEditar.equals(recurso.podeEditar) : recurso.podeEditar != null) return false;
        if (podeVisualizar != null ? !podeVisualizar.equals(recurso.podeVisualizar) : recurso.podeVisualizar != null)
            return false;
        if (podeCadastrar != null ? !podeCadastrar.equals(recurso.podeCadastrar) : recurso.podeCadastrar != null)
            return false;
        return podeExcluir != null ? podeExcluir.equals(recurso.podeExcluir) : recurso.podeExcluir == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (grupoAcesso != null ? grupoAcesso.hashCode() : 0);
        result = 31 * result + (recursoSistema != null ? recursoSistema.hashCode() : 0);
        result = 31 * result + (podeEditar != null ? podeEditar.hashCode() : 0);
        result = 31 * result + (podeVisualizar != null ? podeVisualizar.hashCode() : 0);
        result = 31 * result + (podeCadastrar != null ? podeCadastrar.hashCode() : 0);
        result = 31 * result + (podeExcluir != null ? podeExcluir.hashCode() : 0);
        return result;
    }
}
