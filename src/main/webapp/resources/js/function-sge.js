$(window).load(function () {
    $("#spinner").fadeOut("slow");
    $("#menu > li > ").click(function (event) {
        aguarde();
    });
    ajustaDivPrincipalDoConteudo();
});


function aguarde() {
    $("#aguarde").fadeIn("slow");
}

function fechaAguarde() {
    $("#aguarde").fadeOut("slow");
}

function abrirStatusDialog() {
    PF('statusDialog').show();
}

function fechaStatusDialog() {
    PF('statusDialog').hide();
}

function ajustaDivPrincipalDoConteudo() {
    var hTotal = window.innerHeight;
    var diferencaH = hTotal * 0.5;
    $("#conteudo").css("height", (hTotal - diferencaH) + "px");
}

function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}
function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

function maiuscula(id) {
    document.getElementById(id).value = document.getElementById(id).value.toString().toUpperCase();
}

function redimensionarDialog(id) {
    var info = document.getElementById(id);
    var style = info.style;
    var wTotal = window.innerWidth;
    var diferencaW = wTotal * 0.2;
    style.left = diferencaW / 2 + "px";
    style.minWidth = (wTotal - diferencaW) + "px";
    redimensionarAlturaDialog(id);
}

function redimensionarAlturaDialog(id) {

    var info = document.getElementById(id);
    var style = info.style;
    var wTotal = window.innerHeight;
    var diferencaW = wTotal * 0.2;
    style.top = diferencaW / 2 + "px";

}

function soNumeros(v) {
    return v.replace(/\D/g, "")
}

function mvalor(v) {

    v = soNumeros(v);
    v = v.replace(/\D/g, "");//Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{14})$/, "$1.$2");//coloca o ponto dos trilhoes
    v = v.replace(/(\d)(\d{11})$/, "$1.$2");//coloca o ponto dos bilhoes
    v = v.replace(/(\d)(\d{8})$/, "$1.$2");//coloca o ponto dos milhões
    v = v.replace(/(\d)(\d{5})$/, "$1.$2");//coloca o ponto dos milhares

    v = v.replace(/(\d)(\d{2})$/, "$1,$2");//coloca a virgula antes dos 2 últimos dígitos
    if (v.toString().length > 22) {
        v = v.replace(v, v.toString().substring(0, 22));
    }
    return v;
}

function mdata(v) {
    v = v.replace(/\D/g, "");                    //Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1/$2");
    v = v.replace(/(\d{2})(\d)/, "$1/$2");

    v = v.replace(/(\d{2})(\d{2})$/, "$1$2");
    if (v.toString().length > 10) {
        v = v.replace(v, v.toString().substring(0, 10));
    }
    return v;
}

function somenteNumeros(event, campo, permiteNegativo, permiteZero, permitePositivo) {
    permiteNegativo = permiteNegativo != null ? permiteNegativo : false;
    permiteZero = permiteZero != null ? permiteZero : false;
    permitePositivo = permitePositivo != null ? permitePositivo : true;

    var tecla = event.which || event.charCode || event.keyCode || 0;

    if (permiteNegativo == true) {
        campo.addEventListener("blur", function () {
            if (campo.value.trim() == '-') {
                campo.value = '';
            }
            if (parseInt(campo.value) == 0) {
                campo.value = 0;
            }
        });
    }

    if (permiteZero == false) {
        campo.addEventListener("blur", function () {
            if (parseInt(campo.value.trim()) == 0) {
                campo.value = '';
            }
        })
    }

    if (permitePositivo == false) {
        campo.addEventListener("blur", function () {
            if (parseInt(campo.value.trim()) > 0) {
                campo.value = '';
            }
        })
    }

    if (tecla == 9) {
        return true;
    }

    if (permiteNegativo && (tecla == 45 || tecla == 109)) {
        var valor = campo.value;
        if (valor.startsWith('-')) {
            valor = valor.replace('-', '');
            campo.value = valor;
            event.preventDefault();
            return;
        }

        if (valor.indexOf('-') <= 0) {
            valor = '-'.concat(valor);
            campo.value = valor;
            event.preventDefault();
            return;
        }
    }

    if (permiteNegativo == false && (tecla == 45 || tecla == 109)) {
        event.preventDefault();
        return;
    }


    if ((tecla > 47 && tecla < 58) || tecla == 45) {
        return true;
    }
    else {
        if (tecla == 8 || tecla == 0) return true;
        else  event.preventDefault();
    }


}
